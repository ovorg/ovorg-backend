<?php

namespace App\Notifications;

use App\Mail\MeetingMail;
use App\Models\Meeting;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Collection;

class MeetingNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $meetings;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($meetings)
    {
        $this->meetings = $meetings;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param User $user
     * @return array
     */
    public function via(User $user)
    {
        if ($user->notificationPreference->meeting_notifications == false) {
            return [];
        }

        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  User $user
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail(User $user)
    {
        return (new MeetingMail($this->meetings, $user))->to($user->email);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  User $user
     * @return array
     */
    public function toArray(User $user)
    {
        if ($this->meetings instanceof Meeting) {
            return [
                'meetings' => [$this->singleMeetingToArray($this->meetings)],
            ];
        }
        if ($this->meetings instanceof Collection && $this->meetings->count() == 1) {
            return [
                'meetings' => [$this->singleMeetingToArray($this->meetings->first())],
            ];
        }
        if ($this->meetings instanceof Collection) {
            return [
                'meetings' => $this->meetings->map(function ($meeting) {
                    return $this->singleMeetingToArray($meeting);
                }),
            ];
        }
    }

    /**
     * Get the array representation for a single meeting.
     *
     * @param  \App\Models\Meeting $meeting
     * @return array
     */
    private function singleMeetingToArray(Meeting $meeting)
    {
        return [
            'id'             => $meeting->id,
            'start'          => $meeting->start,
            'end'            => $meeting->end,
            'register_until' => $meeting->register_until,
            'description'    => clean($meeting->description),
        ];
    }
}
