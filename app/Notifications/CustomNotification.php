<?php

namespace App\Notifications;

use App\Mail\CustomMail;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Collection;

class CustomNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $subject;
    public $body;
    public $sender;
    public $files;

    /**
     * Create a new notification instance.
     *
     * @param  string $subject
     * @param  string $body
     * @param  \App\Models\User $sender
     * @param  \Illuminate\Support\Collection $files
     * @return void
     */
    public function __construct(string $subject, string $body, User $sender, Collection $files)
    {
        $this->subject = $subject;
        $this->body = $body;
        $this->sender = $sender;
        $this->files = $files;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  User $user
     * @return array
     */
    public function via(User $user)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  User $user
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail(User $user)
    {
        $mail = (new CustomMail($this->subject, $this->body))
            ->from(config('mail.from.address'), $this->sender->getFullName())
            ->replyTo($this->sender->email, $this->sender->getFullName())
            ->to($user->email);

        foreach ($this->files as $file) {
            $mail->attachFromStorage($file['path'], $file['name']);
        }

        return $mail;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  User $user
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'subject' => $this->subject,
            'body'    => $this->body,
            'sender'  => [
                'id'        => $this->sender->id,
                'full_name' => $this->sender->getFullName(),
                'email'     => $this->sender->email,
            ],
        ];
    }
}
