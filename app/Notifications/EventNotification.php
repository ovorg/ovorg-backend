<?php

namespace App\Notifications;

use App\Models\Event;
use App\Models\EventDateOption;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class EventNotification extends Notification
{
    use Queueable;

    private $event;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Event $event)
    {
        $this->event = $event;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  \App\Models\User $user
     * @return array
     */
    public function via(User $user)
    {
        if ($user->notificationPreference->event_notifications == false) {
            return [];
        }

        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail(User $user)
    {
        return (new MailMessage())
            ->subject("Verfügbarkeitsabfrage: " . $this->event->title)
            ->markdown('mails.events.event', [
                'event' => $this->event,
                'user'  => $user,
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  \App\Models\User $user
     * @return array
     */
    public function toArray(User $user)
    {
        return [
            'id'          => $this->event->id,
            'description' => $this->event->description,
        ];
    }
}
