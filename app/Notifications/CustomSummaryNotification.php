<?php

namespace App\Notifications;

use App\Mail\CustomSummaryMail;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Collection;

class CustomSummaryNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $subject;
    public $body;
    public $users;
    public $files;
    private $userGroups;
    private $meetings;
    private $attendance;

    /**
     * Create a new notification instance.
     *
     * @param  string $subject
     * @param  string $body
     * @param  \Illuminate\Support\Collection $users
     * @param  \Illuminate\Support\Collection $files
     * @return void
     */
    public function __construct(string $subject, string $body, Collection $users, Collection $userGroups, Collection $meetings, Collection $attendance, Collection $files)
    {
        $this->subject = $subject;
        $this->body = $body;
        $this->users = $users;
        $this->files = $files;
        $this->userGroups = $userGroups;
        $this->meetings = $meetings;
        $this->attendance = $attendance;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  User $user
     * @return array
     */
    public function via(User $user)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  User $user
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail(User $user)
    {
        $mail = (new CustomSummaryMail($this->subject, $this->body, $user, $this->users, $this->userGroups, $this->meetings, $this->attendance))
            // ->from(config('mail.from.address'), $this->sender->getFullName())
            // ->replyTo($this->sender->email, $this->sender->getFullName())
            ->to($user->email);

        // foreach ($this->files as $file) {
        //     $mail->attachFromStorage($file['path'], $file['name']);
        // }

        return $mail;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  User $user
     * @return array
     */
    public function toArray(User $user)
    {
        return [
            'subject' => $this->subject,
            'body'    => $this->body,
            'users'   => $this->users->map(function (User $user) {
                return [
                    'id'        => $user->id,
                    'full_name' => $user->getFullName(),
                    'email'     => $user->email,
                ];
            }),
        ];
    }
}
