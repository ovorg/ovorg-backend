<?php

namespace App\Notifications;

use App\Mail\MeetingSummaryMail;
use App\Models\Meeting;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Collection;

class MeetingSummaryNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $meetings;

    /**
     * Create a new notification instance.
     *
     * @param \Illuminate\Support\Collection $meetings
     */
    public function __construct(Collection $meetings)
    {
        $this->meetings = $meetings;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  \App\Models\User $user
     * @return array
     */
    public function via(User $user)
    {
        if ($user->notificationPreference->meeting_notifications == false) {
            return [];
        }

        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail(User $user)
    {
        return (new MeetingSummaryMail($this->meetings, $user))->to($user->email);
    }
    /**
     * Get the array representation of the notification.
     *
     * @param  \App\Models\User $user
     * @return array
     */
    public function toArray(User $user)
    {
        return [
            'meetings' => $this->meetings->map(function (Meeting $meeting) {
                return [
                    'id'    => $meeting->id,
                    'start' => $meeting->start,
                    'end'   => $meeting->end,
                ];
            }),
        ];
    }
}
