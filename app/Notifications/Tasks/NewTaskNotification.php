<?php

namespace App\Notifications\Tasks;

use App\Models\Task;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewTaskNotification extends Notification implements ShouldQueue
{
    use Queueable;

    private $task;

    /**
     * Create a new notification instance.
     *
     * @param  \App\Models\Task $task
     * @return void
     */
    public function __construct(Task $task)
    {
        $this->task = $task;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  \App\Models\User $user
     * @return array
     */
    public function via(User $user)
    {
        if ($user->notificationPreference->task_notifications == false) {
            return [];
        }

        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail(User $user)
    {
        if ($user == $this->task->responsible) {
            return (new MailMessage())
                ->subject('Neue Aufgabe: ' . $this->task->title)
                ->markdown('mails.tasks.new_responsible_task', [
                    'task' => $this->task,
                    'user' => $user,
                ]);
        }
        return (new MailMessage())
            ->subject('Neue Aufgabe: ' . $this->task->title)
            ->markdown('mails.tasks.new_task', [
                'task' => $this->task,
                'user' => $user,
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  \App\Models\User $user
     * @return array
     */
    public function toArray(User $user)
    {
        return [
            'task' => [
                'user_is_responsible' => $user == $this->task->responsible,
                'id'                  => $this->task->id,
                'title'               => $this->task->title,
                'creator'             => $this->task->creator ? $this->task->creator->getFullName() : null,
                'category'            => $this->task->category ? $this->task->category->name : null,
                'duedate'             => $this->task->duedate ? $this->task->duedate->isoFormat('D.MM.YYYY') : null,
                'description'         => clean($this->task->description),
            ],
        ];
    }
}
