<?php

namespace App\Notifications\Tasks;

use App\Models\Task;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Collection;

class UsersUpdatedNotification extends Notification implements ShouldQueue
{
    use Queueable;

    private $task;
    private $addedUsers;
    private $removedUsers;
    private $author;

    /**
     * Create a new notification instance.
     *
     * @param  \App\Models\Task $task
     * @param  \App\Models\User|null $author
     * @param  \Illuminate\Support\Collection $addedUsers
     * @param  \Illuminate\Support\Collection $removedUsers
     * @return void
     */
    public function __construct(Task $task, ?User $author, Collection $addedUsers, Collection $removedUsers)
    {
        $this->task = $task;
        $this->addedUsers = $addedUsers;
        $this->removedUsers = $removedUsers;
        $this->author = $author;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  \App\Models\User $user
     * @return array
     */
    public function via(User $user)
    {
        if ($user->notificationPreference->task_notifications == false) {
            return [];
        }

        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail(User $user)
    {
        return (new MailMessage())
            ->subject('Aufgabe bearbeitet: ' . $this->task->title)
            ->markdown('mails.tasks.users_updated', [
                'task'         => $this->task,
                'user'         => $user,
                'author'       => $this->author,
                'addedUsers'   => $this->addedUsers,
                'removedUsers' => $this->removedUsers,
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  \App\Models\User $user
     * @return array
     */
    public function toArray(User $user)
    {
        return [
            'task'         => [
                'id'     => $this->task->id,
                'titlel' => $this->task->title,
                'users'  => $this->task->users->map(function ($user) {
                    return $user->getFullName();
                }),
            ],
            'author'       => $this->author ? $this->author->getFullName() : null,
            'addedUsers'   => $this->addedUsers->map(function ($user) {
                return $user->getFullName();
            }),
            'removedUsers' => $this->removedUsers->map(function ($user) {
                return $user->getFullName();
            }),
        ];
    }
}
