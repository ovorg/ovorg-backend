<?php

namespace App\Notifications\Tasks;

use App\Models\Task;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Collection;

class FilesAddedNotification extends Notification implements ShouldQueue
{
    use Queueable;

    private $task;
    private $newFiles;

    /**
     * Create a new notification instance.
     *
     * @param  \App\Models\Task $task
     * @param  Illuminate\Support\Collection $newFiles
     * @return void
     */
    public function __construct(Task $task, Collection $newFiles)
    {
        $this->task = $task;
        $this->newFiles = $newFiles;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  \App\Models\User $user
     * @return array
     */
    public function via(User $user)
    {
        if ($user->notificationPreference->task_notifications == false) {
            return [];
        }

        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail(User $user)
    {
        return (new MailMessage())
            ->subject('Dateien wurden einer Aufgabe hinzugefügt: ' . $this->task->title)
            ->markdown('mails.tasks.files_added', [
                'task'     => $this->task,
                'user'     => $user,
                'newFiles' => $this->newFiles,
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  \App\Models\User $user
     * @return array
     */
    public function toArray(User $user)
    {
        return [
            'task'     => [
                'id'    => $this->task->id,
                'title' => $this->task->title,
            ],
            'newFiles' => $this->newFiles->map(function ($file) {
                return [
                    'name' => $file->name,
                ];
            }),
        ];
    }
}
