<?php

namespace App\Notifications\Tasks;

use App\Models\Task;
use App\Models\TaskCategory;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class AttributeChangedNotification extends Notification implements ShouldQueue
{
    use Queueable;

    private $task;
    private $changes;
    private $author;

    /**
     * Create a new notification instance.
     *
     * @param  \App\Models\Task|null $task
     * @param  string $oldDescription
     * @return void
     */
    public function __construct(Task $task, ?User $author)
    {
        $this->task = $task;
        $this->author = $author;
        $this->changes = $this->parseChanges($task);
    }

    /**
     * Parse changes into array that can be passed into the markdown template.
     *
     * @param  \App\Models\Task $task
     * @return array
     */
    private function parseChanges(Task $task): array
    {
        $changes = [];
        foreach ($task->getChanges() as $key => $newValue) {
            $new = $this->parseChange($task, $key, $newValue);
            if ($new === null) {
                continue;
            }
            $changes = array_merge($changes, $new);
        }
        return $changes;
    }

    /**
     * Parse a change into an array that can be passed into the markdown template.
     *
     * @param  \App\Models\Task $task
     * @param  string $key
     * @param  mixed $newValue
     * @return array|null
     */
    private function parseChange(Task $task, string $key, $newValue): ?array
    {
        switch ($key) {
            case "responsible_id":
                $new = User::find($newValue);
                $old = User::find($task->getOriginal("responsible_id"));
                return ["responsible" => [
                    $new ? $new->getFullName() : null,
                    $old ? $old->getFullName() : null,
                ]];
            case "task_category_id":
                $new = TaskCategory::find($newValue);
                $old = TaskCategory::find($task->getOriginal("task_category_id"));
                return ["task_category" => [
                    $new ? $new->name : null,
                    $old ? $old->name : null,
                ]];
            case "duedate":
                return ["duedate" => [
                    $newValue ? Carbon::parse($newValue) : null,
                    $task->getOriginal("duedate") ? Carbon::parse($task->getOriginal("duedate")) : null,
                ]];
            case "description":
            case "addition":
                $old = clean($task->getOriginal($key));
                $new = clean($newValue);

                /**
                 * Task::getChanges() sometimes returns changes for addition or description
                 * although there are not changes to the text. These cases are caught here.
                 */
                if (strcmp($old, $new) === 0) {
                    return null;
                }

                return [$key => [
                    $new,
                    $old,
                ]];
            case "state":
            case "priority":
            case "title":
                return [$key => [
                    $newValue,
                    $task->getOriginal($key),
                ]];
            default:
                return null;
        }
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  \App\Models\User $user
     * @return array
     */
    public function via(User $user)
    {
        if ($user->notificationPreference->task_notifications == false) {
            return [];
        }

        // Don't send anything when there is nothing to send.
        if (count($this->changes) == 0) {
            return [];
        }

        // Don't send anything to the author of the changes.
        if ($this->author == $user) {
            return [];
        }

        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail(User $user)
    {
        return (new MailMessage())
            ->subject('Aufgabe bearbeitet: ' . $this->task->title)
            ->markdown('mails.tasks.attribute_changed', [
                'task'    => $this->task,
                'changes' => $this->changes,
                'user'    => $user,
                'author'  => $this->author,
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  \App\Models\User $user
     * @return array
     */
    public function toArray(User $user)
    {
        return [
            'task'    => [
                'id'    => $this->task->id,
                'title' => $this->task->title,
            ],
            'changes' => $this->changes,
            'author'  => $this->author ? $this->author->getFullName() : null,
        ];
    }
}
