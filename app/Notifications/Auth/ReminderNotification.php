<?php

namespace App\Notifications\Auth;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ReminderNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $reminder;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($reminder)
    {
        $this->reminder = $reminder;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  User $user
     * @return array
     */
    public function via(User $user)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  User $user
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail(User $user)
    {
        return (new MailMessage())->subject("Passwort zurücksetzen")->markdown('mails.auth.password_reset', ['reminder' => $this->reminder, 'user' => $user]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  User $user
     * @return array
     */
    public function toArray(User $user)
    {
        return [
            'reminder' => [
                'code' => $this->reminder->code,
            ],
        ];
    }
}
