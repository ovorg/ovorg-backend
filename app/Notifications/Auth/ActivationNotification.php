<?php

namespace App\Notifications\Auth;

use Cartalyst\Sentinel\Activations\EloquentActivation;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ActivationNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $activation;

    /**
     * Create a new notification instance.
     *
     * @param EloquentActivation $activation
     * @return void
     */
    public function __construct($activation)
    {
        $this->activation = $activation;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage())->subject("Benutzerkonto Aktivieren")->markdown('mails.auth.activation', ['activation' => $this->activation]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'activation' => [
                'code' => $this->activation->code,
            ],
        ];
    }
}
