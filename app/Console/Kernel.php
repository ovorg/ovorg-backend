<?php

namespace App\Console;

use App\Jobs\CheckTaskDuedates;
use App\Jobs\DeleteUnusedFiles;
use App\Jobs\HandleMeetingNotifications;
use App\Jobs\SendMeetingSummary;
use App\Jobs\UpdateUserPresent;
use Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->job(new DeleteUnusedFiles())->hourly();
        $schedule->job(new UpdateUserPresent())->hourly();
        $schedule->job(new SendMeetingSummary())->dailyAt('10:00')->when(function () {
            return (new Carbon('second saturday of this month'))->isToday();
        });
        $schedule->job(new HandleMeetingNotifications())->dailyAt('10:00');
        $schedule->job(new CheckTaskDuedates())->dailyAt('10:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
