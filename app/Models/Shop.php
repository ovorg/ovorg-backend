<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Shop extends Model
{
    use HasFactory;

    // Basic Eloquent relations

    public function orderItems(): HasMany
    {
        return $this->hasMany(OrderItem::class);
    }
}
