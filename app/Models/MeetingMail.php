<?php

namespace App\Models;

use App\Models\Meeting;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class MeetingMail extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'date',
        'sent',
        'include_maybes',
    ];

    protected $casts = [
        'sent'           => 'bool',
        'include_maybes' => 'bool',
    ];

    protected $dates = [
        'date',
    ];

    // Basic Eloquent relations

    public function meeting(): BelongsTo
    {
        return $this->belongsTo(Meeting::class);
    }
}
