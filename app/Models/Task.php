<?php

namespace App\Models;

use App\Models\File;
use App\Models\TaskCategory;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

class Task extends Model
{
    use HasFactory;

    const PARTICIPATION_NO = 0;
    const PARTICIPATION_MEMBER = 1;
    const PARTICIPATION_RESPONSIBLE_MEMBER = 2;
    const PARTICIPATION_RESPONSIBLE = 3;

    protected $dates = [
        'duedate',
    ];

    protected $casts = [
        'inform_executives' => 'bool',
    ];

    /**
     * Get the participation type for the task.
     *
     * @param  \App\Models\User|null $user
     * @return int
     */
    public function getParticipationType(?User $user): int
    {
        if (!$user) {
            return self::PARTICIPATION_NO;
        }
        if ($this->responsible && $this->responsible->id == $user->id) {
            if ($this->isMember($user)) {
                return self::PARTICIPATION_RESPONSIBLE_MEMBER;
            }
            return self::PARTICIPATION_RESPONSIBLE;
        }

        if ($this->isMember($user)) {
            return self::PARTICIPATION_MEMBER;
        }

        return self::PARTICIPATION_NO;
    }

    /**
     * Check is $user is in $this->users
     *
     * @param  \App\Models\User $user
     * @return bool
     */
    public function isMember(User $user): bool
    {
        return $this->users->contains($user);
    }

    /**
     * Return the users and the responsible in a collection.
     *
     * @return \Illuminate\Support\Collection
     */
    public function getAllParticipants(): Collection
    {
        return $this->users->merge($this->responsible()->get());
    }

    // Basic Eloquent relations

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class);
    }

    public function responsible(): BelongsTo
    {
        return $this->belongsTo(User::class, 'responsible_id');
    }

    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'creator_id');
    }

    public function taskCategory(): BelongsTo
    {
        return $this->belongsTo(TaskCategory::class);
    }

    public function files(): HasMany
    {
        return $this->hasMany(File::class);
    }
}
