<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Staudenmeir\EloquentHasManyDeep\HasManyDeep;

class EventRegistration extends Model
{
    use \Staudenmeir\EloquentHasManyDeep\HasRelationships;
    use HasFactory;

    protected $fillable = [
        'event_id',
        'user_id',
        'event_date_option_id',
    ];

    // Basic Eloquent relations

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function event(): HasManyDeep
    {
        return $this->hasManyDeepFromRelations($this->dateOption(), (new EventDateOption())->event());
    }

    public function dateOption(): BelongsTo
    {
        return $this->belongsTo(EventDateOption::class, 'event_date_option_id');
    }

    public function eventDateOption(): BelongsTo
    {
        return $this->belongsTo(EventDateOption::class);
    }
}
