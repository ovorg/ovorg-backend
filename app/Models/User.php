<?php

namespace App\Models;

use Activation;
use App\Models\Division;
use App\Models\Driver;
use App\Models\Meeting;
use App\Models\Position;
use App\Models\Privacy;
use App\Models\SCBA;
use App\Models\UserGroup;
use Carbon\Carbon;
use Cartalyst\Sentinel\Users\EloquentUser as CartalystUser;
use Illuminate\Auth\Authenticatable as AuthenticatableTrait;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;
use Staudenmeir\EloquentHasManyDeep\HasManyDeep;

class User extends CartalystUser implements Authenticatable
{
    use AuthenticatableTrait;
    use HasApiTokens;
    use Notifiable;
    use \Staudenmeir\EloquentHasManyDeep\HasRelationships;
    use HasFactory;

    protected $fillable = [
        'username',
        'password',
    ];

    protected $loginNames = [
        'username',
    ];

    protected $hidden = [
        'password', 'permissions', 'doodle_token',
    ];

    protected $dates = [
        'scan_timeout',
        'birth',
    ];

    protected $casts = [
        'present'     => 'bool',
        'vegetarian'  => 'bool',
        'permissions' => 'json',
    ];

    /**
     * Get the full name.
     *
     * @return string
     */
    public function getFullName()
    {
        return "$this->first_name $this->last_name";
    }

    /**
     * Find the user instance for the given username.
     *
     * @param  string  $username
     * @return \App\Models\User
     */
    public function findForPassport($login)
    {
        $user = $this->where('email', $login)->first();
        if ($user) {
            return $user;
        }

        return $this->where('username', $login)->first();
    }

    /**
     * Check if the user is activated and if the password is correct
     */
    public function validateForPassportPasswordGrant($password)
    {
        return Activation::completed($this) && Hash::check($password, $this->password);
    }

    /**
     * Return the position name for the Users gender.
     *
     * @return string
     */
    public function genderedPosition(): ?string
    {
        if (!$this->position) {
            return null;
        }

        switch ($this->gender) {
            case 'm':
                return $this->position->male;
            case 'f':
                return $this->position->female;
            default:
                return $this->position->inter;
        }
    }

    /**
     * Return all Meetings assigned to an User.
     *
     * @return Illuminate\Support\Collection
     */
    public function getAllMeetings(): Collection
    {
        return $this->meetings->merge($this->meetingsFromGroups)->merge($this->responsibleMeetings);
    }

    /**
     * Return all Events assigned to an User.
     *
     * @return Illuminate\Support\Collection
     */
    public function getAllEvents(): Collection
    {
        return $this->events->merge($this->eventsFromGroups)->merge($this->responsibleEvents);
    }

    /**
     * Return all Events with EventDateOptions in the future.
     *
     * @return Illuminate\Support\Collection
     */
    public function getAllFutureEvents(): Collection
    {
        $futureQuery = function (Builder $query) {
            $query->where('date', '>=', Carbon::tomorrow());
        };

        return $this->events()
            ->whereHas('dateOptions', $futureQuery)->get()
            ->merge($this->eventsFromGroups()
                ->whereHas('dateOptions', $futureQuery)->get())
            ->merge($this->responsibleEvents()
                ->whereHas('dateOptions', $futureQuery)->get());
    }

    /**
     * Return all Meetings assigned to an User that start in $month.
     *
     * @param $month Carbon date that is in the month
     * @return Illuminate\Support\Collection
     */
    public function getAllMeetingsForMonth($month): Collection
    {
        return $this->getMeetingsForMonth($month)
            ->merge($this->getMeetingsFromGroupsForMonth($month))
            ->merge($this->getResponsibleMeetingsForMonth($month));
    }

    /**
     * Return all meetings that start in the same month as $month
     * that are attached directly to the User.
     *
     * @param $month Carbon date that is in the month
     */
    public function getMeetingsForMonth($month): Collection
    {
        return $this->meetings()->whereMonth('start', $month)->whereYear('start', $month)->get();
    }

    /**
     * Return all meetings that start in the same month as $month
     * that are attached to the User via UserGroups.
     *
     * @param $month Carbon date that is in the month
     */
    public function getMeetingsFromGroupsForMonth($month): Collection
    {
        return $this->meetingsFromGroups()->whereMonth('start', $month)->whereYear('start', $month)->get();
    }

    /**
     * Return all meetings that start in the same month as $month
     * where User is responsible.
     *
     * @param $month Carbon date that is in the month
     */
    public function getResponsibleMeetingsForMonth($month): Collection
    {
        return $this->responsibleMeetings()->whereMonth('start', $month)->whereYear('start', $month)->get();
    }

    /**
     * Return true when the User is invited to a Meeting.
     * Otherwise return false.
     *
     * @param  \App\Models\Meeting $meeting
     * @return bool
     */
    public function isInvitedToMeeting(Meeting $meeting): bool
    {
        return $this->getAllMeetings()->contains($meeting);
    }

    /**
     * Return true when the User is invited to an Event.
     * Otherwise return false.
     *
     * @param  \App\Models\Event $event
     * @return bool
     */
    public function isInvitedToEvent(Event $event): bool
    {
        return $this->getAllEvents()->contains($event);
    }

    /**
     * Get the permissions from all roles and from the user.
     *
     * @return array
     */
    public function getAllPermissions(): array
    {
        $rolePermissions = $this->getRolePermissions();
        $userPermissions = $this->getUserPermissions();
        if (!$rolePermissions && !$userPermissions) {
            return [];
        }
        if (!$rolePermissions) {
            return $userPermissions;
        }
        if (!$userPermissions) {
            return $rolePermissions;
        }
        return array_merge($this->permissions, $rolePermissions);
    }

    /**
     * Get the permissions from the user.
     *
     * @return array
     */
    public function getUserPermissions(): array
    {
        $permissions = $this->permissions;
        if (!$permissions) {
            return [];
        }
        return $permissions;
    }

    /**
     * Get the permissions from all roles.
     *
     * @return array
     */
    private function getRolePermissions(): array
    {
        $permissions = [];
        foreach ($this->roles as $role) {
            $rolePermissions = $role->permissions;
            if (!$rolePermissions) {
                continue;
            }
            $permissions = array_merge($permissions, $rolePermissions);
        }
        return $permissions;
    }

    /**
     * Receive all meetings for which the user has not registered,
     * but to which he is invited.
     *
     * @return Illuminate\Support\Collection
     */
    public function notRegisteredMeetings(): Collection
    {
        $invitedMeetings = $this->getAllMeetings();
        $registeredMeetings = $this->registeredMeetings()->get();

        return $invitedMeetings->filter(function (Meeting $meeting) use ($registeredMeetings) {
            return !$registeredMeetings->contains('id', $meeting->id);
        });
    }

    /**
     * Return all MeetingMails assigned to the User for today
     * where sent is false.
     *
     * @return Illuminate\Support\Collection
     */
    public function getAllUnsentMeetingMailsForToday(): Collection
    {
        return $this->getAllMeetingMailsForToday()->where('sent', false);
    }

    /**
     * Return all MeetingMails assigned to the User for today.
     *
     * @return Illuminate\Support\Collection
     */
    public function getAllMeetingMailsForToday(): Collection
    {
        return $this->getAllMeetingMails()->filter(function (MeetingMail $meetingMail) {
            return $meetingMail->date->isToday();
        });
    }

    /**
     * Return all MeetingMails assigned to the User.
     *
     * @return Illuminate\Support\Collection
     */
    public function getAllMeetingMails(): Collection
    {
        return $this->meetingMails()->get()->merge($this->meetingMailsFromGroups()->get())->merge($this->meetingMailsfromResponsible()->get());
    }

    // Basic Eloquent relations

    public function position(): BelongsTo
    {
        return $this->belongsTo(Position::class);
    }

    public function division(): BelongsTo
    {
        return $this->belongsTo(Division::class);
    }

    public function userGroups(): BelongsToMany
    {
        return $this->belongsToMany(UserGroup::class)->withTimestamps();
    }

    public function meetings(): BelongsToMany
    {
        return $this->belongsToMany(Meeting::class)->withTimestamps();
    }

    public function responsibleMeetings(): HasMany
    {
        return $this->hasMany(Meeting::class, 'responsible_id');
    }

    public function driver(): HasOne
    {
        return $this->hasOne(Driver::class);
    }

    public function scba(): HasOne
    {
        return $this->hasOne(SCBA::class);
    }

    public function notificationPreference(): HasOne
    {
        return $this->hasOne(NotificationPreference::class)->withDefault();
    }

    public function privacy(): HasOne
    {
        return $this->hasOne(Privacy::class)->withDefault();
    }

    public function registeredMeetings(): BelongsToMany
    {
        return $this->belongsToMany(Meeting::class, 'meeting_registration')->withPivot(['comment', 'attendance']);
    }

    public function tasks(): HasMany
    {
        return $this->hasMany(Task::class);
    }

    public function createdOrderItems(): HasOne
    {
        return $this->hasOne(OrderItem::class, 'creator_id');
    }

    public function responsibleEvents(): HasMany
    {
        return $this->hasMany(Event::class, 'responsible_id');
    }

    public function events(): BelongsToMany
    {
        return $this->belongsToMany(Event::class)->withTimestamps();
    }

    public function eventRegistrations(): HasMany
    {
        return $this->hasMany(EventRegistration::class);
    }

    // Deep relations

    public function meetingsFromGroups(): HasManyDeep
    {
        return $this->hasManyDeepFromRelations($this->userGroups(), (new UserGroup())->meetings());
    }

    public function meetingMails(): HasManyDeep
    {
        return $this->hasManyDeepFromRelations(
            $this->meetings(),
            (new Meeting())->meetingMails()
        );
    }

    public function meetingMailsFromGroups(): HasManyDeep
    {
        return $this->hasManyDeepFromRelations($this->userGroups(), (new UserGroup())->meetings(), (new Meeting())->meetingMails());
    }

    public function meetingMailsFromResponsible(): HasManyDeep
    {
        return $this->hasManyDeepFromRelations($this->responsibleMeetings(), (new Meeting())->meetingMails());
    }

    public function eventsFromGroups(): HasManyDeep
    {
        return $this->hasManyDeepFromRelations($this->userGroups(), (new UserGroup())->events());
    }

    public function registeredEvents(): HasManyDeep
    {
        return $this->hasManyDeepFromRelations($this->eventRegistrations(), (new EventRegistration())->event())->distinct('event.id');
    }
}
