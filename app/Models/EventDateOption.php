<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class EventDateOption extends Model
{
    use HasFactory;

    protected $fillable = [
        'event_id',
    ];

    protected $dates = [
        'date',
    ];

    // Basic Eloquent relations

    public function event()
    {
        return $this->belongsTo(Event::class);
    }

    public function registrations(): HasMany
    {
        return $this->hasMany(EventRegistration::class);
    }
}
