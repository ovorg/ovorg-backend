<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Support\Collection;
use Staudenmeir\EloquentHasManyDeep\HasManyDeep;

class Event extends Model
{
    use \Staudenmeir\EloquentHasManyDeep\HasRelationships;
    use HasFactory;

    protected $casts = [
        'force_comment' => 'bool',
    ];

    /**
     * Return all Users assigned to this event.
     *
     * @return \Illuminate\Support\Collection
     */
    public function allUsers(): Collection
    {
        return $this->users->merge($this->usersFromGroups)->merge($this->responsible()->get());
    }

    /**
     * Return all Users that are not registered to all DateOptions.
     *
     * @return \Illuminate\Support\Collection
     */
    public function usersWithMissingRegistration(): Collection
    {
        $users = collect();
        foreach ($this->allUsers() as $user) {
            foreach ($this->dateOptions as $date) {
                $registration = EventRegistration::where('event_date_option_id', $date->id)
                    ->where('user_id', $user->id)->first();
                if (!$registration) {
                    // If User did not register to this DateOption,
                    // add it to the Users list and continue to the next User.
                    $users->push($user);
                    break;
                }
            }
        }
        return $users;
    }

    // Basic Eloquent relations

    /**
     * @deprecated use eventDateOptions()
     */
    public function dateOptions(): HasMany
    {
        return $this->hasMany(EventDateOption::class);
    }

    public function eventDateOptions(): HasMany
    {
        return $this->hasMany(EventDateOption::class);
    }

    public function registrations(): HasManyThrough
    {
        return $this->hasManyThrough(EventRegistration::class, EventDateOption::class);
    }

    public function responsible(): BelongsTo
    {
        return $this->belongsTo(User::class, 'responsible_id');
    }

    public function userGroups(): BelongsToMany
    {
        return $this->belongsToMany(UserGroup::class);
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class);
    }

    // Deep Eloquent relations

    /**
     * Return all Users assigned to the Event via groups.
     */
    public function usersFromGroups(): HasManyDeep
    {
        return $this->hasManyDeep(
            User::class,
            ['event_user_group', UserGroup::class, 'user_user_group']
        );
    }

    /**
     * Return all Users registered to the Event.
     * No matter whether they are still assigned to it or not.
     */
    public function registeredUsers(): HasManyDeep
    {
        return $this->hasManyDeepFromRelations($this->registrations(), (new EventRegistration())->user())->distinct('users.id');
    }
}
