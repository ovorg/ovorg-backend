<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Privacy extends Model
{
    use HasFactory;

    protected $primaryKey = 'user_id';
    public $incrementing = false;

    protected $fillable = [
        'user_id',
    ];

    protected $casts = [
        'email'  => 'bool',
        'mobile' => 'bool',
        'phone'  => 'bool',
    ];

    protected $attributes = [
        'email'  => false,
        'mobile' => false,
        'phone'  => false,
    ];

    // Basic Eloquent relations

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
