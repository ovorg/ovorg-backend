<?php

namespace App\Models;

use App\Models\Task;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class File extends Model
{
    use HasFactory;

    public static $taskFolder = 'tasks';
    public static $uploadFolder = 'upload';

    /** Check if a new file would exceed the space limit for the storage.
     *
     * @param  int $size The size of the file in bytes.
     * @return bool
     */
    static function isEnoughSpace(int $size): bool
    {
        $maxSize = config('filesystems.max_storage_size') * 1000 * 1000;
        $actuallSize = self::sum('size');
        return $actuallSize + $size <= $maxSize;
    }

    /**
     * Associate the file with the task and
     * move it into the task folder
     *
     * @param \App\Models\Task $task
     */
    public function moveToTask(Task $task)
    {
        $this->task()->associate($task);

        $path = Str::replaceFirst(self::$uploadFolder, self::$taskFolder, $this->path);
        Storage::move($this->path, $path);
        $this->path = $path;

        if (!$this->save()) {
            abort(500, "Fehler beim Speichern.");
        }
    }

    // Basic Eloquent relations

    public function task(): BelongsTo
    {
        return $this->belongsTo(Task::class);
    }

    public function uploader(): BelongsTo
    {
        return $this->belongsTo(User::class, 'uploader_id');
    }
}
