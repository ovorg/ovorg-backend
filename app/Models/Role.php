<?php

namespace App\Models;

use Cartalyst\Sentinel\Roles\EloquentRole as CartalystRole;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends CartalystRole
{
    use HasFactory;
}
