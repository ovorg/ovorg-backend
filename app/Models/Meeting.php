<?php

namespace App\Models;

use App\Models\MeetingMail;
use App\Models\User;
use App\Models\UserGroup;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;
use Staudenmeir\EloquentHasManyDeep\HasManyDeep;

class Meeting extends Model
{
    use \Staudenmeir\EloquentHasManyDeep\HasRelationships;
    use HasFactory;

    protected $dates = [
        'start',
        'end',
        'register_until',
    ];

    protected $casts = [
        'comment' => 'bool',
    ];

    /**
     * Return Users that are invited to the meeting but didn't register yet
     *
     * @return \Illuminate\Support\Collection
     */
    public function notRegistered(): Collection
    {
        $registeredUsers = $this->registeredUsers;
        return $this->allUsers()->filter(function ($item) use ($registeredUsers) {
            return !$registeredUsers->contains('id', $item->id);
        });
    }

    /**
     * Return all Users assigned to this meeting
     *
     * @return \Illuminate\Support\Collection
     */
    public function allUsers(): Collection
    {
        return $this->users->merge($this->usersFromGroups)->merge($this->responsible()->get());
    }

    /**
     * Return Users where attendance == yes
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function attendanceYes(): BelongsToMany
    {
        return $this->attendance('yes');
    }

    /**
     * Return Users where attendance == no
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function attendanceNo(): BelongsToMany
    {
        return $this->attendance('no');
    }

    /**
     * Return Users where attendance == maybe
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function attendanceMaybe(): BelongsToMany
    {
        return $this->attendance('maybe');
    }

    /**
     * Return Users where attendance == $attendance
     *
     * @param  string $attendance
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    private function attendance(string $attendance): BelongsToMany
    {
        return $this->registeredUsers()->wherePivot('attendance', $attendance);
    }

    /**
     * Return if the User is already registered in the Meeting.
     *
     * @param \App\Models\User $user
     * @return bool
     */
    public function isUserRegistered(User $user)
    {
        return $this->registeredUsers()->where('user_id', $user->id)->count() > 0;
    }

    // Basic Eloquent relations

    public function responsible(): BelongsTo
    {
        return $this->belongsTo(User::class, 'responsible_id');
    }

    public function userGroups(): BelongsToMany
    {
        return $this->belongsToMany(UserGroup::class);
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class);
    }

    public function registeredUsers(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'meeting_registration')->withPivot('comment', 'attendance')->withTimestamps();
    }

    public function meetingMails(): HasMany
    {
        return $this->hasMany(MeetingMail::class);
    }

    // Deep Eloquent relations

    /**
     * Return all Users assigned to the Meeting via groups.
     */
    public function usersFromGroups(): HasManyDeep
    {
        return $this->hasManyDeep(
            User::class,
            ['meeting_user_group', UserGroup::class, 'user_user_group']
        );
    }
}
