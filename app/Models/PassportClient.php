<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\Client as BaseClient;

class PassportClient extends BaseClient
{
    /**
     * Get the client for password grant.
     *
     * @return PassportClient
     */
    static public function passwordGrantClient()
    {
        return self::where('name', config('app.name') . ' Password Grant Client')->firstOrFail();
    }
}
