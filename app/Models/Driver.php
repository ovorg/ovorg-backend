<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Driver extends Model
{
    use HasFactory;

    protected $primaryKey = 'user_id';
    public $incrementing = false;

    protected $fillable = [
        'user_id',
    ];

    protected $casts = [
        'b'  => 'bool',
        'be' => 'bool',
        'c'  => 'bool',
        'ce' => 'bool',
    ];

    // Basic Eloquent relations

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
