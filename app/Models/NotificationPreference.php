<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class NotificationPreference extends Model
{
    use HasFactory;

    protected $primaryKey = "user_id";
    public $incrementing = false;

    protected $fillable = [
        "meeting_notifications",
        "task_notifications",
        "event_notifications",
    ];

    protected $casts = [
        "meeting_notifications" => "bool",
        "task_notifications" => "bool",
        "event_notifications" => "bool",
    ];

    protected $attributes = [
        "meeting_notifications" => true,
        "task_notifications" => true,
        "event_notifications" => true,
    ];

    // Basic Eloquent relations

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
