<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class SCBA extends Model
{
    use HasFactory;

    protected $primaryKey = 'user_id';
    public $incrementing = false;
    protected $table = 'scbas';

    protected $dates = [
        'next_examination',
        'last_instruction',
        'last_excercise',
        'last_deployment',
        'last_cbrn_deployment',
    ];

    protected $fillable = [
        'user_id',
    ];

    protected $casts = [
        'cbrn' => 'bool',
    ];

    // Basic Eloquent relations

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
