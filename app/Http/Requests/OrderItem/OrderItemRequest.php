<?php

namespace App\Http\Requests\OrderItem;

use Illuminate\Foundation\Http\FormRequest;

class OrderItemRequest extends FormRequest {
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'name'           => 'required|string|max:191',
            'price'          => 'nullable|numeric',
            'count'          => 'nullable|integer',
            'url'            => 'nullable|string|max:65535',
            'article_number' => 'nullable|string|max:191',
            'description'    => 'nullable|string|max:65535',
            'ordered'        => 'required|boolean',
            'shop'           => 'nullable|integer|exists:shops,id',
        ];
    }
}
