<?php

namespace App\Http\Requests\FeedbackRecipient;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class FeedbackRecipientRequest extends FormRequest {
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'email' => [
                'required',
                'string',
                'email',
                'max:191',
                Rule::unique('feedback_recipients')->ignore($this->feedbackRecipient),
            ],
        ];
    }
}
