<?php

namespace App\Http\Requests\Event;

use Illuminate\Foundation\Http\FormRequest;

class EventRequest extends FormRequest {
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'title'          => 'required|string|max:191',
            'description'    => 'nullable|string|max:65535',
            'force_comment'  => 'nullable|boolean',
            'user_groups'    => 'nullable|array',
            'user_groups.*'  => 'nullable|distinct|integer|exists:user_groups,id',
            'users'          => 'nullable|array',
            'users.*'        => 'nullable|distinct|integer|exists:users,id',
            'responsible'    => 'nullable|integer|exists:users,id',
            'date_options'   => 'nullable|array',
            'date_options.*' => 'nullable|date',
        ];
    }
}
