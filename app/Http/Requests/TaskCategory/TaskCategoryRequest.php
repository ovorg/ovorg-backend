<?php

namespace App\Http\Requests\TaskCategory;

use Illuminate\Foundation\Http\FormRequest;

class TaskCategoryRequest extends FormRequest {
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'name' => 'required|string|max:191|unique:task_categories,name',
        ];
    }
}
