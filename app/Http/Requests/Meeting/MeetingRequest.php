<?php

namespace App\Http\Requests\Meeting;

use Illuminate\Foundation\Http\FormRequest;

class MeetingRequest extends FormRequest {
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'title'          => 'required|string|max:191',
            'start'          => 'required|date',
            'end'            => 'required|date|after_or_equal:start',
            'register_until' => 'nullable|date|before_or_equal:start',
            'description'    => 'nullable|string',
            'comment'        => 'required|boolean',
            'user_groups'    => 'nullable|array',
            'user_groups.*'  => 'nullable|distinct|integer|exists:user_groups,id',
            'users'          => 'nullable|array',
            'users.*'        => 'nullable|distinct|integer|exists:users,id',
            'responsible'    => 'nullable|integer|exists:users,id',
        ];
    }
}
