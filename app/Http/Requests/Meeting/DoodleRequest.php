<?php

namespace App\Http\Requests\Meeting;

use Illuminate\Foundation\Http\FormRequest;

class DoodleRequest extends FormRequest {
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'attendance' => 'required|in:yes,no,maybe',
            'comment'    => 'nullable|string',
        ];
    }
}
