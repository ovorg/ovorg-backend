<?php

namespace App\Http\Requests\NotificationPreference;

use Illuminate\Foundation\Http\FormRequest;

class NotificationPreferenceRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "meeting_notifications" => "boolean",
            "task_notifications" => "boolean",
            "event_notifications" => "boolean",
        ];
    }
}
