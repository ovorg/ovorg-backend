<?php

namespace App\Http\Requests\Position;

use Illuminate\Foundation\Http\FormRequest;

class PositionRequest extends FormRequest {
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'male'   => 'required|string|max:191',
            'female' => 'required|string|max:191',
            'inter'  => 'required|string|max:191',
        ];
    }
}
