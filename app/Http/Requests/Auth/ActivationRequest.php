<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ActivationRequest extends FormRequest {
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'password' => 'required|string|min:8',
            'user'     => 'required|integer|exists:users,id',
            'username' => [
                'required',
                'string',
                'max:191',
                Rule::unique('users')->ignore($this->user),
            ],
            'token'    => 'required',
        ];
    }
}
