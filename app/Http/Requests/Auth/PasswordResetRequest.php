<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class PasswordResetRequest extends FormRequest {
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'password'      => 'required|string|min:8',
            'user'          => 'required|integer|exists:users,id',
            'reminder_code' => 'required|string',
        ];
    }
}
