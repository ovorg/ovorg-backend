<?php

namespace App\Http\Requests\FeedbackTemplate;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class FeedbackTemplateRequest extends FormRequest {
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'name'     => [
                'required',
                'string',
                'max:191',
                Rule::unique('feedback_templates')->ignore($this->feedback_template),
            ],
            'template' => 'required|string|max:65535',
        ];
    }
}
