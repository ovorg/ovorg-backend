<?php

namespace App\Http\Requests\EventDoodle;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;
use Illuminate\Validation\Rule;

class EventDoodleRequest extends FormRequest {
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'availability'   => 'required|in:yes,no,maybe',
            'comment'        => [
                Rule::requiredIf(function () {
                    $availability = Request::input('availability');
                    return ($this->event->force_comment &&
                        ($availability == 'no' || $availability == 'maybe'));
                }),
                'nullable',
                'string',
                'max:65535',
            ],
            'date_options'   => 'required|array',
            'date_options.*' => [
                'required',
                'exists:event_date_options,id',
            ],
        ];
    }
}
