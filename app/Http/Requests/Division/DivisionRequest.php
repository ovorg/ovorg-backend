<?php

namespace App\Http\Requests\Division;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class DivisionRequest extends FormRequest {
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'name'       => [
                'required',
                'string',
                'max:191',
                Rule::unique('divisions')->ignore($this->division),
            ],
            'short_name' => [
                'required',
                'string',
                'max:191',
                Rule::unique('divisions')->ignore($this->division),
            ],
            'users'      => 'nullable|array',
            'users.*'    => 'nullable|integer|exists:users,id|distinct',
        ];
    }
}
