<?php

namespace App\Http\Requests\UserGroup;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserGroupRequest extends FormRequest {
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'name'    => [
                'required',
                'max:191',
                Rule::unique('user_groups')->ignore($this->user_group),
            ],
            'users'   => 'nullable|array',
            'users.*' => 'nullable|integer|exists:users,id',
        ];
    }
}
