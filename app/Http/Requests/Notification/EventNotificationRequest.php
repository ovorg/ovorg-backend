<?php

namespace App\Http\Requests\Notification;

use Illuminate\Foundation\Http\FormRequest;

class EventNotificationRequest extends FormRequest {
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'users'                     => 'required_unless:to_all,true|array',
            'users.*'                   => 'nullable|integer|exists:users,id',
            'to_all'                    => 'required_without:users|boolean',
            'only_missing_registration' => 'required_if:to_all,true|boolean',
        ];
    }
}
