<?php

namespace App\Http\Requests\Notification;

use Illuminate\Foundation\Http\FormRequest;

class MeetingNotificationRequest extends FormRequest {
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'users'          => 'required_unless:to_all,true|array',
            'users.*'        => 'nullable|integer|exists:users,id',
            'to_all'         => 'required_without:users|boolean',
            'maybe'          => 'required_if:to_all,true|boolean',
            'yes'            => 'required_if:to_all,true|boolean',
            'no'             => 'required_if:to_all,true|boolean',
            'not_registered' => 'required_if:to_all,true|boolean',
        ];
    }
}
