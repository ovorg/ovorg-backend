<?php

namespace App\Http\Requests\Notification;

use Illuminate\Foundation\Http\FormRequest;

class CustomNotificationRequest extends FormRequest {
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'body'           => 'required|string',
            'subject'        => 'required|string',
            'users'          => 'nullable|array',
            'users.*'        => 'nullable|integer|exists:users,id',
            'user_groups'    => 'nullable|array',
            'user_groups.*'  => 'nullable|integer|exists:user_groups,id',
            'meetings'       => 'nullable|array',
            'meetings.*'     => 'nullable|integer|exists:meetings,id',
            'maybe'          => 'required_with:meetings|boolean',
            'yes'            => 'required_with:meetings|boolean',
            'no'             => 'required_with:meetings|boolean',
            'not_registered' => 'required_with:meetings|boolean',
            'files'          => 'nullable|array',
            'files.*'        => 'nullable|integer|exists:files,id',
            'send_summary'   => 'nullable|boolean',
        ];
    }
}
