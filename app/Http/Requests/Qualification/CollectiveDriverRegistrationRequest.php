<?php

namespace App\Http\Requests\Qualification;

use Illuminate\Foundation\Http\FormRequest;

class CollectiveDriverRegistrationRequest extends FormRequest {
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'users'   => 'required|array',
            'users.*' => 'required|distinct|integer|exists:users,id',
            'b'       => 'required|boolean',
            'be'      => 'required|boolean',
            'c'       => 'required|boolean',
            'ce'      => 'required|boolean',
        ];
    }
}
