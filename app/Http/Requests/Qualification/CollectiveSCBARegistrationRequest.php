<?php

namespace App\Http\Requests\Qualification;

use Illuminate\Foundation\Http\FormRequest;

class CollectiveSCBARegistrationRequest extends FormRequest {
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'users'                => 'required|array',
            'users.*'              => 'required|distinct|integer|exists:users,id',
            'date'                 => 'nullable|date',
            'next_examination'     => 'required|boolean',
            'last_instruction'     => 'required|boolean',
            'last_excercise'       => 'required|boolean',
            'last_deployment'      => 'required|boolean',
            'last_cbrn_deployment' => 'required|boolean',
        ];
    }
}
