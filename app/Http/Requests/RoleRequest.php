<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RoleRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => [
                'required',
                'string',
                'max:191',
                Rule::unique('roles', 'name')->ignore($this->role),
            ],
            'permissions'   => 'nullable|array',
            'permissions.*' => 'nullable|string',
            'users'         => 'nullable|array',
            'users.*'       => 'nullable|integer|exists:users,id',
        ];
    }
}
