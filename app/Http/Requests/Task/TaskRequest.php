<?php

namespace App\Http\Requests\Task;

use Illuminate\Foundation\Http\FormRequest;

class TaskRequest extends FormRequest {
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'title'             => 'required|string|max:191',
            'description'       => 'nullable|string|max:65535',
            'addition'          => 'nullable|string|max:65535',
            'duedate'           => 'required|date',
            'priority'          => 'required|integer|max:255|min:0',
            'state'             => 'required|integer|max:100|min:0',
            'inform_executives' => 'nullable|boolean',
            'responsible'       => 'required|integer|exists:users,id',
            'task_category'     => 'required|integer|exists:task_categories,id',
            'users'             => 'nullable|array',
            'users.*'           => 'nullable|integer|exists:users,id',
            'files'             => 'nullable|array',
            'files.*'           => 'nullable|integer|exists:files,id',
        ];
    }
}
