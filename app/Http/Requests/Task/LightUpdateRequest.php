<?php

namespace App\Http\Requests\Task;

use Illuminate\Foundation\Http\FormRequest;

class LightUpdateRequest extends FormRequest {
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'description'   => 'nullable|string|max:65535',
            'addition'      => 'nullable|string|max:65535',
            'state'         => 'required|integer|max:100|min:0',
            'participating' => 'required|boolean',
            'files'         => 'nullable|array',
            'files.*'       => 'nullable|integer|exists:files,id',
        ];
    }
}
