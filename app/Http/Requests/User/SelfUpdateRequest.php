<?php

namespace App\Http\Requests\User;

use Auth;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SelfUpdateRequest extends FormRequest {
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'username'           => [
                'required',
                'string',
                'max:191',
                Rule::unique('users')->ignore(Auth::user()->id),
            ],
            'mobile'             => [
                'nullable',
                'string',
                'regex:/^(\+\d{1,15})$/',
                Rule::unique('users')->ignore(Auth::user()->id),
            ],
            'email'              => [
                'nullable',
                'email',
                Rule::unique('users')->ignore(Auth::user()->id),
            ],
            'phone'              => 'nullable|string',
            'vegetarian'         => 'nullable|boolean',
            'health_information' => 'nullable|string',
            'privacy'            => 'nullable|array',
            'privacy.email'      => 'nullable|boolean',
            'privacy.mobile'     => 'nullable|boolean',
            'privacy.phone'      => 'nullable|boolean',
        ];
    }
}
