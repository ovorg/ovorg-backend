<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest {
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'first_name'                => 'required|string',
            'last_name'                 => 'required|string',
            'gender'                    => 'required|in:m,f,x',
            'position'                  => 'required|integer|exists:positions,id',
            'division'                  => 'nullable|integer|exists:divisions,id',
            'birth'                     => 'nullable|date',
            'phone'                     => 'nullable|string',
            'user_groups'               => 'nullable|array',
            'user_groups.*'             => 'nullable|integer|exists:user_groups,id|distinct',
            'scba'                      => 'nullable|array',
            'scba.next_examination'     => 'nullable|date',
            'scba.last_instruction'     => 'nullable|date',
            'scba.last_excercise'       => 'nullable|date',
            'scba.last_deployment'      => 'nullable|date',
            'scba.cbrn'                 => 'nullable|boolean',
            'scba.last_cbrn_deployment' => 'nullable|date',
            'driver'                    => 'nullable|array',
            'driver.b'                  => 'nullable|boolean',
            'driver.be'                 => 'nullable|boolean',
            'driver.c'                  => 'nullable|boolean',
            'driver.ce'                 => 'nullable|boolean',
            'driver.expiry_date'        => 'nullable|date',
            'email'                     => [
                'nullable',
                'email',
                Rule::unique('users')->ignore($this->user),
            ],
            'thwin_id'                  => [
                'nullable',
                'size:8',
                Rule::unique('users')->ignore($this->user),
            ],
            'mobile'                    => [
                'nullable',
                'string',
                'regex:/^(\+\d{1,15})$/',
                Rule::unique('users')->ignore($this->user),
            ],
        ];
    }
}
