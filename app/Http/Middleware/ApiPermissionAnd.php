<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class ApiPermissionAnd
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle()
    {
        $args = func_get_args();
        $request = $args[0];
        $next = $args[1];
        $permissions = array_slice($args, 2);

        $user = Auth::user();
        abort_unless(($user ? $user->hasAccess($permissions) : false), 403);
        return $next($request);
    }
}
