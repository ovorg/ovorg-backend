<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Sentinel;

class ApiPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permission)
    {
        $user = Auth::user();
        abort_unless(($user ? $user->hasAccess($permission) : false), 403);
        return $next($request);
    }
}
