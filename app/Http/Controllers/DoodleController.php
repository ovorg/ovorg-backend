<?php

namespace App\Http\Controllers;

use App\Http\Requests\Meeting\DoodleRequest;
use App\Http\Resources\Meeting\DoodleMeetingResource;
use App\Http\Resources\Meeting\SimplifiedDoodleMeetingResource;
use App\Models\Meeting;
use App\Models\User;
use Auth;
use Carbon;

class DoodleController extends Controller
{
    /**
     * Display a listing of future Meetings.
     *
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function index()
    {
        $meetings = Auth::user()->getAllMeetings()->where('end', '>=', Carbon::now());
        return SimplifiedDoodleMeetingResource::collection($meetings);
    }

    /**
     * Display a listing of future Meetings
     * using the doodle_token to idenfity the User.
     *
     * @param string $token
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function indexToken($token)
    {
        Auth::setUser(User::where('doodle_token', $token)->firstOrFail());
        return $this->index();
    }

    /**
     * Display a listing of past Meetings.
     */
    public function archive()
    {
        $meetings = Auth::user()->getAllMeetings()->where('end', '<', Carbon::now());
        return SimplifiedDoodleMeetingResource::collection($meetings);
    }

    /**
     * Display a listing of past Meetings
     * using the doodle_token to idenfity the User.
     */
    public function archiveToken($token)
    {
        Auth::setUser(User::where('doodle_token', $token)->firstOrFail());
        return $this->archive();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Meeting $meeting
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function show(Meeting $meeting)
    {
        $user = Auth::user();
        if (!$user->isInvitedToMeeting($meeting)) {
            abort(403, 'Du bist zu diesem Dienst nicht eingeladen.');
        }
        return new DoodleMeetingResource($meeting);
    }

    /**
     * Display the specified resource
     * using the doodle_token to idenfity the User.
     *
     * @param  \App\Models\Meeting $meeting
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function showToken(Meeting $meeting, $token)
    {
        Auth::setUser(User::where('doodle_token', $token)->firstOrFail());
        return $this->show($meeting);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Meeting\DoodleRequest $request
     * @param  \App\Models\Meeting $meeting
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function update(DoodleRequest $request, Meeting $meeting)
    {
        $user = Auth::user();

        // Check if user is invited to this meeting
        if (!$user->isInvitedToMeeting($meeting)) {
            abort(403, 'Du kannst dich zu diesem Dienst nicht eintragen.');
        }

        // Check if user can still sign up for this meeting
        if (($meeting->start <= Carbon::now()) ||
            ($meeting->register_until &&
                $meeting->register_until->endOfDay() < Carbon::now())
        ) {
            abort(400, 'Zu diesem Dienst kann man sich nicht mehr eintragen.');
        }

        // Update registration
        if ($meeting->isUserRegistered($user)) {
            $meeting->registeredUsers()->updateExistingPivot($user->id, [
                'attendance' => $request->input('attendance'),
                'comment'    => $request->input('comment'),
            ]);
        } else {
            $meeting->registeredUsers()->attach($user, [
                'attendance' => $request->input('attendance'),
                'comment'    => $request->input('comment'),
            ]);
        }
        $meeting->load('registeredUsers');

        return new DoodleMeetingResource($meeting);
    }

    /**
     * Update the specified resource in storage
     * using the doodle_token to idenfity the User.
     *
     * @param  \App\Http\Requests\Meeting\DoodleRequest $request
     * @param  \App\Models\Meeting $meeting
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function updateToken(DoodleRequest $request, Meeting $meeting, $token)
    {
        Auth::setUser(User::where('doodle_token', $token)->firstOrFail());
        return $this->update($request, $meeting);
    }
}
