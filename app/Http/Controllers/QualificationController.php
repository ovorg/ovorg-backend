<?php

namespace App\Http\Controllers;

use App\Http\Requests\Qualification\CollectiveDriverRegistrationRequest;
use App\Http\Requests\Qualification\CollectiveSCBARegistrationRequest;
use App\Http\Resources\Driver\DriverResource;
use App\Http\Resources\SCBA\SCBAResource;
use App\Models\Driver;
use App\Models\SCBA;
use Carbon\Carbon;
use Illuminate\Http\Request;

class QualificationController extends Controller
{
    /**
     * Initialize permissions middleware.
     */
    public function __construct()
    {
        $this->middleware('permission.api:qualifications.general')->only(['scba', 'driver']);
    }

    /**
     * Display a listing of scbas.
     *
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function scba()
    {
        return SCBAResource::collection(SCBA::get());
    }

    /**
     * Display a listing of drivers.
     *
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function driver()
    {
        return DriverResource::collection(Driver::get());
    }

    /**
     * Update the specified fields for several scbas.
     *
     * @param  \App\Http\Requests\Qualification\CollectiveSCBARegistrationRequest $request
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function updateSCBAs(CollectiveSCBARegistrationRequest $request)
    {
        $scbas = collect();
        foreach ($request->input('users') as $userID) {
            $scba = $this->updateSCBA($request, $userID);
            if (!$scba) {
                continue;
            }
            $scbas->push($scba);
        }

        return SCBAResource::collection($scbas);
    }

    /**
     * Update the specified fields for several one scba.
     *
     * @param  \App\Http\Requests\Qualification\CollectiveSCBARegistrationRequest $request
     * @param  int $userID
     * @return \App\Models\SCBA|null
     */
    public function updateSCBA(CollectiveSCBARegistrationRequest $request, int $userID): ?SCBA
    {
        // Get date. Use today if null.
        $date = $request->input('date');
        $date = $date ? Carbon::parse($date) : Carbon::today();

        // Get SCBA. Create if it doesn't exist.
        $scba = SCBA::find($userID);
        if (!$scba) {
            $scba = new SCBA(['user_id' => $userID]);
        }

        // Update data.
        if ($request->input('next_examination')) {
            $scba->next_examination = $date;
        }
        if ($request->input('last_instruction')) {
            $scba->last_instruction = $date;
        }
        if ($request->input('last_excercise')) {
            $scba->last_excercise = $date;
        }
        if ($request->input('last_deployment')) {
            $scba->last_deployment = $date;
        }
        if ($request->input('last_cbrn_deployment')) {
            $scba->cbrn = true;
            $scba->last_cbrn_deployment = $date;
        }

        // Save.
        if (!$scba->save()) {
            return null;
        }
        return $scba;
    }

    /**
     * Update several drivers.
     *
     * @param  \App\Http\Requests\Qualification\CollectiveRegistrationRequest $request
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function updateDrivers(CollectiveDriverRegistrationRequest $request)
    {
        $drivers = collect();
        foreach ($request->input('users') as $userID) {
            $driver = $this->updateDriver($request, $userID);
            if (!$driver) {
                continue;
            }
            $drivers->push($driver);
        }

        return DriverResource::collection($drivers);
    }

    /**
     * Update the specified fields one several driver.
     *
     * @param  \App\Http\Requests\Qualification\CollectiveRegistrationRequest $request
     * @param  int $userID
     * @return \App\Models\Driver
     */
    public function updateDriver(CollectiveDriverRegistrationRequest $request, int $userID): ?Driver
    {
        // Get Driver. Create if it doesn't exist.
        $driver = Driver::find($userID);
        if (!$driver) {
            $driver = new Driver(['user_id' => $userID]);
        }

        // Update data.
        $driver->b = $request->input('b') ? true : false;
        $driver->be = $request->input('be') ? true : false;
        $driver->c = $request->input('c') ? true : false;
        $driver->ce = $request->input('ce') ? true : false;

        // Save.
        if (!$driver->save()) {
            return null;
        }
        return $driver;
    }
}
