<?php

namespace App\Http\Controllers;

use App\Http\Requests\FeedbackRequest;
use App\Mail\FeedbackMail;
use App\Models\FeedbackRecipient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class FeedbackController extends Controller
{
    /**
     * Send feedback.
     */
    public function send(FeedbackRequest $request)
    {
        $user = $request->user();
        $feedbackRecipients = FeedbackRecipient::all();

        foreach ($feedbackRecipients as $recipient) {
            $mail = new FeedbackMail($request->input('subject'), $request->input('body'));
            $mail->from(config('mail.from.address'), $user->getFullName());
            $mail->replyTo($user->email, $user->getFullName());
            // $mail->to($recipient->email);

            Mail::to($recipient->email)->send($mail);
        }

        return response()->json();
    }
}
