<?php

namespace App\Http\Controllers;

use App\Http\Requests\NotificationPreference\NotificationPreferenceRequest;
use App\Http\Resources\NotificationPreference\NotificationPreferenceResource;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class NotificationPreferenceController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(): JsonResource
    {
        $user = Auth::user();
        $preference = $user->notificationPreference;

        return new NotificationPreferenceResource($preference);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(
        NotificationPreferenceRequest $request
    ): JsonResource {
        $user = Auth::user();
        $preference = $user->notificationPreference;

        if ($request->exists("meeting_notifications")) {
            $preference->meeting_notifications = $request->input("meeting_notifications");
        }

        if ($request->exists("task_notifications")) {
            $preference->task_notifications = $request->input("task_notifications");
        }
        if ($request->exists("event_notifications")) {
            $preference->event_notifications = $request->input("event_notifications");
        }

        $preference->save();

        return new NotificationPreferenceResource($preference);
    }
}
