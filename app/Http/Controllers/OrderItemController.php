<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderItem\OrderItemRequest;
use App\Http\Resources\OrderItem\OrderItemResource;
use App\Models\OrderItem;
use App\Models\Shop;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class OrderItemController extends Controller
{
    /**
     * Initialize permissions middleware.
     */
    public function __construct()
    {
        $this->middleware('permission.api:order_list.general');
    }

    /**
     * Display a listing of the resource where ordered == false.
     *
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function index()
    {
        return OrderItemResource::collection(OrderItem::where('ordered', false)->get());
    }

    /**
     * Display a listing of the resource where ordered == true.
     *
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function archive()
    {
        return OrderItemResource::collection(OrderItem::where('ordered', true)->get());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OrderItem  $orderItem
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function show(OrderItem $orderItem)
    {
        return new OrderItemResource($orderItem);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\OrderItem\OrderItemRequest $request
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function store(OrderItemRequest $request)
    {
        $user = Auth::user();
        $orderItem = new OrderItem();
        $orderItem->creator()->associate($user);
        return $this->save($request, $orderItem);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\OrderItem\OrderItemRequest $request
     * @param  \App\Models\OrderItem  $orderItem
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function update(OrderItemRequest $request, OrderItem $orderItem)
    {
        return $this->save($request, $orderItem);
    }

    /**
     * Save the specific resource with the values from the request.
     *
     * @param  \App\Http\Requests\OrderItem\OrderItemRequest $request
     * @param  \App\Models\OrderItem $orderItem
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    private function save(OrderItemRequest $request, OrderItem $orderItem): JsonResource
    {
        $orderItem->name = $request->input('name');
        $orderItem->price = $request->input('price');
        $orderItem->count = $request->input('count');
        $orderItem->url = $request->input('url');
        $orderItem->article_number = $request->input('article_number');
        $orderItem->description = $request->input('description');
        $orderItem->ordered = $request->input('ordered') ?: false;

        // Shop
        $shop = $request->input('shop');
        if ($shop) {
            $orderItem->shop()->associate(Shop::find($shop));
        } else {
            $orderItem->shop()->dissociate();
        }

        // Save OrderItem.
        if (!$orderItem->save()) {
            abort(500, 'Fehler beim Speichern.');
        }

        return new OrderItemResource($orderItem);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OrderItem  $orderItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderItem $orderItem)
    {
        $orderItem->delete();
        return new OrderItemResource($orderItem);
    }
}
