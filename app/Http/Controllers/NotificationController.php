<?php

namespace App\Http\Controllers;

use App\Http\Requests\Notification\CustomNotificationRequest;
use App\Http\Requests\Notification\EventNotificationRequest;
use App\Http\Requests\Notification\MeetingNotificationRequest;
use App\Http\Resources\User\SimplifiedUserResource;
use App\Jobs\Auth\CreateActivation;
use App\Jobs\Auth\CreateReminder;
use App\Models\Event;
use App\Models\EventRegistration;
use App\Models\File;
use App\Models\Meeting;
use App\Models\User;
use App\Models\UserGroup;
use App\Notifications\CustomNotification;
use App\Notifications\CustomSummaryNotification;
use App\Notifications\EventNotification;
use App\Notifications\MeetingNotification;
use Auth;
use Illuminate\Http\Request;
use Notification;

class NotificationController extends Controller
{
    /**
     * Initialize permissions middleware.
     */
    public function __construct()
    {
        $this->middleware('permission.api.or:notifications.administration,admin.general')->only(['activation', 'reminder']);
        $this->middleware('permission.api:notifications.general')->only(['custom', 'meeting']);
        $this->middleware('permission.api:events.general')->only(['event']);
    }

    /**
     * Send an activation notification to a single user.
     *
     * @param  \Illiminate\Http\Request $request
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function activation(Request $request, User $user)
    {
        CreateActivation::dispatch($user);
        return new SimplifiedUserResource($user);
    }

    /**
     * Send a reminder notification to a single user.
     *
     * @param  \Illiminate\Http\Request $request
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function reminder(Request $request, User $user)
    {
        CreateReminder::dispatch($user);
        return new SimplifiedUserResource($user);
    }

    /**
     * Send a meeting mail.
     *
     * @param  \App\Http\Requests\Notification\MeetingNotificationRequest $request
     * @param  \App\Models\Meeting $meeting
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function meeting(MeetingNotificationRequest $request, Meeting $meeting)
    {
        $users = null;
        if ($request->input('to_all')) {
            $users = collect();

            if ($request->input('yes')) {
                $users = $users->merge($meeting->attendanceYes);
            }
            if ($request->input('no')) {
                $users = $users->merge($meeting->attendanceNo);
            }
            if ($request->input('maybe')) {
                $users = $users->merge($meeting->attendanceMaybe);
            }
            if ($request->input('not_registered')) {
                $users = $users->merge($meeting->notRegistered());
            }
        } else {
            $users = User::whereIn('id', $request->input('users'))->get();
            $users = $users->filter(function (User $user) use ($meeting) {
                return $user->isInvitedToMeeting($meeting);
            });
        }

        Notification::send($users, new MeetingNotification($meeting));
        return SimplifiedUserResource::collection($users);
    }

    /**
     * Send a EventNotification.
     *
     * @param  \App\Http\Requests\Notification\EventNotificationRequest $request
     * @param  \App\Models\Event $event
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function event(EventNotificationRequest $request, Event $event)
    {
        $users = null;
        if ($request->input('to_all')) {
            if ($request->input('only_missing_registration')) {
                $users = $event->usersWithMissingRegistration();
            } else {
                $users = $event->allUsers();
            }
        } else {
            $users = User::whereIn('id', $request->input('users'))->get();
            $users = $users->filter(function (User $user) use ($event) {
                return $user->isInvitedToEvent($event);
            });
        }

        Notification::send($users, new EventNotification($event));
        return SimplifiedUserResource::collection($users);
    }

    /**
     * Send a custom notification.
     *
     * @param  \App\Http\Requests\Notification\CustomNotificationRequest $request
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function custom(CustomNotificationRequest $request)
    {
        $sender = Auth::user();
        $users = collect();

        // Get files
        $fileIds = $request->input('files');
        $files = collect();
        if ($fileIds) {
            foreach ($fileIds as $fileId) {
                $files->push(File::findOrFail($fileId));
            }
        }
        $maxFileSize = config('mail.max_attachment_size');
        if ($files->sum('size') > $maxFileSize * 1000 * 1000) {
            abort(422, "Es können maximal  $maxFileSize MB an Anhängen versendet werden");
        }

        // Add all selected users
        if ($request->input('users')) {
            foreach ($request->input('users') as $userId) {
                $users->push(User::find($userId));
            }
        }

        // Add all users from user groups
        if ($request->input('user_groups')) {
            foreach ($request->input('user_groups') as $userGroupId) {
                $userGroup = UserGroup::find($userGroupId);
                $users = $users->concat($userGroup->users);
            }
        }

        // Add users from meetings
        if ($request->input('meetings')) {
            foreach ($request->input('meetings') as $meetingId) {
                $meeting = Meeting::find($meetingId);

                if ($request->input('yes')) {
                    $users = $users->merge($meeting->attendanceYes);
                }
                if ($request->input('no')) {
                    $users = $users->merge($meeting->attendanceNo);
                }
                if ($request->input('maybe')) {
                    $users = $users->merge($meeting->attendanceMaybe);
                }
                if ($request->input('not_registered')) {
                    $users = $users->merge($meeting->notRegistered());
                }
            }
        }

        // Remove sender from the users list if send_summary is set.
        // Otherwise add him to the users list so he will always be notified.
        if ($request->input('send_summary')) {
            $users = $users->filter(function (User $user) use ($sender) {
                return $user->id != $sender->id;
            });
        } else {
            $users->push($sender);
        }

        // Remove multiple entries
        $unique = $users->unique('id');

        if ($request->input('send_summary')) {
            $userGroups = collect();
            if ($request->input('user_groups')) {
                foreach ($request->input('user_groups') as $userGroupId) {
                    $userGroups->push(UserGroup::find($userGroupId));
                }
            }

            $meetings = collect();
            if ($request->input('meetings')) {
                foreach ($request->input('meetings') as $meetingId) {
                    $meetings->push(Meeting::find($meetingId));
                }
            }

            $attendance = collect();
            if ($meetings) {
                if ($request->input('yes')) {
                    $attendance->push('ja');
                }
                if ($request->input('no')) {
                    $attendance->push('nein');
                }
                if ($request->input('maybe')) {
                    $attendance->push('vielleicht');
                }
                if ($request->input('not_registered')) {
                    $attendance->push('nicht eingetragen');
                }
            }

            Notification::send($sender, new CustomSummaryNotification($request->input('subject'), $request->input('body'), $unique, $userGroups, $meetings, $attendance, $files));
        }

        Notification::send($unique, new CustomNotification($request->input('subject'), $request->input('body'), $sender, $files));

        return SimplifiedUserResource::collection($unique);
    }
}
