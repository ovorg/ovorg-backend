<?php

namespace App\Http\Controllers;

use App\Http\Requests\Shop\ShopRequest;
use App\Http\Resources\Shop\ShopResource;
use App\Http\Resources\Shop\SimplifiedShopResource;
use App\Models\Shop;
use Illuminate\Http\Resources\Json\JsonResource;

class ShopController extends Controller
{
    /**
     * Initialize permissions middleware.
     */
    public function __construct()
    {
        $this->middleware('permission.api:order_list.general');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function index()
    {
        return SimplifiedShopResource::collection(Shop::all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Shop  $shop
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function show(Shop $shop)
    {
        return new ShopResource($shop);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Shop\ShopRequest $request
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function store(ShopRequest $request)
    {
        $shop = new Shop();
        return $this->save($request, $shop);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Shop\ShopRequest $request
     * @param  \App\Models\Shop  $shop
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function update(ShopRequest $request, Shop $shop): JsonResource
    {
        return $this->save($request, $shop);
    }

    /**
     * Save the specific resource with the values from the request.
     *
     * @param  \App\Http\Requests\Shop\ShopRequest $request
     * @param  \App\Models\Shop  $shop
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function save(ShopRequest $request, Shop $shop)
    {
        $shop->name = $request->input('name');
        $shop->url = $request->input('url');

        // Save Shop.
        if (!$shop->save()) {
            abort(500, 'Fehler beim Speichern.');
        }

        return new ShopResource($shop);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Shop  $shop
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function destroy(Shop $shop)
    {
        $shop->delete();
        return new ShopResource($shop);
    }
}
