<?php

namespace App\Http\Controllers;

use App\Http\Requests\EventDoodle\EventDoodleRequest;
use App\Http\Resources\Event\DoodleEventResource;
use App\Http\Resources\Event\SimplifiedDoodleEventResource;
use App\Models\Event;
use App\Models\EventDateOption;
use App\Models\EventRegistration;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class EventDoodleController extends Controller
{
    /**
     * Display a listing of future Events.
     *
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function index()
    {
        $events = Auth::user()->getAllEvents()->filter(function (Event $event) {
            return (!$event->dateOptions()->exists() ||
                $event->dateOptions()
                ->where('date', '>=', Carbon::today()->startOfDay())
                ->first() != null);
        });
        // $events = Auth::user()->getAllEvents();
        return SimplifiedDoodleEventResource::collection($events);
    }

    /**
     * Display a listing of future Events.
     * using the doodle_token to idenfity the User.
     *
     * @param string $token
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function indexToken($token)
    {
        $this->setAuthUser($token);
        return $this->index();
    }

    /**
     * Display a listing of past Events.
     */
    public function archive()
    {
        $events = Auth::user()->getAllEvents()->filter(function (Event $event) {
            return $event->dateOptions()->where('date', '<', Carbon::today()->startOfDay())->first() != null;
        });
        return SimplifiedDoodleEventResource::collection($events);
    }

    /**
     * Display a listing of past Events
     * using the doodle_token to idenfity the User.
     */
    public function archiveToken($token)
    {
        $this->setAuthUser($token);
        return $this->archive();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Event $event
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function show(Event $event)
    {
        $user = Auth::user();
        if (!$user->isInvitedToEvent($event)) {
            abort(403, 'Du bist zu dieser Abfrage nicht eingeladen.');
        }
        return new DoodleEventResource($event);
    }

    /**
     * Display the specified resource
     * using the doodle_token to idenfity the User.
     *
     * @param  \App\Models\Event $event
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function showToken(Event $event, $token)
    {
        $this->setAuthUser($token);
        return $this->show($event);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\EventDoodle\EventDoodleRequest $request
     * @param  \App\Models\Event $event
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function update(EventDoodleRequest $request, Event $event)
    {
        $user = Auth::user();

        if (!$user->isInvitedToEvent($event)) {
            abort(403, 'Du kannst dich zu diesem Ereigniss nicht eintragen.');
        }

        foreach ($request->input('date_options') as $dateOption) {
            $dateOption = EventDateOption::findOrFail($dateOption);

            // Skip dateOption if it is for another Event.
            if ($dateOption->event->id !== $event->id) {
                continue;
            }

            // Skip dateOption if it is in the past.
            if (!$dateOption->date->isToday() && $dateOption->date->isPast()) {
                continue;
            }

            // Update EventRegistration
            $registration = $event->registrations()->where('user_id', $user->id)->where('event_date_option_id', $dateOption->id)->first();
            if (!$registration) {
                // Create EventRegistration
                $registration = new EventRegistration([
                    'user_id'              => $user->id,
                    'event_date_option_id' => $dateOption->id,
                ]);
            }
            $registration->comment = $request->input('comment');
            $registration->availability = $request->input('availability');
            $registration->save();
        }

        return new DoodleEventResource($event);
    }

    /**
     * Update the specified resource in storage
     * using the doodle_token to idenfity the User.
     *
     * @param  \App\Http\Requests\EventDoodle\EventDoodleRequest $request
     * @param  \App\Models\Event $event
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function updateToken(EventDoodleRequest $request, Event $event, $token)
    {
        $this->setAuthUser($token);
        return $this->update($request, $event);
    }

    /**
     * Set the User with doodle_token == $token as the authenticated User.
     *
     * @param mixed $token
     */
    private function setAuthUser($token)
    {
        Auth::setUser(User::where('doodle_token', $token)->firstOrFail());
    }
}
