<?php

namespace App\Http\Controllers;

use App\Http\Requests\RoleRequest;
use App\Http\Resources\Role\RoleResource;
use App\Http\Resources\Role\SimplifiedRoleResource;
use App\Models\Role;
use Illuminate\Http\Resources\Json\JsonResource;
use Sentinel;

class RoleController extends Controller
{
    /**
     * Initialize permissions middleware.
     */
    public function __construct()
    {
        $this->middleware('permission.api:roles.general')->only(['index', 'show', 'store', 'update', 'destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function index()
    {
        return SimplifiedRoleResource::collection(Sentinel::getRoleRepository()->get());
    }

    /**
     * Display the specified resource.
     *
     * @param  Role $role
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function show(Role $role)
    {
        return new RoleResource($role);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param App\Http\Requests\RoleRequest $request
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function store(RoleRequest $request)
    {
        $role = new Role();
        return $this->save($request, $role);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param App\Http\Requests\RoleRequest $request
     * @param Role $role
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function update(RoleRequest $request, Role $role)
    {
        return $this->save($request, $role);
    }

    /**
     * Save the specific resource with the values from the request.
     *
     * @param App\Http\Requests\RoleRequest $request
     * @param Role $role
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    private function save(RoleRequest $request, Role $role): JsonResource
    {
        $role->name = $request->input('name');
        $role->permissions = [];
        $permissions = $request->input('permissions');
        if ($permissions) {
            foreach ($permissions as $permission) {
                $role->addPermission($permission);
            }
        }

        if (!$role->save()) {
            abort(500, 'Fehler beim Speichern.');
        }

        $users = $request->input('users');
        if ($users) {
            $role->users()->sync($users);
        } else {
            $role->users()->detach();
        }

        return new RoleResource($role);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Role $role
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function destroy(Role $role)
    {
        $role->delete();
        return new RoleResource($role);
    }
}
