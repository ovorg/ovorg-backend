<?php

namespace App\Http\Controllers;

use App\Http\Requests\Division\DivisionRequest;
use App\Http\Resources\Division\DivisionResource;
use App\Http\Resources\Division\SimplifiedDivisionResource;
use App\Models\Division;
use App\Models\User;

class DivisionController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission.api:divisions.general')->only(['store', 'update', 'destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function index()
    {
        return SimplifiedDivisionResource::collection(Division::get());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Division $division
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function show(Division $division)
    {
        return new DivisionResource($division);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Division\DivisionRequest $request
     * @param  \App\Division  $division
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function update(DivisionRequest $request, Division $division)
    {
        return $this->save($request, $division);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Division\DivisionRequest $request
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function store(DivisionRequest $request)
    {
        $division = new Division();
        return $this->save($request, $division);
    }

    /**
     * Save the specific resource with the values from the request.
     *
     * @param App\Http\Requests\Division\DivisionRequest $request
     * @param App\Models\Division $division
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    private function save(DivisionRequest $request, Division $division)
    {
        $division->name = $request->input('name');
        $division->short_name = $request->input('short_name');

        if (!$division->save()) {
            abort(500, 'Fehler beim Speichern.');
        }

        $success = true;
        // Remove users from division
        foreach ($division->users as $user) {
            $user->division()->dissociate();
            if (!$user->save()) {
                $success = false;
            }
        }

        // Add users to division
        $userIds = $request->input("users");
        if ($userIds) {
            foreach ($userIds as $userId) {
                $user = User::find($userId);
                $user->division()->associate($division);
                if (!$user->save()) {
                    $success = false;
                }
            }
        }
        if (!$success) {
            abort(500, 'Fehler beim Hinzufügen oder Entfernen von Helfern.');
        }
        $division->load('users');

        return new DivisionResource($division);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Division $division
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function destroy(Division $division)
    {
        $division->delete();
        return new DivisionResource($division);
    }
}
