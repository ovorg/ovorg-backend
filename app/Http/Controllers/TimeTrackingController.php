<?php

namespace App\Http\Controllers;

use App\Http\Requests\TimeTracking\ScannedRequest;
use App\Http\Resources\User\TimeTrackingUserResource;
use App\Models\User;
use Carbon;

class TimeTrackingController extends Controller
{
    /**
     * Display a listing of the resource for time_tracking devices.
     *
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function index()
    {
        return TimeTrackingUserResource::collection(User::get());
    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function show(User $user)
    {
        return new TimeTrackingUserResource($user);
    }

    /**
     * User has been scanned. Update if timeout did not exceed.
     *
     * @param  \App\Http\Requests\TimeTracking\ScannedRequest $request
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function scanned(ScannedRequest $request)
    {
        $user = User::where('thwin_id', $request->input('thwin_id'))->firstOrFail();
        if (
            $user->scan_timeout
            && $user->scan_timeout->greaterThanOrEqualTo(Carbon::now())
        ) {
            // Scan timeout did not exceed. Don't update $user.
            return new TimeTrackingUserResource($user);
        }

        // scan_timeout exceeded. Update $user.
        $user->present = !$user->present;
        $user->scan_timeout = Carbon::now()->addSeconds(config('time_tracking.timeout'));

        if (!$user->save()) {
            abort(500, 'Fehler beim Speichern.');
        }

        return new TimeTrackingUserResource($user);
    }
}
