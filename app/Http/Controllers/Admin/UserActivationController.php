<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\User\UserActivationResource;
use App\Models\User;
use Illuminate\Http\Request;

class UserActivationController extends Controller {
    /**
     * Initialize permissions middleware.
     */
    public function __construct() {
        $this->middleware('permission.api:admin.general')->only(['index', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $activated = User::whereHas('activations', function ($query) {
            $query->where('completed', true);
        })->get();
        foreach ($activated as $a) {
            $a->activated = true;
        }

        $notActivated = User::whereDoesntHave('activations', function ($query) {
            $query->where('completed', true);
        })->get();
        foreach ($notActivated as $a) {
            $a->activated = false;
        }

        $users = $activated->merge($notActivated);

        return UserActivationResource::collection($users);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user) {
        $activations = $user->activations->where('completed', true)->count();
        $activated = $activations > 0;
        $user->activated = $activated;

        return new UserActivationResource($user);
    }
}
