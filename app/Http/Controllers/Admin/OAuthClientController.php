<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\OAuthClient\OAuthClientRequest;
use App\Http\Resources\OAuthClient\OAuthClientResource;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Laravel\Passport\Client;
use Laravel\Passport\ClientRepository;
use Laravel\Passport\Passport;
use Str;

class OAuthClientController extends Controller {
    /**
     * Initialize permissions middleware.
     */
    public function __construct() {
        $this->middleware('permission.api:admin.general')->only(['index', 'show', 'destroy', 'store', 'update']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $clients = Client::where('password_client', false)
            ->where('personal_access_client', false)->where('user_id', null)
            ->where('revoked', false)->get();
        return OAuthClientResource::collection($clients);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id) {
        $client = Client::where('id', $id)
            ->where('personal_access_client', false)
            ->where('password_client', false)->first();
        if (!$client) {
            $ex = new ModelNotFoundException();
            $ex->setModel(Client::class, $id);
            throw $ex;}
        return new OAuthClientResource($client);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\OAuthClient\OAuthClientRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OAuthClientRequest $request) {
        $client = new Client();
        $client->secret = hash_hmac('sha256', Str::random(40), config('app.key'));
        $client->redirect = '';
        $client->personal_access_client = false;
        $client->password_client = false;
        $client->revoked = false;
        return $this->save($request, $client);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\OAuthClient\OAuthClientRequest  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(OAuthClientRequest $request, int $id) {
        $client = Client::where('id', $id)
            ->where('personal_access_client', false)
            ->where('password_client', false)
            ->where('revoked', false)->first();
        if (!$client) {
            $ex = new ModelNotFoundException();
            $ex->setModel(Client::class, $id);
            throw $ex;
        }
        return $this->save($request, $client);
    }
    /**
     * Save the specific resource with the values from the request.
     *
     * @param  \App\Http\Requests\OAuthClient\OAuthClientRequest  $request
     * @param  \Laravel\Passport\Client $client
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function save(OAuthClientRequest $request, Client $client) {
        $client->name = $request->input('name');
        if (!$client->save()) {
            abort(500, "Fehler beim Speichern.");
        }

        return new OAuthClientResource($client);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id) {
        $client = Client::where('id', $id)
            ->where('personal_access_client', false)
            ->where('password_client', false)->first();
        if (!$client) {
            $ex = new ModelNotFoundException();
            $ex->setModel(Client::class, $id);
            throw $ex;}

        if ($client->personal_access_client || $client->password_client) {
            abort(401);
        }

        $client->revoked = true;
        $client->save();
        return new OAuthClientResource($client);
    }
}
