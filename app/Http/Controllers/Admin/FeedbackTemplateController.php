<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\FeedbackTemplate\FeedbackTemplateRequest;
use App\Http\Resources\FeedbackTemplate\FeedbackTemplateResource;
use App\Models\FeedbackTemplate;
use Illuminate\Http\Request;

class FeedbackTemplateController extends Controller {
    /**
     * Initialize permissions middleware.
     */
    public function __construct() {
        $this->middleware('permission.api:admin.general')->only(['destroy', 'store', 'update']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function index() {
        $feedbackTemplates = FeedbackTemplate::all();
        return FeedbackTemplateResource::collection($feedbackTemplates);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FeedbackTemplate $feedbackTemplate
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function show(FeedbackTemplate $feedbackTemplate) {
        return new FeedbackTemplateResource($feedbackTemplate);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\FeedbackTemplate\FeedbackTemplateRequest $request
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function store(FeedbackTemplateRequest $request) {
        $feedbackTemplate = new FeedbackTemplate();
        return $this->save($request, $feedbackTemplate);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\FeedbackTemplate\FeedbackTemplateRequest $request
     * @param  \App\Models\FeedbackTemplate $feedbackTemplate
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function update(FeedbackTemplateRequest $request, FeedbackTemplate $feedbackTemplate) {
        return $this->save($request, $feedbackTemplate);
    }

    /**
     * Save the specific resource with the values from the request.
     *
     * @param  \App\Http\Requests\FeedbackTemplate\FeedbackTemplateRequest $request
     * @param  \App\Models\FeedbackTemplate $feedbackTemplate
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function save(FeedbackTemplateRequest $request, FeedbackTemplate $feedbackTemplate) {
        $feedbackTemplate->name = $request->input('name');
        $feedbackTemplate->template = $request->input('template');

        if (!$feedbackTemplate->save()) {
            abort(500, 'Fehler beim Speichern.');
        }

        return new FeedbackTemplateResource($feedbackTemplate);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FeedbackTemplate $feedbackTemplate
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function destroy(FeedbackTemplate $feedbackTemplate) {
        $feedbackTemplate->delete();
        return new FeedbackTemplateResource($feedbackTemplate);
    }
}
