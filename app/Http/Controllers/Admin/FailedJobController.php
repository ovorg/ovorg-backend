<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\FailedJobs\FailedJobResource;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class FailedJobController extends Controller {
    /**
     * Initialize permissions middleware.
     */
    public function __construct() {
        $this->middleware('permission.api:admin.general')->only(['index', 'show', 'destroy', 'flush']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $failedJobs = app()['queue.failer']->all();
        return FailedJobResource::collection($failedJobs);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $failedJob = app()['queue.failer']->find($id);
        if (!$failedJob) {
            abort(404, 'Fehlgeschlagener Job nicht gefunden.');
        }
        return new FailedJobResource($failedJob);
    }

    /**
     * Remove all failed jobs from storage.
     */
    public function flush() {
        $failedJobs = app()['queue.failer']->all();
        app()['queue.failer']->flush();
        return FailedJobResource::collection($failedJobs);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $failedJob = app()['queue.failer']->find($id);
        if (!$failedJob) {
            abort(404, 'Fehlgeschlagener Job nicht gefunden.');
        }
        app()['queue.failer']->forget($id);
        return new FailedJobResource($failedJob);
    }
}
