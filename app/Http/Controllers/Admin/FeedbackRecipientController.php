<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\FeedbackRecipient\FeedbackRecipientRequest;
use App\Http\Resources\FeedbackRecipient\FeedbackRecipientResource;
use App\Models\FeedbackRecipient;
use Illuminate\Http\Resources\Json\JsonResource;

class FeedbackRecipientController extends Controller {
    /**
     * Initialize permissions middleware.
     */
    public function __construct() {
        $this->middleware('permission.api:admin.general')->only(['index', 'show', 'destroy', 'store', 'update']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function index() {
        $feedbackRecipients = FeedbackRecipient::all();
        return FeedbackRecipientResource::collection($feedbackRecipients);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FeedbackRecipient  $feedbackRecipient
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function show(FeedbackRecipient $feedbackRecipient) {
        return new FeedbackRecipientResource($feedbackRecipient);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function store(FeedbackRecipientRequest $request) {
        $feedbackRecipient = new FeedbackRecipient();
        return $this->save($request, $feedbackRecipient);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FeedbackRecipient  $feedbackRecipient
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function update(FeedbackRecipientRequest $request, FeedbackRecipient $feedbackRecipient) {
        return $this->save($request, $feedbackRecipient);
    }

    /**
     * Save the specific resource with the values from the request.
     *
     * @param  \App\Http\Requests\FeedbackRecipient\FeedbackRecipientRequest $request
     * @param  \App\Models\FeedbackRecipient $feedbackRecipient
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function save(FeedbackRecipientRequest $request, FeedbackRecipient $feedbackRecipient): JsonResource {
        $feedbackRecipient->email = $request->input('email');
        if (!$feedbackRecipient->save()) {
            abort(500, 'Fehler beim Speichern.');
        }

        return new FeedbackRecipientResource($feedbackRecipient);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FeedbackRecipient  $feedbackRecipient
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function destroy(FeedbackRecipient $feedbackRecipient) {
        $feedbackRecipient->delete();
        return new FeedbackRecipientResource($feedbackRecipient);
    }
}
