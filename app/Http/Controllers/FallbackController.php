<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FallbackController extends Controller
{
    function fallback()
    {
        abort(404, 'Diese Seite existiert nicht.');
    }
}
