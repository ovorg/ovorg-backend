<?php

namespace App\Http\Controllers;

use App\Events\UsersRemovedFromUserGroup;
use App\Http\Requests\UserGroup\UserGroupRequest;
use App\Http\Resources\UserGroup\SimplifiedUserGroupResource;
use App\Http\Resources\UserGroup\UserGroupResource;
use App\Models\User;
use App\Models\UserGroup;

class UserGroupController extends Controller
{
    public function __construct()
    {
        // meeting.general
        $this->middleware('permission.api:user_groups.general')->only(['store', 'update', 'destroy', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function index()
    {
        return SimplifiedUserGroupResource::collection(UserGroup::all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserGroup $userGroup
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function show(UserGroup $userGroup)
    {
        return new UserGroupResource($userGroup);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param App\Http\Requests\UserGroup\UserGroupRequest $request
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function store(UserGroupRequest $request)
    {
        return $this->save($request, new UserGroup());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param App\Http\Requests\UserGroup\UserGroupRequest $request
     * @param  \App\UserGroup $userGroup
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function update(UserGroupRequest $request, UserGroup $userGroup)
    {
        return $this->save($request, $userGroup);
    }

    /**
     * Save the specific resource with the values from the request.
     *
     * @param App\Http\Requests\UserGroup\UserGroupRequest $request
     * @param App\Models\UserGroup $userGroup
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    private function save(UserGroupRequest $request, UserGroup $userGroup)
    {
        $userGroup->name = $request->input('name');

        if (!$userGroup->save()) {
            abort(500, 'Fehler beim Speichern.');
        }

        $users = $request->input('users');
        if ($users) {
            $changes = $userGroup->users()->sync($users);
            $removedUsers = collect($changes['detached']);
            $removedUsers = $removedUsers->map(function (int $id) {
                return User::find($id);
            });
        } else {
            $removedUsers = $userGroup->users;
            $userGroup->users()->detach();
        }

        event(new UsersRemovedFromUserGroup($removedUsers));

        return new UserGroupResource($userGroup);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserGroup $userGroup
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function destroy(UserGroup $userGroup)
    {
        $userGroup->delete();
        return new UserGroupResource($userGroup);
    }
}
