<?php

namespace App\Http\Controllers;

use App\Events\UsersRemovedFromMeeting;
use App\Http\Requests\Meeting\MeetingRequest;
use App\Http\Resources\Meeting\FullMeetingResource;
use App\Http\Resources\Meeting\MeetingResource;
use App\Http\Resources\Meeting\SimplifiedMeetingResource;
use App\Models\Meeting;
use App\Models\User;
use App\Models\UserGroup;
use Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class MeetingController extends Controller
{
    /**
     * Initialize permissions middleware.
     */
    public function __construct()
    {
        $this->middleware('permission.api:meetings.general')->only(['index', 'archive', 'show', 'store', 'update', 'destroy']);
        $this->middleware('permission.api.or:meetings.advanced,meetings.general')->only(['showEnhanced']);
    }

    /**
     * Display a listing of the resource for all future meetings.
     *
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function index()
    {
        $meetings = Meeting::where('end', '>=', Carbon::now())->orderBy('start', 'asc')->orderBy('title', 'asc')->get();
        return SimplifiedMeetingResource::collection($meetings);
    }

    /**
     * Display a listing of the resource for all past meetings.
     *
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function archive()
    {
        $meetings = Meeting::where('end', '<', Carbon::now())->orderBy('start', 'desc')->orderBy('title', 'asc')->get();
        return SimplifiedMeetingResource::collection($meetings);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Meeting $meeting
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function show(Meeting $meeting)
    {
        return new MeetingResource($meeting);
    }

    /**
     * Display an enhanced version of the specified resource.
     *
     * @param  \App\Models\Meeting $meeting
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function showEnhanced(Meeting $meeting)
    {
        return new FullMeetingResource($meeting);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\MeetingRequest $request
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function store(MeetingRequest $request)
    {
        $meeting = new Meeting();

        return $this->save($request, $meeting);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\MeetingRequest $request
     * @param  \App\Models\Meeting $meeting
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function update(MeetingRequest $request, Meeting $meeting)
    {
        return $this->save($request, $meeting);
    }

    /**
     * Save the specific resource with the values from the request.
     *
     * @param  \App\Http\Requests\MeetingRequest $request
     * @param  \App\Models\Meeting $meeting
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    private function save(MeetingRequest $request, Meeting $meeting): JsonResource
    {
        $meeting->title = $request->input('title');
        $meeting->start = Carbon::parse($request->input('start'));
        $meeting->end = Carbon::parse($request->input('end'));
        $meeting->description = $request->input('description');
        $meeting->comment = $request->input('comment') ? true : false;
        if ($request->input('register_until')) {
            $meeting->register_until = Carbon::parse($request->input('register_until'));
            if ($meeting->register_until->isSameDay($meeting->start)) {
                $meeting->register_until = null;
            }
        }

        // Responsible
        if ($request->input('responsible')) {
            $meeting->responsible()->associate(User::find($request->responsible));
        } else {
            $meeting->responsible()->dissociate();
        }

        if (!$meeting->save()) {
            abort(500, 'Fehler beim Speichern.');
        }

        // User groups
        if ($request->input('user_groups')) {
            $userGroups = collect();
            foreach ($request->input('user_groups') as $userGroupId) {
                $userGroups->push($userGroupId);
            }
            $changes = $meeting->userGroups()->sync($userGroups);
            $removedUserGroups = collect($changes['detached']);
            $removedUserGroups = $removedUserGroups->map(function (int $userGroupID) {
                return UserGroup::find($userGroupID);
            });
        } else {
            $removedUserGroups = $meeting->userGroups;
            $meeting->userGroups()->detach();
        }

        // Users
        if ($request->input('users')) {
            $users = collect();
            foreach ($request->input('users') as $usersId) {
                $users->push($usersId);
            }
            $changes = $meeting->users()->sync($users);
            $removedUsers = collect($changes['detached']);
            $removedUsers = $removedUsers->map(function ($userID) {
                return User::find($userID);
            });
        } else {
            $removedUsers = $meeting->users;
            $meeting->users()->detach();
        }

        // Delete removed Users registrations
        foreach ($removedUserGroups as $removedUserGroup) {
            $removedUsers = $removedUsers->concat($removedUserGroup->users);
        }
        event(new UsersRemovedFromMeeting($removedUsers));

        return new MeetingResource($meeting);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Meeting $meeting
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function destroy(Meeting $meeting)
    {
        $meeting->delete();
        return new MeetingResource($meeting);
    }
}
