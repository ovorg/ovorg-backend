<?php

namespace App\Http\Controllers;

use App\Events\UsersRemovedFromUserGroup;
use App\Http\Requests\User\SelfUpdateRequest;
use App\Http\Requests\User\UserRequest;
use App\Http\Resources\User\FullUserResource;
use App\Http\Resources\User\SimplifiedUserDivisionResource;
use App\Http\Resources\User\SimplifiedUserResource;
use App\Http\Resources\User\UserResource;
use App\Models\Division;
use App\Models\Driver;
use App\Models\Position;
use App\Models\Privacy;
use App\Models\SCBA;
use App\Models\User;
use Auth;
use Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class UserController extends Controller
{
    /**
     * Initialize permissions middleware.
     */
    public function __construct()
    {
        $this->middleware('permission.api:users.general')->only(['index', 'show', 'store', 'update', 'destroy']);
    }

    /**
     * Display the authenticated User.
     *
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function me()
    {
        return new FullUserResource(Auth::user());
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function index()
    {
        return SimplifiedUserDivisionResource::collection(User::all());
    }

    /**
     * Display a simplified listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function simplifiedIndex()
    {
        return SimplifiedUserResource::collection(User::all());
    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function show(User $user)
    {
        return new UserResource($user);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param App\Http\Requests\User\UserRequest $request
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function store(UserRequest $request)
    {
        return $this->save($request, new User());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param App\Http\Requests\User\UserRequest $request
     * @param  User $user
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function update(UserRequest $request, User $user)
    {
        return $this->save($request, $user);
    }

    /**
     * Save the specific resource with the values from the request.
     *
     * @param App\Http\Requests\User\UserRequest $request
     * @param User $user
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    private function save(UserRequest $request, User $user): JsonResource
    {
        // User data
        $user->thwin_id = $request->input('thwin_id');
        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        $user->email = $request->input('email');
        $user->gender = $request->input('gender');
        if ($request->input('birth')) {
            $user->birth = Carbon::parse($request->input('birth'));
        }
        $user->mobile = $request->input('mobile');
        $user->phone = $request->input('phone');

        // Position
        $user->position()->associate(Position::find($request->input("position")));

        // Division
        if ($request->input("division")) {
            $user->division()->associate(Division::find($request->input("division")));
        } else {
            $user->division()->dissociate();
        }

        if (!$user->save()) {
            abort(500, 'Fehler beim Speichern.');
        }

        // User Groups
        if ($request->input('user_groups')) {
            $userGroups = collect();
            foreach ($request->input('user_groups') as $userGroupId) {
                $userGroups->push($userGroupId);
            }
            $changes = $user->userGroups()->sync($userGroups);
            if (count($changes["detached"]) > 0) {
                $userGroupRemoved = true;
            } else {
                $userGroupRemoved = false;
            }
        } else {
            $userGroupRemoved = !$user->userGroups->isEmpty();
            $user->userGroups()->detach();
        }

        // SCBA
        $scba = $user->scba;
        $scbaInput = $request->input('scba');
        if ($scbaInput && $this->containsValues($scbaInput)) {
            if (!$scba) {
                $scba = new SCBA(['user_id' => $user->id]);
            }

            $scba->next_examination = array_key_exists('next_examination', $scbaInput) ? $scbaInput['next_examination'] ? Carbon::parse($scbaInput['next_examination']) : null : null;
            $scba->last_instruction = array_key_exists('last_instruction', $scbaInput) ? $scbaInput['last_instruction'] ? Carbon::parse($scbaInput['last_instruction']) : null : null;
            $scba->last_excercise = array_key_exists('last_excercise', $scbaInput) ? $scbaInput['last_excercise'] ? Carbon::parse($scbaInput['last_excercise']) : null : null;
            $scba->last_deployment = array_key_exists('last_deployment', $scbaInput) ? $scbaInput['last_deployment'] ? Carbon::parse($scbaInput['last_deployment']) : null : null;
            $scba->cbrn = array_key_exists('cbrn', $scbaInput) ? ($scbaInput['cbrn'] ? true : false) : false;
            $scba->last_cbrn_deployment = array_key_exists('last_cbrn_deployment', $scbaInput) ? $scbaInput['last_cbrn_deployment'] ? Carbon::parse($scbaInput['last_cbrn_deployment']) : null : null;

            if (!$scba->save()) {
                abort(500, 'Fehler beim Speichern.');
            }
        } elseif ($scba) {
            $scba->delete();
        }

        // Driver
        $driver = $user->driver;
        $driverInput = $request->input('driver');
        if ($driverInput && $this->containsValues($driverInput)) {
            if (!$driver) {
                $driver = new Driver(['user_id' => $user->id]);
            }

            $driver->b = array_key_exists('b', $driverInput) ? ($driverInput['b'] ? true : false) : false;
            $driver->be = array_key_exists('be', $driverInput) ? ($driverInput['be'] ? true : false) : false;
            $driver->c = array_key_exists('c', $driverInput) ? ($driverInput['c'] ? true : false) : false;
            $driver->ce = array_key_exists('ce', $driverInput) ? ($driverInput['ce'] ? true : false) : false;
            $driver->expiry_date = array_key_exists('expiry_date', $driverInput) ? $driverInput['expiry_date'] ? Carbon::parse($driverInput['expiry_date']) : null : null;

            if (!$driver->save()) {
                abort(500, 'Fehler beim Speichern.');
            }
        } elseif ($driver) {
            $driver->delete();
        }

        if ($userGroupRemoved) {
            event(new UsersRemovedFromUserGroup(collect()->push($user)));
        }

        $user->load('scba');
        $user->load('driver');

        return new UserResource($user);
    }

    /**
     * Update the authenticated User in storage.
     *
     * @param App\Http\Requests\SelfUpdateRequest $request
     * @param  User $user
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function selfUpdate(SelfUpdateRequest $request)
    {
        $user = Auth::user();
        if (!$user) {
            abort(500);
        }

        $user->username = $request->input('username');
        $user->mobile = $request->input('mobile');
        $user->phone = $request->input('phone');
        $user->email = $request->input('email');
        $user->vegetarian = $request->input('vegetarian') ? true : false;
        $user->health_information = $request->input('health_information');

        // Privacy
        $privacy = $user->privacy;
        $privacyInput = $request->input('privacy');
        if ($privacyInput && $this->containsValues($privacyInput)) {
            if (!$privacy) {
                $privacy = new Privacy(['user_id' => $user->id]);
            }

            $privacy->email = array_key_exists('email', $privacyInput) ? $privacyInput['email'] ? true : false : false;
            $privacy->mobile = array_key_exists('mobile', $privacyInput) ? $privacyInput['mobile'] ? true : false : false;
            $privacy->phone = array_key_exists('phone', $privacyInput) ? $privacyInput['phone'] ? true : false : false;

            if (!$privacy->save()) {
                abort(500, 'Fehler beim Speichern.');
            }
        } elseif ($privacy) {
            $privacy->delete();
        }

        if (!$user->save()) {
            abort(500, 'Fehler beim Speichern.');
        }

        $user->load('privacy');
        return new FullUserResource($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function destroy(User $user)
    {
        $user->delete();
        return new UserResource($user);
    }
}
