<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Determine if the array contains at least one value != False || != null
     *
     * @param iterable $array
     * @return bool
     */
    protected function containsValues(iterable $array)
    {
        foreach ($array as $item) {
            if ($item !== Null && $item !== False) {
                return true;
            }
        }
        return false;
    }
}
