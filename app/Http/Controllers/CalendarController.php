<?php

namespace App\Http\Controllers;

use App\Lib\ICal;
use App\Models\User;
use Illuminate\Http\Request;

class CalendarController extends Controller
{

    /**
     * Show a calendar for the specified Meetings.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  string $calendar_token
     * @return \Illuminate\Http\Response|\Illuminate\Contracts\Routing\ResponseFactory
     */
    public function calendar(Request $request, $calendar_token)
    {
        $user = User::where('calendar_token', $calendar_token)->firstOrFail();

        $yes = $request->has('yes');
        $no = $request->has('no');
        $maybe = $request->has('maybe');
        $not_registered = $request->has('not_registered');

        $calendar = ICal::createCalendarFor($user, $yes, $no, $maybe, $not_registered);

        return response($calendar->render())
            ->header('Content-Type', 'text/calendar; charset=utf-8')
            ->header('Content-Disposition', 'attachment; filename="dienste.ics"');
    }
}
