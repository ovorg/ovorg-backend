<?php

namespace App\Http\Controllers;

use App\Http\Requests\File\FileStoreRequest;
use App\Http\Requests\File\FileUpdateRequest;
use App\Http\Resources\File\FileResource;
use App\Models\File;
use Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class FileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function index()
    {
        return FileResource::collection(File::get());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\File  $file
     * @return \Illuminate\Http\Response
     */
    public function show(File $file)
    {
        return Storage::download($file->path, $file->name);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function store(FileStoreRequest $request)
    {
        $rawFile = $request->file('file');

        // Check used file storage
        if (!File::isEnoughSpace($rawFile->getSize())) {
            abort(507, "Auf dem Server ist nicht genügend freier Speicher für diese Datei vorhanden.");
        }

        // Save file
        $path = Storage::put(File::$uploadFolder, $rawFile);

        // Create File object
        $file = new File();
        $file->path = $path;
        $name = $request->input('name');
        if (!$name) {
            $rawFileName = $rawFile->getClientOriginalName();
            $name = Str::limit($rawFileName, 191, '');
        }
        $file->name = $name;
        $file->size = $rawFile->getSize();
        $file->uploader()->associate(Auth::user());

        if (!$file->save()) {
            abort(500, 'Fehler beim Speichern!');
        }

        return new FileResource($file);
    }

    /**
     * Update the specified resource in storage.
     * Only the name can be updated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\File  $file
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function update(FileUpdateRequest $request, File $file)
    {
        $file->name = $request->input('name');

        if (!$file->save()) {
            abort(500, 'Fehler beim Speichern!');
        }

        return new FileResource($file);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\File  $file
     * @return \Illuminate\Http\Response
     */
    public function destroy(File $file)
    {
        $file->delete();
        return new FileResource($file);
    }
}
