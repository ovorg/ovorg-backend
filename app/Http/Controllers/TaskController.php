<?php

namespace App\Http\Controllers;

use App\Events\TaskFilesUpdated;
use App\Events\TaskUsersUpdated;
use App\Http\Requests\Task\LightUpdateRequest;
use App\Http\Requests\Task\TaskRequest;
use App\Http\Resources\Task\SimplifiedTaskResource;
use App\Http\Resources\Task\TaskResource;
use App\Models\File;
use App\Models\Task;
use App\Models\TaskCategory;
use App\Models\User;
use Auth;
use Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class TaskController extends Controller
{
    /**
     * Initialize permissions middleware.
     */
    public function __construct()
    {
        $this->middleware('permission.api:tasks.general')->only(['store', 'update', 'destroy']);
    }

    /**
     * Display a listing of all pending tasks.
     *
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function index()
    {
        return SimplifiedTaskResource::collection(Task::where('state', '<', '100')->get());
    }

    /**
     * Display a listing of all finished tasks.
     *
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function archive()
    {
        return SimplifiedTaskResource::collection(Task::where('state', '>=', '100')->get());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Task $task
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function show(Task $task)
    {
        return new TaskResource($task);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function store(TaskRequest $request)
    {
        $task = new Task();
        $task->creator()->associate(Auth::user());
        return $this->save($request, $task);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Task $task
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function update(TaskRequest $request, Task $task)
    {
        return $this->save($request, $task);
    }

    /**
     * Save the specific resource with the values from the request.
     *
     * @param  \App\Http\Requests\Task\TaskRequest $request
     * @param  \App\Models\Task $task
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    private function save(TaskRequest $request, Task $task): JsonResource
    {
        // Task data
        $task->title = $request->input('title');
        $task->description = $request->input('description');
        $task->addition = $request->input('addition');
        $task->duedate = $request->input('duedate') ? Carbon::parse($request->input('duedate')) : null;
        $task->state = $request->input('state');
        $task->priority = $request->input('priority');
        $task->inform_executives = $request->input('inform_executives') ?: false;

        // TaskCategory
        $task->taskCategory()->associate(TaskCategory::findOrFail($request->input('task_category')));

        // Responsible
        $task->responsible()->associate(User::findOrFail($request->input('responsible')));

        // Save task if it was created during this request.
        // Otherwise adding users will fail, because the task has no id.
        if (!$task->exists) {
            if (!$task->save()) {
                abort(500, 'Fehler beim Speichern.');
            }
        }

        // Users
        $users = $request->input('users');
        if ($users) {
            $changes = $task->users()->sync($users);
        } else {
            $removedUsers = $task->users;
            $task->users()->detach();
            $changes = [
                'attached' => [],
                'detached' => $removedUsers->map(function (User $user) {
                    return $user->id;
                })->toArray(),
                'updated'  => [],
            ];
        }

        // Save task if it was not created during this request.
        // If it would be saved before adding users,
        // removed users would be notified about attribute changes.
        if ($task->exists) {
            if (!$task->save()) {
                abort(500, 'Fehler beim Speichern.');
            }
        }

        // Files
        $this->syncFiles($task, $request->input('files'));

        // Dispatch TaskUserUpdated event
        event(new TaskUsersUpdated($task, $changes, auth()->user()));

        return new TaskResource($task);
    }

    /**
     * Update selected attributes of the specified resource in storage.
     * This can be called by every User.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Task $task
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function lightUpdate(LightUpdateRequest $request, Task $task)
    {
        // Task data
        $task->description = $request->input('description');
        $task->addition = $request->input('addition');
        $task->state = $request->input('state');

        // Participating
        $user = Auth::user();
        if ($request->input('participating')) {
            if (!$task->users->contains($user->id)) {
                $task->users()->attach($user);
                $changes = [
                    'attached' => [$user->id],
                    'detached' => [],
                    'updated'  => [],
                ];
            } else {
                $changes = null;
            }
        } else {
            $task->users()->detach($user);
            $changes = [
                'attached' => [],
                'detached' => [$user->id],
                'updated'  => [],
            ];
        }

        // Save Task
        if (!$task->save()) {
            abort(500, 'Fehler beim Speichern.');
        }

        // Dispatch TaskUserUpdated event
        event(new TaskUsersUpdated($task, $changes, auth()->user()));

        // Files
        $this->syncFiles($task, $request->input('files'));

        return new TaskResource($task);
    }

    /**
     * Sync files for the task.
     *
     * @param \App\Models\Task $task
     * @param ?array $files
     */
    private function syncFiles(Task $task, ?array $files): void
    {
        if (!$files) {
            $files = collect();
        }

        $attached = collect();
        $detached = collect();

        // Delete removed files. The FileObserver deletes the files from the disk.
        $deleteFiles = File::where('task_id', $task->id)->whereNotIn('id', $files)->get();
        foreach ($deleteFiles as $file) {
            $file->delete();
            $detached->push($file);
        }

        // Add Files
        $files = File::whereIn('id', $files)->get();
        $files->each(function ($file) use ($task, $attached) {
            if ($file->task === null || $file->task->id !== $task->id) {
                $file->moveToTask($task);
                $attached->push($file);
            }
        });

        $changes = [
            "attached" => $attached->map(function (File $file) {
                return $file->id;
            })->toArray(),
            "detached" => $detached->map(function (File $file) {
                return $file->id;
            })->toArray(),
            "updated"  => [],
        ];

        event(new TaskFilesUpdated($task, $changes));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Task $task
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function destroy(Task $task)
    {
        $task->delete();
        return new TaskResource($task);
    }
}
