<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\UserPresenceRequest;
use App\Http\Resources\User\PresenceUserResource;
use App\Models\User;

class UserPresenceController extends Controller
{
    /**
     * Initialize permissions middleware.
     */
    public function __construct()
    {
        // meeting.general
        $this->middleware('permission.api:users.presence')->only(['index', 'update', 'destroy', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return PresenceUserResource::collection(User::all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return new PresenceUserResource($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserPresenceRequest $request, User $user)
    {
        $user->present = $request->input('present') ?: false;
        $user->scan_timeout = NULL;

        if (!$user->save()) {
            abort(500, "Fehler beim Speichern.");
        }
        return new PresenceUserResource($user);
    }
}
