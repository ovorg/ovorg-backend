<?php

namespace App\Http\Controllers;

use App\Events\UsersRemovedFromEvent;
use App\Http\Requests\Event\EventRequest;
use App\Http\Resources\Event\EventResource;
use App\Http\Resources\Event\FullEventResource;
use App\Http\Resources\Event\SimplifiedEventResource;
use App\Models\Event;
use App\Models\EventDateOption;
use App\Models\User;
use App\Models\UserGroup;
use Carbon\Carbon;

class EventController extends Controller
{
    /**
     * Initialize permissions middleware.
     */
    public function __construct()
    {
        $this->middleware('permission.api:events.general')->only(['index', 'archive', 'show', 'store', 'update', 'destroy']);
        $this->middleware('permission.api.or:events.advanced,events.general')->only(['showEnhanced']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function index()
    {
        return SimplifiedEventResource::collection(Event::all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function show(Event $event)
    {
        return new EventResource($event);
    }

    /**
     * Display an enhanced version of the specified resource.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function showEnhanced(Event $event)
    {
        return new FullEventResource($event);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function store(EventRequest $request)
    {
        $event = new Event();
        return $this->save($request, $event);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function update(EventRequest $request, Event $event)
    {
        return $this->save($request, $event);
    }

    /**
     * Save the specific resource with the values from the request.
     *
     * @param App\Http\Requests\Division\DivisionRequest $request
     * @param App\Models\Division $division
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function save(EventRequest $request, Event $event)
    {
        $event->title = $request->input('title');
        $event->description = $request->input('description');
        $event->force_comment = $request->input('force_comment') ?: false;

        // Responsible
        if ($request->input('responsible')) {
            $event->responsible()->associate(User::findOrFail($request->responsible));
        } else {
            $event->responsible()->dissociate();
        }

        if (!$event->save()) {
            abort(500, 'Fehler beim Speichern.');
        }

        // DateOptions
        $dateOptions = $request->input('date_options');
        if ($dateOptions) {
            // Parse dates with Carbon
            $dateOptions = array_map(function ($date) {
                return Carbon::parse($date)->startOfDay();
            }, $dateOptions);

            // Remove deleted DateOptions
            $event->dateOptions()->whereNotIn('date', $dateOptions)->delete();

            // Add new DateOptions
            foreach ($dateOptions as $dateOption) {
                if ($event->dateOptions()->where('date', $dateOption)->first() == null) {
                    $date = new EventDateOption(['event_id' => $event->id]);
                    $date->date = $dateOption;
                    $date->save();
                }
            }
        } else {
            $event->dateOptions()->delete();
        }

        // User groups
        if ($request->input('user_groups')) {
            $userGroups = collect();
            foreach ($request->input('user_groups') as $userGroupId) {
                $userGroups->push($userGroupId);
            }
            $changes = $event->userGroups()->sync($userGroups);
            $removedUserGroups = collect($changes['detached']);
            $removedUserGroups = $removedUserGroups->map(function (int $userGroupID) {
                return UserGroup::find($userGroupID);
            });
        } else {
            $removedUserGroups = $event->userGroups;
            $event->userGroups()->detach();
        }

        // Users
        if ($request->input('users')) {
            $users = collect();
            foreach ($request->input('users') as $usersId) {
                $users->push($usersId);
            }
            $changes = $event->users()->sync($users);
            $removedUsers = collect($changes['detached']);
            $removedUsers = $removedUsers->map(function ($userID) {
                return User::find($userID);
            });
        } else {
            $removedUsers = $event->users;
            $event->users()->detach();
        }

        // Get removed Users.
        foreach ($removedUserGroups as $removedUserGroup) {
            $removedUsers = $removedUsers->concat($removedUserGroup->users);
        }
        if (!$removedUsers->isEmpty()) {
            event(new UsersRemovedFromEvent($removedUsers));
        }

        return new EventResource($event);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function destroy(Event $event)
    {
        $event->delete();
        return new EventResource($event);
    }
}
