<?php

namespace App\Http\Controllers;

use App\Http\Requests\TaskCategory\TaskCategoryRequest;
use App\Http\Resources\TaskCategory\SimplifiedTaskCategoryResource;
use App\Http\Resources\TaskCategory\TaskCategoryResource;
use App\Models\TaskCategory;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class TaskCategoryController extends Controller
{
    /**
     * Initialize permissions middleware.
     */
    public function __construct()
    {
        $this->middleware('permission.api:tasks.general')->only(['store', 'update', 'destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function index()
    {
        return SimplifiedTaskCategoryResource::collection(TaskCategory::get());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TaskCategory  $taskCategory
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function show(TaskCategory $taskCategory)
    {
        return new TaskCategoryResource($taskCategory);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function store(TaskCategoryRequest $request)
    {
        return $this->save($request, new TaskCategory());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TaskCategory  $taskCategory
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function update(TaskCategoryRequest $request, TaskCategory $taskCategory)
    {
        return $this->save($request, $taskCategory);
    }

    /**
     * Save the specific resource with the values from the request.
     *
     * @param  \App\Http\Requests\TaskCategory\TaskCategoryRequest $request
     * @param  \App\Models\TaskCategory $taskCategory
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function save(TaskCategoryRequest $request, TaskCategory $taskCategory): JsonResource
    {
        $taskCategory->name = $request->input('name');

        if (!$taskCategory->save()) {
            abort(500, 'Fehler beim Speichern');
        }

        return new TaskCategoryResource($taskCategory);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TaskCategory  $taskCategory
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function destroy(TaskCategory $taskCategory)
    {
        $taskCategory->delete();
        return new TaskCategoryResource($taskCategory);
    }
}
