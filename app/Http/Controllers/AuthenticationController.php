<?php

namespace App\Http\Controllers;

use Activation;
use App\Http\Requests\Auth\ActivationRequest;
use App\Http\Requests\Auth\ChangePasswordRequest;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\PasswordResetRequest;
use App\Http\Requests\Auth\RefreshRequest;
use App\Http\Requests\Auth\RequestPasswordResetRequest;
use App\Jobs\Auth\CreateReminder;
use App\Models\PassportClient;
use Auth;
use Illuminate\Http\Request;
use Reminder;
use Sentinel;

class AuthenticationController extends Controller
{
    private $scopes = ['app'];

    /**
     * Try to get the error message from $response.
     * Return a standard message if it fails.
     *
     * @return string
     */
    private function getErrorMessage($response)
    {
        try {
            return json_decode($response->content())->message;
        } catch (Exception $ex) {
            return 'Fehler!';
        }
    }

    public function login(LoginRequest $request)
    {
        $client = PassportClient::passwordGrantClient();

        // Send login request
        $request = Request::create(route('passport.token'), 'POST', [
            'grant_type'    => 'password',
            'client_id'     => $client->id,
            'client_secret' => $client->secret,
            'username'      => $request->username,
            'password'      => $request->password,
            'scope'         => $this->scopes,
        ], [], [], [
            'HTTP_Accept' => 'application/json',
        ]);
        $response = app()->handle($request);

        // Handle errors
        $status = $response->status();
        if ($status == 400) {
            abort(400, 'Die Logindaten sind falsch.');
        }
        if ($status >= 400) {
            $message = $this->getErrorMessage($response);
            abort($status, $message);
        }

        // Success. Return tokens
        return $response->content();
    }

    public function refresh(RefreshRequest $request)
    {
        $client = PassportClient::passwordGrantClient();

        // Send refresh request
        $request = Request::create(route('passport.token'), 'POST', [
            'grant_type'    => 'refresh_token',
            'client_id'     => $client->id,
            'client_secret' => $client->secret,
            'refresh_token' => $request->refresh_token,
            'scope'         => $this->scopes,
        ], [], [], [
            'HTTP_Accept' => 'application/json',
        ]);
        $response = app()->handle($request);

        // Handle errors
        $status = $response->status();
        if ($status == 401) {
            abort(401, 'Der Refresh Token ist ungültig.');
        }
        if ($status >= 400) {
            $message = $this->getErrorMessage($response);
            abort($status, $message);
        }

        // Success. Return tokens
        return $response->content();
    }

    public function loggedIn()
    {
        // Logic is done by the middleware.
        return response()->json();
    }

    public function logout()
    {
        Auth::user()->token()->revoke();
        return response()->json();
    }

    public function activate(ActivationRequest $request)
    {
        // Get user
        $user = Sentinel::findById($request->input('user'));
        $activation = Activation::exists($user);
        if (!$activation) {
            if (Activation::completed($user)) {
                abort(400, 'Das Benutzerkonto wurde schon aktiviert.');
            } else {
                abort(400, 'Dieser Benutzer kann nicht aktiviert werden.');
            }
        }

        // Activate user
        if (!Activation::complete($user, $request->input('token'))) {
            abort(400, 'Der Aktivierungstoken ist ungültig.');
        }

        // Get credentials to update user
        $credentials = [
            'username' => $request->input('username'),
            'password' => $request->input('password'),
        ];

        // Update user
        if (!Sentinel::validForUpdate($user, $credentials)) {
            abort(500, 'Fehler bei der Aktivierung');
        }
        Sentinel::update($user, $credentials);

        return response()->json();
    }

    public function changePassword(ChangePasswordRequest $request)
    {
        // Get current user
        $user = Auth::user();

        // Check old password
        if (!Sentinel::getHasher()->check($request->input('password'), $user->password)) {
            abort(400, 'Passwort ist falsch.');
        }

        // Set new password
        Sentinel::update($user, ['password' => $request->input('new_password')]);

        return response()->json();
    }

    public function requestPasswordReset(RequestPasswordResetRequest $request)
    {
        // Get user
        $user = Sentinel::getUserRepository()->where('email', $request->input('email'))->firstOrFail();
        if (!Activation::completed($user)) {
            abort(400, 'Das Benutzerkonto wurde noch nicht aktiviert.');
        }

        // Create a new reminder and send an email to the user
        CreateReminder::dispatch($user);

        return response()->json();
    }

    public function resetPassword(PasswordResetRequest $request)
    {
        // Get user
        $user = Sentinel::getUserRepository()->findOrFail($request->input('user'));

        if (!Reminder::exists($user)) {
            abort(404, 'Passwort-Reset existiert nicht oder ist schon abgelaufen.');
        }

        // Complete password reset
        if (!Reminder::complete($user, $request->input('reminder_code'), $request->input('password'))) {
            abort(400, 'Beim Zurücksetzen des Passworts ist ein Fehler aufgetreten.');
        }

        return response()->json();
    }
}
