<?php

namespace App\Http\Controllers;

use App\Http\Requests\Position\PositionRequest;
use App\Http\Resources\Position\PositionResource;
use App\Http\Resources\Position\SimplifiedPositionResource;
use App\Models\Position;

class PositionController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission.api:positions.general')->only(['store', 'update', 'destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return SimplifiedPositionResource::collection(Position::get());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Position $position
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function show(Position $position)
    {
        return new PositionResource($position);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Position\PositionRequest $request
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function store(PositionRequest $request)
    {
        return $this->save($request, new Position());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Position\PositionRequest $request
     * @param  \App\Position $position
     * @return \Illuminate\Http\Response
     */
    public function update(PositionRequest $request, Position $position)
    {
        return $this->save($request, $position);
    }

    /**
     * Save the specific resource with the values from the request.
     *
     * @param App\Http\Requests\Position\PositionRequest $request
     * @param App\Models\Position $division
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    private function save(PositionRequest $request, Position $position)
    {
        $position->male = $request->input('male');
        $position->female = $request->input('female');
        $position->inter = $request->input('inter');

        if (!$position->save()) {
            abort(500, 'Fehler beim Speichern.');
        }

        return new PositionResource($position);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Position $position
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function destroy(Position $position)
    {
        $position->delete();
        return new PositionResource($position);
    }
}
