<?php

namespace App\Http\Resources\OrderItem;

use App\Http\Resources\Shop\SimplifiedShopResource;
use App\Http\Resources\User\ContactUserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderItemResource extends JsonResource {
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        return [
            'id'             => $this->id,
            'name'           => $this->name,
            'price'          => $this->price,
            'count'          => $this->count,
            'url'            => $this->url,
            'article_number' => $this->article_number,
            'description'    => clean($this->description),
            'ordered'        => $this->ordered,
            'shop'           => new SimplifiedShopResource($this->shop),
            'creator'        => new ContactUserResource($this->creator),
        ];
    }
}
