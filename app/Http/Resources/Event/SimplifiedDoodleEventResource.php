<?php

namespace App\Http\Resources\Event;

use App\Http\Resources\EventDateOption\SimplifiedEventDateOptionResource;
use Illuminate\Http\Resources\Json\JsonResource;

class SimplifiedDoodleEventResource extends JsonResource {
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        return [
            'id'           => $this->id,
            'title'        => $this->title,
            'date_options' => SimplifiedEventDateOptionResource::collection($this->dateOptions),
        ];
    }
}
