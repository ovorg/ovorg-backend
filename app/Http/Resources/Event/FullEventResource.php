<?php

namespace App\Http\Resources\Event;

use App\Http\Resources\EventDateOption\EventDateOptionResource;
use App\Http\Resources\UserGroup\SimplifiedUserGroupResource;
use App\Http\Resources\User\AvailabilityUserResource;
use App\Http\Resources\User\SimplifiedUserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class FullEventResource extends JsonResource {
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        return [
            'id'            => $this->id,
            'title'         => $this->title,
            'description'   => clean($this->description),
            'responsible'   => new SimplifiedUserResource($this->responsible),
            'force_comment' => $this->force_comment,
            'users'         => SimplifiedUserResource::collection($this->users),
            'user_groups'   => SimplifiedUserGroupResource::collection($this->userGroups),
            'all_users'     => AvailabilityUserResource::collection($this->allusers()),
            'date_options'  => EventDateOptionResource::collection($this->dateOptions),
        ];
    }
}
