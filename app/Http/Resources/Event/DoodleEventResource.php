<?php

namespace App\Http\Resources\Event;

use App\Http\Resources\EventDateOption\EventDateOptionResource;
use App\Http\Resources\User\ContactUserResource;
use App\Http\Resources\User\SimplifiedUserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class DoodleEventResource extends JsonResource {
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        return [
            'id'                 => $this->id,
            'title'              => $this->title,
            'description'        => clean($this->description),
            'responsible'        => new ContactUserResource($this->responsible),
            'force_comment'      => $this->force_comment,
            'all_users'          => SimplifiedUserResource::collection($this->allusers()),
            'date_options'       => EventDateOptionResource::collection($this->dateOptions),
            'authenticated_user' => new SimplifiedUserResource($request->user()),
        ];
    }
}
