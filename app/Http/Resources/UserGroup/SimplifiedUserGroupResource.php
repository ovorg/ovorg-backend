<?php

namespace App\Http\Resources\UserGroup;

use Illuminate\Http\Resources\Json\JsonResource;

class SimplifiedUserGroupResource extends JsonResource {
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        return [
            'id'        => $this->id,
            'name'      => $this->name,
            'generated' => $this->generated,
        ];
    }
}
