<?php

namespace App\Http\Resources\User;

class MeetingUserResource extends SimplifiedUserResource {
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        return array_merge(parent::toArray($request), [
            'vegetarian'         => $this->vegetarian,
            'health_information' => $this->health_information,
            'comment'            => $this->pivot ? $this->pivot->comment : null,
        ]);
    }
}
