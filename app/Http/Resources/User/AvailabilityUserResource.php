<?php

namespace App\Http\Resources\User;

use App\Http\Resources\Position\SimplifiedPositionResource;
use Illuminate\Http\Resources\Json\JsonResource;

class AvailabilityUserResource extends JsonResource {
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        return [
            'id'                 => $this->id,
            'first_name'         => $this->first_name,
            'last_name'          => $this->last_name,
            'health_information' => $this->health_information,
            'vegetarian'         => $this->vegetarian,
            'position'           => new SimplifiedPositionResource($this->position),
        ];
    }
}
