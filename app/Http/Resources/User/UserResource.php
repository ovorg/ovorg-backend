<?php

namespace App\Http\Resources\User;

use App\Http\Resources\Division\SimplifiedDivisionResource;
use App\Http\Resources\Driver\SimplifiedDriverResource;
use App\Http\Resources\Position\SimplifiedPositionResource;
use App\Http\Resources\SCBA\SimplifiedSCBAResource;
use App\Http\Resources\UserGroup\SimplifiedUserGroupResource;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource {
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        return [
            'id'          => $this->id,
            'thwin_id'    => $this->thwin_id,
            'first_name'  => $this->first_name,
            'last_name'   => $this->last_name,
            'username'    => $this->username,
            'email'       => $this->email,
            'gender'      => $this->gender,
            'birth'       => $this->birth,
            'mobile'      => $this->mobile,
            'phone'       => $this->phone,
            'position'    => new SimplifiedPositionResource($this->position),
            'division'    => new SimplifiedDivisionResource($this->division),
            'driver'      => new SimplifiedDriverResource($this->driver),
            'scba'        => new SimplifiedSCBAResource($this->scba),
            'user_groups' => SimplifiedUserGroupResource::collection($this->userGroups),
        ];
    }
}
