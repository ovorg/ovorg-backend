<?php

namespace App\Http\Resources\User;

use App\Http\Resources\Division\SimplifiedDivisionResource;
use App\Http\Resources\Position\PositionResource;

class ContactUserResource extends SimplifiedUserResource {
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        return array_merge(
            parent::toArray($request),
            array_merge(
                [
                    'position' => $this->genderedPosition(),
                    'division' => new SimplifiedDivisionResource($this->division),
                ],
                $this->checkPrivacyAndGet($this->privacy, 'mobile'),
                $this->checkPrivacyAndGet($this->privacy, 'phone'),
                $this->checkPrivacyAndGet($this->privacy, 'email')
            ));
    }

    /**
     * Return an array element for the key with the corresponding value or null,
     * depending on the privacy settings.
     *
     * @param App\Privacy $privacy
     * @param string $key
     * @return array
     */
    public function checkPrivacyAndGet($privacy, $key) {
        return [$key => $privacy ? $privacy->$key ? $this->$key : null : null];
    }
}
