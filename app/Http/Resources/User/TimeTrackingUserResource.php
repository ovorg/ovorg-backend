<?php

namespace App\Http\Resources\User;

use App\Http\Resources\Division\SimplifiedDivisionResource;
use App\Http\Resources\Position\TimeTrackingPositionResource;

class TimeTrackingUserResource extends SimplifiedUserResource {
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        return array_merge(parent::toArray($request), [
            'thwin_id'     => $this->thwin_id,
            'present'      => $this->present,
            'gender'       => $this->gender,
            'position'     => new TimeTrackingPositionResource($this->position),
            'division'     => new SimplifiedDivisionResource($this->division),
            'scan_timeout' => $this->scan_timeout,
        ]);
    }
}
