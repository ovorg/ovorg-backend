<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

class BroadcastingTimeTrackingUserResource extends JsonResource {
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        return [
            'id'           => $this->id,
            'present'      => $this->present,
            'scan_timeout' =>$this->scan_timeout,
        ];
    }
}
