<?php

namespace App\Http\Resources\User;


class DoodleUserResource extends SimplifiedUserResource {
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        return array_merge(parent::toArray($request), [
            'comment'    => $this->pivot ? $this->pivot->comment : null,
            'attendance' => $this->pivot ? $this->pivot->attendance : null,
        ]);
    }
}
