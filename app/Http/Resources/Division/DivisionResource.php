<?php

namespace App\Http\Resources\Division;

use App\Http\Resources\User\SimplifiedUserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class DivisionResource extends JsonResource {
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        return [
            'id'         => $this->id,
            'name'       => $this->name,
            'short_name' => $this->short_name,
            'users'      => SimplifiedUserResource::collection($this->users),
        ];
    }
}
