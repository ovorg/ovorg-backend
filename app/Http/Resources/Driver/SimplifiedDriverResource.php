<?php

namespace App\Http\Resources\Driver;

use Illuminate\Http\Resources\Json\JsonResource;

class SimplifiedDriverResource extends JsonResource {
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        return [
            'b'           => $this->b,
            'be'          => $this->be,
            'c'           => $this->c,
            'ce'          => $this->ce,
            'expiry_date' => $this->expiry_date,
        ];
    }
}
