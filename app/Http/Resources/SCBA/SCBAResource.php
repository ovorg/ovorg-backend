<?php

namespace App\Http\Resources\SCBA;

use App\Http\Resources\User\SimplifiedUserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class SCBAResource extends JsonResource {
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        return [
            'next_examination'     => $this->next_examination,
            'last_instruction'     => $this->last_instruction,
            'last_excercise'       => $this->last_excercise,
            'last_deployment'      => $this->last_deployment,
            'cbrn'                 => $this->cbrn,
            'last_cbrn_deployment' => $this->last_cbrn_deployment,
            'user'                 => new SimplifiedUserResource($this->user),
        ];
    }
}
