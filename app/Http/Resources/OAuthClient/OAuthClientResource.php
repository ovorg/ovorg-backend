<?php

namespace App\Http\Resources\OAuthClient;

use Illuminate\Http\Resources\Json\JsonResource;

class OAuthClientResource extends JsonResource {
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        return [
            'id'      => $this->id,
            'name'    => $this->name,
            'secret'  => $this->secret,
            'revoked' => $this->revoked,
        ];
    }
}
