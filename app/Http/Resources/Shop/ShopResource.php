<?php

namespace App\Http\Resources\Shop;

use App\Http\Resources\OrderItem\SimplifiedOrderItemResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ShopResource extends JsonResource {
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        return [
            'id'          => $this->id,
            'name'        => $this->name,
            'url'         => $this->url,
            'order_items' => SimplifiedOrderItemResource::collection($this->orderItems),
        ];
    }
}
