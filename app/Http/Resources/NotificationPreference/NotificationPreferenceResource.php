<?php

namespace App\Http\Resources\NotificationPreference;

use Illuminate\Http\Resources\Json\JsonResource;

class NotificationPreferenceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "meeting_notifications" => $this->meeting_notifications,
            "task_notifications" => $this->task_notifications,
            "event_notifications" => $this->event_notifications,
        ];
    }
}
