<?php

namespace App\Http\Resources\FeedbackTemplate;

use Illuminate\Http\Resources\Json\JsonResource;

class FeedbackTemplateResource extends JsonResource {
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        return [
            'id'       => $this->id,
            'name'     => $this->name,
            'template' => $this->template,
        ];
    }
}
