<?php

namespace App\Http\Resources\File;

use App\Http\Resources\Task\SimplifiedTaskResource;
use App\Http\Resources\User\SimplifiedUserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class FileResource extends JsonResource {
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        return [
            'id'       => $this->id,
            'name'     => $this->name,
            'size'     => $this->size,
            'uploader' => new SimplifiedUserResource($this->uploader),
            'task'     => new SimplifiedTaskResource($this->task),
        ];
    }
}
