<?php

namespace App\Http\Resources\Privacy;

use Illuminate\Http\Resources\Json\JsonResource;

class SimplifiedPrivacyResource extends JsonResource {
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        return [
            'email'  => $this->email,
            'mobile' => $this->mobile,
            'phone'  => $this->phone,
        ];
    }
}
