<?php

namespace App\Http\Resources\Position;

use App\Http\Resources\User\SimplifiedUserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class PositionResource extends JsonResource {
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        return [
            'id'     => $this->id,
            'male'   => $this->male,
            'female' => $this->female,
            'inter'  => $this->inter,
            'users'  => SimplifiedUserResource::collection($this->users),
        ];
    }
}
