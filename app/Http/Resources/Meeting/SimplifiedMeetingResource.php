<?php

namespace App\Http\Resources\Meeting;

use App\Http\Resources\UserGroup\SimplifiedUserGroupResource;
use Illuminate\Http\Resources\Json\JsonResource;

class SimplifiedMeetingResource extends JsonResource {
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        return [
            'id'             => $this->id,
            'title'          => $this->title,
            'start'          => $this->start,
            'end'            => $this->end,
            'register_until' => $this->register_until ? $this->register_until->toDateString() : null,
            'user_groups'    => SimplifiedUserGroupResource::collection($this->usergroups),
        ];
    }
}
