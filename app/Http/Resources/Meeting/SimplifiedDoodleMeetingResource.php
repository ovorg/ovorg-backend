<?php

namespace App\Http\Resources\Meeting;

use App\Http\Resources\User\DoodleUserResource;
use Auth;

class SimplifiedDoodleMeetingResource extends SimplifiedMeetingResource {
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        $currentUser = Auth::user();
        if ($currentUser) {
            $currentMeetingUser = $this->registeredUsers()->find($currentUser->id);
            $currentUser = $currentMeetingUser ?: $currentUser;
        }

        return array_merge(parent::toArray($request), [
            'user' => new DoodleUserResource($currentUser),
        ]);
    }
}
