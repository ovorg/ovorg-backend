<?php

namespace App\Http\Resources\Meeting;

use App\Http\Resources\User\ContactUserResource;
use App\Http\Resources\User\DoodleUserResource;

class DoodleMeetingResource extends SimplifiedDoodleMeetingResource {
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        return array_merge(parent::toArray($request), [
            'comment'     => $this->comment,
            'responsible' => new ContactUserResource($this->responsible),
            'users'       => DoodleUserResource::collection($this->registeredUsers),
            'description' => clean($this->description),
        ]);
    }
}
