<?php

namespace App\Http\Resources\Meeting;

use App\Http\Resources\UserGroup\UserGroupResource;
use App\Http\Resources\User\SimplifiedUserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class MeetingResource extends JsonResource {
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        return [
            'id'             => $this->id,
            'title'          => $this->title,
            'start'          => $this->start,
            'end'            => $this->end,
            'description'    => clean($this->description),
            'comment'        => $this->comment,
            'register_until' => $this->register_until ? $this->register_until->toDateString() : null,
            'user_groups'    => UserGroupResource::collection($this->userGroups),
            'users'          => SimplifiedUserResource::collection($this->users),
            'responsible'    => new SimplifiedUserResource($this->responsible),
        ];
    }
}
