<?php

namespace App\Http\Resources\Meeting;

use App\Http\Resources\MeetingMail\SimplifiedMeetingMailResource;
use App\Http\Resources\UserGroup\UserGroupResource;
use App\Http\Resources\User\ContactUserResource;
use App\Http\Resources\User\MeetingUserResource;
use App\Http\Resources\User\SimplifiedUserResource;
use App\Http\Resources\User\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class FullMeetingResource extends JsonResource {
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        return [
            'id'               => $this->id,
            'title'            => $this->title,
            'start'            => $this->start,
            'end'              => $this->end,
            'description'      => clean($this->description),
            'comment'          => $this->comment,
            'register_until'   => $this->register_until ? $this->register_until->toDateString() : null,
            'attendance_yes'   => MeetingUserResource::collection($this->attendanceYes()->get()),
            'attendance_no'    => MeetingUserResource::collection($this->attendanceNo()->get()),
            'attendance_maybe' => MeetingUserResource::collection($this->attendanceMaybe()->get()),
            'not_registered'   => UserResource::collection($this->notRegistered()),
            'user_groups'      => UserGroupResource::collection($this->userGroups),
            'users'            => SimplifiedUserResource::collection($this->users),
            'responsible'      => new ContactUserResource($this->responsible),
            'meeting_mails'    => SimplifiedMeetingMailResource::collection($this->meetingMails),
        ];
    }
}
