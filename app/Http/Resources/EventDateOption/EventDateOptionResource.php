<?php

namespace App\Http\Resources\EventDateOption;

use App\Http\Resources\EventRegistration\SimplifiedEventRegistrationResource;
use Illuminate\Http\Resources\Json\JsonResource;

class EventDateOptionResource extends JsonResource {
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        return [
            'id'            => $this->id,
            'date'          => $this->date ? $this->date->toDateString() : null,
            'registrations' => SimplifiedEventRegistrationResource::collection($this->registrations),
        ];
    }
}
