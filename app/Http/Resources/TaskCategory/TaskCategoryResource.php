<?php

namespace App\Http\Resources\TaskCategory;

use App\Http\Resources\Task\SimplifiedTaskResource;
use Illuminate\Http\Resources\Json\JsonResource;

class TaskCategoryResource extends JsonResource {
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        return [
            'id'    => $this->id,
            'name'  => $this->name,
            'tasks' => SimplifiedTaskResource::collection($this->tasks),
        ];
    }
}
