<?php

namespace App\Http\Resources\EventRegistration;

use App\Http\Resources\User\SimplifiedUserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class SimplifiedEventRegistrationResource extends JsonResource {
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        return [
            'user_id'      => $this->user->id,
            'comment'      => $this->comment,
            'availability' => $this->availability,
        ];
    }
}
