<?php

namespace App\Http\Resources\MeetingMail;

use App\Http\Resources\Meeting\SimplifiedMeetingResource;
use Illuminate\Http\Resources\Json\JsonResource;

class MeetingMailResource extends JsonResource {
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        return [
            'id' => $this->id,
            'date'           => $this->date,
            'sent'           => $this->sent,
            'include_maybes' => $this->include_maybes,
            'meeting'        => new SimplifiedMeetingResource($this->meeting),
        ];
    }
}
