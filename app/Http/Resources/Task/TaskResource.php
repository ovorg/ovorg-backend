<?php

namespace App\Http\Resources\Task;

use App\Http\Resources\File\SimplifiedFileResource;
use App\Http\Resources\TaskCategory\SimplifiedTaskCategoryResource;
use App\Http\Resources\User\ContactUserResource;
use App\Http\Resources\User\SimplifiedUserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class TaskResource extends JsonResource {
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        return [
            'id'                => $this->id,
            'title'             => $this->title,
            'duedate'           => $this->duedate,
            'priority'          => $this->priority,
            'state'             => $this->state,
            'description'       => clean($this->description),
            'addition'          => clean($this->addition),
            'users'             => SimplifiedUserResource::collection($this->users),
            'inform_executives' => $this->inform_executives,
            'responsible'       => new ContactUserResource($this->responsible),
            'creator'           => new ContactUserResource($this->creator),
            'task_category'     => new SimplifiedTaskCategoryResource($this->taskCategory),
            'files'             => SimplifiedFileResource::collection($this->files),
        ];
    }
}
