<?php

namespace App\Http\Resources\Task;

use App\Http\Resources\TaskCategory\SimplifiedTaskCategoryResource;
use Auth;
use Illuminate\Http\Resources\Json\JsonResource;

class SimplifiedTaskResource extends JsonResource {
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        return [
            'id'                => $this->id,
            'title'             => $this->title,
            'duedate'           => $this->duedate,
            'priority'          => $this->priority,
            'state'             => $this->state,
            'task_category'     => new SimplifiedTaskCategoryResource($this->taskCategory),
            'participationtype' => $this->getParticipationType(Auth::user()),
            'users_count'       => count($this->users),
            'inform_executives' => $this->inform_executives,
        ];
    }
}
