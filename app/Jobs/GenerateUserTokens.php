<?php

namespace App\Jobs;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Str;

class GenerateUserTokens implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $users = User::where('doodle_token', null)->orWhere('doodle_token', '')->get();
        foreach ($users as $user) {
            $user->doodle_token = hash_hmac('sha256', Str::random(40), config('app.key'));
            $user->save();
        }

        $users = User::where('calendar_token', null)->orWhere('calendar_token', '')->get();
        foreach ($users as $user) {
            $user->calendar_token = hash_hmac('sha256', Str::random(40), config('app.key'));
            $user->save();
        }
    }
}
