<?php

namespace App\Jobs;

use App\Models\File;
use Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DeleteUnusedFiles implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Delete all files that are not assigned to a task and older than 24 hours.
        $files = File::where('task_id', null)->get();
        $referenceDate = Carbon::now()->subHours(config('filesystems.keep_unlinked'));
        foreach ($files as $file) {
            if ($file->created_at->lessThan($referenceDate)) {
                $file->delete();
            }
        }
    }
}
