<?php

namespace App\Jobs;

use App\Models\Task;
use App\Notifications\Tasks\DuedateExpiredNotification;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Notification;

class CheckTaskDuedates implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $dueToday = Task::where('duedate', Carbon::today())->where('state', '<', 100)->get();
        foreach ($dueToday as $task) {
            $users = $task->getAllParticipants();
            Notification::send($users, new DuedateExpiredNotification($task));
        }
    }
}
