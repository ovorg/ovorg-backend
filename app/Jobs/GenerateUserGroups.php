<?php

namespace App\Jobs;

use App\Models\Division;
use App\Models\Driver;
use App\Models\SCBA;
use App\Models\User;
use App\Models\UserGroup;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class GenerateUserGroups implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->divisions();
        $this->scba();
        $this->driver();
        $this->all();
    }

    /**
     * Return callback that returns only the variable $var of the collection.
     *
     * @param mixed $var
     * @return mixed
     */
    private function filter($var)
    {
        return function ($item) use ($var) {
            return $item->$var;
        };
    }

    /**
     * Create user groups for divisions
     */
    private function divisions()
    {
        foreach (Division::all() as $division) {
            $syncIds = $division->users->map($this->filter('id'));
            $this->createUserGroup($division->name, $syncIds);
        }
    }

    /**
     *  Create a user group for all scba's
     */
    private function scba()
    {
        // Get UserGroup. Create one if it doesn't exist.
        $name = 'Atemschutzgeräteträger';
        $syncIds = SCBA::all()->map($this->filter('user_id'));
        $this->createUserGroup($name, $syncIds);
    }

    /**
     *  Create a user groups for drivers
     */
    private function driver()
    {
        $licences = ['b', 'be', 'c', 'ce'];
        $namePrefix = 'Fahrer';

        foreach ($licences as $licence) {
            $name = strtoupper($licence) . ' ' . $namePrefix;
            $syncIds = Driver::where($licence, true)->get()->map($this->filter('user_id'));
            $this->createUserGroup($name, $syncIds);
        }
    }

    /**
     *  Create a user group with name = $name and users in $syncIds
     */
    private function createUserGroup($name, $syncIds)
    {
        // Get UserGroup. Create one if it doesn't exist.
        $userGroup = UserGroup::where('name', $name)->first();
        if (!$userGroup) {
            $userGroup = new UserGroup(['name' => $name, 'generated' => true]);
            $userGroup->save();
        }

        // Make sure generated is set to true.
        if ($userGroup->generated !== true) {
            $userGroup->generated = true;
            $userGroup->save();
        }

        // Sync users.
        $userGroup->users()->sync($syncIds);
    }

    /**
     * Create user group with all users.
     */
    private function all()
    {
        $syncIds = User::all()->map($this->filter('id'));
        $this->createUserGroup('Alle Helfer (Ja, wirklich alle. Also auch Junghelfer usw.)', $syncIds);
    }
}
