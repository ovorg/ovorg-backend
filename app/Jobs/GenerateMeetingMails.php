<?php

namespace App\Jobs;

use App\Models\Meeting;
use App\Models\MeetingMail;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;

class GenerateMeetingMails implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    const HANDLE_DELAY = 15;

    /**
     * Mails to send for single meetings.
     */
    private $mails = [
        [14, false],
        [7, true],
        [3, true],
    ];
    private $meeting;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Meeting $meeting)
    {
        $this->meeting = $meeting;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Don't create meeting mails for meetings where the register_until is in the past.
        if ($this->meeting->register_until && $this->meeting->register_until->isPast()) {
            return;
        }

        // Delete unsent mails for $meeting
        $this->meeting->meetingMails()->where('sent', false)->delete();

        // Don't create meeting mails for meetings in the past.
        if ($this->meeting->start->isPast()) {
            return;
        }

        // Don't create meeting mails for meetings where register_until is in the past.
        if ($this->meeting->register_until && $this->meeting->register_until->isPast()) {
            return;
        }

        // Create MeetingMails that haven't been sent
        $mails = $this->generateUnsentMeetingMailData($this->meeting);
        $this->createMeetingMails($this->meeting, $mails);

        // Dispatch job to send meeting mails for today
        HandleMeetingNotifications::dispatch()->delay(now()->addMinutes(self::HANDLE_DELAY));
    }

    /**
     * Generate the attributes for the MeetingMails for $meeting for meetings,
     * removing the mails that has.
     *
     * @param  \App\Models\Meeting $meeting
     * @return \Illuminate\Support\Collection
     */
    private function generateUnsentMeetingMailData(Meeting $meeting): Collection
    {
        $sentCount = $meeting->meetingMails()->where('sent', true)->count();
        $mails = $this->generateMeetingMailData($meeting);
        if ($sentCount != $mails->count()) {
            // Remove sent mails
            $mails = $mails->sortBy('date')->splice($sentCount);
        }
        return $mails;
    }

    /**
     * Generate the attributes for the MeetingMails for $meeting
     *
     * @param  \App\Models\Meeting $meeting
     * @return \Illuminate\Support\Collection
     */
    private function generateMeetingMailData(Meeting $meeting): Collection
    {
        return collect($this->mails)->map(function (array $mail) use ($meeting) {
            return [
                'date'           => $meeting->register_until ? $meeting->register_until->subDays($mail[0]) : $meeting->start->subDays($mail[0]),
                'include_maybes' => $mail[1],
            ];
        });
    }

    /**
     * Create MeetingMails for $meeting.
     * At least one MeetingMail will be created, even if the Meeting is in the past.
     *
     * @param \App\Models\Meeting $meeting
     * @param \Illuminate\Support\Collection $mails
     */
    private function createMeetingMails(Meeting $meeting, Collection $mails)
    {
        $createdMails = $mails->filter(function (array $mail) use ($meeting) {
            // Dont create MeetingMail if date is in the past.
            if ($mail['date']->lessThan(Carbon::today())) {
                return false;
            }
            $meeting->meetingMails()->save(new MeetingMail($mail));
            return true;
        });

        // Create a MeetingMail for today if no other mail has been created and no (sent) mail for today exists.
        if ($createdMails->isEmpty() && $meeting->meetingMails()->where('date', Carbon::today())->get()->isEmpty()) {
            $meeting->meetingMails()->save(new MeetingMail([
                'date'           => Carbon::today(),
                'include_maybes' => true,
            ]));
        }
    }
}
