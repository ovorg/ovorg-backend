<?php

namespace App\Jobs\Auth;

use App\Notifications\Auth\ReminderNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Reminder;

class CreateReminder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $user;
    public $reminder;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $reminder = Reminder::get($user);
        if (!$reminder) {
            $reminder = Reminder::create($user);
        }

        $this->user = $user;
        $this->reminder = $reminder;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->user->notify(new ReminderNotification($this->reminder));
    }
}
