<?php

namespace App\Jobs\Auth;

use Activation;
use App\Notifications\Auth\ActivationNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CreateActivation implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $user;

    private $activated;
    private $activation;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $activation = Activation::get($user);
        if (!$activation) {
            if (!Activation::completed($user)) {
                $activation = Activation::create($user);
            } else {
                $this->activated = true;
                return;
            }
        }
        $this->activation = $activation;
        $this->activated = false;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!$this->activated) {
            $this->user->notify(new ActivationNotification($this->activation));
        }
    }
}
