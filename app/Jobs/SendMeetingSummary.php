<?php

namespace App\Jobs;

use App\Models\User;
use App\Notifications\MeetingSummaryNotification;
use Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendMeetingSummary implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $month;

    /**
     * Create a new job instance.
     *
     * @param $month Carbon date in the month. Default is in the next month.
     */
    public function __construct($month = null)
    {
        if ($month) {
            $this->month = $month;
        } else {
            $this->month = new Carbon('first day of next month');
        }
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        foreach (User::all() as $user) {
            $meetings = $user->getAllMeetingsForMonth($this->month)->sortBy('start');
            if ($meetings->isEmpty()) {
                continue;
            }

            $user->notify(new MeetingSummaryNotification($meetings));
        }
    }
}
