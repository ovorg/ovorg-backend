<?php

namespace App\Jobs;

use App\Models\MeetingMail;
use App\Models\User;
use App\Notifications\MeetingNotification;
use Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class HandleMeetingNotifications implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach (User::get() as $user) {
            $this->notifyUser($user);
        }

        MeetingMail::where('date', Carbon::today())->update(['sent' => true]);
    }

    /**
     * Notify the User about meetings.
     *
     * @param \App\Models\User $user
     */
    private function notifyUser(User $user): void
    {
        // Get all the meetings that the user needs to be notified about
        $registeredMeetings = $user->registeredMeetings;
        $meetings = $user->getAllUnsentMeetingMailsForToday()->map(function (MeetingMail $meetingMail) use ($registeredMeetings) {
            // Check if user is already registered
            $registeredMeeting = $registeredMeetings->firstWhere('id', $meetingMail->meeting_id);
            if (!$registeredMeeting) {
                // User is not registered
                return $meetingMail->meeting;
            }

            // User is registered. Add meeting according to attendance.
            if ($registeredMeeting->pivot->attendance == 'no' || $registeredMeeting->pivot->attendance == 'yes') {
                return null;
            } elseif ($registeredMeeting->pivot->attendance == 'maybe' && !$meetingMail->include_maybes) {
                return null;
            }
            return $registeredMeeting;
        })->reject(function ($item) {
            // Remove null objects
            return !$item;
        });

        // Notify user
        if (!$meetings->isEmpty()) {
            $user->notify(new MeetingNotification($meetings));
        }
    }
}
