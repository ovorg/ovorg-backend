<?php

namespace App\Lib;

use App\Models\User;
use Eluceo\iCal\Component\Calendar;
use Eluceo\iCal\Component\Event;
use Eluceo\iCal\Component\Timezone;
use Illuminate\Database\Eloquent\Collection;

class ICal
{
    private static $prodid = 'ovorg.thw-baiersdorf.de';
    private static $location = 'Königsberger Straße 32, 91083 Baiersdorf';
    private static $locationName = 'THW OV Baiersdorf';

    /**
     * Create a calendar for the specified Meetings.
     *
     * @param  \App\Models\User $user
     * @param  bool $yes
     * @param  bool $no
     * @param  bool $maybe
     * @param  bool $notRegistered
     * @return \Eluceo\iCal\Component\Calendar
     */
    public static function createCalendarFor(User $user, bool $yes, bool $no, bool $maybe, bool $notRegistered): Calendar
    {
        $meetings = new Collection();

        if ($yes) {
            $meetings = $meetings->merge($user->registeredMeetings()->where('attendance', 'yes')->get());
        }
        if ($no) {
            $meetings = $meetings->merge($user->registeredMeetings()->where('attendance', 'no')->get());
        }
        if ($maybe) {
            $meetings = $meetings->merge($user->registeredMeetings()->where('attendance', 'maybe')->get());
        }
        if ($notRegistered) {
            $meetings = $meetings->merge($user->notRegisteredMeetings());
        }

        $calendar = self::createCalendar($meetings);

        return $calendar;
    }

    /**
     * Create an ical calendar with all $meetings.
     *
     * @param  \Illuminate\Database\Eloquent\Collection
     * @return \Eluceo\iCal\Component\Calendar
     */
    private static function createCalendar(Collection $meetings): Calendar
    {
        // Create calendar
        $calendar = new Calendar(self::$prodid);
        $calendar->setname('THW Dienste');
        $calendar->setCalendarColor('003399');

        // Add events
        foreach ($meetings as $meeting) {
            $url = config('app.frontend_url') . "/doodle/$meeting->id";
            $event = new Event();
            $event
                ->setUniqueId($meeting->id)
                ->setDtStart($meeting->start)
                ->setDtEnd($meeting->end)
                ->setDescription("Mehr Informationen unter: $url")
                ->setUrl($url)
                ->setSummary($meeting->title)
                ->setLocation(self::$location, self::$locationName);

            $calendar->addComponent($event);
        }

        // Add timezone
        $tz = new Timezone('Europe/Berlin');
        $calendar->addComponent($tz);

        return $calendar;
    }
}
