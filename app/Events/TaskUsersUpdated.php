<?php

namespace App\Events;

use App\Models\Task;
use App\Models\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class TaskUsersUpdated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $changes;
    public $task;
    public $author;

    /**
     * Create a new event instance.
     * $changes = ["attached" => [1,2,3], "detached" => [1,2,3], "updated" => [1,2,3]]
     *
     * @param  array $changes The changes returned by the sync method.
     * @param  \App\Models\Task $task
     * @param  \App\Models\User $author
     * @return void
     */
    public function __construct(Task $task, ?array $changes, ?User $author)
    {
        $this->task = $task;
        $this->changes = $changes;
        $this->author = $author;
    }
}
