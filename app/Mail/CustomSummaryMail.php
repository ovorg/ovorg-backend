<?php

namespace App\Mail;

use App\Models\Meeting;
use App\Models\User;
use App\Models\UserGroup;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;

class CustomSummaryMail extends Mailable
{
    use Queueable, SerializesModels;

    private $body;
    private $users;
    private $user;
    private $userGroups;
    private $meetings;
    private $attendance;

    /**
     * Create a new message instance.
     *
     * @param string $subject
     * @param string $body
     * @param \App\Models\User $user
     * @param \Illuminate\Support\Collection $users
     */
    public function __construct(string $subject, string $body, User $user, Collection $users, Collection $userGroups, Collection $meetings, Collection $attendance)
    {
        $this->subject = $subject;
        $this->body = $body;
        $this->user = $user;
        $this->users = $users->sortBy('first_name')->sortBy('last_name')
            ->map(function (User $user) {
                return $user->getFullName();
            });
        $this->userGroups = $userGroups->sortBy('name')
            ->map(function (UserGroup $userGroup) {
                return $userGroup->name;
            });
        $this->meetings = $meetings->sortBy('title')
            ->map(function (Meeting $meeting) {
                return $meeting->title;
            });
        $this->attendance = $attendance->filter(function ($a) {
            return $a;
        });
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('mails.custom_summary')->with([
            'body'       => $this->body,
            'subject'    => $this->subject,
            'user'       => $this->user,
            'users'      => $this->users,
            'userGroups' => $this->userGroups,
            'meetings'   => $this->meetings,
            'attendance' => $this->attendance,
        ]);
    }
}
