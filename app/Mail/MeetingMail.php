<?php

namespace App\Mail;

use App\Models\Meeting;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;

class MeetingMail extends Mailable
{
    use Queueable, SerializesModels;

    private $meetings;
    private $user;

    /**
     * Create a new message instance.
     *
     * @param  \App\Models\Meeting|\Illuminate\Support\Collection $meetings
     * @param  \App\Models\User $user
     */
    public function __construct($meetings, User $user)
    {
        $this->meetings = $meetings;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->meetings instanceof Meeting) {
            $this->subject = $this->meetings->title;
            return $this->markdown('mails.meetings.single_meeting')->with([
                'user'    => $this->user,
                'meeting' => $this->meetings,
            ]);
        } else if ($this->meetings instanceof Collection && $this->meetings->count() == 1) {
            $meeting = $this->meetings->first();
            $this->subject = $meeting->title;
            return $this->markdown('mails.meetings.single_meeting')->with([
                'user'    => $this->user,
                'meeting' => $meeting,
            ]);
        } else if ($this->meetings instanceof Collection) {
            $this->subject = $this->meetings->implode('title', ', ');
            return $this->markdown('mails.meetings.multiple_meetings')->with([
                'user'     => $this->user,
                'meetings' => $this->meetings->values(),
            ]);
        }
    }
}
