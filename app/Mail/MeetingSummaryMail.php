<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;

class MeetingSummaryMail extends Mailable
{
    use Queueable, SerializesModels;

    public $meetings;
    public $user;

    /**
     * Create a new message instance.
     *
     * @param \Illuminate\Support\Collection $meetings
     */
    public function __construct(Collection $meetings, User $user)
    {
        $this->meetings = $meetings;
        $this->user = $user;

        $this->subject = "Dienste für " . $meetings->first()->start->isoFormat('MMMM');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('mails.meetings.meeting_summary')->with([
            'meetings' => $this->meetings,
            'user'     => $this->user,
        ]);
    }
}
