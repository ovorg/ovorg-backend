<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // Laravel passport

        // Register routes
        Passport::routes(function ($router) {
            $router->forAccessTokens();
        });

        // Add scopes
        Passport::tokensCan([
            'app'           => 'Scope for the main application api.',
            'time_tracking' => 'Scope for the time tracking api.',
        ]);

        // Set token ttl
        Passport::tokensExpireIn(now()->addDays(1));
        Passport::refreshTokensExpireIn(now()->addDays(14));
    }
}
