<?php

namespace App\Providers;

use App\Models\Division;
use App\Models\Driver;
use App\Models\Event;
use App\Models\File;
use App\Models\Meeting;
use App\Models\SCBA;
use App\Models\Task;
use App\Models\User;
use App\Observers\DivisionObserver;
use App\Observers\DriverObserver;
use App\Observers\EventObserver;
use App\Observers\FileObserver;
use App\Observers\MeetingObserver;
use App\Observers\SCBAObserver;
use App\Observers\TaskObserver;
use App\Observers\UserObserver;
use Auth;
use Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Laravel\Passport\Passport;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Passport::ignoreMigrations();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Set standard date serialization
        Carbon::serializeUsing(function (Carbon $carbon) {
            return $carbon->toDateTimeString();
        });

        // Set default string length to avoid errors
        Schema::defaultStringLength(191);

        // Observers
        User::observe(UserObserver::class);
        Meeting::observe(MeetingObserver::class);
        Division::observe(DivisionObserver::class);
        Driver::observe(DriverObserver::class);
        SCBA::observe(SCBAObserver::class);
        File::observe(FileObserver::class);
        Task::observe(TaskObserver::class);
        Event::observe(EventObserver::class);

        // Fix that $request->user() returns null due to a duplicate naming in Sentinel and Passport
        $this->app->rebinding('request', function ($app, $request) {
            $request->setUserResolver(function () use ($app) {
                return Auth::user();
            });
        });
    }
}
