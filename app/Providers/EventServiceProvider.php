<?php

namespace App\Providers;

use App\Events\TaskFilesUpdated;
use App\Events\TaskUsersUpdated;
use App\Events\UsersRemovedFromEvent;
use App\Events\UsersRemovedFromMeeting;
use App\Events\UsersRemovedFromUserGroup;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        TaskUsersUpdated::class          => [
            'App\Listeners\SendTaskUserUpdatedNotifications',
        ],
        TaskFilesUpdated::class          => [
            'App\Listeners\SendTaskFilesUpdatedNotifications',
        ],
        UsersRemovedFromMeeting::class   => [
            'App\Listeners\CheckUserMeetingRegistrations@handleMeeting',
        ],
        UsersRemovedFromUserGroup::class => [
            'App\Listeners\CheckUserMeetingRegistrations@handleUserGroup',
            'App\Listeners\CheckUserEventRegistrations@handleUserGroup',
        ],
        UsersRemovedFromEvent::class     => [
            'App\Listeners\CheckUserEventRegistrations@handleEvent',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
