<?php

namespace App\Listeners;

use App\Events\TaskFilesUpdated;
use App\Models\File;
use App\Notifications\Tasks\FilesAddedNotification;
use Illuminate\Support\Facades\Notification;

class SendTaskFilesUpdatedNotifications
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TaskFilesUpdated  $event
     * @return void
     */
    public function handle(TaskFilesUpdated $event)
    {
        if (!$event->task) {
            return;
        }

        if (!$event->changes) {
            return;
        }

        // Get added Files.
        $addedFiles = collect();
        foreach ($event->changes['attached'] as $fileID) {
            $file = File::find($fileID);
            if (!$file) {
                continue;
            }
            $addedFiles->push($file);
        }

        if ($addedFiles->count() > 0) {
            // Notify Users.
            $users = $event->task->users->merge($event->task->responsible()->get());
            Notification::send($users, new FilesAddedNotification($event->task, $addedFiles));
        }
    }
}
