<?php

namespace App\Listeners;

use App\Events\UsersRemovedFromEvent;
use App\Events\UsersRemovedFromUserGroup;
use App\Models\User;
use Carbon\Carbon;
use Carbon\Traits\Date;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

class CheckUserEventRegistrations implements shouldQueue
{
    /**
     * Handle the UsersRemovedFromEvent Event.
     *
     * @param  UsersRemovedFromEvent $event
     */
    public function handleEvent(UsersRemovedFromEvent $event)
    {
        $this->handle($event->users);
    }

    /**
     * Handle the UsersRemovedFromEvent Event.
     *
     * @param  UsersRemovedFromEvent $event
     */
    public function handleUserGroup(UsersRemovedFromUserGroup $event)
    {
        $this->handle($event->users);
    }

    /**
     * Handle checking the Users
     *
     * @param  \Illuminate\Support\Collection $users
     */
    public function handle(Collection $users)
    {
        foreach ($users as $user) {
            $events = $user->registeredEvents;
            $this->detachUninvitedEvents($user, $events);
        }
    }

    /**
     * Detach all Event registrations where the User is not invited.
     *
     * @param \App\Models\User $user
     * @param \Illuminate\Support\Collection $events
     */
    private function detachUninvitedEvents(User $user, Collection $events)
    {
        foreach ($events as $event) {
            if (!$user->isInvitedToEvent($event)) {
                $user->eventRegistrations()->whereHas(
                    'dateOption',
                    function (Builder $query) use ($event) {
                        $query->where('event_id', $event->id)
                            ->where('date', '>=', Carbon::tomorrow());
                    }
                )->delete();
            }
        }
    }
}
