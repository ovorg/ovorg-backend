<?php

namespace App\Listeners;

use App\Events\UsersRemovedFromMeeting;
use App\Events\UsersRemovedFromUserGroup;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;

class CheckUserMeetingRegistrations
{

    /**
     * Handle the UsersRemovedFromMeeting Event.
     *
     * @param  UsersRemovedFromMeeting $event
     */
    public function handleMeeting(UsersRemovedFromMeeting $event)
    {
        $this->handle($event->users);
    }

    /**
     * Handle the UsersRemovedFromMeeting Event.
     *
     * @param  UsersRemovedFromUserGroup $event
     */
    public function handleUserGroup(UsersRemovedFromUserGroup $event)
    {
        $this->handle($event->users);
    }

    /**
     * Handle checking the Users
     *
     * @param  \Illuminate\Support\Collection $users
     */
    public function handle(Collection $users)
    {
        foreach ($users as $user) {
            $meetings = $user->registeredMeetings()->where('start', '>', Carbon::now())->get();
            $this->detachUninvitedMeetings($user, $meetings);
        }
    }

    /**
     * Detach all Meeting registrations where the User is not invited.
     *
     * @param \App\Models\User $user
     * @param \Illuminate\Support\Collection $meetings
     */
    private function detachUninvitedMeetings(User $user, Collection $meetings)
    {
        foreach ($meetings as $meeting) {
            if (!$user->isInvitedToMeeting($meeting)) {
                $user->registeredMeetings()->detach($meeting);
            }
        }
    }
}
