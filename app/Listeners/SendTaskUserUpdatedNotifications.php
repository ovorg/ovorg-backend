<?php

namespace App\Listeners;

use App\Events\TaskUsersUpdated;
use App\Models\User;
use App\Notifications\Tasks\SelfAddedNotification;
use App\Notifications\Tasks\UsersAddedNotification;
use App\Notifications\Tasks\UsersRemovedNotification;
use App\Notifications\Tasks\UsersUpdatedNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Notification;

class SendTaskUserUpdatedNotifications implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\TaskUsersUpdated $event
     * @return void
     */
    public function handle(TaskUsersUpdated $event)
    {
        if (!$event->task) {
            return;
        }

        if (!$event->changes) {
            return;
        }

        // Get added Users.
        $addedUsers = collect();
        foreach ($event->changes['attached'] as $userID) {
            $user = User::find($userID);
            if (!$user) {
                continue;
            }
            $addedUsers->push($user);
        }

        // Get removed Users.
        $removedUsers = collect();
        foreach ($event->changes['detached'] as $userID) {
            $user = User::find($userID);
            if (!$user) {
                continue;
            }
            $removedUsers->push($user);
        }

        // Notify Users.
        $this->notifyAddedUsers($event, $addedUsers);
        $this->notifyResponsible($event, $addedUsers, $removedUsers);
    }

    /**
     * Notify users that they have been added.
     *
     * @param  \App\Events\TaskUsersUpdated $event
     * @param  \Illuminate\Support\Collection $addedUsers
     * @return void
     */
    public function notifyAddedUsers(TaskUsersUpdated $event, Collection $addedUsers)
    {
        Notification::send($addedUsers, new SelfAddedNotification($event->task));
    }

    /**
     * Notify responsible about added and removed users.
     *
     * @param  \App\Events\TaskUsersUpdated $event
     * @param  \Illuminate\Support\Collection $addedUsers
     * @param  \Illuminate\Support\Collection $removedUsers
     * @return void
     */
    public function notifyResponsible(TaskUsersUpdated $event, Collection $addedUsers, Collection $removedUsers)
    {
        if (!$event->task->responsible) {
            return;
        }

        if ($addedUsers->count() > 0 || $removedUsers->count() > 0) {
            $event->task->responsible->notify(new UsersUpdatedNotification($event->task, $event->author, $addedUsers, $removedUsers));
        }
    }
}
