<?php

namespace App\Observers;

use App\Jobs\GenerateUserGroups;
use App\Models\SCBA;

class SCBAObserver
{
    /**
     * Handle the scba "saved" event.
     *
     * @param  \App\Models\SCBA  $sCBA
     * @return void
     */
    public function saved(SCBA $scba)
    {
        GenerateUserGroups::dispatch();
    }
}
