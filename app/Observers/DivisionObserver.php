<?php

namespace App\Observers;

use App\Jobs\GenerateUserGroups;
use App\Models\Division;
use App\Models\UserGroup;

class DivisionObserver
{
    /**
     * Handle the division "saved" event.
     *
     * @param  \App\Models\Division  $division
     * @return void
     */
    public function saved(Division $division)
    {
        GenerateUserGroups::dispatch();
    }

    /**
     * Handle the division "updated" event.
     *
     * @param  \App\Models\Division  $division
     * @return void
     */
    public function updating(Division $division)
    {
        // If the name changed, change the name of the generated user group
        $original = $division->getOriginal();
        if ($division->name == $original["name"]) {
            return;
        }

        $userGroup = UserGroup::where('generated', true)->where('name', $original["name"])->first();
        if (!$userGroup) {
            return;
        }

        $userGroup->name = $division->name;
        $userGroup->save();
    }

    /**
     * Handle the division "deleted" event.
     *
     * @param  \App\Models\Division  $division
     * @return void
     */
    public function deleted(Division $division)
    {
        // Delete generated user group
        UserGroup::where('generated', true)->where('name', $division->name)->first()->delete();
    }
}
