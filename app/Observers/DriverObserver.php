<?php

namespace App\Observers;

use App\Jobs\GenerateUserGroups;
use App\Models\Driver;

class DriverObserver
{
    /**
     * Handle the division "saved" event.
     *
     * @param  \App\Models\Driver  $driver
     * @return void
     */
    public function saved(Driver $driver)
    {
        GenerateUserGroups::dispatch();
    }
}
