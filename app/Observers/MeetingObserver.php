<?php

namespace App\Observers;

use App\Jobs\GenerateMeetingMails;
use App\Models\Meeting;

class MeetingObserver
{

    /**
     * Handle the meeting "created" event.
     *
     * @param  \App\Meeting  $meeting
     * @return void
     */
    public function created(Meeting $meeting)
    {
        GenerateMeetingMails::dispatch($meeting);
    }

    /**
     * Handle the meeting "updated" event.
     *
     * @param  \App\Meeting  $meeting
     * @return void
     */
    public function updated(Meeting $meeting)
    {
        // Update MeetingMails if $meeting->start has changed
        if (
            $meeting->start != $meeting->getOriginal()['start'] ||
            $meeting->register_until != $meeting->getOriginal()['register_until']
        ) {
            GenerateMeetingMails::dispatch($meeting);
        }
    }
}
