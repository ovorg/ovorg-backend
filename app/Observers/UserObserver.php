<?php

namespace App\Observers;

use App\Events\TimeTracking\UserPresentUpdated;
use App\Jobs\Auth\CreateActivation;
use App\Jobs\GenerateUserGroups;
use App\Models\User;
use Illuminate\Support\Str;

class UserObserver
{
    /**
     * Handle the user "creating" event.
     * Create a unique doodle_token for the User.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function creating(User $user)
    {
        // Set unique doodle_token
        do {
            $user->doodle_token = hash_hmac('sha256', Str::random(40), config('app.key'));
        } while (User::where('doodle_token', $user->doodle_token)->exists());

        // Set unique calendar_token
        do {
            $user->calendar_token = hash_hmac('sha256', Str::random(40), config('app.key'));
        } while (User::where('calendar_token', $user->calendar_token)->exists());
    }

    /**
     * Handle the user "created" event.
     * Create activation.
     *
     * @param  \App\User  $user
     */
    public function created(User $user)
    {
        CreateActivation::dispatch($user);
    }

    /**
     * Handle the user "saved" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function saved(User $user)
    {
        GenerateUserGroups::dispatch();
    }

    /**
     * Handle the user "updated" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function updated(User $user)
    {
        if ($user->isDirty('present')) {
            event(new UserPresentUpdated($user));
        }
    }
}
