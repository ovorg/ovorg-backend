<?php

namespace App\Observers;

use App\Jobs\SendEventNotification;
use App\Models\Event;

class EventObserver
{
    /**
     * Handle the event "created" event.
     *
     * @param  \App\Models\Event  $event
     * @return void
     */
    public function created(Event $event)
    {
        SendEventNotification::dispatch($event)->delay(now()->addMinutes(5));
    }
}
