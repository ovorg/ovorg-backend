<?php

namespace App\Observers;

use App\Models\Position;
use App\Models\Task;
use App\Models\User;
use App\Notifications\Tasks\AttributeChangedNotification;
use App\Notifications\Tasks\NewTaskNotification;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Notification;

class TaskObserver
{

    /**
     * Handle the task "created" event.
     *
     * @param  \App\Models\Task  $task
     * @return void
     */
    public function created(Task $task)
    {
        /** Since $task->users is a many to many relation,
         *  the task cannot have any users who could be notified here.
         *  Only the responsible and (if selected) the executives can be notified here.
         */

        $users = collect();

        // Add responsible
        if ($task->responsible) {
            $users->push($task->responsible);
        }

        // Add executives
        if ($task->inform_executives) {
            $users = $users->merge($this->getExecutives());
        }

        Notification::send($users, new NewTaskNotification($task));
    }

    /**
     * Handle the meeting "updated" event.
     *
     * @param  \App\Models\Task  $task
     * @return void
     */
    public function updated(Task $task)
    {
        // Get all users
        $users = $task->getAllParticipants();

        // Add old responsible if responsible changed
        if (array_key_exists("responsible_id", $task->getChanges())) {
            $oldResponsible = User::find($task->getOriginal("responsible_id"));
            if ($oldResponsible) {
                $users->push($oldResponsible);
            }
        }

        // Add executives to $users collection
        if ($task->inform_executives) {
            $users = $users->merge($this->getExecutives());
        }

        // Remove duplicate entries
        $users = $users->unique('id');

        Notification::send($users, new AttributeChangedNotification($task, auth()->user()));
    }

    /**
     * Return executives that should be notified
     * if inform_executives is set to true.
     * Returned collection could contain user multiple times.
     *
     * @return \Illuminate\Support\Collection
     */
    private function getExecutives(): Collection
    {
        $users = collect();

        // "Truppführer"
        $position = Position::where('male', 'Truppführer')->first();
        if ($position) {
            $users = $users->merge($position->users()->get());
        }

        // "Gruppenführer"
        $position = Position::where('male', 'Gruppenführer')->first();
        if ($position) {
            $users = $users->merge($position->users()->get());
        }

        // "Zugführer"
        $position = Position::where('male', 'Zugführer')->first();
        if ($position) {
            $users = $users->merge($position->users()->get());
        }

        // "Ortsbeauftragter"
        $position = Position::where('male', 'Ortsbeauftragter')->first();
        if ($position) {
            $users = $users->merge($position->users()->get());
        }

        // "Stellv. OB"
        $position = Position::where('male', 'Stellv. OB')->first();
        if ($position) {
            $users = $users->merge($position->users()->get());
        }

        return $users;
    }

    /**
     * Handle the task "deleting" event.
     *
     * @param  \App\Models\Task  $task
     * @return void
     */
    public function deleting(Task $task)
    {
        // Delete all Files attached to the Task
        foreach ($task->files as $file) {
            $file->delete();
        }
    }
}
