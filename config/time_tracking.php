<?php
return [
    // How long should it not be possible to be scanned again.
    'timeout' => env("SCAN_TIMEOUT", 60),
];
