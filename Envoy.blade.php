@servers(['server' => "$ssh_user@$ssh_ip"])

@setup
    $release = date('Y.m.d.H.i.s');
    $app_dir = '~/ovorg-backend';
    if ($dev) {
        $app_dir = $app_dir.'-dev';
    }
    $release_dir = $app_dir.'/releases';
    $new_release_dir = $release_dir.'/'.$release;
    $prev_link = $app_dir.'/prev';
    $curr_link = $app_dir.'/current';
    $storage_path = $app_dir.'/storage';

    $branch = isset($branch) ? $branch : 'master';

    $err_msg = "Error while deploying ";
    if ($dev) {
        $msg = "Staging backend";
        $err_msg = $err_msg."staging backend";
    } else {
        $msg = "Backend";
        $err_msg = $err_msg."backend";
    }
    $msg = $msg." has been deployed.";

    $branch_msg = "\nBranch: $branch";
    if ($commit) {
        $commit_msg = "\nCommit: $commit";
    } else {
        $commit_msg = "";
    }

    $error_message = "```\n".$err_msg.$branch_msg.$commit_msg."\n```";
    $success_message = "```\n".$msg.$branch_msg.$commit_msg."\n```";
@endsetup

# Create folder structure and copy .env
# Do not build the application
@story('init')
    create_dirs
    clone_repo
    move_storage
    create_env
    link_env
    composer_install
    generate_key
    delete_release
@endstory

# Create and link a new release
@story('deploy')
    cd
    clone_repo
    set_permissions
    link_env
    link_storage
    composer_install
    delete_prev
    create_prev
    down
    publish
    migrate
    up
    cache
    restart_worker
@endstory

@task('composer_install')
    echo "Installing dependencies..."
    cd {{ $new_release_dir }}
    composer install --prefer-dist --no-scripts -q -o --optimize-autoloader --no-dev
@endtask

@task('down')
    php {{ $new_release_dir }}/artisan down --retry=5
@endtask

@task('up')
    php {{ $new_release_dir }}/artisan up
@endtask

@task('migrate')
    php {{ $new_release_dir }}/artisan migrate --force 
@endtask

@task('cache')
    echo "Caching..."
    php {{ $new_release_dir }}/artisan config:clear
    php {{ $new_release_dir }}/artisan clear-compiled
    php {{ $new_release_dir }}/artisan cache:clear
    php {{ $new_release_dir }}/artisan config:cache
    php {{ $new_release_dir }}/artisan route:cache
    php {{ $new_release_dir }}/artisan event:cache
    php {{ $new_release_dir }}/artisan view:cache
@endtask

@task('set_permissions')
    echo "Setting file permissions..."
    find {{ $new_release_dir }} -type f -exec chmod 664 {} \;
    find {{ $new_release_dir }} -type d -exec chmod 775 {} \;
    chmod -R ug+rwx {{ $new_release_dir }}/bootstrap/cache
@endtask

@task('generate_key')
    echo "Generating application key..."
    php {{ $new_release_dir }}/artisan key:generate
@endtask

@task('move_storage')
    echo "Moving storage..."
    mv {{ $new_release_dir }}/storage {{ $app_dir }}/storage
    chmod -R ug+rwx {{ $app_dir }}/storage
@endtask

@task('publish')
    echo "Publishing application..."
    ln -nfs {{ $new_release_dir }} {{ $curr_link }}
@endtask

@task('link_storage')
    echo "Linking storage..."
    rm -rf {{ $new_release_dir }}/storage
    ln -nfs {{ $app_dir }}/storage {{ $new_release_dir }}/storage
@endtask

@task('link_env')
    echo "Linking .env..."
    ln -nfs {{ $app_dir }}/.env {{ $new_release_dir }}/.env
@endtask

@task('delete_prev')
    if [ -d $(readlink -f {{ $prev_link }} ) ]; then
        rm -rf $(readlink -f {{ $prev_link }})
        echo "Old backup deleted."
    fi
@endtask

@task('create_prev')
    if [ -d $(readlink -f {{ $curr_link }} ) ]; then
        ln -nfs $(readlink -f {{ $curr_link }}) {{ $prev_link }}
        echo "Backup created."
    fi
@endtask

@task('cd')
    if [ -d {{ $app_dir }} ]; then
        cd {{ $app_dir }}
        echo "Deploying application..."
    else
        echo "Directory does not exist. Aborting..."
        echo "Hint: Run init task."
        false
    fi
@endtask

@task('create_dirs')
    if [ -d {{ $app_dir }} ]; then
        echo "App directory already exists. Aborting..."
        false
    else
        echo "Creating app directory..."
        mkdir -p -m=711 {{ $release_dir }}
    fi
@endtask

@task('clone_repo')
    echo "Cloning repository..."

    git clone {{ $repo_url }} --branch={{ $branch }} {{ $new_release_dir }}

    @if($commit)
        cd {{ $new_release_dir }}
        git reset --hard {{ $commit }}
    @endif
@endtask

@task('create_env')
    echo "Creating .env..."
    cp {{ $new_release_dir }}/.env.example {{ $app_dir }}/.env
@endtask

@task('delete_release')
    echo "Deleting repository..."
    rm -r {{ $new_release_dir }}
@endtask

@task('restart_worker')
    echo "Restarting queue worker..."
    php {{ $new_release_dir }}/artisan queue:restart
@endtask

@finished
    if(isset($discord_webhook)) {
        @discord($discord_webhook, $success_message)
    }
@endfinished

@error
    if(isset($discord_webhook)) {
        @discord($discord_webhook, $error_message)
    }

    exit;
@enderror
