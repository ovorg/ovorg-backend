<?php

namespace Database\Seeders;

use App\Models\Driver;
use App\Models\NotificationPreference;
use App\Models\SCBA;
use App\Models\User;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create activated test user with all permissions.
        $user = User::factory()->count(1)->create(['username' => 'test'])->first();
        $user->permissions = [
            'roles.general'                => true,
            'users.general'                => true,
            'user_groups.general'          => true,
            'positions.general'            => true,
            'divisions.general'            => true,
            'meetings.general'             => true,
            'tasks.general'                => true,
            'notifications.general'        => true,
            'notifications.administration' => true,
            'qualifications.general'       => true,
        ];
        $user->save();
        $act = Activation::create($user);
        Activation::complete($user, $act->code);

        User::factory()->count(100)->create()->each(function ($user) {
            if (rand() % 3 == 0) {
                SCBA::factory()->count(1)->create(['user_id' => $user->id]);
            }
            if (rand() % 3 == 0) {
                Driver::factory()->count(1)->create(['user_id' => $user->id]);
            }
            NotificationPreference::factory()->create(['user_id' => $user->id]);
        });
    }
}
