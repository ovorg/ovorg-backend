<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Bus;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Don't execute any jobs during seeding.
        Bus::fake();

        $this->call(DivisionTableSeeder::class);
        $this->call(MeetingTableSeeder::class);
        $this->call(PositionTableSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(TaskCategoryTableSeeder::class);
        $this->call(TaskTableSeeder::class);
        $this->call(UserGroupTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(EventSeeder::class);
    }
}
