<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->bigIncrements('id')->unique();
            $table->string('title');
            $table->text('description')->nullable();
            $table->text('addition')->nullable();
            $table->date('duedate')->nullable();
            $table->unsignedTinyInteger('priority')->default(0);
            $table->unsignedTinyInteger('state')->default(0);
            $table->unsignedBigInteger('responsible_id')->nullable();
            $table->unsignedBigInteger('creator_id')->nullable();
            $table->bigInteger('task_category_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('responsible_id')
                ->references('id')->on('users')
                ->onDelete('Set Null');
            $table->foreign('creator_id')
                ->references('id')->on('users')
                ->onDelete('Set Null');
            $table->foreign('task_category_id')
                ->references('id')->on('task_categories')
                ->onDelete('Set Null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
