<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSCBASTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scbas', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')->primary();
            $table->date('next_examination')->nullable();
            $table->date('last_instruction')->nullable();
            $table->date('last_excercise')->nullable();
            $table->date('last_deployment')->nullable();
            $table->boolean('cbrn')->nullable();
            $table->date('last_cbrn_deployment')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scbas');
    }
}
