<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ExtendUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->char('thwin_id', 8)->after('calendar_token')->unique()->nullable();
            $table->enum('gender', ['m', 'f', 'x'])->after('thwin_id');
            $table->date('birth')->nullable()->after('gender');
            $table->boolean('vegetarian')->default(false)->index()->after('birth');
            $table->mediumText('health_information')->nullable()->after('vegetarian');
            $table->string('mobile', 16)->nullable()->unique()->after('health_information');
            $table->string('phone', 16)->nullable()->after('mobile');
            $table->unsignedBigInteger('position_id')->nullable()->index()->after('phone');
            $table->unsignedBigInteger('division_id')->nullable()->index()->after('position_id');
            $table->boolean('present')->index()->default(false)->after('division_id');
            $table->timestamp('scan_timeout')->nullable()->after('present');

            $table->foreign('position_id')->references('id')->on('positions')->onDelete('set null');
            $table->foreign('division_id')->references('id')->on('divisions')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('thwin_id');
            $table->dropColumn('gender');
            $table->dropColumn('birth');
            $table->dropColumn('vegetarian');
            $table->dropColumn('health_information');
            $table->dropColumn('mobile');
            $table->dropColumn('phone');
            $table->dropForeign(['position_id']);
            $table->dropColumn('position_id');
            $table->dropForeign(['division_id']);
            $table->dropColumn('division_id');
            $table->dropColumn('present');
            $table->dropColumn('scan_timeout');
        });
    }
}
