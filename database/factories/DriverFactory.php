<?php

namespace Database\Factories;

use App\Models\Driver;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class DriverFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Driver::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'b'           => true,
            'be'          => $this->faker->boolean(50),
            'c'           => $this->faker->boolean(30),
            'ce'          => $this->faker->boolean(20),
            'expiry_date' => Carbon::parse(
                $this->faker->dateTimeBetween('-1 years', '+5 years')
            )->toDateString(),
        ];
    }
}
