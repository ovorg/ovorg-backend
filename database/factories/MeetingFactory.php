<?php

namespace Database\Factories;

use App\Models\Meeting;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class MeetingFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Meeting::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $start = $this->faker->dateTimeInInterval('now', '+1 years');
        return [
            'title'          => $this->faker->words($this->faker->numberBetween(1, 5), true),
            'description'    => "<p>{$this->faker->text}</p>",
            'start'          => $start,
            'end'            => $this->faker->dateTimeInInterval($start, '+8 hours'),
            'register_until' => $this->faker->boolean(75) ? null : Carbon::parse(
                $this->faker->dateTimeInInterval($start, '-5 days')
            )->toDateString(),
            'comment'        => $this->faker->boolean(75),
        ];
    }
}
