<?php

namespace Database\Factories;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'first_name'         => $this->faker->firstName,
            'last_name'          => $this->faker->lastName,
            'username'           => $this->faker->unique()->username,
            'email'              => $this->faker->unique()->safeEmail,
            'password'           => Hash::make('123'),
            'thwin_id'           => strval(
                $this->faker->unique()->randomNumber(8, true)
            ),
            'gender'             => $this->faker->randomElement([
                'f', 'm', 'x',
            ]),
            'birth'              => Carbon::parse(
                $this->faker->dateTimeBetween('-80 years', '-10 years')
            )->toDateString(),
            'vegetarian'         => $this->faker->boolean(10),
            'health_information' => $this->faker->sentence,
            'mobile'             => $this->faker->regexify('^(\+\d{6,15})$'),

            'phone'              => "{$this->faker->randomNumber(9)}",
            'present'            => $this->faker->boolean(10),
            'scan_timeout'       => $this->faker->dateTimeBetween('-7 days', '+5 minutes'),
            'doodle_token'       => $this->faker->unique()->uuid,
            'calendar_token'     => $this->faker->unique()->uuid,
        ];
    }
}
