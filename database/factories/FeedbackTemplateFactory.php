<?php

namespace Database\Factories;

use App\Models\FeedbackTemplate;
use Illuminate\Database\Eloquent\Factories\Factory;

class FeedbackTemplateFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = FeedbackTemplate::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'     => $this->faker->unique()->word,
            'template' => $this->faker->text,
        ];
    }
}
