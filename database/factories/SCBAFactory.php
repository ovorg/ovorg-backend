<?php

namespace Database\Factories;

use App\Models\SCBA;
use Illuminate\Database\Eloquent\Factories\Factory;

class SCBAFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SCBA::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'next_examination'     => $this->faker->dateTimeBetween('-1 years', '+3 years'),
            'last_instruction'     => $this->faker->dateTimeBetween('-2 years', 'now'),
            'last_excercise'       => $this->faker->dateTimeBetween('-2 years', 'now'),
            'last_deployment'      => $this->faker->dateTimeBetween('-2 years', 'now'),
            'cbrn'                 => $this->faker->boolean,
            'last_cbrn_deployment' => $this->faker->dateTimeBetween('-2 years', 'now'),
        ];
    }
}
