<?php

namespace Database\Factories;

use App\Models\OrderItem;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderItemFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = OrderItem::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'           => $this->faker->word,
            'price'          => $this->faker->randomFloat(2, 0, 999999),
            'count'          => $this->faker->randomNumber(),
            'url'            => $this->faker->url,
            'article_number' => $this->faker->word,
            'description'    => $this->faker->randomHtml(),
            'ordered'        => $this->faker->boolean,
        ];
    }
}
