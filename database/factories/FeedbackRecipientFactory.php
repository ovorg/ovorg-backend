<?php

namespace Database\Factories;

use App\Models\FeedbackRecipient;
use Illuminate\Database\Eloquent\Factories\Factory;

class FeedbackRecipientFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = FeedbackRecipient::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'email' => $this->faker->unique()->safeEmail,
        ];
    }
}
