<?php

namespace Database\Factories;

use App\Models\Task;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class TaskFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Task::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title'             => $this->faker->words($this->faker->numberBetween(1, 5), true),
            'description'       => "<p>{$this->faker->text}</p>",
            'addition'          => "<p>{$this->faker->text}</p>",
            'duedate'           => Carbon::parse($this->faker->dateTimeBetween('-1 years', '+3 years'))->toDateString(),
            'priority'          => $this->faker->numberBetween(0, 255),
            'state'             => $this->faker->numberBetween(0, 100),
            'inform_executives' => $this->faker->boolean,
        ];
    }
}
