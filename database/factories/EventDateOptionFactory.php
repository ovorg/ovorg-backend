<?php

namespace Database\Factories;

use App\Models\EventDateOption;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class EventDateOptionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EventDateOption::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'date' => Carbon::parse($this->faker->dateTimeInInterval('-1 years', '+2 years'))->toDateString(),
        ];
    }
}
