FROM php:7.4-fpm-alpine

RUN mkdir -p /var/app
WORKDIR /var/app

RUN apk add --no-cache \
      libpng-dev \
      libjpeg-turbo \
      libjpeg-turbo-dev && \
    docker-php-ext-configure gd --with-jpeg && \
    docker-php-ext-install \
	    pdo_mysql \
      gd
