<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

// Calendar

use App\Http\Controllers\CalendarController;

Route::get('/calendar/{calendar_token}', [CalendarController::class, 'calendar'])->name('calendar');
