<?php

use App\Http\Controllers\Admin\FailedJobController;
use App\Http\Controllers\Admin\FeedbackRecipientController;
use App\Http\Controllers\Admin\FeedbackTemplateController;
use App\Http\Controllers\Admin\OAuthClientController;
use App\Http\Controllers\Admin\UserActivationController;
use App\Http\Controllers\AuthenticationController;
use App\Http\Controllers\DivisionController;
use App\Http\Controllers\DoodleController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\EventDoodleController;
use App\Http\Controllers\FallbackController;
use App\Http\Controllers\FeedbackController;
use App\Http\Controllers\FileController;
use App\Http\Controllers\MeetingController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\NotificationPreferenceController;
use App\Http\Controllers\OrderItemController;
use App\Http\Controllers\PositionController;
use App\Http\Controllers\QualificationController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\ShopController;
use App\Http\Controllers\TaskCategoryController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\TimeTrackingController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserGroupController;
use App\Http\Controllers\UserPresenceController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

// Routes for users
Route::middleware(['auth:api', 'scopes:app'])->group(function () {
    // Auth
    Route::post('/logout', [AuthenticationController::class, 'logout'])->name('logout');
    Route::post('/change_password', [AuthenticationController::class, 'changePassword'])->name('change_password');
    Route::get('/logged_in', [AuthenticationController::class, 'loggedIn'])->name('logged_in');

    // Users
    Route::name('users.')->group(function () {
        Route::get('/me', [UserController::class, 'me'])->name('me');
        Route::get('/users_simple', [UserController::class, 'simplifiedIndex'])->name('simplified_index');
        Route::post('/self_update', [UserController::class, 'selfUpdate'])->name('self_update');
    });

    // Meetings
    Route::name('meetings.')->group(function () {
        Route::get('/meetings_archive', [MeetingController::class, 'archive'])->name('archive');
        Route::get('/meetings/advanced/{meeting}', [MeetingController::class, 'showEnhanced'])->name('show_enhanced');
    });

    // Doodle
    Route::name('doodle.')->group(function () {
        Route::get('/doodle', [DoodleController::class, 'index'])->name('index');
        Route::get('/doodle/{meeting}', [DoodleController::class, 'show'])->name('show');
        Route::put('/doodle/{meeting}', [DoodleController::class, 'update'])->name('update');
        Route::get('/doodle_archive', [DoodleController::class, 'archive'])->name('archive');
    });

    // Qualifications
    Route::name('qualifications.')->group(function () {
        Route::get('/qualification/scba', [QualificationController::class, 'scba'])->name('scba');
        Route::get('/qualification/driver', [QualificationController::class, 'driver'])->name('driver');
        Route::post('qualification/scba', [QualificationController::class, 'updateSCBAs'])->name('scba.update');
        Route::post('qualification/driver', [QualificationController::class, 'updateDrivers'])->name('driver.update');
    });

    // Notifications
    Route::name('notifications.')->group(function () {
        Route::post('/notifications/custom', [NotificationController::class, 'custom'])->name('custom');
        Route::post('/notifications/activation/{user}', [NotificationController::class, 'activation'])->name('activation');
        Route::post('/notifications/reminder/{user}', [NotificationController::class, 'reminder'])->name('reminder');
        Route::post('/notifications/meeting/{meeting}', [NotificationController::class, 'meeting'])->name('meeting');
        Route::post('/notifications/event/{event}', [NotificationController::class, 'event'])->name('event');
    });

    // Tasks
    Route::name('tasks.')->group(function () {
        Route::get('tasks_archive', [TaskController::class, 'archive'])->name('archive');
        Route::patch('tasks_light/{task}', [TaskController::class, 'lightUpdate'])->name('light.update');
    });

    // OrderItems
    Route::name('order_items.')->group(function () {
        Route::get('order_items_archive', [OrderItemController::class, 'archive'])->name('archive');
    });

    // Feedback
    Route::post('/feedback', [FeedbackController::class, 'send'])->name('feedback.send');

    // Failed jobs
    Route::delete('admin/failed_jobs/flush', [FailedJobController::class, 'flush'])->name('failed_jobs.flush');
    Route::apiResource('admin/failed_jobs', FailedJobController::class)->except(['store', 'update']);

    // User Activations
    Route::apiResource('admin/activations', UserActivationController::class)->parameter('activations', 'user')->only(['index', 'show']);

    // User presence
    Route::apiResource('/presence', UserPresenceController::class)->parameter('presence', 'user')->except(['store', 'destroy']);

    // Event Doodle
    Route::name('event_doodle.')->group(function () {
        Route::get('/event_doodle', [EventDoodleController::class, 'index'])->name('index');
        Route::get('/event_doodle/{event}', [EventDoodleController::class, 'show'])->name('show');
        Route::put('/event_doodle/{event}', [EventDoodleController::class, 'update'])->name('update');
        Route::get('/event_doodle_archive', [EventDoodleController::class, 'archive'])->name('archive');
    });

    // Events
    Route::get('/events/advanced/{event}', [EventController::class, 'showEnhanced'])->name('events.show_enhanced');

    // Notification Preference
    Route::name('notification_preference.')->group(function () {
        Route::get('/notification_preference', [NotificationPreferenceController::class, 'show'])->name('show');
        Route::put('/notification_preference', [NotificationPreferenceController::class, 'update'])->name('update');
    });

    Route::apiResources([
        '/roles'                     => RoleController::class,
        '/users'                     => UserController::class,
        '/divisions'                 => DivisionController::class,
        '/positions'                 => PositionController::class,
        '/user_groups'               => UserGroupController::class,
        '/meetings'                  => MeetingController::class,
        '/files'                     => FileController::class,
        '/tasks'                     => TaskController::class,
        '/task_categories'           => TaskCategoryController::class,
        '/shops'                     => ShopController::class,
        '/order_items'               => OrderItemController::class,
        '/admin/feedback_recipients' => FeedbackRecipientController::class,
        '/admin/oauth_clients'       => OAuthClientController::class,
        '/admin/feedback_templates'  => FeedbackTemplateController::class,
        '/events'                    => EventController::class,
    ]);
});

// Routes for guests
Route::middleware(['guest'])->group(function () {
    // Auth
    Route::post('/login', [AuthenticationController::class, 'login'])->name('login');
    Route::post('/activate', [AuthenticationController::class, 'activate'])->name('activate');
    Route::post('/request_password_reset', [AuthenticationController::class, 'requestPasswordReset'])->name('request_password_reset');
    Route::post('/reset_password', [AuthenticationController::class, 'resetPassword'])->name('reset_password');
});

// Routes for clients

// Time tracking
Route::name('time_tracking.')->middleware(['auth.client:time_tracking'])->group(function () {
    Route::patch('/time_tracking/scanned', [TimeTrackingController::class, 'scanned'])->name('scanned');
    Route::get('/time_tracking', [TimeTrackingController::class, 'index'])->name('index');
    Route::get('/time_tracking/{user}', [TimeTrackingController::class, 'show'])->name('show');
});

// Routes for everyone

// Auth
Route::post('/refresh', [AuthenticationController::class, 'refresh'])->name('refresh');

// Doodle
Route::name('doodle.')->group(function () {
    Route::get('/token_doodle/{token}', [DoodleController::class, 'indexToken'])->name('index_token');
    Route::get('/token_doodle/{meeting}/{token}', [DoodleController::class, 'showToken'])->name('show_token');
    Route::put('/token_doodle/{meeting}/{token}', [DoodleController::class, 'updateToken'])->name('update_token');
    Route::get('/token_doodle_archive/{token}', [DoodleController::class, 'archiveToken'])->name('archive_token');
});

// Event Doodle
Route::name('event_doodle.')->group(function () {
    Route::get('/token_event_doodle/{token}', [EventDoodleController::class, 'indexToken'])->name('index_token');
    Route::get('/token_event_doodle/{event}/{token}', [EventDoodleController::class, 'showToken'])->name('show_token');
    Route::put('/token_event_doodle/{event}/{token}', [EventDoodleController::class, 'updateToken'])->name('update_token');
    Route::get('/token_event_doodle_archive/{token}', [EventDoodleController::class, 'archiveToken'])->name('archive_token');
});

// Fallback route
Route::fallback([FallbackController::class, 'fallback'])->name('fallback');
