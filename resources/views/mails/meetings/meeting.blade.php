@component('mail::panel')
**{{$meeting->title}}**<br>
{{ $meeting->start->isoFormat('ddd D. MMM. YYYY H:mm')}}
{{ $meeting->start->isSameDay($meeting->end) ?
    '\-'.$meeting->end->isoFormat(' H:mm [Uhr]') : 'Uhr \- '.$meeting->end->isoFormat('ddd D. MMM. YYYY H:mm [Uhr]') }}<br>
@if ($meeting->register_until)
Anmeldung möglich bis: {{ $meeting->register_until->isoFormat('ddd D. MMM. YYYY') }}
@endif

{!! clean($meeting->description) !!}
@endcomponent

@component('mail::button', ['url' => config('app.frontend_url')."/doodle/$meeting->id/$user->doodle_token"])
Dienst anzeigen
@endcomponent
