@component('mail::message', ['unsubscribe' => isset($unsubscribe) ? $unsubscribe : false, 'url' => '#'])
Hallo {{$user->first_name}},<br>
bitte trage Dich zu den folgenden Diensten ein.

@for($i=0; $i < count($meetings); $i++)
@include('mails.meetings.meeting', ['meeting' => $meetings[$i]])
@if($i < count($meetings)-1)
<hr>
@endif
@endfor

Grüße,<br>
{{ config('app.organisation_name') }}
@endcomponent
