@component('mail::message')
Hallo {{ $user->first_name }},<br>
im {{ $meetings->first()->start->isoFormat('MMMM') }} gibt es folgende Dienste:

@foreach ($meetings as $meeting)
**{{ $meeting->start->isoFormat('ddd D. MMM.  H:mm [Uhr]') }}**
@if(!$meeting->start->isSameDay($meeting->end))
**bis {{ $meeting->end->isoFormat('ddd D. MMM.  H:mm [Uhr]') }}**
@endif
<a href="{{ config('app.frontend_url')."/doodle/$meeting->id/$user->doodle_token" }}" class="button button-small">&Ouml;ffnen</a><br>
{{ $meeting->title }}<br><br>
@endforeach

@endcomponent
