@component('mail::message', ['unsubscribe' => isset($unsubscribe) ? $unsubscribe : false, 'url' => '#'])
Hallo {{$user->first_name}},<br>
bitte trage dich zu diesem Dienst ein.

@include('mails.meetings.meeting', ['meeting' => $meeting])

Grüße,<br>
{{ config('app.organisation_name') }}
@endcomponent
