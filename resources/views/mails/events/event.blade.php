@component('mail::message', ['unsubscribe' => isset($unsubscribe) ? $unsubscribe : false, 'url' => '#'])
Hallo {{$user->first_name}},<br>
bitte trage Dich in folgende Verfügbarkeitsabfrage ein.

@component('mail::panel')
**{{ $event->title }}**<br>
{!! clean($event->description) !!}
@endcomponent

@component('mail::button', ['url' => config('app.frontend_url')."/abfragen/$event->id/$user->doodle_token"])
Verfügbarkeitsabfrage anzeigen
@endcomponent

@endcomponent
