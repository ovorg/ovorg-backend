@component('mail::message')
Hallo {{ $user->first_name }},

Du hast folgende Mail versendet:
## {{ $subject }}
{!! clean($body) !!}
<br>

## Empfänger:
@if(!$meetings->isEmpty())
**Dienste:** {{ $meetings->implode(', ') }}<br>
*mit den Anwesenheiten:* {{ $attendance->implode(', ') }}<br>
@endif
@if(!$userGroups->isEmpty())
**Benutzergruppen:** {{ $userGroups->implode(', ') }}<br>
@endif
**Helfer:** {{ $users->implode(', ') }}
@endcomponent
