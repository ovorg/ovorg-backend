@component('mail::message')
# Passwort zurücksetzten

Hier ist dein angeforderter Link zum Zurücksetzten des Passworts.<br>
Dein Benutzername lautet: {{ $user->username }}

@component('mail::button', ['url' => config('app.frontend_url')."/passwort_zuruecksetzen/$reminder->user_id/$reminder->code"])
Passwort zurücksetzten
@endcomponent

Grüße,<br>
{{ config('app.organisation_name') }}
@endcomponent
