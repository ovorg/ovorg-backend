@component('mail::message')
# Benutzerkonto aktivieren

Klicke auf diesen Link um dein Benutzerkonto zu aktivieren. Wenn du dein Konto nicht aktivierst, kannst du dich nicht anmelden.

@component('mail::button', ['url' => config('app.frontend_url')."/registrieren/$activation->user_id/$activation->code"])
Benutzerkonto aktivieren
@endcomponent

Grüße,<br>
{{ config('app.organisation_name') }}
@endcomponent
