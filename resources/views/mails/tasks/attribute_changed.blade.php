@component('mail::message')
Hallo {{ $user->first_name }},

@if($author)
{{ $author->getFullName() }} hat eine Aufgabe bearbeitet.
@else
eine Aufgabe wurde bearbeitet.
@endif

@component('mail::panel')
# {{ $task->title }}<br>

@if(array_key_exists("title", $changes) || array_key_exists("task_category", $changes) || array_key_exists("responsible", $changes) || array_key_exists("duedate", $changes) || array_key_exists("priority", $changes) || array_key_exists("state", $changes))
@component('mail::table')
|Attribut|Neuer Wert|Alter Wert|
|:-------|:---------|:---------|
@if(array_key_exists("title", $changes))
Titel|{{ $changes["title"][0] }}|{{ $changes["title"][1] }}|
@endif
@if(array_key_exists("task_category", $changes))
Kategorie|{{ $changes["task_category"][0] }}|{{ $changes["task_category"][1] }}|
@endif
@if(array_key_exists("responsible", $changes))
Ansprechpartner|{{ $changes["responsible"][0] }}|{{ $changes["responsible"][1] }}|
@endif
@if(array_key_exists("duedate", $changes))
Fälligkeit|{{ $changes["duedate"][0]? $changes["duedate"][0]->isoFormat('ddd D. MMM. YYYY') : "" }}|{{ $changes["duedate"][1]? $changes["duedate"][1]->isoFormat('ddd D. MMM. YYYY') : "" }}|
@endif
@if(array_key_exists("priority", $changes))
Priorität|{{ $changes["priority"][0] === 0? "Normal" : "Priorisiert" }}|{{ $changes["priority"][1] === 0? "Normal" : "Priorisiert" }}|
@endif
@if(array_key_exists("state", $changes))
Status|{{ $changes["state"][0] }} %|{{ $changes["state"][1] }} %|
@endif
@endcomponent
@endif

@if(array_key_exists("description", $changes))
---
## Neue Beschreibung: {!! clean($changes["description"][0]) !!}

## Alte Beschreibung: {!! clean($changes["description"][1]) !!}
@endif

@if(array_key_exists("addition", $changes))
---
## Neu Aktuelles/Probleme: {!! clean($changes["addition"][0]) !!}

## Alt Aktuelles/Probleme: {!! clean($changes["addition"][1]) !!}
@endif

@endcomponent

@component('mail::button', ['url' => config('app.frontend_url')."/aufgaben/$task->id"])
Aufgabe anzeigen
@endcomponent

@endcomponent
