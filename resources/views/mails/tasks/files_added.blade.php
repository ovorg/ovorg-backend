@component('mail::message')
Hallo {{ $user->first_name }},

einer Aufgabe, an der Du teilnimmst, wurden Dateien hinzugefügt.

@component('mail::panel')
**{{ $task->title }}**<br>
Neue Dateien:
{{ $newFiles->map(function($file) {
    return $file->name;
})->implode(', ') }}
@endcomponent

@component('mail::button', ['url' => config('app.frontend_url')."/aufgaben/$task->id"])
Aufgabe anzeigen
@endcomponent

@endcomponent
