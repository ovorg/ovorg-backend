@component('mail::message')
Hallo {{ $user->first_name }},

@if($author)
{{ $author->getFullName()}} hat Helfer zu einer Aufgabe hinzugefügt oder entfernt.
@else
es wurden Helfer zu einer Aufgabe hinzugefügt oder entfernt.
@endif

@component('mail::panel')
**{{ $task->title }}**<br>
@if($addedUsers->count() > 0)
## Hinzugefügte Helfer
{{ $addedUsers->map(function($user) {
    return $user->getFullName();
})->implode(', ') }}
@endif

@if($removedUsers->count() > 0)
## Entfernte Helfer
{{ $removedUsers->map(function($user) {
    return $user->getFullName();
})->implode(', ') }}
@endif

@if($task->users->count() > 0)
## Aktuelle Helfer
{{ $task->users->map(function($user) {
    return $user->getFullName();
})->implode(', ') }}
@else
Aktuell sind keine Helfer in der Aufgabe eingetragen.
@endif
@endcomponent

@component('mail::button', ['url' => config('app.frontend_url')."/aufgaben/$task->id"])
Aufgabe anzeigen
@endcomponent

@endcomponent
