@component('mail::message')
Hallo {{ $user->first_name }},

Du bist der Ansprechpartner für eine neue Aufgabe,
@if($task->creator)
die von {{ $task->creator->getFullName() }} erstellt wurde.
@endif

@component('mail::panel')
**{{ $task->title }}**<br>
@if($task->taskCategory)
Kategorie: {{ $task->taskCategory->name }}<br>
@endif
@if($task->duedate)
Fälligkeit: {{$task->duedate->isoFormat('D.MM.YYYY')}}
@endif
{!! clean($task->description) !!}
@endcomponent

@component('mail::button', ['url' => config('app.frontend_url')."/aufgaben/$task->id"])
Aufgabe anzeigen
@endcomponent

@endcomponent
