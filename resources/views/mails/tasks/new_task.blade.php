@component('mail::message')
Hallo {{ $user->first_name }},

@if($task->creator)
{{ $task->creator->getFullName() }} hat eine neue Aufgabe erstellt.
@else
es gibt eine neue Aufgabe.
@endif

@component('mail::panel')
**{{ $task->title }}**<br>
@if($task->taskCategory)
Kategorie: {{ $task->taskCategory->name }}<br>
@endif
@if($task->duedate)
Fälligkeit: {{$task->duedate->isoFormat('D.MM.YYYY')}}
@endif
{!! clean($task->description) !!}
@endcomponent

@component('mail::button', ['url' => config('app.frontend_url')."/aufgaben/$task->id"])
Aufgabe anzeigen
@endcomponent

@endcomponent
