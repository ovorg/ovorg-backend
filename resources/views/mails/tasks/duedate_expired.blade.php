@component('mail::message')
Hallo {{ $user->first_name }},

das Fälligtkeitdatum für eine Aufgabe, an der Du teilnimmst, ist abgelaufen.

@component('mail::panel')
**{{ $task->title }}**<br>
{!! clean($task->description) !!}
@endcomponent

@component('mail::button', ['url' => config('app.frontend_url')."/aufgaben/$task->id"])
Aufgabe anzeigen
@endcomponent

@endcomponent
