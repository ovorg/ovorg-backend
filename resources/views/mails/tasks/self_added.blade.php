@component('mail::message')
Hallo {{ $user->first_name }},

Du wurdest zu einer Aufgabe hinzugefügt.

@component('mail::panel')
**{{ $task->title }}**<br>
@if($task->responsible)
Ansprechpartner: {{ $task->responsible->getFullName() }}
@endif
{!! clean($task->description) !!}
@endcomponent

@component('mail::button', ['url' => config('app.frontend_url')."/aufgaben/$task->id"])
Aufgabe anzeigen
@endcomponent

@endcomponent
