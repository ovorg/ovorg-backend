<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Model Language Lines
    |--------------------------------------------------------------------------
     */

    'user'                      => 'Helfer',
    'division'                  => 'Gruppe',
    'driver'                    => 'Fahrer',
    'meeting'                   => 'Dienst',
    'position'                  => 'Funktion',
    'role'                      => 'Benutzerrolle',
    'scba'                      => 'Atemschutzgeräteträger',
    'usergroup'                 => 'Helfergruppe',
    'file'                      => 'Datei',
    'meetingmail'               => 'Dienstmail',
    'privacy'                   => 'Datenschutzeinstellungen',
    'task'                      => 'Aufgabe',
    'taskcategory'              => 'Aufgaben Kategorie',
    'passportclient'            => 'Passport Client',
    'laravel\\passport\\client' => 'OAuth Client',
    'orderitem'                 => 'Bestellgegenstand',
    'shop'                      => 'Shop',
    'event'                     => 'Verfügbarkeitsabfrage',
    'eventdateoption'           => 'Mögliches Veranstaltungsdatum',
    'eventregistration'         => 'Verfügbarkeitsabfragenregistrierung',
    'file'                      => "Datei",
];
