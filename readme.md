# OVOrg Backend

This is the backend for the OVOrg system. A management system adapted to the needs of the Federal Agency for Technical Relief.

## Requirements

OVOrg is build using Laravel. You can check the requirements of Laravel in their [documentation](https://laravel.com/docs/6.x#server-requirements).<br>
You need a HTTP server to serve the application.<br>
All other dependencies can be install using [composer](https://getcomposer.org/download/) by executing `composer install` in the root folder of the project.

## Server setup

You have to make sure that both the user which is deploying the applications and the user used by the HTTP server have permissions to read and write files in the `storage` folder and all subfolders. During the deployment of the application, it is possible that something is written to these folders. One way to do this is by adding the users to each others groups and updating the default file and folder permissions so that they have read and write rights on group files.

### Envoy

The Envoy script should handle most of the setup.
To install envoy run `composer global require laravel/envoy`.
You need to have to setup your ssh config so that you can connect to your server using ssh keys.<br>
Use the `init` (`envoy run init`) story to initialize the folder strucuture. It also creates a .env file which you have to customize for your own needs. For production environments you should create a new app key using `php artisan key:generate`. After this you can use the `deploy` (`envoy run deploy`) story to deploy the application. For both stories you have to pass several arguments to envoy:
- `--ssh_user="<ssh_username>"`
- `--ssh_ip="<ssh_ip>"`
- `--repo_url="<repository_url>"`

Optionally:
- `--commit="<commit_sha>"` Default is HEAD
- `--branch="<branch/tag>"` Default ist master
- `--dev` Add "-dev" to the deployment folder
- `--discord_webhook="<discord_webhhook_url>"` Discord Webhook to notify about deployment

The deployment part can be handled by the GitLab CI/CD system.

### Queue Worker

At least one queue worker should always run in the background.
The way Laravel recommends to do this is supervisor. Here is an example config:
```
[program:laravel-worker]
process*name=%program_name)s*%(process_num)02d
command=php <path-to-project>/artisan queue:work --delay=10 --sleep=10 --tries=3
autostart=true
autorestart=true
user=<user>
numprocs=1
redirect_stderr=true
stdout_logfile=<path-to-log-file>
```
The `autorestart=true` option is important, because after every deployment the queue workers are stopped after they finished their current job and need to be restarted.

### HTTP Server

Your HTTP server should point at the `public` folder of the application.

### Passport

You can use `php artisan passport:init` to create the default clients required by the OVOrg Backend.

## License

Copyright (C) 2020 Lukas Schneider<br>
Copyright (C) 2020 Andreas Roll<br>
Copyright (C) 2020 Oliver Friedel<br>

OVOrg Backend is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see https://www.gnu.org/licenses/.
