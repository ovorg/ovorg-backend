<?php

namespace Tests\Feature;

use App\Models\Event;
use App\Models\EventDateOption;
use App\Models\EventRegistration;
use App\Models\Position;
use App\Models\User;
use App\Models\UserGroup;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Tests\Traits\DisablesAuth;

class RemoveUsersFromEventsTest extends TestCase
{
    use DisablesAuth;

    /**
     * Disable auth.
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->disableAuthentication();
        $this->disableAuthorisation();
    }

    /**
     * Test that the EventRegistrations are removed when the User is removed
     * from the UserGroup in the UserGroupController.
     */
    public function testRemoveOneFromUserGroup()
    {
        $this->withoutExceptionHandling();

        [$userGroup, $users, $event] = $this->prepareUserGroupData(2);
        $data = $this->createUserGroupRequestData($userGroup);
        $usersArray = $data["users"];
        array_shift($usersArray);
        $data["users"] = $usersArray;

        $response = $this->json('PUT', route('user_groups.update', $userGroup->id), $data);

        $response->assertStatus(200);
        $this->assertEquals(2, $event->registeredUsers()->count());
        $this->assertEquals(6, $event->registrations()->count());
    }

    /**
     * Test that the EventRegistrations for all Users are removed
     * when removing all Users from the UserGroup in the UserGroupController.
     */
    public function testRemoveAllFromUserGroup()
    {
        [$userGroup, $users, $event] = $this->prepareUserGroupData(2);
        $data = $this->createUserGroupRequestData($userGroup);
        $data["users"] = [];

        $response = $this->json('PUT', route('user_groups.update', $userGroup->id), $data);

        $response->assertStatus(200);
        foreach ($users as $user) {
            $this->assertEquals(2, $user->eventRegistrations()->count());
            foreach ($user->eventRegistrations as $eventRegistration) {
                $this->assertTrue($eventRegistration->dateOption->date < Carbon::tomorrow());
            }
        }
    }

    /**
     * Test that the EventRegistrations are removed when one UserGroup
     * is removed from the User in the UserController.
     */
    public function testRemoveOneUserGroupFromUser()
    {
        [$userGroups, $user, $event] = $this->prepareUserData(2);
        $data = $this->createUserRequestData($user);
        $userGroupsArray = $data["user_groups"];
        array_shift($userGroupsArray);
        $data["user_groups"] = $userGroupsArray;

        $response = $this->json('PUT', route('users.update', $user->id), $data);

        $response->assertStatus(200);
        $this->assertEquals(2, $user->eventRegistrations()->count());
        foreach ($user->eventRegistrations as $eventRegistration) {
            $this->assertTrue($eventRegistration->dateOption->date < Carbon::tomorrow());
        }
    }

    /**
     * Test that the EventRegistrations are removed when all UserGroups
     * are removed from the User in the UserController.
     */
    public function testRemoveAllUserGroupsFromUser()
    {
        [$userGroups, $user, $event] = $this->prepareUserData(2);
        $data = $this->createUserRequestData($user);
        $data["user_groups"] = [];

        $response = $this->json('PUT', route('users.update', $user->id), $data);

        $response->assertStatus(200);
        $this->assertEquals(2, $user->eventRegistrations()->count());
        foreach ($user->eventRegistrations as $eventRegistration) {
            $this->assertTrue($eventRegistration->dateOption->date < Carbon::tomorrow());
        }
    }

    /**
     * Test that the EventRegistrations are removed when one UserGroup
     * is removed from the Event in the EventController.
     */
    public function testRemoveOneUserGroupFromEvent()
    {
        [$userGroups, $user, $event] = $this->prepareEventUserGroupData(2);
        $data = $this->createEventRequestData($event);
        $userGroupsArray = $data["user_groups"];
        $removed = array_shift($userGroupsArray);
        $data["user_groups"] = $userGroupsArray;
        $user->userGroups()->attach($removed);

        $response = $this->json('PUT', route('events.update', $event->id), $data);

        $response->assertStatus(200);
        $this->assertEquals(2, $user->eventRegistrations()->count());
        foreach ($user->eventRegistrations as $eventRegistration) {
            $this->assertTrue($eventRegistration->dateOption->date < Carbon::tomorrow());
        }
    }

    /**
     * Test that the EventRegistrations are removed when all UserGroups
     * are removed from the Event in the EventController.
     */
    public function testRemoveAllUserGroupsFromEvent()
    {
        [$userGroups, $user, $event] = $this->prepareEventUserGroupData(2);
        $data = $this->createEventRequestData($event);
        $data["user_groups"] = [];
        $user->userGroups()->attach($userGroups);

        $response = $this->json('PUT', route('events.update', $event->id), $data);

        $response->assertStatus(200);
        $this->assertEquals(2, $user->eventRegistrations()->count());
        foreach ($user->eventRegistrations as $eventRegistration) {
            $this->assertTrue($eventRegistration->dateOption->date < Carbon::tomorrow());
        }
    }

    /**
     * Test that the EventRegistrations are removed when one User
     * is removed from the Event in the EventController.
     */
    public function testRemoveOneUserFromEvent()
    {
        [$users, $event] = $this->prepareEventUserData(2);
        $data = $this->createEventRequestData($event);
        $usersArray = $data['users'];
        $removed = array_shift($usersArray);
        $data['users'] = $usersArray;

        $response = $this->json('PUT', route('events.update', $event->id), $data);

        $response->assertStatus(200);
        $eventUser = $event->users();
        $this->assertEquals(1, $eventUser->count());
        $eventUser = $eventUser->first();

        $removedUser = User::find($removed);
        $this->assertNotNull($removedUser);

        $this->assertEquals(4, $eventUser->eventRegistrations()->count());

        $this->assertEquals(2, $removedUser->eventRegistrations()->count());
        foreach ($removedUser->eventRegistrations as $eventRegistration) {
            $this->assertTrue($eventRegistration->dateOption->date < Carbon::tomorrow());
        }
    }

    /**
     * Test that the EventRegistrations are removed when all Users
     * is removed from the Event in the EventController.
     */
    public function testRemoveAllUserFromEvent()
    {
        [$users, $event] = $this->prepareEventUserData(2);
        $data = $this->createEventRequestData($event);
        $data['users'] = [];

        $response = $this->json('PUT', route('events.update', $event->id), $data);

        $response->assertStatus(200);
        foreach ($users as $user) {
            $this->assertEquals(2, $user->eventRegistrations()->count());
            foreach ($user->eventRegistrations as $eventRegistration) {
                $this->assertTrue($eventRegistration->dateOption->date < Carbon::tomorrow());
            }
        }
    }

    /**
     * Test that the registrations are not removed when the UserGroup is changed
     * and the User is in both UserGroups.
     */
    public function testEventSwitchUserGroup()
    {
        [$userGroups, $user, $event] = $this->prepareUserData(2);
        $data = $this->createEventRequestData($event);
        $data['user_groups'] = [$userGroups[0]->id];
        $event->userGroups()->attach($userGroups[1]);

        $response = $this->json('PUT', route('events.update', $event->id), $data);

        $response->assertStatus(200);
        $this->assertEquals(4, $user->eventRegistrations()->count());
    }

    /**
     * Create data for \App\Http\Requests\EventRequest
     *
     * @param  \App\Models\Event $event
     * @return array
     */
    private function createEventRequestData($event)
    {
        return [
            'title'        => $event->title,
            'user_groups'  => $event->userGroups->map(function ($userGroup) {
                return $userGroup->id;
            })->toArray(),
            'users'        => $event->users->map(function ($user) {
                return $user->id;
            })->toArray(),
            'date_options' => $event->dateOptions->map(function ($dateOption) {
                return $dateOption->date->toDateString();
            })->toArray(),
        ];
    }

    /**
     * Create data for \App\Http\Request\UserRequest
     *
     * @param  \App\Models\User $user
     * @return array
     */
    private function createUserRequestData(User $user): array
    {
        return [
            'first_name'  => $user->first_name,
            'last_name'   => $user->last_name,
            'gender'      => $user->gender,
            'position'    => $user->position ?
                $user->position->id : Position::factory()->create()->id,
            'user_groups' => $user->userGroups->map(function (UserGroup $userGroup) {
                return $userGroup->id;
            })->toArray(),
        ];
    }

    /**
     * Create data for \App\Http\Request\UserGroupRequest
     *
     * @param  \App\Models\UserGroup $userGroup
     * @return array
     */
    private function createUserGroupRequestData(UserGroup $userGroup): array
    {
        return [
            'name'  => $userGroup->name,
            'users' => $userGroup->users->map(function (User $user) {
                return $user->id;
            })->toArray(),
        ];
    }

    /**
     * Prepare an Event with Users attached and registered to it.
     */
    private function prepareEventUserData(int $userCount)
    {
        $event = Event::factory()->create();
        $dateOptions = $this->prepareDateOptions($event);
        $users = User::factory()->count($userCount)->has(
            EventRegistration::factory()->count(4)->state(new Sequence(
                ['event_date_option_id' => $dateOptions[0]->id],
                ['event_date_option_id' => $dateOptions[1]->id],
                ['event_date_option_id' => $dateOptions[2]->id],
                ['event_date_option_id' => $dateOptions[3]->id]
            ))
        )->create();
        $event->users()->attach($users);

        return [$users, $event];
    }
    /**
     * Prepare an Event with UserGroups attached to it
     * and an User attached to nothing, but registered to the Event.
     */
    private function prepareEventUserGroupData(int $userGroupCount)
    {
        $userGroups = UserGroup::factory()->count($userGroupCount)->create();
        $event = Event::factory()->create();
        $dateOptions = $this->prepareDateOptions($event);
        $user = User::factory()->has(
            EventRegistration::factory()->count(4)->state(new Sequence(
                ['event_date_option_id' => $dateOptions[0]->id],
                ['event_date_option_id' => $dateOptions[1]->id],
                ['event_date_option_id' => $dateOptions[2]->id],
                ['event_date_option_id' => $dateOptions[3]->id]
            ))
        )->create();
        $event->userGroups()->attach($userGroups);

        return [$userGroups, $user, $event];
    }

    /**
     * Prepare a User with UserGroups and register it to an Event.
     * Doesn't assign the UserGroups to the Event.
     *
     * @param int $userGroupCount How many UserGroups to create.
     */
    private function prepareUserData(int $userGroupCount)
    {
        $userGroups = UserGroup::factory()->count($userGroupCount)->create();
        $event = Event::factory()->create();
        $dateOptions = $this->prepareDateOptions($event);
        $user = User::factory()->has(
            EventRegistration::factory()->count(4)->state(new Sequence(
                ['event_date_option_id' => $dateOptions[0]->id],
                ['event_date_option_id' => $dateOptions[1]->id],
                ['event_date_option_id' => $dateOptions[2]->id],
                ['event_date_option_id' => $dateOptions[3]->id]
            ))
        )->create();
        $user->userGroups()->attach($userGroups);

        return [$userGroups, $user, $event];
    }

    /**
     * Prepare a UserGroup with Users registered to an Event.
     *
     * @param int $userCount How many Users to create.
     */
    private function prepareUserGroupData(int $userCount)
    {
        $userGroup = UserGroup::factory()->create();
        $event = Event::factory()->create();
        $dateOptions = $this->prepareDateOptions($event);
        $users = User::factory()->count($userCount)->has(
            EventRegistration::factory()->count(4)->state(new Sequence(
                ['event_date_option_id' => $dateOptions[0]->id],
                ['event_date_option_id' => $dateOptions[1]->id],
                ['event_date_option_id' => $dateOptions[2]->id],
                ['event_date_option_id' => $dateOptions[3]->id]
            ))
        )->create();

        // Add UserGroup to Event
        $event->userGroups()->attach($userGroup);

        // Add Users to UserGroups
        $userGroup->users()->sync($users->map(function (User $user) {
            return $user->id;
        }));

        return [$userGroup, $users, $event];
    }

    /**
     * Create EventDateOptions for the event with the dates
     * yesterday, today, tomorrow, tomorrow +1 Day.
     */
    private function prepareDateOptions($event)
    {
        return EventDateOption::factory()->count(4)->state(new Sequence(
            ['event_id' => $event->id, 'date' => Carbon::yesterday()],
            ['event_id' => $event->id, 'date' => Carbon::today()],
            ['event_id' => $event->id, 'date' => Carbon::tomorrow()],
            ['event_id' => $event->id, 'date' => Carbon::tomorrow()->addDay()]
        ))->create();
    }
}
