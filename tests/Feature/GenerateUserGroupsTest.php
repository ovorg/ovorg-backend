<?php

namespace Tests\Feature;

use App\Models\Division;
use App\Models\Driver;
use App\Models\SCBA;
use App\Models\User;
use App\Models\UserGroup;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Tests\Traits\CreatesModels;

class GenerateUserGroupsTest extends TestCase
{
    use CreatesModels;

    private $userCount = 5;

    /**
     * Test if a new UserGroup is created when a new Division is created.
     */
    public function testCreateDivision()
    {
        $division = Division::factory()->create();

        $userGroup = UserGroup::where('name', $division->name)->where('generated', true)->first();
        $this->assertNotNull($userGroup);
    }

    /**
     * Test if Users are added to the UserGroup when they are associated with a Division.
     */
    public function testDivisionsAddUser()
    {
        $division = Division::factory()->create();

        $user0 = User::factory()->create();
        $user0->division()->associate($division);
        $user0->save();
        $userGroup = UserGroup::where('name', $division->name)->where('generated', true)->first();
        $this->assertNotNull($userGroup);
        $this->assertNotNull($userGroup->users()->where('id', $user0->id)->first());

        $user1 = User::factory()->create();
        $user1->division()->associate($division);
        $user1->save();
        $userGroup = $userGroup->fresh();
        $this->assertNotNull($userGroup);
        $this->assertNotNull($userGroup->users()->where('id', $user0->id)->first());
        $this->assertNotNull($userGroup->users()->where('id', $user1->id)->first());
    }

    /**
     * Test if SCBA UserGroup is generated and Users are added to it.
     */
    public function testSCBA()
    {
        $user0 = User::factory()->create();
        self::createModelFor(SCBA::class, 'user_id', $user0->id);
        $user1 = User::factory()->create();
        self::createModelFor(SCBA::class, 'user_id', $user1->id);

        $userGroup = UserGroup::where('name', 'Atemschutzgeräteträger')->where('generated', true)->first();
        $this->assertNotNull($userGroup);
        $this->assertNotNull($userGroup->users()->where('id', $user0->id));
        $this->assertNotNull($userGroup->users()->where('id', $user1->id));
    }

    /**
     * Test if Driver UserGroups are generated and Users are added to it.
     */
    public function testDriver()
    {
        $user0 = User::factory()->create();
        $driver0 = new Driver();
        $driver0->b = true;
        $driver0->be = false;
        $driver0->c = false;
        $driver0->ce = false;
        $driver0->user()->associate($user0);
        $driver0->save();

        $user1 = User::factory()->create();
        $driver1 = new Driver();
        $driver1->b = false;
        $driver1->be = true;
        $driver1->c = false;
        $driver1->ce = false;
        $driver1->user()->associate($user1);
        $driver1->save();

        $user2 = User::factory()->create();
        $driver2 = new Driver();
        $driver2->b = false;
        $driver2->be = false;
        $driver2->c = true;
        $driver2->ce = false;
        $driver2->user()->associate($user2);
        $driver2->save();

        $user3 = User::factory()->create();
        $driver3 = new Driver();
        $driver3->b = false;
        $driver3->be = false;
        $driver3->c = false;
        $driver3->ce = true;
        $driver3->user()->associate($user3);
        $driver3->save();

        $b = UserGroup::where('name', 'B Fahrer')->where('generated', true)->first();
        $this->assertNotNull($b);
        $this->assertNotNull($b->users()->where('id', $user0->id)->first());
        $this->assertNull($b->users()->where('id', $user1->id)->first());
        $this->assertNull($b->users()->where('id', $user2->id)->first());
        $this->assertNull($b->users()->where('id', $user3->id)->first());

        $be = UserGroup::where('name', 'BE Fahrer')->where('generated', true)->first();
        $this->assertNotNull($be);
        $this->assertNull($be->users()->where('id', $user0->id)->first());
        $this->assertNotNull($be->users()->where('id', $user1->id)->first());
        $this->assertNull($be->users()->where('id', $user2->id)->first());
        $this->assertNull($be->users()->where('id', $user3->id)->first());

        $c = UserGroup::where('name', 'C Fahrer')->where('generated', true)->first();
        $this->assertNotNull($c);
        $this->assertNull($c->users()->where('id', $user0->id)->first());
        $this->assertNull($c->users()->where('id', $user1->id)->first());
        $this->assertNotNull($c->users()->where('id', $user2->id)->first());
        $this->assertNull($c->users()->where('id', $user3->id)->first());

        $ce = UserGroup::where('name', 'CE Fahrer')->where('generated', true)->first();
        $this->assertNotNull($ce);
        $this->assertNull($ce->users()->where('id', $user0->id)->first());
        $this->assertNull($ce->users()->where('id', $user1->id)->first());
        $this->assertNull($ce->users()->where('id', $user2->id)->first());
        $this->assertNotNull($ce->users()->where('id', $user3->id)->first());
    }

    /**
     * Test if UserGroup for all Users is generated and Users are added to it.
     */
    public function testAll()
    {
        $users = User::factory()->count(5)->create();

        $userGroup = UserGroup::where('name', 'Alle Helfer (Ja, wirklich alle. Also auch Junghelfer usw.)')->first();
        foreach ($users as $user) {
            $this->assertNotNull($userGroup->users()->where('id', $user->id));
        }
    }

    /**
     * Test that a existing not generated usergroup is overwritten.
     */
    public function testNameExists()
    {
        UserGroup::factory()->create(['name' => '::name::']);
        Division::factory()->create(['name' => '::name::']);

        $userGroup = UserGroup::where('name', '::name::')->first();
        $this->assertNotNull($userGroup);
        $this->assertTrue($userGroup->generated);
    }
}
