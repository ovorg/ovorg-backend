<?php

namespace Tests\Feature\Controller;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CalendarControllerTest extends TestCase
{

    /**
     * Test calendar functionality for yes Meetings.
     */
    public function testYes()
    {
        $user = User::factory()->create();

        $response = $this->get(route('calendar', $user->calendar_token) . "?yes");

        $response->assertStatus(200);
    }

    /**
     * Test calendar functionality for no Meetings.
     */
    public function testNo()
    {
        $user = User::factory()->create();

        $response = $this->get(route('calendar', $user->calendar_token) . "?no");

        $response->assertStatus(200);
    }

    /**
     * Test calendar functionality for maybe Meetings.
     */
    public function testMaybe()
    {
        $user = User::factory()->create();

        $response = $this->get(route('calendar', $user->calendar_token) . "?maybe");

        $response->assertStatus(200);
    }

    /**
     * Test calendar functionality for not_registered Meetings.
     */
    public function testNotRegistered()
    {
        $user = User::factory()->create();

        $response = $this->get(route('calendar', $user->calendar_token) . "?not_registered");

        $response->assertStatus(200);
    }

    /**
     * Test calendar functionality for all Meetings.
     */
    public function testAll()
    {
        $user = User::factory()->create();

        $response = $this->get(route('calendar', $user->calendar_token) . "?yes&no&maybe&not_registered");

        $response->assertStatus(200);
    }
}
