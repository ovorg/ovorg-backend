<?php

namespace Tests\Feature\Controller;

use App\Http\Resources\OrderItem\OrderItemResource;
use App\Models\OrderItem;
use App\Models\Shop;
use App\Models\User;
use Illuminate\Support\Str;
use Laravel\Passport\Passport;
use Tests\TestCase;
use Tests\Traits\DisablesAuth;

class OrderItemControllerTest extends TestCase
{
    use DisablesAuth;

    /**
     * Disable authorisation and authentication.
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->disableAuthorisation();
    }

    /**
     * Test the index functionality without adding any models.
     */
    public function testIndexWithoutModels(): void
    {
        $this->disableAuthentication();

        $response = $this->json('GET', route('order_items.index'));

        $response->assertStatus(200);
        $response->assertExactJson(["data" => []]);
    }

    /**
     * Test the index functionality with models.
     */
    public function testIndex(): void
    {
        $this->disableAuthentication();

        $orderItems = OrderItem::factory()->count(5)->create(['ordered' => false]);

        $response = $this->json('GET', route('order_items.index'));

        $response->assertStatus(200);
        $response->assertResource(OrderItemResource::collection($orderItems));
    }

    /**
     * Test the archive functionality without adding any models.
     */
    public function testArchiveWithoutModels(): void
    {
        $this->disableAuthentication();

        $response = $this->json('GET', route('order_items.archive'));

        $response->assertStatus(200);
        $response->assertExactJson(["data" => []]);
    }

    /**
     * Test the archive functionality with models.
     */
    public function testArchive(): void
    {
        $this->disableAuthentication();

        $orderItems = OrderItem::factory()->count(5)->create(['ordered' => true]);

        $response = $this->json('GET', route('order_items.archive'));

        $response->assertStatus(200);
        $response->assertResource(OrderItemResource::collection($orderItems));
    }

    /**
     * Test the show functionality without model.
     */
    public function testShowWithoutModel(): void
    {
        $this->disableAuthentication();

        $response = $this->json('GET', route('order_items.show', 0));

        $response->assertStatus(404);
    }

    /**
     * Test the show functionality with a model.
     */
    public function testShow(): void
    {
        $this->disableAuthentication();

        $orderItem = OrderItem::factory()->create();

        $response = $this->json('GET', route('order_items.show', $orderItem->id));

        $response->assertStatus(200);
        $response->assertResource(new OrderItemResource($orderItem));
    }

    /**
     * Test the store functionality.
     */
    public function testStore(): void
    {
        $user = User::factory()->create();
        Passport::actingAs($user);

        $orderItem = OrderItem::factory()
            ->for(Shop::factory())
            ->create();
        $data = $this->getData($orderItem);
        $orderItem->delete();
        $orderItem->id++;

        $response = $this->json('POST', route('order_items.store'), $data);
        $orderItem->creator()->associate($user);

        $response->assertStatus(201);
        $response->assertResource(new OrderItemResource($orderItem));
        $this->assertNotNull(OrderItem::find($orderItem->id));
    }

    /**
     * Test the update functionality.
     */
    public function testUpdate(): void
    {
        $this->disableAuthentication();

        $orderItems = OrderItem::factory()->count(2)->create();
        $shop = Shop::factory()->create();
        $orderItems[0]->shop()->associate($shop);
        $data = $this->getData($orderItems[1]);
        $orderItems[1]->delete();
        $orderItems[1]->id = $orderItems[0]->id;

        $response = $this->json('PUT', route('order_items.update', $orderItems[0]->id), $data);

        $response->assertStatus(200);
        $response->assertResource(new OrderItemResource($orderItems[1]));
    }

    /**
     * Test the destroy functionality.
     */
    public function testDestroyShops(): void
    {
        $this->disableAuthentication();

        $orderItem = OrderItem::factory()->create();

        $response = $this->json('DELETE', route('order_items.destroy', $orderItem->id));

        $response->assertStatus(200);
        $response->assertResource(new OrderItemResource($orderItem));
        $this->assertNull(OrderItem::find($orderItem->id));
    }

    /**
     * Return the data for the OrderItem/OrderItemRequest.
     *
     * @param  \App\Models\OrderItem $orderItem
     * @return array
     */
    private function getData(OrderItem $orderItem): array
    {
        return [
            'name'           => $orderItem->name,
            'price'          => $orderItem->price,
            'count'          => $orderItem->count,
            'url'            => $orderItem->url,
            'article_number' => $orderItem->article_number,
            'description'    => $orderItem->description,
            'ordered'        => $orderItem->ordered,
            'shop'           => $orderItem->shop ? $orderItem->shop->id : null,
        ];
    }
}
