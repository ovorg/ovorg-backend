<?php

namespace Tests\Feature\Controller;

use App\Http\Resources\Meeting\DoodleMeetingResource;
use App\Http\Resources\Meeting\SimplifiedDoodleMeetingResource;
use App\Models\Meeting;
use App\Models\User;
use Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Laravel\Passport\Passport;
use Tests\TestCase;
use Tests\Traits\TestsResourceControllers;

class DoodleControllerTest extends TestCase
{
    use TestsResourceControllers;
    public function setUp(): void
    {
        parent::setUp();

        $this->modelType = Meeting::class;
        $this->routeNamePrefix = 'doodle';
        $this->permissions = [
            'index'   => null,
            'store'   => null,
            'update'  => null,
            'destroy' => null,
            'show'    => null,
        ];
    }

    /**
     * Test the index functionality without models.
     */
    public function testIndexWithoutModels()
    {
        $this->indexWithoutModels();
    }

    /**
     * Test the index functionality with models.
     */
    public function testIndexWithModels()
    {
        $this->indexWithModels(SimplifiedDoodleMeetingResource::class, [$this, "prepareModels"]);
    }

    /**
     * Test the index functinality with models and with the doodle token.
     */
    public function testIndexWithToken()
    {
        // Authentication
        $user = $this::setActingAs([$this->permissions['index']]);

        // Preparation
        $models = $this->prepareModels($this->modelCount, $user);

        // Execution
        $response = $this->json('GET', route('doodle.index_token', $user->doodle_token));

        // Assertions
        $response->assertStatus(200);
        $response->assertResource(SimplifiedDoodleMeetingResource::collection($models));
    }

    /**
     * Test the archive functionality.
     */
    public function testArchive()
    {
        $pastMeetings = Meeting::factory()->count(5)
            ->create(['end' => Carbon::yesterday()]);
        $futureMeetings = Meeting::factory()->count(5)
            ->create(['end' => Carbon::tomorrow()]);
        $user = User::factory()->create();
        Passport::actingAs($user, ['app']);
        $user->meetings()->attach($pastMeetings);
        $user->meetings()->attach($futureMeetings);

        $response = $this->json('GET', route('doodle.archive'));

        $response->assertStatus(200);
        $response->assertResource(SimplifiedDoodleMeetingResource::collection($pastMeetings));
    }

    /**
     * Test the archive functionality with the doodle token.
     */
    public function testArchiveToken()
    {
        $pastMeetings = Meeting::factory()->count(5)
            ->create(['end' => Carbon::yesterday()]);
        $futureMeetings = Meeting::factory()->count(5)
            ->create(['end' => Carbon::tomorrow()]);
        $user = User::factory()->create();
        $user->meetings()->attach($pastMeetings);
        $user->meetings()->attach($futureMeetings);

        $response = $this->json('GET', route('doodle.archive_token', $user->doodle_token));

        $response->assertStatus(200);
        $response->assertResource(SimplifiedDoodleMeetingResource::collection($pastMeetings));
    }

    /**
     * Test the update functionality.
     */
    public function testUpdate()
    {
        $this->updateModel(DoodleMeetingResource::class, [$this, "prepareModel"], [$this, "prepareData"]);
        $this->updateInvalidModel([$this, "prepareModel"], [$this, "prepareData"]);
    }

    /**
     * Test the update functionality withd the doodel_token.
     */
    public function testUpdateToken()
    {
        $user = User::factory()->create();
        $model = $this->prepareModel($user);
        $data = $this->prepareData($user);

        $response = $this->json('PUT', route('doodle.update_token', [$model->id, $user->doodle_token]), $data);

        $response->assertStatus(200);
        $response->assertResource(new DoodleMeetingResource($model));
    }

    /**
     * Test show functionality with a valid model.
     */
    public function testShowValid()
    {
        $this->showModel(DoodleMeetingResource::class, [$this, "prepareModel"]);
    }

    /**
     * Test show functionality with a valid model with the doodel_token.
     */
    public function testShowValidToken()
    {
        $user = User::factory()->create();
        $model = $this->prepareModel();
        $model->users()->attach($user);

        $response = $this->json('GET', route('doodle.show_token', [$model->id, $user->doodle_token]));

        $response->assertStatus(200);
        $response->assertResource(new DoodleMeetingResource($model));
    }

    /**
     * Test show functionality with a valid model when the user is not invited to the meeting.
     */
    public function testShowNotInvited()
    {
        $user = User::factory()->create();
        Passport::actingAs($user);
        $model = $this->prepareModel();

        $response = $this->json('GET', route('doodle.show', $model->id));

        $response->assertForbidden();
    }

    /**
     * Test show functionality with an invalid model.
     */
    public function testShowInvalid()
    {
        $this->showInvalidModel();
    }

    /**
     * Prepare the data to be send to the store and update routes.
     *
     * @param \Illuminate\Database\Eloquent\Model
     * @return array
     */
    private function prepareData(Model $model): array
    {
        return [
            'attendance' => 'yes',
            'comment'    => '123',
        ];
    }

    /**
     * Prepare $count models.
     *
     * @param int $count
     * @param \App\Models\User $user
     * @return \Illuminate\Support\Collection
     */
    private function prepareModels($count, $user)
    {
        $collection = new Collection();
        for ($i = 0; $i < $count; $i++) {
            $collection->push($this->prepareModel($user));
        }
        return $collection;
    }

    /**
     * Prepare the model.
     *
     * @param \App\Models\User $user
     * @return \Illuminate\Database\Eloquent\Model
     */
    private function prepareModel($user = null): Model
    {
        $meeting = Meeting::factory()->create();
        $meeting->users()->attach($user);

        $user = User::factory()->create();
        $meeting->registeredUsers()->attach($user, [
            'attendance' => 'yes',
            'comment'    => '123',
        ]);
        $user = User::factory()->create();
        $meeting->registeredUsers()->attach($user, [
            'attendance' => 'no',
            'comment'    => '',
        ]);
        $user = User::factory()->create();
        $meeting->registeredUsers()->attach($user, [
            'attendance' => 'maybe',
            'comment'    => null,
        ]);
        $meeting->start = Carbon::tomorrow();
        $meeting->end = Carbon::tomorrow();
        $meeting->register_until = Carbon::tomorrow();

        $user = User::factory()->create();
        $meeting->responsible()->associate($user);

        $meeting->save();
        return $meeting->fresh();
    }
}
