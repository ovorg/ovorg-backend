<?php

namespace Tests\Feature\Controller;

use App\Jobs\Auth\CreateActivation;
use App\Jobs\Auth\CreateReminder;
use App\Models\Event;
use App\Models\EventDateOption;
use App\Models\EventRegistration;
use App\Models\File;
use App\Models\Meeting;
use App\Models\User;
use App\Models\UserGroup;
use App\Notifications\CustomNotification;
use App\Notifications\CustomSummaryNotification;
use App\Notifications\EventNotification;
use App\Notifications\MeetingNotification;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;
use Laravel\Passport\Passport;
use Tests\TestCase;
use Tests\Traits\DisablesAuth;

class NotificationControllerTest extends TestCase
{
    use DisablesAuth;
    use WithFaker;

    /**
     * Disable authorisation.
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->disableAuthorisation();
    }

    /**
     * Test sending an Activation.
     */
    public function testActivation()
    {
        $this->disableAuthentication();
        $user = User::factory()->create();
        Bus::fake();

        $response = $this->json('POST', route('notifications.activation', $user->id));

        $response->assertStatus(200);
        Bus::assertDispatched(CreateActivation::class, function ($job) use ($user) {
            return $job->user->id === $user->id;
        });
    }

    /**
     * Test sending a Reminder.
     */
    public function testReminder()
    {
        $this->disableAuthentication();
        $user = User::factory()->create();
        Bus::fake();

        $response = $this->json('POST', route('notifications.reminder', $user->id));

        $response->assertStatus(200);
        Bus::assertDispatched(CreateReminder::class, function ($job) use ($user) {
            return $job->user->id === $user->id;
        });
    }

    /**
     * Test sending a Meeting Notification.
     */
    public function testMeetingToUsers()
    {
        $this->disableAuthentication();
        $users = User::factory()->count(5)->create();
        $meeting = Meeting::factory()->create();
        foreach ($users as $user) {
            $user->meetings()->attach($meeting);
        }
        $notInvitedUser = User::factory()->create();
        $userIds = $users->map(function (User $user) {
            return $user->id;
        })->toArray();
        array_push($userIds, $notInvitedUser->id);
        $data = [
            'users' => $userIds,
        ];
        Notification::fake();

        $response = $this->json('POST', route('notifications.meeting', $meeting->id), $data);

        $response->assertStatus(200);
        foreach ($users as $user) {
            Notification::assertSentTo($user, MeetingNotification::class, function ($notification, $channels) use ($meeting) {
                return $notification->meetings->id === $meeting->id;
            });
        }
        Notification::assertNotSentTo($notInvitedUser, MeetingNotification::class);
    }

    /**
     * Create an User registered to $meeting with no.
     *
     * @param  \App\Models\Meeting $meeting
     * @return \App\Models\User
     */
    private function createAttendanceNoUser(Meeting $meeting): User
    {
        $user = User::factory()->create();
        $user->meetings()->attach($meeting);
        $user->registeredMeetings()->attach($meeting, ['attendance' => 'no']);
        return $user;
    }

    /**
     * Create an User registered to $meeting with yes.
     *
     * @param  \App\Models\Meeting $meeting
     * @return \App\Models\User
     */
    private function createAttendanceYesUser(Meeting $meeting): User
    {
        $user = User::factory()->create();
        $user->meetings()->attach($meeting);
        $user->registeredMeetings()->attach($meeting, ['attendance' => 'yes']);
        return $user;
    }

    /**
     * Create an User registered to $meeting with maybe.
     *
     * @param  \App\Models\Meeting $meeting
     * @return \App\Models\User
     */
    private function createAttendanceMaybeUser(Meeting $meeting): User
    {
        $user = User::factory()->create();
        $user->meetings()->attach($meeting);
        $user->registeredMeetings()->attach($meeting, ['attendance' => 'maybe']);
        return $user;
    }

    /**
     * Create an User invited to $meeting but not registered.
     *
     * @param  \App\Models\Meeting $meeting
     * @return \App\Models\User
     */
    private function createNotRegisteredUser(Meeting $meeting): User
    {
        $user = User::factory()->create();
        $user->meetings()->attach($meeting);
        return $user;
    }

    /**
     * Attach Users to a Meeting and register them with $attendance
     *
     * @param \Illuminate\Support\Collection $users
     * @param \App\Models\Meeting $meeting
     * @param string $attendance
     */
    private function attachUsersWithAttendance(Collection $users, Meeting $meeting, string $attendance)
    {
        foreach ($users as $user) {
            $user->meetings()->attach($meeting);
            $user->registeredMeetings()->attach($meeting, ['attendance' => $attendance]);
        }
    }

    /**
     * Test sending meeting notifications to all Users registered to the Meeting with yes.
     */
    public function testMeetingToAllYes()
    {
        $this->disableAuthentication();
        $users = User::factory()->count(5)->create();
        $meeting = Meeting::factory()->create();
        $this->attachUsersWithAttendance($users, $meeting, 'yes');
        $userNo = $this->createAttendanceNoUser($meeting);
        $userMaybe = $this->createAttendanceMaybeUser($meeting);
        $userNotRegistered = $this->createNotRegisteredUser($meeting);
        $data = [
            'to_all'         => true,
            'yes'            => true,
            'no'             => false,
            'maybe'          => false,
            'not_registered' => false,
        ];
        Notification::fake();

        $response = $this->json('POST', route('notifications.meeting', $meeting->id), $data);

        $response->assertStatus(200);
        foreach ($users as $user) {
            Notification::assertSentTo($user, MeetingNotification::class, function ($notification, $channels) use ($meeting) {
                return $notification->meetings->id === $meeting->id;
            });
        }
        Notification::assertNotSentTo($userNo, MeetingNotification::class);
        Notification::assertNotSentTo($userMaybe, MeetingNotification::class);
        Notification::assertNotSentTo($userNotRegistered, MeetingNotification::class);
    }

    /**
     * Test sending meeting notifications to all Users registered to the Meeting with no.
     */
    public function testMeetingToAllNo()
    {
        $this->disableAuthentication();
        $users = User::factory()->count(5)->create();
        $meeting = Meeting::factory()->create();
        $this->attachUsersWithAttendance($users, $meeting, 'no');
        $userYes = $this->createAttendanceYesUser($meeting);
        $userMaybe = $this->createAttendanceMaybeUser($meeting);
        $userNotRegistered = $this->createNotRegisteredUser($meeting);
        $data = [
            'to_all'         => true,
            'yes'            => false,
            'no'             => true,
            'maybe'          => false,
            'not_registered' => false,
        ];
        Notification::fake();

        $response = $this->json('POST', route('notifications.meeting', $meeting->id), $data);

        $response->assertStatus(200);
        foreach ($users as $user) {
            Notification::assertSentTo($user, MeetingNotification::class, function ($notification, $channels) use ($meeting) {
                return $notification->meetings->id === $meeting->id;
            });
        }
        Notification::assertNotSentTo($userYes, MeetingNotification::class);
        Notification::assertNotSentTo($userMaybe, MeetingNotification::class);
        Notification::assertNotSentTo($userNotRegistered, MeetingNotification::class);
    }

    /**
     * Test sending meeting notifications to all Users registered to the Meeting with maybe.
     */
    public function testMeetingToAllMaybe()
    {
        $this->disableAuthentication();
        $users = User::factory()->count(5)->create();
        $meeting = Meeting::factory()->create();
        $this->attachUsersWithAttendance($users, $meeting, 'maybe');
        $userNo = $this->createAttendanceNoUser($meeting);
        $userYes = $this->createAttendanceYesUser($meeting);
        $userNotRegistered = $this->createNotRegisteredUser($meeting);
        $data = [
            'to_all'         => true,
            'yes'            => false,
            'no'             => false,
            'maybe'          => true,
            'not_registered' => false,
        ];
        Notification::fake();

        $response = $this->json('POST', route('notifications.meeting', $meeting->id), $data);

        $response->assertStatus(200);
        foreach ($users as $user) {
            Notification::assertSentTo($user, MeetingNotification::class, function ($notification, $channels) use ($meeting) {
                return $notification->meetings->id === $meeting->id;
            });
        }
        Notification::assertNotSentTo($userYes, MeetingNotification::class);
        Notification::assertNotSentTo($userNo, MeetingNotification::class);
        Notification::assertNotSentTo($userNotRegistered, MeetingNotification::class);
    }

    /**
     * Test sending meeting notifications to all Users not registered to the Meeting but invited.
     */
    public function testMeetingToAllNotRegistered()
    {
        $this->disableAuthentication();
        $users = User::factory()->count(5)->create();
        $meeting = Meeting::factory()->create();
        foreach ($users as $user) {
            $user->meetings()->attach($meeting);
        }
        $userNo = $this->createAttendanceNoUser($meeting);
        $userMaybe = $this->createAttendanceMaybeUser($meeting);
        $userYes = $this->createAttendanceYesUser($meeting);
        $data = [
            'to_all'         => true,
            'yes'            => false,
            'no'             => false,
            'maybe'          => false,
            'not_registered' => true,
        ];
        Notification::fake();

        $response = $this->json('POST', route('notifications.meeting', $meeting->id), $data);

        $response->assertStatus(200);
        foreach ($users as $user) {
            Notification::assertSentTo($user, MeetingNotification::class, function ($notification, $channels) use ($meeting) {
                return $notification->meetings->id === $meeting->id;
            });
        }
        Notification::assertNotSentTo($userYes, MeetingNotification::class);
        Notification::assertNotSentTo($userNo, MeetingNotification::class);
        Notification::assertNotSentTo($userMaybe, MeetingNotification::class);
    }

    /**
     * Test if the CustomNotification is sent to the authenticated User.
     */
    public function testCustomMailAuthUser()
    {
        $authUser = User::factory()->create();
        Passport::actingAs($authUser);
        $body = $this->faker()->text;
        $subject = $this->faker()->word;
        $data = [
            'body'    => $body,
            'subject' => $subject,
        ];
        Notification::fake();

        $response = $this->json('POST', route('notifications.custom'), $data);

        $response->assertStatus(200);
        Notification::assertSentTo($authUser, CustomNotification::class, function ($notification, $channels) use ($body, $subject, $authUser) {
            return $notification->subject == $subject && $notification->body == $body && $notification->sender->id == $authUser->id;
        });
    }

    /**
     * Assert that a custom mail is sent to $authUser and to all $users.
     *
     * @param \App\Models\User $authUser
     * @param \Illuminate\Support\Collection $users
     * @param string $body
     * @param string $subject
     */
    public function assertCustomMail(User $authUser, Collection $users, string $body, string $subject)
    {
        Notification::assertSentTo($authUser, CustomNotification::class, function ($notification, $channels) use ($body, $subject, $authUser) {
            return $notification->subject == $subject && $notification->body == $body && $notification->sender->id == $authUser->id;
        });
        foreach ($users as $user) {
            Notification::assertSentTo($user, CustomNotification::class, function ($notification, $channels) use ($body, $subject, $authUser) {
                return $notification->subject == $subject && $notification->body == $body && $notification->sender->id == $authUser->id;
            });
        }
    }

    /**
     * Test that the CustomMailSummary Notification is not sent if it is not requested.
     */
    public function testNoCustomMailSummary()
    {
        $authUser = User::factory()->create();
        Passport::actingAs($authUser);
        $users = User::factory()->count(5)->create();
        $body = $this->faker()->text;
        $subject = $this->faker()->word;
        $data = [
            'body'    => $body,
            'subject' => $subject,
            'users'   => $users->map(function (User $user) {
                return $user->id;
            }),
        ];
        Notification::fake();

        $response = $this->json('POST', route('notifications.custom'), $data);

        $response->assertStatus(200);
        foreach ($users as $user) {
            Notification::assertSentTo($user, CustomNotification::class, function ($notification, $channels) use ($body, $subject, $authUser) {
                return $notification->subject == $subject && $notification->body == $body && $notification->sender->id == $authUser->id;
            });
        }
        Notification::assertNotSentTo($authUser, CustomSummaryNotification::class);
    }

    /**
     * Test that the CustomMailSummary Notification is sent if it is requested.
     */
    public function testCustomMailSummary()
    {
        $authUser = User::factory()->create();
        Passport::actingAs($authUser);
        $users = User::factory()->count(5)->create();
        $body = $this->faker()->text;
        $subject = $this->faker()->word;
        $data = [
            'body'         => $body,
            'subject'      => $subject,
            'users'        => $users->map(function (User $user) {
                return $user->id;
            }),
            'send_summary' => true,
        ];
        Notification::fake();

        $response = $this->json('POST', route('notifications.custom'), $data);

        $response->assertStatus(200);
        foreach ($users as $user) {
            Notification::assertSentTo($user, CustomNotification::class, function ($notification, $channels) use ($body, $subject, $authUser) {
                return $notification->subject == $subject && $notification->body == $body && $notification->sender->id == $authUser->id;
            });
        }
        Notification::assertSentTo($authUser, CustomSummaryNotification::class);
    }

    /**
     * Test if the CustomNotification is sent Users.
     */
    public function testCustomMailUsers()
    {
        $authUser = User::factory()->create();
        Passport::actingAs($authUser);
        $users = User::factory()->count(5)->create();
        $userIds = $users->map(function (User $user) {
            return $user->id;
        });
        $body = $this->faker()->text;
        $subject = $this->faker()->word;
        $data = [
            'body'    => $body,
            'subject' => $subject,
            'users'   => $userIds,
        ];
        Notification::fake();

        $response = $this->json('POST', route('notifications.custom'), $data);

        $response->assertStatus(200);
        $this->assertCustomMail($authUser, $users, $body, $subject);
    }

    /**
     * Test if the CustomNotification is sent to UserGroups.
     */
    public function testCustomMailUserGroups()
    {
        $authUser = User::factory()->create();
        Passport::actingAs($authUser);
        $users = User::factory()->count(2)->create();
        $userGroups = UserGroup::factory()->count(2)->create();
        $userGroups[0]->users()->attach($users[0]);
        $userGroups[1]->users()->attach($users[1]);
        $userGroupIds = $userGroups->map(function (UserGroup $userGroup) {
            return $userGroup->id;
        });
        $body = $this->faker()->text;
        $subject = $this->faker()->word;
        $data = [
            'body'        => $body,
            'subject'     => $subject,
            'user_groups' => $userGroupIds,
        ];
        Notification::fake();

        $response = $this->json('POST', route('notifications.custom'), $data);

        $response->assertStatus(200);
        $this->assertCustomMail($authUser, $users, $body, $subject);
    }

    /**
     * Test if the CustomNotification is sent to Meetings.
     */
    public function testCustomMailMeetings()
    {
        $authUser = User::factory()->create();
        Passport::actingAs($authUser);
        $users = User::factory()->count(4)->create();
        $meetings = Meeting::factory()->count(4)->create();
        $meetings[0]->users()->attach($users[0]);
        $meetings[0]->registeredUsers()->attach($users[0], ['attendance' => 'yes']);
        $meetings[1]->users()->attach($users[1]);
        $meetings[1]->registeredUsers()->attach($users[1], ['attendance' => 'no']);
        $meetings[2]->users()->attach($users[2]);
        $meetings[2]->registeredUsers()->attach($users[2], ['attendance' => 'maybe']);
        $meetings[3]->users()->attach($users[3]);
        $meetingIds = $meetings->map(function (Meeting $meeting) {
            return $meeting->id;
        });
        $body = $this->faker()->text;
        $subject = $this->faker()->word;
        $data = [
            'body'           => $body,
            'subject'        => $subject,
            'meetings'       => $meetingIds,
            'yes'            => true,
            'no'             => true,
            'maybe'          => true,
            'not_registered' => true,
        ];
        Notification::fake();

        $response = $this->json('POST', route('notifications.custom'), $data);

        $response->assertStatus(200);
        $this->assertCustomMail($authUser, $users, $body, $subject);
    }

    /**
     * Test if the CustomNotification sends attachments.
     */
    public function testCustomMailAttachment()
    {
        $authUser = User::factory()->create();
        Passport::actingAs($authUser);
        $body = $this->faker()->text;
        $subject = $this->faker()->word;
        $fileModel = File::factory()->create(['size' => '10000000']);
        $file = UploadedFile::fake()->image($fileModel->name);
        $fileModel->path = Storage::put(File::$uploadFolder, $file);
        $fileModel->save();
        $data = [
            'body'    => $body,
            'subject' => $subject,
            'files'   => [$fileModel->id],
        ];
        Notification::fake();

        $response = $this->json('POST', route('notifications.custom'), $data);
        $response->assertStatus(200);
        Notification::assertSentTo($authUser, CustomNotification::class, function ($notification, $channels) use ($body, $subject, $authUser, $fileModel) {
            return $notification->subject == $subject && $notification->body == $body && $notification->sender->id == $authUser->id && $notification->files->first()->id == $fileModel->id;
        });
    }

    /**
     * Test if the CustomNotification does not send to large attachments.
     */
    public function testCustomMailAttachmentToLarge()
    {
        $authUser = User::factory()->create();
        Passport::actingAs($authUser);
        $body = $this->faker()->text;
        $subject = $this->faker()->word;
        $fileModel1 = File::factory()->create(['size' => '5000000']);
        $fileModel2 = File::factory()->create(['size' => '5000001']);
        $file = UploadedFile::fake()->image($fileModel1->name);
        $fileModel1->path = Storage::put(File::$uploadFolder, $file);
        $fileModel2->path = $fileModel1->path;
        $fileModel1->save();
        $data = [
            'body'    => $body,
            'subject' => $subject,
            'files'   => [$fileModel1->id, $fileModel2->id],
        ];
        Notification::fake();

        $response = $this->json('POST', route('notifications.custom'), $data);
        $response->assertStatus(422);
        Notification::assertNothingSent();
    }

    /**
     * Test that with the 'only_missing_registration parameter,
     * only notifications to Users with missing registrations are sent.
     */
    public function testEventMissingRegistration()
    {
        $this->disableAuthentication();
        [$users, $event] = $this->createEventData();
        $data = [
            'to_all'                    => true,
            'only_missing_registration' => true,
        ];
        Notification::fake();

        $response = $this->json('POST', route('notifications.event', $event->id), $data);

        $response->assertStatus(200);
        Notification::assertNotSentTo($users[0], EventNotification::class);
        Notification::assertSentTo($users[1], EventNotification::class);
    }

    /**
     * Test that 'to_all' sends a Notification to all assigned Users.
     */
    public function testEventToAll()
    {
        $this->disableAuthentication();
        [$users, $event] = $this->createEventData();
        $data = [
            'to_all'                    => true,
            'only_missing_registration' => false,
        ];
        Notification::fake();

        $response = $this->json('POST', route('notifications.event', $event->id), $data);

        $response->assertStatus(200);
        Notification::assertSentTo($users[0], EventNotification::class);
        Notification::assertSentTo($users[1], EventNotification::class);
    }

    /**
     * Test that sending it to Users will sent it to all Users in the list,
     * that are assigned to the Event.
     */
    public function testEventToUsers()
    {
        $this->withoutExceptionHandling();
        $this->disableAuthentication();
        [$users, $event] = $this->createEventData();
        $notInvitedUser = User::factory()->create();
        $data = [
            'users' => [
                $users[0]->id, $users[1]->id, $notInvitedUser->id,
            ],
        ];
        Notification::fake();

        $response = $this->json('POST', route('notifications.event', $event->id), $data);

        $response->assertStatus(200);
        Notification::assertSentTo($users[0], EventNotification::class);
        Notification::assertSentTo($users[1], EventNotification::class);
        Notification::assertNotSentTo($notInvitedUser, EventNotification::class);
    }

    /**
     * Create data for the Event tests.
     * Creates 1 Event with 2 Users assigned to it.
     * Creates 2 EventDateOptions for the Event.
     * $user[0] is registered at both dates,
     * $user[1] at the first.
     *
     * @return array [$users, $event]
     */
    private function createEventData(): array
    {
        $users = User::factory()->count(2)->create();
        $event = Event::factory()->create();
        $event->users()->attach($users);
        $dateOptions = EventDateOption::factory()->count(2)->create([
            'event_id' => $event->id,
        ]);
        EventRegistration::factory()->create([
            'user_id'              => $users[0]->id,
            'event_date_option_id' => $dateOptions[0]->id,
        ]);
        EventRegistration::factory()->create([
            'user_id'              => $users[0]->id,
            'event_date_option_id' => $dateOptions[1]->id,
        ]);
        EventRegistration::factory()->create([
            'user_id'              => $users[1]->id,
            'event_date_option_id' => $dateOptions[0]->id,
        ]);

        return [$users, $event];
    }
}
