<?php

namespace Tests\Feature\Controller;

use App\Http\Resources\Role\RoleResource;
use App\Http\Resources\Role\SimplifiedRoleResource;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Tests\TestCase;
use Tests\Traits\TestsResourceControllers;

class RoleControllerTest extends TestCase
{
    use TestsResourceControllers;

    public function setUp(): void
    {
        parent::setUp();

        $this->modelType = Role::class;
        $this->routeNamePrefix = 'roles';
        $this->permissions = [
            'index'   => 'roles.general',
            'store'   => 'roles.general',
            'update'  => 'roles.general',
            'destroy' => 'roles.general',
            'show'    => 'roles.general',
        ];
    }

    /**
     * Test the index functionality without models.
     */
    public function testIndexWithoutModels()
    {
        $this->indexWithoutModels();
    }

    /**
     * Test the index functionality with models.
     */
    public function testIndexWithModels()
    {
        $this->indexWithModels(SimplifiedRoleResource::class);
    }

    /**
     * Test the store functionality.
     */
    public function testStore()
    {
        $this->storeModel(RoleResource::class, [$this, "prepareModel"], [$this, "prepareData"]);
    }

    /**
     * Test the update functionality.
     */
    public function testUpdate()
    {
        $this->updateModel(RoleResource::class, [$this, "prepareModel"], [$this, "prepareData"]);
        $this->updateInvalidModel([$this, "prepareModel"], [$this, "prepareData"]);
    }

    /**
     * Test destroy functionality with a valid model.
     */
    public function testDestroyValid()
    {
        $this->destroyModel(RoleResource::class);
    }

    /**
     * Test destroy functionality with an invalid model.
     */
    public function testDestroyInvalid()
    {
        $this->destroyInvalidModel();
    }

    /**
     * Test show functionality with a valid mode.
     */
    public function testShowValid()
    {
        $this->showModel(RoleResource::class, [$this, "prepareModel"]);
    }

    /**
     * Test show functionality with an invalid model.
     */
    public function testShowInvalid()
    {
        $this->showInvalidModel();
    }

    /**
     * Prepare the data to be send to the store and update routes.
     *
     * @param \Illuminate\Database\Eloquent\Model
     * @return array
     */
    private function prepareData(Model $model): array
    {
        $permissions = [];
        foreach ($model->permissions as $permission => $val) {
            array_push($permissions, $permission);
        }
        return [
            'name'        => $model->name,
            'permissions' => $permissions,
            'users'       => [$model->users()->firstOrFail()->id],
        ];
    }

    /**
     * Prepare the model for the store and update functionality.
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    private function prepareModel(): Model
    {
        $role = Role::factory()->create();
        $user = User::factory()->create();
        $role->users()->attach($user);
        $role->permissions = [
            'permission0' => true,
            'permission1' => true,
        ];
        $role->save();
        return $role->fresh();
    }
}
