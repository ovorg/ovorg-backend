<?php

namespace Tests\Feature\Controller;

use App\Http\Resources\Shop\ShopResource;
use App\Http\Resources\Shop\SimplifiedShopResource;
use App\Models\Shop;
use Tests\TestCase;
use Tests\Traits\DisablesAuth;

class ShopControllerTest extends TestCase
{
    use DisablesAuth;

    /**
     * Disable authorisation and authentication.
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->disableAuthorisation();
        $this->disableAuthentication();
    }

    /**
     * Test the index functionality without adding any models.
     */
    public function testIndexWithoutModels(): void
    {
        $response = $this->json('GET', route('shops.index'));

        $response->assertStatus(200);
        $response->assertExactJson(["data" => []]);
    }

    /**
     * Test the index functionality with models.
     */
    public function testIndex(): void
    {
        $shops = Shop::factory()->count(5)->create();

        $response = $this->json('GET', route('shops.index'));

        $response->assertStatus(200);
        $response->assertResource(SimplifiedShopResource::collection($shops));
    }

    /**
     * Test the show functionality without model.
     */
    public function testShowWithoutModel(): void
    {
        $response = $this->json('GET', route('shops.show', 0));

        $response->assertStatus(404);
    }

    /**
     * Test the show functionality with a model.
     */
    public function testShow(): void
    {
        $shop = Shop::factory()->create();

        $response = $this->json('GET', route('shops.show', $shop->id));

        $response->assertStatus(200);
        $response->assertResource(new ShopResource($shop));
    }

    /**
     * Test the store functionality.
     */
    public function testStore(): void
    {
        $shop = Shop::factory()->create();
        $data = [
            'name' => $shop->name,
            'url'  => $shop->url,
        ];
        $shop->delete();
        $shop->id++;

        $response = $this->json('POST', route('shops.store'), $data);

        $response->assertStatus(201);
        $response->assertResource(new ShopResource($shop));
        $this->assertNotNull(Shop::find($shop->id));
    }

    /**
     * Test the update functionality.
     */
    public function testUpdate(): void
    {
        $shops = Shop::factory()->count(2)->create();
        $data = [
            'name' => $shops[1]->name,
            'url'  => $shops[1]->url,
        ];
        $shops[1]->delete();
        $shops[1]->id = $shops[0]->id;

        $response = $this->json('PUT', route('shops.update', $shops[0]->id), $data);

        $response->assertStatus(200);
        $response->assertResource(new ShopResource($shops[1]));
    }

    /**
     * Test the destroy functionality.
     */
    public function testDestroyShops(): void
    {
        $shop = Shop::factory()->create();

        $response = $this->json('DELETE', route('shops.destroy', $shop->id));

        $response->assertStatus(200);
        $response->assertResource(new ShopResource($shop));
        $this->assertNull(Shop::find($shop->id));
    }
}
