<?php

namespace Tests\Feature\Controller;

use Activation;
use App\Jobs\Auth\CreateReminder;
use Artisan;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\Client;
use Laravel\Passport\Passport;
use Tests\TestCase;
use Tests\Traits\CreatesUsers;

class AuthenticationTest extends TestCase
{
    use CreatesUsers;

    private const tokenResponse = [
        'token_type'    => 'Bearer',
        'expires_in'    => true,
        'access_token'  => true,
        'refresh_token' => true,
    ];

    /**
     * Create the passport clients.
     */
    public function setUp(): void
    {
        parent::setUp();

        Artisan::call('passport:install');
    }

    /**
     * Test the login, logged_in and logout functionality.
     */
    public function testLogin()
    {
        $user = self::createUser();
        $activatedUser = self::createActivatedUser();

        $response = $this->withHeaders(['Accept' => 'application/json'])->get(route('logged_in'));
        $response->assertStatus(401);

        $response = $this->withHeaders([
            'Accept'        => 'application/json',
            'Authorization' => 'Bearer ',
        ])->get(route('logged_in'));
        $response->assertStatus(401);

        // User is not activated, correct password
        $response = $this->withHeaders(['Accept' => 'application/json'])->post(route('login'), [
            'username' => $user->username,
            'password' => self::$password,
        ]);
        $response->assertStatus(400);

        // User is not activated, wrong password
        $response = $this->withHeaders(['Accept' => 'application/json'])->post(route('login'), [
            'username' => $user->username,
            'password' => self::$password . '1',
        ]);
        $response->assertStatus(400);

        // Activated user, wrong password
        $response = $this->post(route('login'), [
            'username' => $activatedUser->username,
            'password' => self::$password . '1',
        ]);
        $response->assertStatus(400);

        // Activated user, correct password
        $response = $this->withHeaders(['Accept' => 'application/json'])->post(route('login'), [
            'username' => $activatedUser->username,
            'password' => self::$password,
        ]);
        $response->assertStatus(200);
        $response->assertJson(self::tokenResponse);

        $token = $response->json()["access_token"];

        $response = $this->withHeaders([
            'Accept'        => 'application/json',
            'Authorization' => 'Bearer ' . $token,
        ])->get(route('logged_in'));
        $response->assertStatus(200);

        $response = $this->withHeaders([
            'Accept'        => 'application/json',
            'Authorization' => 'Bearer ' . $token,
        ])->post(route('logout'));
        $response->assertStatus(200);
        $this->assertTrue($this->app['auth']->guard('api')->user()->token()->revoked);
    }

    /**
     * Test the refresh functionality.
     */
    public function testRefresh()
    {
        $user = self::createActivatedUser();

        $response = $this->withHeaders(['Accept' => 'application/json'])->post(route('login'), [
            'username' => $user->username,
            'password' => self::$password,
        ]);
        $response->assertStatus(200);

        $token = $response->json()["access_token"];
        $refreshToken = $response->json()["refresh_token"];

        $response = $this->withHeaders([
            'Accept'        => 'application/json',
            'Authorization' => 'Bearer ' . $token,
        ])->get(route('logged_in'));
        $response->assertStatus(200);

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->post(route('refresh'), [
            'refresh_token' => $refreshToken,
        ]);
        $response->assertStatus(200);
        $response->assertJson(self::tokenResponse);
    }

    /**
     * Test the activate functionality.
     */
    public function testActivate()
    {
        $user = self::createUser();
        $activation = self::createActivation($user);

        $this->assertFalse(Activation::completed($user));

        // Invalid user, valid activate code
        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->post(route('activate'), [
            'password' => self::$password,
            'user'     => 0,
            'username' => $user->username,
            'token'    => $activation->code,
        ]);
        $response->assertClientError();
        $user = $user->fresh();
        $this->assertFalse(Activation::completed($user));

        // Invalid status code, valid user
        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->post(route('activate'), [
            'password' => self::$password,
            'user'     => $user->id,
            'username' => $user->username,
            'token'    => $activation->code . '1',
        ]);
        $response->assertClientError();
        $user = $user->fresh();
        $this->assertFalse(Activation::completed($user));

        // Valid activate code, valid user
        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->post(route('activate'), [
            'password' => self::$password,
            'user'     => $user->id,
            'username' => $user->username,
            'token'    => $activation->code,
        ]);
        $response->assertStatus(200);
        $user = $user->fresh();
        $this->assertTrue(Activation::completed($user));

        // Already activated
        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->post(route('activate'), [
            'password' => self::$password,
            'user'     => $user->id,
            'username' => $user->username,
            'token'    => $activation->code,
        ]);
        $response->assertClientError();
    }

    /**
     * Test changePassword functionality.
     */
    public function testChangePassword()
    {
        $user = self::createUser();
        $newPassword = self::$password . '1';

        $this->assertTrue(Hash::check(self::$password, $user->password));

        Passport::actingAs($user, ['app']);

        // Wrong old password
        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->post(route('change_password'), [
            'password'     => self::$password . '1',
            'new_password' => $newPassword,
        ]);
        $response->assertClientError();
        $user = $user->fresh();
        $this->assertTrue(Hash::check(self::$password, $user->password));

        // Correct old password
        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->post(route('change_password'), [
            'password'     => self::$password,
            'new_password' => $newPassword,
        ]);
        $response->assertStatus(200);
        $user = $user->fresh();
        $this->assertTrue(Hash::check($newPassword, $user->password));
    }

    /**
     * Test requestPasswordReset functionality
     */
    public function testRequestPasswordReset()
    {

        $user = self::createUser();
        $activatedUser = self::createActivatedUser();

        Bus::fake();

        // User is not activated
        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->post(route('request_password_reset'), [
            'email' => $user->email,
        ]);
        $response->assertClientError();

        // User is activated
        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->post(route('request_password_reset'), [
            'email' => $activatedUser->email,
        ]);
        $response->assertStatus(200);
        Bus::assertDispatched(CreateReminder::class, function ($job) use ($activatedUser) {
            return $job->user->id === $activatedUser->id && $job->reminder != null;
        });
    }

    /**
     * Test resetPassword functionality.
     */
    public function testResetPassword()
    {
        $user = self::createActivatedUser();
        $reminder = self::createReminder($user);
        $newPassword = self::$password . '1';

        $this->assertTrue(Hash::check(self::$password, $user->password));

        // Invalid user
        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->post(route('reset_password'), [
            'password'      => $newPassword,
            'user'          => 0,
            'reminder_code' => $reminder->code,
        ]);
        $response->assertClientError();
        $user = $user->fresh();
        $this->assertTrue(Hash::check(self::$password, $user->password));

        // Invalid reminder code, valid user
        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->post(route('reset_password'), [
            'password'      => $newPassword,
            'user'          => $user->id,
            'reminder_code' => $reminder->code . '1',
        ]);
        $response->assertClientError();
        $user = $user->fresh();
        $this->assertTrue(Hash::check(self::$password, $user->password));

        // Valid reminder code and valid user
        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->post(route('reset_password'), [
            'password'      => $newPassword,
            'user'          => $user->id,
            'reminder_code' => $reminder->code,
        ]);
        $response->assertStatus(200);
        $user = $user->fresh();
        $this->assertTrue(Hash::check($newPassword, $user->password));
    }
}
