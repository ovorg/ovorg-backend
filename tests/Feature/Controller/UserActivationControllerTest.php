<?php

namespace Tests\Feature\Controller;

use App\Http\Resources\User\UserActivationResource;
use App\Models\User;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Tests\Traits\DisablesAuth;

class UserActivationControllerTest extends TestCase
{
    use DisablesAuth;

    /**
     * Disable auth.
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->disableAuthorisation();
        $this->disableAuthentication();
    }

    /**
     * Test the index functionality.
     */
    public function testIndex()
    {
        $users = User::factory()->count(3)->create();
        Activation::create($users[0]);
        $a = Activation::create($users[1]);
        Activation::complete($users[1], $a->code);
        $users[0]->activated = false;
        $users[1]->activated = true;
        $users[2]->activated = false;

        $response = $this->json('GET', route('activations.index'));

        $response->assertStatus(200);
        $response->assertResource(UserActivationResource::collection($users));
    }

    /**
     * Test the show functionality for activated user.
     */
    public function testShowActivated()
    {
        $user = User::factory()->create();
        $a = Activation::create($user);
        Activation::complete($user, $a->code);
        $user->activated = true;

        $response = $this->json('GET', route('activations.show', $user->id));

        $response->assertStatus(200);
        $response->assertResource(new UserActivationResource($user));
    }

    /**
     * Test the show functionality for user with uncomplete activation.
     */
    public function testShowUncompleteActivation()
    {
        $user = User::factory()->create();
        $a = Activation::create($user);
        $user->activated = false;

        $response = $this->json('GET', route('activations.show', $user->id));

        $response->assertStatus(200);
        $response->assertResource(new UserActivationResource($user));
    }

    /**
     * Test the show functionality for user with no activation.
     */
    public function testShowNoActivation()
    {
        $user = User::factory()->create();
        $user->activated = false;

        $response = $this->json('GET', route('activations.show', $user->id));

        $response->assertStatus(200);
        $response->assertResource(new UserActivationResource($user));
    }
}
