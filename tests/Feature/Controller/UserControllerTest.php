<?php

namespace Tests\Feature\Controller;

use App\Http\Resources\User\FullUserResource;
use App\Http\Resources\User\SimplifiedUserDivisionResource;
use App\Http\Resources\User\SimplifiedUserResource;
use App\Http\Resources\User\UserResource;
use App\Models\Division;
use App\Models\Driver;
use App\Models\Position;
use App\Models\SCBA;
use App\Models\User;
use App\Models\UserGroup;
use Illuminate\Database\Eloquent\Model;
use Tests\TestCase;
use Tests\Traits\TestsResourceControllers;

class UserControllerTest extends TestCase
{
    use TestsResourceControllers;

    public function setUp(): void
    {
        parent::setUp();

        $this->modelType = User::class;
        $this->routeNamePrefix = 'users';
        $this->permissions = [
            'index'   => 'users.general',
            'store'   => 'users.general',
            'update'  => 'users.general',
            'destroy' => 'users.general',
            'show'    => 'users.general',
        ];
    }

    /**
     * Test the index functionality.
     */
    public function testIndex()
    {
        // Authentication
        $this::setActingAs([$this->permissions['index']]);

        // Preparation
        self::createModels(User::class, $this->modelCount);

        // Execution
        $response = $this->makeRequestToRoute('index');

        // Assertions
        $response->assertStatus(200);
        $response->assertResource(SimplifiedUserDivisionResource::collection(User::all()));
    }

    /**
     * Test the simplified index functionality.
     */
    public function testSimplifiedIndex()
    {
        // Authentication
        $this::setActingAs();

        // Preparation
        self::createModels(User::class, $this->modelCount);

        // Execution
        $response = $this->json('GET', route('users.simplified_index'));

        // Assertions
        $response->assertStatus(200);
        $response->assertResource(SimplifiedUserResource::collection(User::all()));
    }

    /**
     * Test the me functionality
     */
    public function testMe()
    {
        // Authentication
        $user = $this::setActingAs();

        // Execution
        $response = $this->json('GET', route('users.me'));

        // Assertions
        $response->assertStatus(200);
        $response->assertResource(new FullUserResource($user));
    }

    public function testSelfUpdateNewData()
    {
        // Authentication
        $user = $this::setActingAs();
        $user = $this->prepareSelfUpdateModel($user);
        $data = $this->prepareSelfUpdateData($user);

        $response = $this->json('POST', route('users.self_update'), $data);
        $response->assertStatus(200);
        $response->assertResource(new FullUserResource($user));
    }

    private function prepareSelfUpdateData(User $user)
    {
        return [
            'username'           => $user->username,
            'mobile'             => $user->mobile,
            'email'              => $user->email,
            'phone'              => $user->phone,
            'vegetarian'         => $user->vegetarian,
            'health_information' => $user->health_information,
            'privacy'            => [
                'email'  => $user->privacy->email,
                'mobile' => $user->privacy->mobile,
                'phone'  => $user->privacy->phone,
            ],
        ];
    }

    private function prepareSelfUpdateModel(User $user): User
    {
        $newUser = User::factory()->make();

        $user->username = $newUser->username;
        $user->mobile = $newUser->mobile;
        $user->email = $newUser->email;
        $user->phone = $newUser->phone;
        $user->vegetarian = $newUser->vegetarian;
        $user->health_information = $newUser->health_information;
        $user->privacy->email = $newUser->privacy->email;
        $user->privacy->mobile = $newUser->privacy->mobile;
        $user->privacy->phone = $newUser->privacy->phone;

        return $user;
    }

    /**
     * Test the store functionality.
     */
    public function testStore()
    {
        $this->storeModel(UserResource::class, [$this, "prepareModel"], [$this, "prepareData"]);
    }

    /**
     * Test the update functionality.
     */
    public function testUpdate()
    {
        $this->updateModel(UserResource::class, [$this, "prepareModel"], [$this, "prepareData"]);
        $this->updateInvalidModel([$this, "prepareModel"], [$this, "prepareData"]);
    }

    /**
     * Test destroy functionality with a valid model.
     */
    public function testDestroyValid()
    {
        $this->destroyModel(UserResource::class);
    }

    /**
     * Test destroy functionality with an invalid model.
     */
    public function testDestroyInvalid()
    {
        $this->destroyInvalidModel();
    }

    /**
     * Test show functionality with a valid mode.
     */
    public function testShowValid()
    {
        $this->showModel(UserResource::class, [$this, "prepareModel"]);
    }

    /**
     * Test show functionality with an invalid model.
     */
    public function testShowInvalid()
    {
        $this->showInvalidModel();
    }

    /**
     * Prepare the data to be send to the store and update routes.
     *
     * @param \Illuminate\Database\Eloquent\Model
     * @return array
     */
    private function prepareData(Model $user): array
    {
        $userGroups = [];
        foreach ($user->userGroups as $userGroup) {
            array_push($userGroups, $userGroup->id);
        }
        return [
            'first_name'  => $user->first_name,
            'last_name'   => $user->last_name,
            'gender'      => $user->gender,
            'position'    => $user->position->id,
            'division'    => $user->division->id,
            'birth'       => $user->birth,
            'phone'       => $user->phone,
            'user_groups' => $userGroups,
            'scba'        => [
                'next_examination'     => $user->scba->next_examination,
                'last_instruction'     => $user->scba->last_instruction,
                'last_excercise'       => $user->scba->last_excercise,
                'last_deployment'      => $user->scba->last_deployment,
                'cbrn'                 => $user->scba->cbrn,
                'last_cbrn_deployment' => $user->scba->last_cbrn_deployment,
            ],
            'driver'      => [
                'b'           => $user->driver->b,
                'be'          => $user->driver->be,
                'c'           => $user->driver->c,
                'ce'          => $user->driver->ce,
                'expiry_date' => $user->driver->expiry_date,
            ],
            'email'       => $user->email,
            'thwin_id'    => $user->thwin_id,
            'mobile'      => $user->mobile,
        ];
    }

    /**
     * Prepare the model for the store and update functionality.
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    private function prepareModel(): Model
    {
        $user = User::factory()->create();

        // Store and update do not set username
        $user->username = null;

        // Create relations
        self::createModelFor(Driver::class, 'user_id', $user->id);
        self::createModelFor(SCBA::class, 'user_id', $user->id);
        $user->division()->associate(Division::factory()->create());
        $user->position()->associate(Position::factory()->create());
        $user->userGroups()->attach(UserGroup::factory()->create());
        $user->userGroups()->attach(UserGroup::factory()->create());

        $user->save();
        return $user->fresh();
    }
}
