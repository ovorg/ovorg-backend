<?php

namespace Tests\Feature\Controller;

use App\Http\Resources\Event\DoodleEventResource;
use App\Http\Resources\Event\SimplifiedDoodleEventResource;
use App\Models\Event;
use App\Models\EventDateOption;
use App\Models\EventRegistration;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Passport\Passport;
use Tests\TestCase;
use Tests\Traits\AddAssertions;
use Tests\Traits\DisablesAuth;

class EventDoodleControllerTest extends TestCase
{
    use DisablesAuth;
    use AddAssertions;
    use WithFaker;

    /**
     * Disable authorisation.
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->disableAuthorisation();
    }

    /**
     * Test the index functionality using passport for authentication.
     */
    public function testIndex()
    {
        $user = User::factory()->create();
        Passport::actingAs($user);
        $events = $this->prepareIndexData($user)->slice(0, 2);

        $response = $this->json('GET', route('event_doodle.index'));

        $response->assertStatus(200);
        $response->assertResource(SimplifiedDoodleEventResource::collection($events));
    }

    /**
     * Test the index functionality using the doodle_token for authentication.
     */
    public function testIndexToken()
    {
        $user = User::factory()->create();
        $events = $this->prepareIndexData($user)->slice(0, 2);

        $response = $this->json('GET', route('event_doodle.index_token', $user->doodle_token));

        $response->assertStatus(200);
        $response->assertResource(SimplifiedDoodleEventResource::collection($events));
    }

    /**
     * Test that an Event without EventDateOptions
     * is returned from index.
     */
    public function testIndexWithoutDateOptions()
    {
        $user = User::factory()->create();
        Passport::actingAs($user);
        $events = Event::factory()->count(5)->create();
        $user->events()->attach($events);

        $response = $this->json('GET', route('event_doodle.index'));

        $response->assertStatus(200);
        $response->assertResource(SimplifiedDoodleEventResource::collection($events));
    }

    /**
     * Test the index functionality using passport for authentication.
     */
    public function testArchive()
    {
        $user = User::factory()->create();
        Passport::actingAs($user);
        $events = $this->prepareIndexData($user)->slice(2, 2);

        $response = $this->json('GET', route('event_doodle.archive'));

        $response->assertStatus(200);
        $response->assertResource(SimplifiedDoodleEventResource::collection($events));
    }

    /**
     * Test the index functionality using the doodle_token for authentication.
     */
    public function testArchiveToken()
    {
        $user = User::factory()->create();
        $events = $this->prepareIndexData($user)->slice(2, 2);

        $response = $this->json('GET', route('event_doodle.archive_token', $user->doodle_token));

        $response->assertStatus(200);
        $response->assertResource(SimplifiedDoodleEventResource::collection($events));
    }

    /**
     * Prepare the data needed to test the index functionality.
     */
    private function prepareIndexData(User $user)
    {
        $events = Event::factory()->count(4)->create();
        $user->events()->attach($events);
        $dateOptions = EventDateOption::factory()->count(2)
            ->create(['event_id' => $events->first()->id, 'date' => Carbon::tomorrow()]);
        EventDateOption::factory()->count(2)
            ->create(['event_id' => $events[1]->id, 'date' => Carbon::today()]);
        EventDateOption::factory()->count(2)
            ->create(['event_id' => $events[2]->id, 'date' => Carbon::yesterday()]);
        EventDateOption::factory()->count(2)
            ->create(['event_id' => $events[3]->id, 'date' => Carbon::yesterday()]);
        EventRegistration::factory()->count(5)->create([
            'user_id'              => $user->id,
            'event_date_option_id' => $dateOptions->first()->id,
        ]);
        return $events;
    }
    /*
$events = Event::factory()->count(4)
->has(EventDateOption::factory()->count(6)->state(
new Sequence(
['date' => Carbon::today()],
['date' => Carbon::yesterday()],
['date' => Carbon::yesterday()]
)
))->create();
$user->events()->attach($events);
$dateOptions = EventDateOption::factory()->count(2)
->create(['event_id' => $events->first()->id, 'date' => Carbon::tomorrow()]);
EventRegistration::factory()->count(5)->create([
'user_id'              => $user->id,
'event_date_option_id' => $dateOptions->first()->id,
]);
return $events;
 */

    /**
     * Test the show functionality using passport for authentication.
     */
    public function testShow()
    {
        $user = User::factory()->create();
        Passport::actingAs($user);
        $event = $this->prepareShowData($user);

        $response = $this->json('GET', route('event_doodle.show', $event->id));
        $response->assertStatus(200);
        $response->assertResource(new DoodleEventResource($event));
    }

    /**
     * Test the show functionality using the doodle_token for authentication.
     */
    public function testShowToken()
    {
        $user = User::factory()->create();
        $event = $this->prepareShowData($user);

        $response = $this->json('GET', route('event_doodle.show_token', [$event->id, $user->doodle_token]));

        $response->assertStatus(200);
        $response->assertResource(new DoodleEventResource($event));
    }

    /**
     * Prepare the data needed to test the show functionality.
     */
    private function prepareShowData($user)
    {
        $event = Event::factory()->count(2)->create()->first();
        $event->users()->attach($user);
        $dateOptions = EventDateOption::factory()->count(2)
            ->create(['event_id' => $event->id, 'date' => Carbon::tomorrow()]);
        EventRegistration::factory()->count(5)->create([
            'user_id'              => $user->id,
            'event_date_option_id' => $dateOptions->first()->id,
        ]);
        return $event;
    }

    /**
     * Test the update functionality using passport for authentication.
     */
    public function testUpdate()
    {
        $user = User::factory()->create();
        Passport::actingAs($user);
        [
            $event,
            $dateOption0,
            $dateOption1,
            $dateOption2,
            $data,
        ] = $this->prepareUpdateData($user);

        $response = $this->json(
            'PUT',
            route('event_doodle.update', [$event->id]),
            $data
        );

        $this->assertUpdate(
            $response,
            $event,
            $dateOption0,
            $dateOption1,
            $dateOption2,
            $data
        );
    }

    /**
     * Test the update functionality using a doodle_token for authentication.
     */
    public function testUpdateToken()
    {
        $user = User::factory()->create();
        [
            $event,
            $dateOption0,
            $dateOption1,
            $dateOption2,
            $data,
        ] = $this->prepareUpdateData($user);

        $response = $this->json(
            'PUT',
            route('event_doodle.update_token', [$event->id, $user->doodle_token]),
            $data
        );

        $this->assertUpdate(
            $response,
            $event,
            $dateOption0,
            $dateOption1,
            $dateOption2,
            $data
        );
    }

    /**
     * Make assertions for the update functionality.
     *
     * @param \Illuminate\Foundation\Testing\TestResponse $resonse
     * @param \App\Models\Event $event
     * @param \App\Models\EventDateOption $dateOption0
     * @param \App\Models\EventDateOption $dateOption1
     * @param \App\Models\EventDateOption $dateOption2
     * @param array $data
     */
    private function assertUpdate($response, $event, $dateOption0, $dateOption1, $dateOption2, $data)
    {
        $response->assertStatus(200);
        $response->assertResource(new DoodleEventResource($event));
        $this->assertEquals(1, $event->registrations()->where('event_date_option_id', $dateOption0->id)->count());
        $this->assertEquals(1, $event->registrations()->where('event_date_option_id', $dateOption1->id)->count());
        $this->assertEquals(0, $event->registrations()->where('event_date_option_id', $dateOption2->id)->count());
        $this->assertEquals($data['availability'], $event->registrations()->where('event_date_option_id', $dateOption0->id)->first()->availability);
        $this->assertEquals($data['comment'], $event->registrations()->where('event_date_option_id', $dateOption0->id)->first()->comment);
    }

    /**
     * Prepare the data needed to test the update functionality.
     */
    private function prepareUpdateData($user)
    {
        $event = Event::factory()->count(2)->create()->first();
        $event->users()->attach($user);
        $dateOption0 = EventDateOption::factory()->create([
            'event_id' => $event->id,
            'date'     => Carbon::today(),
        ]);
        $dateOption1 = EventDateOption::factory()->create([
            'event_id' => $event->id,
            'date'     => Carbon::tomorrow(),
        ]);
        $dateOption2 = EventDateOption::factory()->create([
            'event_id' => $event->id,
            'date'     => Carbon::yesterday(),
        ]);
        $data = [
            'availability' => $this->randomAvailability(),
            'comment'      => $this->faker->sentence,
            'date_options' => [
                $dateOption0->id,
                $dateOption1->id,
                $dateOption2->id,
            ],
        ];

        return [$event, $dateOption0, $dateOption1, $dateOption2, $data];
    }

    /**
     * Test the update functionality using passport for authentication,
     * when registering the second time.
     */
    public function testUpdateSecondTime()
    {
        $user = User::factory()->create();
        Passport::actingAs($user);
        $event = Event::factory()->create();
        $event->users()->attach($user);
        $dateOption = EventDateOption::factory()->create([
            'event_id' => $event->id,
            'date'     => Carbon::today(),
        ]);
        $registration = EventRegistration::factory()->create([
            'event_date_option_id' => $dateOption->id,
            'user_id'              => $user->id,
        ]);
        $data = [
            'availability' => $this->randomAvailability(),
            'comment'      => $this->faker->sentence,
            'date_options' => [
                $dateOption->id,
            ],
        ];

        $response = $this->json('PUT', route('event_doodle.update', [$event->id]), $data);

        $response->assertStatus(200);
        $response->assertResource(new DoodleEventResource($event));
        $this->assertEquals(1, $event->registrations()->where('event_date_option_id', $dateOption->id)->count());
        $this->assertEquals($registration->id, $event->registrations()->where('event_date_option_id', $dateOption->id)->first()->id);
        $this->assertEquals($data['availability'], $event->registrations()->where('event_date_option_id', $dateOption->id)->first()->availability);
        $this->assertEquals($data['comment'], $event->registrations()->where('event_date_option_id', $dateOption->id)->first()->comment);
    }

    /**
     * Test that updating an EventDateOption assinged to another Event
     * is not working.
     */
    public function testUpdateWrongDate()
    {
        $user = User::factory()->create();
        Passport::actingAs($user);
        $events = Event::factory()->count(2)->create();
        $user->events()->attach($events);
        $date = EventDateOption::factory()->create([
            'event_id' => $events[1]->id,
            'date'     => Carbon::tomorrow(),
        ]);
        $data = [
            'availability' => $this->randomAvailability(),
            'comment'      => $this->faker->sentence,
            'date_options' => [$date->id],
        ];

        $response = $this->json('PUT', route('event_doodle.update', [$events[0]->id]), $data);
        $date = $date->fresh();

        $response->assertStatus(200);
        $this->assertNull($date->registrations->first());
    }

    /**
     * Test that updating an EventDateOption the User is not invited to
     * is not working.
     */
    public function testUpdateNotInvitedEvent()
    {
        $user = User::factory()->create();
        Passport::actingAs($user);
        $event = Event::factory()->create();
        $date = EventDateOption::factory()->create([
            'event_id' => $event->id,
            'date'     => Carbon::tomorrow(),
        ]);
        $data = [
            'availability' => $this->randomAvailability(),
            'comment'      => $this->faker->sentence,
            'date_options' => [$date->id],
        ];

        $response = $this->json('PUT', route('event_doodle.update', [$event->id]), $data);
        $date = $date->fresh();

        $response->assertStatus(403);
        $this->assertNull($date->registrations->first());
    }

    /**
     * Return a random availability.
     *
     * @return string
     */
    private function randomAvailability(): string
    {
        return $this->faker->randomElement(['yes', 'no', 'maybe']);
    }
}
