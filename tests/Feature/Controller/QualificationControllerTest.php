<?php

namespace Tests\Feature\Controller;

use App\Http\Resources\Driver\DriverResource;
use App\Http\Resources\SCBA\SCBAResource;
use App\Models\Driver;
use App\Models\SCBA;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Tests\Traits\DisablesAuth;

class QualificationControllerTest extends TestCase
{
    use DisablesAuth;

    /**
     * Disable authentication and authorisation.
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->disableAuthentication();
        $this->disableAuthorisation();
    }

    /**
     * Test scba qualification functionality without models.
     */
    public function testSCBAWithoutModels()
    {
        $response = $this->json('GET', route('qualifications.scba'));

        $response->assertStatus(200);
        $response->assertExactJson(["data" => []]);
    }

    /**
     * Test scba qualification functionality.
     */
    public function testSCBA()
    {
        $scbas = $this->createSCBA();

        $response = $this->json('GET', route('qualifications.scba'));

        $response->assertStatus(200);
        $response->assertResource(SCBAResource::collection($scbas));
    }

    /**
     * Create scbas
     *
     * @param  int $count
     * @return \Illuminate\Support\Collection
     */
    private function createSCBA($count = 5): Collection
    {
        $users = User::factory()->count($count)->create();
        foreach ($users as $user) {
            SCBA::factory()->create(['user_id' => $user->id]);
        }
        return SCBA::get();
    }

    /**
     * Test the driver qualification functionality.
     */
    public function testDriver()
    {
        $drivers = $this->createDriver();

        $response = $this->json('GET', route('qualifications.driver'));

        $response->assertStatus(200);
        $response->assertResource(DriverResource::collection($drivers));
    }

    /**
     * Create $count drivers.
     *
     * @param  int $count
     * @return \Illuminate\Support\Collection
     */
    private function createDriver(int $count = 5): Collection
    {
        $users = User::factory()->count($count)->create();
        foreach ($users as $user) {
            Driver::factory()->create(['user_id' => $user->id]);
        }
        return Driver::get();
    }

    /**
     * Test the scba update functionality without date.
     */
    public function testSCBAUpdateWithoutDate()
    {
        $this->checkSCBAUpdate(null);
    }

    /**
     * Test the scba update functionality with date.
     */
    public function testSCBAUpdateWithDate()
    {
        $this->checkSCBAUpdate(Carbon::yesterday());
    }

    /**
     * Test the scba update functionality.
     *
     * @param $date
     */
    public function checkSCBAUpdate($date)
    {
        $scbas = $this->createScba();
        $scbas->first()->cbrn = false;
        $scbas->first()->save();
        $data = [
            'users'                => $scbas->map(function (SCBA $scba) {
                return $scba->user_id;
            }),
            'date'                 => $date,
            'next_examination'     => true,
            'last_instruction'     => true,
            'last_excercise'       => true,
            'last_deployment'      => true,
            'last_cbrn_deployment' => true,
        ];
        if (!$date) {
            $date = Carbon::today();
        }

        $response = $this->json('POST', route('qualifications.scba.update'), $data);

        $scbas = $scbas->map(function (SCBA $scba) use ($date) {
            $scba->next_examination = $date;
            $scba->last_instruction = $date;
            $scba->last_excercise = $date;
            $scba->last_deployment = $date;
            $scba->cbrn = true;
            $scba->last_cbrn_deployment = $date;
            return $scba;
        });
        $response->assertStatus(200);
        $response->assertResource(SCBAResource::collection($scbas));

        foreach ($scbas as $scba) {
            $dbScba = SCBA::find($scba->user_id);
            $this->assertTrue($scba->cbrn);
            $this->assertEquals($scba->next_examination, $dbScba->next_examination);
            $this->assertEquals($scba->last_instruction, $dbScba->last_instruction);
            $this->assertEquals($scba->last_excercise, $dbScba->last_excercise);
            $this->assertEquals($scba->last_deployment, $dbScba->last_deployment);
            $this->assertEquals($scba->last_cbrn_deployment, $dbScba->last_cbrn_deployment);
        }
    }

    public function testDriverUpdate()
    {
        $users = User::factory()->count(5)->create();
        $drivers = collect();
        foreach ($users as $user) {
            $drivers->push(Driver::factory()->create([
                'user_id' => $user->id,
                'b'       => false,
                'be'      => false,
                'c'       => false,
                'ce'      => false,
            ]));
        }
        $data = [
            'users' => $drivers->map(function (Driver $driver) {
                return $driver->user_id;
            }),
            'b'     => true,
            'be'    => true,
            'c'     => true,
            'ce'    => true,
        ];

        $response = $this->json('POST', route('qualifications.driver.update'), $data);

        $response->assertStatus(200);
        $drivers = $drivers->map(function (Driver $driver) {
            $driver->b = true;
            $driver->be = true;
            $driver->c = true;
            $driver->ce = true;
            return $driver;
        });
        $response->assertResource(DriverResource::collection($drivers));

        foreach ($drivers as $driver) {
            $driver = Driver::find($driver->user_id);
            $this->assertTrue($driver->b);
            $this->assertTrue($driver->be);
            $this->assertTrue($driver->c);
            $this->assertTrue($driver->ce);
        }
    }
}
