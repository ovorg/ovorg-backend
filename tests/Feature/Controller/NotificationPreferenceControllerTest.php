<?php

namespace Tests\Feature\Controller;

use App\Http\Resources\NotificationPreference\NotificationPreferenceResource;
use App\Models\NotificationPreference;
use App\Models\User;
use Laravel\Passport\Passport;
use Tests\TestCase;
use Tests\Traits\DisablesAuth;

class NotificationPreferenceControllerTest extends TestCase
{
    use DisablesAuth;

    /**
     * Disable authorisation.
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->disableAuthorisation();
    }

    public function testDefault()
    {
        $user = User::factory()->create();
        Passport::actingAs($user);

        $response = $this->json('GET', route('notification_preference.show'));

        $response->assertStatus(200);
        $response->assertSimilarJson([
            "data" => [
                "meeting_notifications" => true,
                "task_notifications" => true,
                "event_notifications" => true,
            ]
        ]);
    }

    public function testUpdate()
    {
        $user = User::factory()->create();
        Passport::actingAs($user);

        $preference = NotificationPreference::factory([
            "user_id" => $user->id,
            "meeting_notifications" => true,
            "task_notifications" => true,
            "event_notifications" => true,
        ])->create();

        $response = $this->json(
            'PUT',
            route('notification_preference.update'),
            [
                "meeting_notifications" => false,
                "task_notifications" => false,
                "event_notifications" => false,
            ]
        );

        $response->assertStatus(200);
        $preference = $user->notificationPreference;
        $this->assertFalse($preference->meeting_notifications);
        $this->assertFalse($preference->task_notifications);
        $this->assertFalse($preference->event_notifications);
        $response->assertResource(new NotificationPreferenceResource($preference));
    }

    public function testUpdateCreatesModel()
    {
        $user = User::factory()->create();
        Passport::actingAs($user);

        $response = $this->json(
            'PUT',
            route('notification_preference.update'),
            [
                "meeting_notifications" => false,
                "task_notifications" => false,
                "event_notifications" => false,
            ]
        );

        $response->assertStatus(201);
        $this->assertNotNull($user->notificationPreference);
    }
}
