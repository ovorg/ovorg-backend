<?php

namespace Tests\Feature\Controller;

use App\Http\Resources\UserGroup\SimplifiedUserGroupResource;
use App\Http\Resources\UserGroup\UserGroupResource;
use App\Models\User;
use App\Models\UserGroup;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;
use Tests\Traits\TestsResourceControllers;

class UserGroupControllerTest extends TestCase
{
    use TestsResourceControllers;

    public function setUp(): void
    {
        parent::setUp();

        $this->modelType = UserGroup::class;
        $this->routeNamePrefix = 'user_groups';
        $this->permissions = [
            'index'   => null,
            'store'   => 'user_groups.general',
            'update'  => 'user_groups.general',
            'destroy' => 'user_groups.general',
            'show'    => 'user_groups.general',
        ];
    }

    /**
     * Test the index functionality without models.
     */
    public function testIndexWithoutModels()
    {
        Event::fake();
        $this->indexWithoutModels();
    }

    /**
     * Test the index functionality with models.
     */
    public function testIndexWithModels()
    {
        Event::fake();
        $this->indexWithModels(SimplifiedUserGroupResource::class, function ($count) {
            $models = self::createModels(UserGroup::class, $count);
            foreach ($models as $model) {
                $model->generated = false;
                $model->save();
            }
            return $models;
        });
    }

    /**
     * Test the store functionality.
     */
    public function testStore()
    {
        $this->storeModel(UserGroupResource::class, [$this, "prepareModel"], [$this, "prepareData"]);
    }

    /**
     * Test the update functionality.
     */
    public function testUpdate()
    {
        $this->updateModel(UserGroupResource::class, [$this, "prepareModel"], [$this, "prepareData"]);
        $this->updateInvalidModel([$this, "prepareModel"], [$this, "prepareData"]);
    }

    /**
     * Test destroy functionality with a valid model.
     */
    public function testDestroyValid()
    {
        $this->destroyModel(UserGroupResource::class, function () {
            $model = UserGroup::factory()->create();
            return $model->fresh();
        });
    }

    /**
     * Test destroy functionality with an invalid model.
     */
    public function testDestroyInvalid()
    {
        $this->destroyInvalidModel();
    }

    /**
     * Test show functionality with a valid mode.
     */
    public function testShowValid()
    {
        $this->showModel(UserGroupResource::class, [$this, "prepareModel"]);
    }

    /**
     * Test show functionality with an invalid model.
     */
    public function testShowInvalid()
    {
        $this->showInvalidModel();
    }

    /**
     * Prepare the data to be send to the store and update routes.
     *
     * @param \Illuminate\Database\Eloquent\Model
     * @return array
     */
    private function prepareData(Model $model): array
    {
        $users = [];
        foreach ($model->users as $user) {
            array_push($users, $user->id);
        }
        return [
            'name'  => $model->name,
            'users' => $users,
        ];
    }

    /**
     * Prepare the model for the store and update functionality.
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    private function prepareModel(): Model
    {
        $userGroup = UserGroup::factory()->create();
        $user = User::factory()->create();
        $userGroup->users()->attach($user);
        $user = User::factory()->create();
        $userGroup->users()->attach($user);
        $userGroup->save();
        return $userGroup->fresh();
    }
}
