<?php

namespace Tests\Feature\Controller;

use App\Events\TimeTracking\UserPresentUpdated;
use App\Http\Resources\User\TimeTrackingUserResource;
use App\Models\User;
use Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;
use Tests\Traits\DisablesAuth;

class TimeTrackingControllerTest extends TestCase
{
    use DisablesAuth;

    /**
     * Disable authentication.
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->disableAuthentication();
    }

    /**
     * Test index functionality.
     */
    public function testIndex()
    {
        $users = User::factory()->count(5)->create();

        $response = $this->json('GET', route('time_tracking.index'));

        $response->assertStatus(200);
        $response->assertResource(TimeTrackingUserResource::collection($users));
    }

    /**
     * Test the show functionality.
     */
    public function testShow()
    {
        $user = User::factory()->create();

        $response = $this->json('GET', route('time_tracking.show', $user->id));

        $response->assertStatus(200);
        $response->assertResource(new TimeTrackingUserResource($user));
    }

    /**
     * Test scanned functionality when timeout is exceeded.
     */
    public function testScannedExceeded()
    {
        $user = User::factory()->create();
        $data = [
            'thwin_id' => $user->thwin_id,
        ];
        $user->scan_timeout = Carbon::now()->subSeconds(1);
        $user->save();

        Event::fake([UserPresentUpdated::class]);
        $response = $this->json('PATCH', route('time_tracking.scanned'), $data);

        $response->assertStatus(200);
        Event::assertDispatched(UserPresentUpdated::class);
        $newUser = User::find($user->id);
        $response->assertResource(new TimeTrackingUserResource($newUser));
        $this->assertNotEquals($newUser->present, $user->present);
    }

    /**
     * Test scanned functionality when timeout is not exceeded.
     */
    public function testScannedNotExceeded()
    {
        $user = User::factory()->create();
        $data = [
            'thwin_id' => $user->thwin_id,
        ];
        $user->scan_timeout = Carbon::now()->addSeconds(config('time_tracking.timeout') + 10);
        $user->save();

        Event::fake([UserPresentUpdated::class]);
        $response = $this->json('PATCH', route('time_tracking.scanned'), $data);

        $response->assertStatus(200);
        Event::assertNotDispatched(UserPresentUpdated::class);
        $newUser = User::find($user->id);
        $response->assertResource(new TimeTrackingUserResource($newUser));
        $this->assertEquals($newUser->present, $user->present);
    }
}
