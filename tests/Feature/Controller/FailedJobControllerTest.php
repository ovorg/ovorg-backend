<?php

namespace Tests\Feature\Controller;

use App\Http\Resources\FailedJobs\FailedJobResource;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Tests\Traits\DisablesAuth;

class FailedJobControllerTest extends TestCase
{
    use WithFaker;
    use DisablesAuth;

    private $failedJobsProvider;

    /**
     * Disable auth.
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->disableAuthentication();
        $this->disableAuthorisation();
        $this->failedJobsProvider = app()['queue.failer'];
    }

    /**
     * Test the status code for index.
     */
    public function testIndex()
    {
        $this->fakefailedJobs(5);

        $response = $this->json('GET', route('failed_jobs.index'));

        $response->assertStatus(200);
        $response->assertResource(FailedJobResource::collection($this->failedJobsProvider->all()));
    }

    /**
     * Test the show functionality.
     */
    public function testShow()
    {
        $this->fakeFailedJobs(2);
        $failedJob = $this->failedJobsProvider->all()[0];

        $response = $this->json('GET', route('failed_jobs.show', $failedJob->id));

        $response->assertStatus(200);
        $response->assertResource(new FailedJobResource($failedJob));
    }

    /**
     * Test show for with no failed jobs.
     */
    public function testShowNotFound()
    {
        $response = $this->json('GET', route('failed_jobs.show', 1));

        $response->assertStatus(404);
    }

    /**
     * Test the status code for flush.
     */
    public function testFlush()
    {
        $this->fakeFailedJobs(5);
        $failedJobs = $this->failedJobsProvider->all();

        $response = $this->json('DELETE', route('failed_jobs.flush'));

        $response->assertStatus(200);
        $this->assertEquals(0, count($this->failedJobsProvider->all()));
        $response->assertResource(FailedJobResource::collection($failedJobs));
    }

    /**
     * Test the destroy functionality.
     */
    public function testDestroy()
    {
        $this->fakeFailedJobs(2);
        $failedJob = $this->failedJobsProvider->all()[0];

        $response = $this->json('DELETE', route('failed_jobs.destroy', $failedJob->id));

        $response->assertStatus(200);
        $this->assertEquals(1, count($this->failedJobsProvider->all()));
        $response->assertResource(new FailedJobResource($failedJob));
    }

    /**
     * Add $count fake failed jobs.
     *
     * @param int $count
     */
    private function fakeFailedJobs(int $count)
    {
        for ($i = 0; $i < $count; $i++) {
            $this->fakeFailedJob();
        }
    }

    /**
     * Add a fake failed job.
     */
    private function fakeFailedJob()
    {
        $this->failedJobsProvider->log($this->faker->word, $this->faker->sentence, $this->faker->sentence, $this->faker->sentence);
    }
}
