<?php

namespace Tests\Feature\Controller;

use App\Http\Resources\FeedbackRecipient\FeedbackRecipientResource;
use App\Models\FeedbackRecipient;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Tests\Traits\DisablesAuth;

class FeedbackRecipientControllerTest extends TestCase
{
    use DisablesAuth;
    use WithFaker;

    /**
     * Disable auth.
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->disableAuthentication();
        $this->disableAuthorisation();
    }

    /**
     * Test the index functionality.
     *
     * @return void
     */
    public function testIndex()
    {
        $recipients = FeedbackRecipient::factory()->count(5)->create();

        $response = $this->json('GET', route('feedback_recipients.index'));

        $response->assertStatus(200);
        $response->assertResource(FeedbackRecipientResource::collection($recipients));
    }

    /**
     * Test the show functionality.
     */
    public function testShow()
    {
        $recipient = FeedbackRecipient::factory()->count(2)->create()->first();

        $response = $this->json('GET', route('feedback_recipients.show', $recipient->id));

        $response->assertStatus(200);
        $response->assertResource(new FeedbackRecipientResource($recipient));
    }

    /**
     * Test the store functionality
     */
    public function testStore()
    {
        $data = [
            'email' => $this->faker->unique()->email,
        ];
        $recipient = FeedbackRecipient::factory()->create();
        $recipient->id++;
        $recipient->email = $data['email'];

        $response = $this->json('POST', route('feedback_recipients.store'), $data);

        $response->assertStatus(201);
        $response->assertResource(new FeedbackRecipientResource($recipient));
        $this->assertNotNull(FeedbackRecipient::find($recipient->id));
    }

    /**
     * Test the update functionality.
     */
    public function testUpdate()
    {
        $recipient = FeedbackRecipient::factory()->create();
        $recipient->email = $this->faker->unique()->email;
        $data = [
            'email' => $recipient->email,
        ];

        $response = $this->json('PUT', route('feedback_recipients.update', $recipient->id), $data);

        $response->assertStatus(200);
        $response->assertResource(new FeedbackRecipientResource($recipient));
    }

    /**
     * Test the destroy functionality.
     */
    public function testDestroy()
    {
        $recipient = FeedbackRecipient::factory()->create();

        $response = $this->json('DELETE', route('feedback_recipients.destroy', $recipient->id));

        $response->assertStatus(200);
        $response->assertResource(new FeedbackRecipientResource($recipient));
        $this->assertNull(FeedbackRecipient::find($recipient->id));
    }
}
