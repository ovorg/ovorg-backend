<?php

namespace Tests\Feature\Controller;

use App\Http\Resources\FeedbackTemplate\FeedbackTemplateResource;
use App\Models\FeedbackTemplate;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Tests\Traits\DisablesAuth;

class FeedbackTemplateControllerTest extends TestCase
{
    use DisablesAuth;
    use WithFaker;

    /**
     * Disable auth.
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->disableAuthentication();
        $this->disableAuthorisation();
    }

    /**
     * Test the index functionality.
     */
    public function testIndex()
    {
        $feedbackTemplates = FeedbackTemplate::factory()->count(5)->create();

        $response = $this->json('GET', route('feedback_templates.index'));

        $response->assertStatus(200);
        $response->assertResource(FeedbackTemplateResource::collection($feedbackTemplates));
    }

    /**
     * Test the show functionality.
     */
    public function testShow()
    {
        $feedbackTemplate = FeedbackTemplate::factory()->count(2)->create()->first();

        $response = $this->json('GET', route('feedback_templates.show', $feedbackTemplate->id));

        $response->assertStatus(200);
        $response->assertResource(new FeedbackTemplateResource($feedbackTemplate));
    }

    /**
     * Test the store functionality.
     */
    public function testStore()
    {
        $data = [
            'name'     => $this->faker->word,
            'template' => $this->faker->text,
        ];

        $response = $this->json('POST', route('feedback_templates.store'), $data);

        $response->assertStatus(201);
        $feedbackTemplate = FeedbackTemplate::first();
        $this->assertNotNull($feedbackTemplate);
        $response->assertResource(new FeedbackTemplateResource($feedbackTemplate));
    }

    /**
     * Test the update functionality.
     */
    public function testUpdate()
    {
        $feedbackTemplate = FeedbackTemplate::factory()->create();
        $data = [
            'name'     => $this->faker->unique()->word,
            'template' => $this->faker->text,
        ];

        $response = $this->json('PUT', route('feedback_templates.update', $feedbackTemplate->id), $data);
        $feedbackTemplate = $feedbackTemplate->fresh();

        $response->assertStatus(200);
        $response->assertResource(new FeedbackTemplateResource($feedbackTemplate));
        $this->assertEquals($data['name'], $feedbackTemplate->name);
        $this->assertEquals($data['template'], $feedbackTemplate->template);
    }

    /**
     * Test the destory functionality.
     */
    public function testDestroy()
    {
        $feedbackTemplate = FeedbackTemplate::factory()->create();

        $response = $this->json('DELETE', route('feedback_templates.destroy', $feedbackTemplate->id));

        $response->assertStatus(200);
        $response->assertResource(new FeedbackTemplateResource($feedbackTemplate));
        $this->assertNull(FeedbackTemplate::find($feedbackTemplate->id));
    }
}
