<?php

namespace Tests\Feature\Controller;

use App\Http\Resources\Event\EventResource;
use App\Http\Resources\Event\FullEventResource;
use App\Http\Resources\Event\SimplifiedEventResource;
use App\Models\Event;
use App\Models\EventDateOption;
use App\Models\EventRegistration;
use App\Models\User;
use App\Models\UserGroup;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Tests\Traits\DisablesAuth;

class EventControllerTest extends TestCase
{
    use DisablesAuth;
    use WithFaker;

    /**
     * Disable auth.
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->disableAuthorisation();
        $this->disableAuthentication();
    }

    /**
     * Test the index functionality.
     */
    public function testIndex()
    {
        $events = Event::factory()->count(5)
            ->has(UserGroup::factory()->count(2))
            ->create();

        $users = User::factory()->count(2)->create();
        foreach ($users as $user) {
            $user->events()->attach($events);
        }
        $dates = EventDateOption::factory()->count(2)
            ->create(['event_id' => $events->first()->id]);
        EventRegistration::factory()->count(2)->create([
            'user_id'              => $users->first()->id,
            'event_date_option_id' => $dates->first()->id,
        ]);

        $response = $this->json('GET', route('events.index'));

        $response->assertStatus(200);
        $response->assertResource(SimplifiedEventResource::collection($events));
    }

    /**
     * Test the show functionality.
     */
    public function testShow()
    {
        $event = $this->prepareShowData();

        $response = $this->json('GET', route('events.show', $event->id));

        $response->assertStatus(200);
        $response->assertResource(new EventResource($event));
    }

    /**
     * Test the enhanced show functionality.
     */
    public function testEnhancedShow()
    {
        $event = $this->prepareShowData();

        $response = $this->json('GET', route('events.show_enhanced', $event->id));

        $response->assertStatus(200);
        $response->assertResource(new FullEventResource($event));
    }

    /**
     * Prepare data to test the show and the enhanced show functionality.
     *
     * @return \App\Models\Event
     */
    private function prepareShowData(): Event
    {
        $event = Event::factory()
            ->has(UserGroup::factory()->count(2))
            ->create();
        $users = User::factory()->count(2)->create();
        $event->users()->attach($users);
        $dates = EventDateOption::factory()->count(2)
            ->create(['event_id' => $event->id]);
        EventRegistration::factory()->count(2)->create([
            'user_id'              => $users->first()->id,
            'event_date_option_id' => $dates->first()->id,
        ]);
        return $event;
    }

    /**
     * Test the destroy functionality.
     */
    public function testDestroy()
    {
        $event = Event::factory()->create();

        $response = $this->json('DELETE', route('events.destroy', $event->id));

        $response->assertStatus(200);
        $response->assertResource(new EventResource($event));
        $this->assertNull(Event::find($event->id));
    }

    /**
     * Test the store functionality.
     */
    public function testStore()
    {
        [$event, $users, $userGroups, $data] = $this->prepareStoreUpdateData();

        $response = $this->json('POST', route('events.store'), $data);

        $response->assertStatus(201);
        $this->assertStoreUpdate($event, $users, $userGroups);
    }

    /**
     * Test the update functionality.
     */
    public function testUpdate()
    {
        [$event, $users, $userGroups, $data] = $this->prepareStoreUpdateData();
        $updateEvent = $this->preparePersistentUpdateData();

        $response = $this->json('PUT', route('events.update', $updateEvent->id), $data);

        $response->assertStatus(200);
        $this->assertStoreUpdate($event, $users, $userGroups);
    }

    /**
     * Test the update function removing the relations.
     */
    public function testeUpdateRemoveRelations()
    {
        $event = $this->preparePersistentUpdateData();
        $data = [
            'title'         => $event->title,
            'description'   => $event->description,
            'force_comment' => $event->force_comment,
            'responsible'   => $event->responsible->id,
            'users'         => [],
            'user_groups'   => [],
            'date_options'  => [],
        ];

        $response = $this->json('PUT', route('events.update', $event->id), $data);

        $response->assertStatus(200);
        $event = $event->fresh();
        $this->assertTrue($event->users->isEmpty());
        $this->assertTrue($event->userGroups->isEmpty());
        $this->assertTrue($event->dateOptions->isEmpty());
    }

    /**
     * Test that update doenst create new DateOptions for the same day.
     */
    public function testUpdateDateOptionsDontCreateNew()
    {
        $event = $this->preparePersistentUpdateData();
        $data = [
            'title'         => $event->title,
            'description'   => $event->description,
            'force_comment' => $event->force_comment,
            'responsible'   => $event->responsible->id,
            'users'         => [],
            'user_groups'   => [],
            'date_options'  => $event->dateOptions->map(function (EventDateOption $dateOption) {
                return $dateOption->date;
            }),
        ];

        $response = $this->json('PUT', route('events.update', $event->id), $data);

        $response->assertStatus(200);
        $newEvent = Event::find($event->id);
        foreach ($event->dateOptions as $dateOption) {
            $this->assertTrue($newEvent->dateOptions->contains($dateOption));
        }
        $this->assertEquals($event->dateOptions()->count(), $newEvent->dateOptions()->count());
    }

    /**
     * Assertions for the store and the update functionality.
     *
     * @param \App\Models\Event $event
     * @param \Illuminate\Support\Collection $users
     * @param \Illuminate\Support\Collection $userGroups
     */
    private function assertStoreUpdate(Event $event, Collection $users, Collection $userGroups)
    {
        $this->assertEquals(1, Event::count());
        $createdEvent = Event::first();
        $this->assertEquals($event->title, $createdEvent->title);
        $this->assertEquals($event->description, $createdEvent->description);
        $this->assertEquals($event->force_comment, $createdEvent->force_comment);
        $this->assertEquals($event->responsible, $createdEvent->responsible);
        foreach ($users as $user) {
            $this->assertTrue($createdEvent->users->contains($user));
        }
        foreach ($userGroups as $userGroup) {
            $this->assertTrue($createdEvent->userGroups->contains($userGroup));
        }
        $this->assertEquals(
            1,
            $createdEvent->dateOptions()->where('date', Carbon::yesterday())->count()
        );
        $this->assertEquals(
            1,
            $createdEvent->dateOptions()->where('date', Carbon::today())->count()
        );
        $this->assertEquals(
            1,
            $createdEvent->dateOptions()->where('date', Carbon::tomorrow())->count()
        );
    }

    /**
     * Prepare data for the update functionality
     * which is saved to the database.
     *
     * @return \App\Models\Event
     */
    private function preparePersistentUpdateData(): Event
    {
        return Event::factory()
            ->has(User::factory()->count(5))
            ->has(UserGroup::factory()->count(5))
            ->has(EventDateOption::factory()->count(5))
            ->for(User::factory(), 'responsible')
            ->create();
    }

    /**
     * Prepare data for the store and update functionality
     * which is saved to the database.
     *
     * @return array with:
     * @return \App\Models\Event
     * @return \Illuminate\Support\Collection
     * @return \Illuminate\Support\Collection
     * @return array
     */
    private function prepareStoreUpdateData(): array
    {
        $event = Event::factory()
            ->has(User::factory()->count(5))
            ->has(UserGroup::factory()->count(5))
            ->for(User::factory(), 'responsible')
            ->make();

        $data = [
            'title'         => $event->title,
            'description'   => $event->description,
            'force_comment' => $event->force_comment,
            'responsible'   => $event->responsible->id,
            'users'         => $event->users->map(function (User $user) {
                return $user->id;
            }),
            'user_groups'   => $event->userGroups->map(function (UserGroup $userGroup) {
                return $userGroup->id;
            }),
            'date_options'  => [
                Carbon::today(),
                Carbon::yesterday(),
                Carbon::tomorrow(),
            ],
        ];

        return [$event, $event->users, $event->userGroups, $data];
    }
}
