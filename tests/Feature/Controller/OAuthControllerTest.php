<?php

namespace Tests\Feature\Controller;

use App\Http\Resources\OAuthClient\OAuthClientResource;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Passport\Client;
use Tests\TestCase;
use Tests\Traits\DisablesAuth;

class OAuthControllerTest extends TestCase
{
    use DisablesAuth;
    use WithFaker;

    /**
     * Disable auth.
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->disableAuthorisation();
        $this->disableAuthentication();
    }

    /**
     * Test the index functionality.
     */
    public function testIndex()
    {
        $clients = Client::factory()->createMany([
            [
                'personal_access_client' => false,
                'password_client'        => false,
                'revoked'                => false,
            ],
            [
                'personal_access_client' => false,
                'password_client'        => false,
                'revoked'                => false,
            ],
            [
                'personal_access_client' => false,
                'password_client'        => false,
                'revoked'                => true,
            ],
            [
                'personal_access_client' => true,
                'password_client'        => false,
                'revoked'                => false,
            ],
            [
                'personal_access_client' => false,
                'password_client'        => true,
                'revoked'                => false,
            ],
            [
                'personal_access_client' => true,
                'password_client'        => true,
                'revoked'                => false,
            ],
        ]);
        $clients = collect()->push($clients->shift())->push($clients->shift());

        $response = $this->json('GET', route('oauth_clients.index'));

        $response->assertStatus(200);
        $response->assertResource(OAuthClientResource::collection($clients));
    }

    /**
     * Test show functionality.
     */
    public function testShow()
    {
        $client = Client::factory()->count(2)->create([
            'personal_access_client' => false,
            'password_client'        => false,
            'revoked'                => false,
        ])->first();

        $response = $this->json('GET', route('oauth_clients.show', $client->id));

        $response->assertStatus(200);
        $response->assertResource(new OAuthClientResource($client));
    }

    /**
     * Test show functionality for revoked clients.
     */
    public function testShowRevoked()
    {
        $client = Client::factory()->create([
            'personal_access_client' => false,
            'password_client'        => false,
            'revoked'                => true,
        ]);

        $response = $this->json('GET', route('oauth_clients.show', $client->id));

        $response->assertStatus(200);
        $response->assertResource(new OAuthClientResource($client));
    }

    /**
     * Test show functionality for personal access clients.
     */
    public function testShowPersonalAccesClient()
    {
        $client = Client::factory()->create([
            'personal_access_client' => true,
            'password_client'        => false,
            'revoked'                => false,
        ]);

        $response = $this->json('GET', route('oauth_clients.show', $client->id));

        $response->assertStatus(404);
    }

    /**
     * Test show functionality for password clients.
     */
    public function testShowPasswordClient()
    {
        $client = Client::factory()->create([
            'personal_access_client' => false,
            'password_client'        => true,
            'revoked'                => false,
        ]);

        $response = $this->json('GET', route('oauth_clients.show', $client->id));

        $response->assertStatus(404);
    }

    /**
     * Test the store functionality.
     */
    public function testStore()
    {
        $data = [
            'name' => $this->faker->word,
        ];

        $response = $this->json('POST', route('oauth_clients.store'), $data);

        $response->assertStatus(201);
        $client = Client::first();
        $response->assertResource(new OAuthClientResource($client));
        $this->assertNotNull($client->secret);
        $this->assertNotEquals('', $client->secret);
        $this->assertFalse($client->personal_access_client);
        $this->assertFalse($client->password_client);
        $this->assertFalse($client->revoked);
    }

    /**
     * Test the update functionality.
     */
    public function testUpdate()
    {
        $client = Client::factory()->create([
            'personal_access_client' => false,
            'password_client'        => false,
            'revoked'                => false,
        ]);

        $data = [
            'name' => $this->faker->word,
        ];

        $response = $this->json('PUT', route('oauth_clients.update', $client->id), $data);

        $response->assertStatus(200);
        $client = $client->fresh();
        $response->assertResource(new OAuthClientResource($client));
        $this->assertEquals($data['name'], $client->name);
    }

    /**
     * Test that update is not working for personal access clients.
     */
    public function testUpdatePersonalAccessClient()
    {
        $client = Client::factory()->create([
            'personal_access_client' => true,
            'password_client'        => false,
            'revoked'                => false,
        ]);

        $data = [
            'name' => $this->faker->word,
        ];

        $response = $this->json('PUT', route('oauth_clients.update', $client->id), $data);

        $response->assertStatus(404);
    }

    /**
     * Test that update is not working for password clients.
     */
    public function testUpdatePasswordClient()
    {
        $client = Client::factory()->create([
            'personal_access_client' => true,
            'password_client'        => false,
            'revoked'                => false,
        ]);

        $data = [
            'name' => $this->faker->word,
        ];

        $response = $this->json('PUT', route('oauth_clients.update', $client->id), $data);

        $response->assertStatus(404);
    }

    /**
     * Test that update is not working for revoked clients.
     */
    public function testUpdateRevokedClient()
    {
        $client = Client::factory()->create([
            'personal_access_client' => true,
            'password_client'        => false,
            'revoked'                => false,
        ]);

        $data = [
            'name' => $this->faker->word,
        ];

        $response = $this->json('PUT', route('oauth_clients.update', $client->id), $data);

        $response->assertStatus(404);
    }

    /**
     * Test the destroy functionality.
     */
    public function testDestroy()
    {
        $client = Client::factory()->create([
            'personal_access_client' => false,
            'password_client'        => false,
            'revoked'                => false,
        ]);

        $response = $this->json('DELETE', route('oauth_clients.destroy', $client->id));

        $response->assertStatus(200);
        $client = $client->fresh();
        $this->assertTrue($client->revoked);
        $response->assertResource(new OAuthClientResource($client));
    }

    /**
     * Test the destroy functionality for revoked clients.
     */
    public function testDestroyRevoked()
    {
        $client = Client::factory()->create([
            'personal_access_client' => false,
            'password_client'        => false,
            'revoked'                => true,
        ]);

        $response = $this->json('DELETE', route('oauth_clients.destroy', $client->id));

        $response->assertStatus(200);
        $client = $client->fresh();
        $this->assertTrue($client->revoked);
        $response->assertResource(new OAuthClientResource($client));
    }

    /**
     * Test that the destory functionality is not working for personal access clients.
     */
    public function testDestroyPersonalAccessClient()
    {
        $client = Client::factory()->create([
            'personal_access_client' => true,
            'password_client'        => false,
            'revoked'                => false,
        ]);

        $response = $this->json('DELETE', route('oauth_clients.destroy', $client->id));

        $response->assertStatus(404);
        $this->assertFalse($client->fresh()->revoked);
    }

    /**
     * Test that the destory functionality is not working for password clients.
     */
    public function testDestroyPasswordClient()
    {
        $client = Client::factory()->create([
            'personal_access_client' => false,
            'password_client'        => true,
            'revoked'                => false,
        ]);

        $response = $this->json('DELETE', route('oauth_clients.destroy', $client->id));

        $response->assertStatus(404);
        $this->assertFalse($client->fresh()->revoked);
    }
}
