<?php

namespace Tests\Feature\Controller;

use App\Http\Resources\Position\PositionResource;
use App\Http\Resources\Position\SimplifiedPositionResource;
use App\Models\Position;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Tests\TestCase;
use Tests\Traits\TestsResourceControllers;

class PositionControllerTest extends TestCase
{
    use TestsResourceControllers;

    public function setUp(): void
    {
        parent::setUp();

        $this->modelType = Position::class;
        $this->routeNamePrefix = 'positions';
        $this->permissions = [
            'index'   => null,
            'store'   => 'positions.general',
            'update'  => 'positions.general',
            'destroy' => 'positions.general',
            'show'    => null,
        ];
    }

    /**
     * Test the index functionality without models.
     */
    public function testIndexWithoutModels()
    {
        $this->indexWithoutModels();
    }

    /**
     * Test the index functionality with models.
     */
    public function testIndexWithModels()
    {
        $this->indexWithModels(SimplifiedPositionResource::class);
    }

    /**
     * Test the store functionality.
     */
    public function testStore()
    {
        $this->storeModel(PositionResource::class, [$this, "prepareModel"], [$this, "prepareData"]);
    }

    /**
     * Test the update functionality.
     */
    public function testUpdate()
    {
        $this->updateModel(PositionResource::class, [$this, "prepareModel"], [$this, "prepareData"]);
        $this->updateInvalidModel([$this, "prepareModel"], [$this, "prepareData"]);
    }

    /**
     * Test destroy functionality with a valid model.
     */
    public function testDestroyValid()
    {
        $this->destroyModel(PositionResource::class);
    }

    /**
     * Test destroy functionality with an invalid model.
     */
    public function testDestroyInvalid()
    {
        $this->destroyInvalidModel();
    }

    /**
     * Test show functionality with a valid mode.
     */
    public function testShowValid()
    {
        $this->showModel(PositionResource::class, [$this, "prepareModel"]);
    }

    /**
     * Test show functionality with an invalid model.
     */
    public function testShowInvalid()
    {
        $this->showInvalidModel();
    }

    /**
     * Prepare the data to be send to the store and update routes.
     *
     * @param \Illuminate\Database\Eloquent\Model
     * @return array
     */
    private function prepareData(Model $model): array
    {
        return [
            'male'   => $model->male,
            'female' => $model->female,
            'inter'  => $model->inter,
        ];
    }

    /**
     * Prepare the model for the store and update functionality.
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    private function prepareModel(): Model
    {
        return Position::factory()->create();
    }
}
