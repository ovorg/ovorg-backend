<?php

namespace Tests\Feature\Controller;

use App\Http\Resources\File\FileResource;
use App\Models\File;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Illuminate\Testing\TestResponse;
use Laravel\Passport\Passport;
use Tests\TestCase;
use Tests\Traits\DisablesAuth;

class FileControllerTest extends TestCase
{
    use WithFaker;
    use DisablesAuth;

    /**
     * Disable authorisation and authentication.
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->disableAuthorisation();
        $user = User::factory()->create();
        Passport::actingAs($user);
    }

    /**
     * Test the index functionality without adding any models.
     */
    public function testIndexWithoutModels(): void
    {
        $response = $this->json('GET', route('files.index'));

        $response->assertStatus(200);
        $response->assertExactJson(["data" => []]);
    }

    /**
     * Test the index functionality with models.
     */
    public function testIndex(): void
    {
        $models = $this->createModels();

        $response = $this->json('GET', route('files.index'));

        $response->assertStatus(200);
        $response->assertResource(FileResource::collection($models));
    }

    /**
     * Test the show functionality without model.
     */
    public function testShowWithoutModel()
    {
        $response = $this->json('GET', route('files.show', 0));

        $response->assertStatus(404);
    }

    /**
     * Test the show functionality with a model.
     */
    public function testShow(): void
    {
        $model = $this->createModel();
        $file = UploadedFile::fake()->image($model->name);
        $model->path = Storage::put(File::$uploadFolder, $file);
        $model->save();

        $response = $this->json('GET', route('files.show', $model->id));

        $response->assertStatus(200);
        $response->assertHeader('Content-Disposition', "attachment; filename=$file->name");

        Storage::delete($model->path);
    }

    /**
     * Test the store functionality.
     */
    public function testStore(): void
    {
        $name = $this->faker()->word;
        $response = $this->json('POST', route('files.store'), [
            'file' => UploadedFile::fake()->image($name . '123'),
            'name' => $name,
        ]);

        $this->assertStore($response, $name);
    }

    /**
     * Test the store functionality without setting name.
     */
    public function testStoreWithoutName(): void
    {
        $name = $this->faker()->word;
        $response = $this->json('POST', route('files.store'), [
            'file' => UploadedFile::fake()->image($name),
        ]);

        $this->assertStore($response, $name);
    }

    /**
     * Assertions for store.
     * Delete the file afterwards.
     *
     * @param \Illuminate\Http\Response $response
     * @param string @name
     */
    private function assertStore(TestResponse $response, string $name): void
    {
        $response->assertStatus(201);
        $file = File::find($response->getData()->data->id);

        $this->assertNotNull($file);
        $this->assertEquals($file->name, $name);
        $response->assertResource(new FileResource($file));
        Storage::disk()->assertExists($file->path);

        Storage::delete($file->path);
    }

    /**
     * Test the update functionality
     */
    public function testUpdate(): void
    {
        $model = $this->createModel();
        $model->name = $this->createModel()->name;
        $data = $this->getData($model);

        $response = $this->json('PUT', route('files.update', $model->id), $data);

        $response->assertStatus(200);
        $response->assertResource(new FileResource($model));
    }

    /**
     * Test the destroy functionality.
     */
    public function testDestroy(): void
    {
        $model = $this->createModel();
        $file = UploadedFile::fake()->image($model->name);
        $model->path = Storage::put(File::$uploadFolder, $file);
        $model->save();

        $response = $this->json('DELETE', route('files.destroy', $model->id));

        $response->assertStatus(200);
        $response->assertResource(new FileResource($model));
        $this->assertNull(File::find($model->id));
        Storage::disk()->assertMissing($model->path);
    }

    /**
     * Create data for a request.
     */
    private function createData(): array
    {
        $model = $this->createModel();
        return $this->getData($model);
    }

    /**
     * Convert a model into the request data.
     *
     * @param  \App\Models\File $file
     * @return array
     */
    private function getData(File $file): array
    {
        return [
            'name' => $file->name,
        ];
    }

    /**
     * Create a model for a request.
     *
     * @return \App\Models\File $file
     */
    private function createModel(): File
    {
        return File::factory()->create();
    }

    /**
     * Create several models for a request.
     *
     * @param  int $count
     * @return \Illuminate\Support\Collection
     */
    private function createModels(int $count = 5): Collection
    {
        return File::factory()->count($count)->create();
    }
}
