<?php

namespace Tests\Feature\Controller;

use App\Events\TimeTracking\UserPresentUpdated;
use App\Http\Resources\User\PresenceUserResource;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;
use Tests\Traits\DisablesAuth;

class UserPresenceControllerTest extends TestCase
{
    use DisablesAuth;

    /**
     * Disable auth.
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->disableAuthentication();
        $this->disableAuthorisation();
    }

    /**
     * Test the index functionality.
     */
    public function testIndex()
    {
        $users = User::factory()->count(5)->create();

        $response = $this->json('GET', route('presence.index'));

        $response->assertStatus(200);
        $response->assertResource(PresenceUserResource::collection($users));
    }

    /**
     * Test the show functionality.
     */
    public function testShow()
    {
        $user = User::factory()->create();

        $response = $this->json('GET', route('presence.show', $user->id));
        $user = $user->fresh();

        $response->assertStatus(200);
        $response->assertResource(new PresenceUserResource($user));
    }

    /**
     * Test the update functionality when setting present to true.
     */
    public function testUpdatePresentTrue()
    {
        $user = User::factory()->create(['present' => false, 'scan_timeout' => Carbon::now()]);
        $data = [
            'present' => true,
        ];

        Event::fake([UserPresentUpdated::class]);
        $response = $this->json('PUT', route('presence.update', $user->id), $data);
        $user = $user->fresh();

        $response->assertStatus(200);
        $this->assertTrue($user->present);
        Event::assertDispatched(UserPresentUpdated::class);
        $response->assertResource(new PresenceUserResource($user));
        $this->assertNull($user->scan_timeout);
    }

    /**
     * Test the update functionality when setting present to false.
     */
    public function testUpdatePresentFalse()
    {
        $user = User::factory()->create(['present' => true, 'scan_timeout' => Carbon::now()]);
        $data = [
            'present' => false,
        ];

        Event::fake([UserPresentUpdated::class]);
        $response = $this->json('PUT', route('presence.update', $user->id), $data);
        $user = $user->fresh();

        $response->assertStatus(200);
        $this->assertFalse($user->present);
        Event::assertDispatched(UserPresentUpdated::class);
        $response->assertResource(new PresenceUserResource($user));
        $this->assertNull($user->scan_timeout);
    }
}
