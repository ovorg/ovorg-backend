<?php

namespace Tests\Feature\Controller;

use App\Http\Resources\Task\SimplifiedTaskResource;
use App\Http\Resources\Task\TaskResource;
use App\Models\File;
use App\Models\Task;
use App\Models\TaskCategory;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Laravel\Passport\Passport;
use Tests\TestCase;
use Tests\Traits\DisablesAuth;

class TaskControllerTest extends TestCase
{
    use DisablesAuth;
    use WithFaker;

    /**
     * Disable authorisation.
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->disableAuthorisation();
    }

    /**
     * Test the index functionality without adding any models.
     */
    public function testIndexWithoutModels(): void
    {
        $this->disableAuthentication();
        $response = $this->json('GET', route('tasks.index'));

        $response->assertStatus(200);
        $response->assertExactJson(["data" => []]);
    }

    /**
     * Test the index functionality with models.
     */
    public function testIndex(): void
    {
        $user = User::factory()->create();
        Passport::actingAs($user);
        $tasks = $this->createPendingTasks();
        $this->createFinishedTasks();

        $response = $this->json('GET', route('tasks.index'));

        $response->assertStatus(200);
        $response->assertResource(SimplifiedTaskResource::collection($tasks));
    }

    /**
     * Test the archive functionality with models.
     */
    public function testArchive(): void
    {
        $user = User::factory()->create();
        Passport::actingAs($user);
        $this->createPendingTasks();
        $finishedTasks = $this->createFinishedTasks();

        $response = $this->json('GET', route('tasks.archive'));

        $response->assertStatus(200);
        $response->assertResource(SimplifiedTaskResource::collection($finishedTasks));
    }

    /**
     * Test the show functionality without model.
     */
    public function testShowWithoutModel()
    {
        $this->disableAuthentication();
        $response = $this->json('GET', route('tasks.show', 0));

        $response->assertStatus(404);
    }

    /**
     * Test the show functionality with a model.
     */
    public function testShow(): void
    {
        $this->disableAuthentication();
        $model = $this->createModel();

        $response = $this->json('GET', route('tasks.show', $model->id));

        $response->assertStatus(200);
        $response->assertResource(new TaskResource($model));
    }

    /**
     * Test the store functionality.
     */
    public function testStore(): void
    {
        $this->disableAuthentication();
        $model = $this->createModel();
        $data = $this->getData($model);
        $model->delete();
        $model->id++;

        $response = $this->json('POST', route('tasks.store'), $data);

        $response->assertStatus(201);
        $response->assertResource(new TaskResource($model));
        $this->assertNotNull(Task::find($model->id));
    }

    /**
     * Test the update functionality.
     */
    public function testUpdate(): void
    {
        $this->disableAuthentication();
        $oldModel = $this->createModel();
        $newModel = $this->createModel();
        $newModel->delete();
        $data = $this->getData($newModel);
        $newModel->id = $oldModel->id;

        $response = $this->json('PUT', route('tasks.update', $oldModel->id), $data);

        $response->assertStatus(200);
        $response->assertResource(new TaskResource($newModel));
    }

    /**
     * Test that tasks.update adds users.
     */
    public function testAddUsers()
    {
        $this->disableAuthentication();
        $users = User::factory()->count(2)->create();
        $task = $this->createModel();
        $data = $this->getData($task);
        $data = array_merge($data, ['users' => [$users[0]->id, $users[1]->id]]);

        $response = $this->json('PUT', route('tasks.update', $task->id), $data);
        $task = $task->fresh();

        $response->assertStatus(200);
        $this->assertTrue($task->users->contains($users[0]));
        $this->assertTrue($task->users->contains($users[1]));
    }

    /**
     * Test that tasks.update removes users.
     */
    public function testDeleteUsers()
    {
        $this->disableAuthentication();
        $users = User::factory()->count(2)->create();
        $task = $this->createModel();
        $data = $this->getData($task);
        $task->users()->attach($users[0]);
        $task->users()->attach($users[1]);

        $response = $this->json('PUT', route('tasks.update', $task->id), $data);

        $response->assertStatus(200);
        $this->assertFalse($task->users->contains($users[0]));
        $this->assertFalse($task->users->contains($users[1]));
    }

    /**
     * Test the light update functionality.
     */
    public function testLightUpdate()
    {
        $user = User::factory()->create();
        Passport::actingAs($user);

        $oldModel = $this->createModel();
        $newModel = $this->createModel();
        $oldModel->description = $newModel->description;
        $oldModel->addition = $newModel->addition;
        $oldModel->state = $newModel->state;
        $data = [
            'description'   => $oldModel->description,
            'addition'      => $oldModel->addition,
            'state'         => $oldModel->state,
            'participating' => false,
        ];

        $response = $this->json('PATCH', route('tasks.light.update', $oldModel->id), $data);

        $response->assertStatus(200);
        $response->assertResource(new TaskResource($oldModel));
        $this->assertFalse($oldModel->fresh()->users->contains($user));
    }

    /**
     * Test that light update adds user.
     */
    public function testLightUpdateAddParticipating()
    {
        $user = User::factory()->create();
        Passport::actingAs($user);

        $task = $this->createModel();
        $data = [
            'description'   => $task->description,
            'addition'      => $task->addition,
            'state'         => $task->state,
            'participating' => true,
        ];

        $response = $this->json('PATCH', route('tasks.light.update', $task->id), $data);
        $task = $task->fresh();

        $response->assertStatus(200);
        $this->assertTrue($task->users->contains($user));
    }

    /**
     * Test that light update removes user.
     */
    public function testLightUpdateRemoveParticipating()
    {
        $user = User::factory()->create();
        Passport::actingAs($user);

        $task = $this->createModel();
        $task->users()->attach($user);
        $data = [
            'description'   => $task->description,
            'addition'      => $task->addition,
            'state'         => $task->state,
            'participating' => false,
        ];

        $response = $this->json('PATCH', route('tasks.light.update', $task->id), $data);

        $response->assertStatus(200);
        $this->assertFalse($task->users->contains($user));
    }

    /**
     * Test that files are added correctly.
     */
    public function testAddFiles(): void
    {
        $this->disableAuthentication();
        $task = $this->createModel();
        $file = $this->createFile();
        $data = $this->getData($task);
        $data = array_merge($data, ['files' => [$file->id]]);

        $response = $this->json('PUT', route('tasks.update', $task->id), $data);

        $file = $file->fresh();

        $response->assertStatus(200);
        $response->assertResource(new TaskResource($task));
        Storage::disk()->assertExists($file->path);
        $this->assertTrue(Str::startsWith($file->path, File::$taskFolder));

        $file->delete(); // Delete the file fom the disk.
    }

    /**
     * Test that files are deleted correctly.
     */
    public function testDeleteFiles(): void
    {
        $this->disableAuthentication();
        $task = $this->createModel();
        $file = $this->createFile();
        $file->moveToTask($task);
        $data = $this->getData($task);

        $response = $this->json('PUT', route('tasks.update', $task->id), $data);

        $response->assertStatus(200);
        $response->assertResource(new TaskResource($task));
        $this->assertNull(File::find($file->id));
        Storage::disk()->assertMissing($file->path);
    }

    /**
     * Test the destroy functionality.
     */
    public function testDestroy(): void
    {
        $this->disableAuthentication();
        $model = $this->createModel();

        $response = $this->json('DELETE', route('tasks.destroy', $model->id));

        $response->assertStatus(200);
        $response->assertResource(new TaskResource($model));
        $this->assertNull(Task::find($model->id));
    }

    /**
     * Test that files are deleted when task is deleted.
     */
    public function testFileDestroy()
    {
        $this->disableAuthentication();
        $task = $this->createModel();
        $file = $this->createFile();
        $file->moveToTask($task);
        $task->load('files');

        $response = $this->json('DELETE', route('tasks.destroy', $task->id));

        $response->assertStatus(200);
        $response->assertResource(new TaskResource($task));
        $this->assertNull(File::find($file->id));
        Storage::disk()->assertMissing($file->path);
    }

    /**
     * Test that you can add users to a newly created task.
     */
    public function testAddUsersToNewTask()
    {
        $this->disableAuthentication();
        $users = User::factory()->count(5)->create();
        $taskCategory = TaskCategory::factory()->create();
        $data = [
            'responsible'   => $users->first()->id,
            'task_category' => $taskCategory->id,
            'duedate'       => Carbon::today(),
            'title'         => $this->faker->name,
            'users'         => $users->map(function ($user) {
                return $user->id;
            }),
            'state'         => 0,
            'priority'      => 0,
        ];

        $response = $this->json('POST', route('tasks.store'), $data);

        $response->assertStatus(201);
        $task = Task::first();
        for ($i = 0; $i < 5; $i++) {
            $this->assertTrue($task->users->contains($users[$i]));
        }
    }

    /**
     * Test that you can add users to a newly created task.
     */
    public function testAddUsersToExistingTask()
    {
        $this->disableAuthentication();
        $users = User::factory()->count(5)->create();
        $task = Task::factory()
            ->for(TaskCategory::factory())
            ->create();
        $task->responsible()->associate($users[0]);
        $data = $this->getData($task);
        $data["users"] = $users->map(function ($user) {
            return $user->id;
        });

        $response = $this->json('PUT', route('tasks.update', $task->id), $data);

        $response->assertStatus(200);
        $task = $task->fresh();
        for ($i = 0; $i < 5; $i++) {
            $this->assertTrue($task->users->contains($users[$i]));
        }
    }

    /**
     * Create a file on the disk and return a File object.
     *
     * @return \App\Models\File
     */
    private function createFile(): File
    {
        $model = File::factory()->create();
        $file = UploadedFile::fake()->image($model->name);
        $model->path = Storage::put(File::$uploadFolder, $file);
        $model->save();
        return $model;
    }

    /**
     * Convert a model into the request data.
     *
     * @param  \App\Models\Task $task
     * @return array
     */
    private function getData(Task $task): array
    {
        return [
            'title'             => $task->title,
            'description'       => $task->description,
            'addition'          => $task->addition,
            'duedate'           => $task->duedate,
            'priority'          => $task->priority,
            'state'             => $task->state,
            'task_category'     => $task->taskCategory->id,
            'responsible'       => $task->responsible->id,
            'inform_executives' => $task->inform_executives,
        ];
    }

    /**
     * Create a model for a request.
     *
     * @return \App\Models\Task $task
     */
    private function createModel(): Task
    {
        $task = Task::factory()
            ->for(TaskCategory::factory())
            ->create();
        $responsible = User::factory()->create();
        $task->responsible()->associate($responsible);
        $task->save();
        return $task;
    }

    /**
     * Create several pending tasks for a request.
     *
     * @param  int $count
     * @return \Illuminate\Support\Collection
     */
    private function createPendingTasks(int $count = 5): Collection
    {
        $tasks = Task::factory()->count($count)
            ->for(User::factory(), 'responsible')
            ->for(TaskCategory::factory())
            ->create(['state' => $this->faker()->numberBetween(0, 99)]);
        return $tasks;
    }

    /**
     * Create several finished tasks for a request.
     *
     * @param  int $count
     * @return \Illuminate\Support\Collection
     */
    private function createFinishedTasks(int $count = 5): Collection
    {
        $tasks = Task::factory()->count($count)
            ->for(User::factory(), 'responsible')
            ->for(TaskCategory::factory())
            ->create(['state' => 100]);
        return $tasks;
    }
}
