<?php

namespace Tests\Feature\Controller;

use App\Http\Resources\TaskCategory\SimplifiedTaskCategoryResource;
use App\Http\Resources\TaskCategory\TaskCategoryResource;
use App\Models\TaskCategory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Tests\Traits\DisablesAuth;

class TaskCategoryControllerTest extends TestCase
{
    use DisablesAuth;

    /**
     * Disable authorisation and authentication.
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->disableAuthorisation();
        $this->disableAuthentication();
    }

    /**
     * Test the index functionality without adding any models.
     */
    public function testIndexWithoutModels(): void
    {
        $response = $this->json('GET', route('task_categories.index'));

        $response->assertStatus(200);
        $response->assertExactJson(["data" => []]);
    }

    /**
     * Test the index functionality with models.
     */
    public function testIndex(): void
    {
        $models = $this->createModels();

        $response = $this->json('GET', route('task_categories.index'));

        $response->assertStatus(200);
        $response->assertResource(SimplifiedTaskCategoryResource::collection($models));
    }

    /**
     * Test the show functionality without model.
     */
    public function testShowWithoutModel()
    {
        $response = $this->json('GET', route('task_categories.show', 0));

        $response->assertStatus(404);
    }

    /**
     * Test the show functionality with a model.
     */
    public function testShow(): void
    {
        $model = $this->createModel();

        $response = $this->json('GET', route('task_categories.show', $model->id));

        $response->assertStatus(200);
        $response->assertResource(new TaskCategoryResource($model));
    }

    /**
     * Test the store functionality.
     */
    public function testStore(): void
    {
        $model = $this->createModel();
        $data = $this->getData($model);
        $model->delete();
        $model->id++;

        $response = $this->json('POST', route('task_categories.store'), $data);

        $response->assertStatus(201);
        $response->assertResource(new TaskCategoryResource($model));
        $this->assertNotNull(TaskCategory::find($model->id));
    }

    /**
     * Test the update functionality
     */
    public function testUpdate(): void
    {
        $oldModel = $this->createModel();
        $newModel = $this->createModel();
        $newModel->delete();
        $data = $this->getData($newModel);
        $newModel->id = $oldModel->id;

        $response = $this->json('PUT', route('task_categories.update', $oldModel->id), $data);

        $response->assertStatus(200);
        $response->assertResource(new TaskCategoryResource($newModel));
    }

    /**
     * Test the destroy functionality.
     */
    public function testDestroy(): void
    {
        $model = $this->createModel();

        $response = $this->json('DELETE', route('task_categories.destroy', $model->id));

        $response->assertStatus(200);
        $response->assertResource(new TaskCategoryResource($model));
        $this->assertNull(TaskCategory::find($model->id));
    }

    /**
     * Create data for a request.
     */
    private function createData(): array
    {
        $model = $this->createModel();
        return $this->getData($model);
    }

    /**
     * Convert a model into the request data.
     *
     * @param  \App\Models\TaskCategory $taskCategory
     * @return array
     */
    private function getData(TaskCategory $taskCategory): array
    {
        return [
            'name' => $taskCategory->name,
        ];
    }

    /**
     * Create a model for a request.
     *
     * @return \App\Models\TaskCategory $taskCategory
     */
    private function createModel(): TaskCategory
    {
        return TaskCategory::factory()->create();
    }

    /**
     * Create several models for a request.
     *
     * @param  int $count
     * @return \Illuminate\Support\Collection
     */
    private function createModels(int $count = 5): Collection
    {
        return TaskCategory::factory()->count($count)->create();
    }
}
