<?php

namespace Tests\Feature\Controller;

use App\Http\Resources\Meeting\FullMeetingResource;
use App\Http\Resources\Meeting\MeetingResource;
use App\Http\Resources\Meeting\SimplifiedMeetingResource;
use App\Models\Meeting;
use App\Models\User;
use App\Models\UserGroup;
use Carbon;
use Illuminate\Database\Eloquent\Model;
use Tests\TestCase;
use Tests\Traits\DisablesAuth;
use Tests\Traits\TestsResourceControllers;

class MeetingControllerTest extends TestCase
{
    use TestsResourceControllers;
    use DisablesAuth;

    public function setUp(): void
    {
        parent::setUp();

        $this->modelType = Meeting::class;
        $this->routeNamePrefix = 'meetings';
        $this->permissions = [
            'index'   => 'meetings.general',
            'store'   => 'meetings.general',
            'update'  => 'meetings.general',
            'destroy' => 'meetings.general',
            'show'    => 'meetings.general',
        ];
    }

    /**
     * Test the index functionality without models.
     */
    public function testIndexWithoutModels()
    {
        $this->indexWithoutModels();
    }

    /**
     * Test the index functionality with models.
     */
    public function testIndexWithModels()
    {
        // Authentication
        $this::setActingAs([$this->permissions['index']]);

        // Preparation
        $meetings = self::createModels(Meeting::class, $this->modelCount);
        foreach ($meetings as $meeting) {
            $meeting->start = Carbon::tomorrow();
            $meeting->end = Carbon::tomorrow();
            $meeting->save();
        }
        $meetings = $meetings->sortBy('start')->sortBy('title');

        $meetingsArchive = self::createModels(Meeting::class, $this->modelCount);
        foreach ($meetingsArchive as $meeting) {
            $meeting->start = Carbon::yesterday();
            $meeting->end = Carbon::yesterday();
            $meeting->save();
        }

        // Execution
        $response = $this->makeRequestToRoute('index');

        // Assertions
        $response->assertStatus(200);
        $response->assertResource(SimplifiedMeetingResource::collection($meetings));

        return $response;
    }

    /**
     * Test the archive functionality with models.
     */
    public function testArchiveWithModels()
    {
        // Authentication
        $this::setActingAs([$this->permissions['index']]);

        // Preparation
        $meetings = self::createModels(Meeting::class, $this->modelCount);
        foreach ($meetings as $meeting) {
            $meeting->start = Carbon::tomorrow();
            $meeting->end = Carbon::tomorrow();
            $meeting->save();
        }

        $meetingsArchive = self::createModels(Meeting::class, $this->modelCount);
        foreach ($meetingsArchive as $meeting) {
            $meeting->start = Carbon::yesterday();
            $meeting->end = Carbon::yesterday();
            $meeting->save();
        }
        $meetingsArchive = $meetingsArchive->sortBy('start')->sortBy('title');

        // Execution
        $response = $this->json('GET', route('meetings.archive'));

        // Assertions
        $response->assertStatus(200);
        $response->assertResource(SimplifiedMeetingResource::collection($meetingsArchive));

        return $response;
    }

    // TODO: Archive test

    /**
     * Test the store functionality.
     */
    public function testStore()
    {
        $this->storeModel(MeetingResource::class, [$this, "prepareModel"], [$this, "prepareData"]);
    }

    /**
     * Test the update functionality.
     */
    public function testUpdate()
    {
        $this->updateModel(MeetingResource::class, [$this, "prepareModel"], [$this, "prepareData"]);
        $this->updateInvalidModel([$this, "prepareModel"], [$this, "prepareData"]);
    }

    /**
     * Test destroy functionality with a valid model.
     */
    public function testDestroyValid()
    {
        $this->destroyModel(MeetingResource::class);
    }

    /**
     * Test destroy functionality with an invalid model.
     */
    public function testDestroyInvalid()
    {
        $this->destroyInvalidModel();
    }

    /**
     * Test show functionality with a valid mode.
     */
    public function testShowValid()
    {
        $this->showModel(MeetingResource::class, [$this, "prepareModel"]);
    }

    /**
     * Test show functionality with an invalid model.
     */
    public function testShowInvalid()
    {
        $this->showInvalidModel();
    }

    public function testShowEnhanced()
    {
        $this->disableAuthentication();
        $this->disableAuthorisation();

        $meeting = $this->prepareModel();

        $response = $this->json('GET', route('meetings.show_enhanced', $meeting->id));

        $response->assertStatus(200);
        $response->assertResource(new FullMeetingResource($meeting));
    }

    /**
     * Prepare the data to be send to the store and update routes.
     *
     * @param \Illuminate\Database\Eloquent\Model
     * @return array
     */
    private function prepareData(Model $model): array
    {
        $users = [];
        foreach ($model->users as $user) {
            array_push($users, $user->id);
        }
        $userGroups = [];
        foreach ($model->userGroups as $userGroup) {
            array_push($userGroups, $userGroup->id);
        }
        return [
            'title'          => $model->title,
            'start'          => $model->start,
            'end'            => $model->end,
            'register_until' => $model->register_until,
            'description'    => $model->description,
            'comment'        => $model->comment,
            'user_groups'    => $userGroups,
            'users'          => $users,
            'responsible'    => $model->responsible->id,
        ];
    }

    /**
     * Prepare the model for the store and update functionality.
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    private function prepareModel(): Model
    {
        $meeting = Meeting::factory()->create();
        $meeting->register_until = $meeting->start->subDays('1');

        $user = User::factory()->create();
        $meeting->users()->attach($user);
        $user = User::factory()->create();
        $meeting->users()->attach($user);

        $userGroup = UserGroup::factory()->create();
        $meeting->userGroups()->attach($userGroup);
        $userGroup = UserGroup::factory()->create();
        $meeting->userGroups()->attach($userGroup);

        $user = User::factory()->create();
        $meeting->responsible()->associate($user);

        $meeting->save();
        return $meeting->fresh();
    }
}
