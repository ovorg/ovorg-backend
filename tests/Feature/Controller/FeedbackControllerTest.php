<?php

namespace Tests\Feature\Controller;

use App\Mail\FeedbackMail;
use App\Models\FeedbackRecipient;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Mail;
use Laravel\Passport\Passport;
use Tests\TestCase;

class FeedbackControllerTest extends TestCase
{
    use WithFaker;

    /**
     * Test the send functionality.
     */
    public function testSend()
    {
        $user = User::factory()->create();
        Passport::actingAs($user, ['app']);

        $recipient = FeedbackRecipient::factory()->create();

        $data = [
            'subject' => $this->faker->word,
            'body'    => $this->faker->sentence,
        ];

        Mail::fake();

        $response = $this->json('POST', route('feedback.send'), $data);

        $response->assertStatus(200);
        Mail::assertQueued(FeedbackMail::class, function ($mail) use ($recipient, $user) {
            return $mail->hasTo($recipient->email) && $mail->hasReplyTo($user->email);
        });
    }
}
