<?php

namespace Tests\Feature\Controller;

use App\Http\Resources\Division\DivisionResource;
use App\Http\Resources\Division\SimplifiedDivisionResource;
use App\Models\Division;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Tests\TestCase;
use Tests\Traits\TestsResourceControllers;

class DivisionControllerTest extends TestCase
{
    use TestsResourceControllers;

    public function setUp(): void
    {
        parent::setUp();

        $this->modelType = Division::class;
        $this->routeNamePrefix = 'divisions';
        $this->permissions = [
            'index'   => null,
            'store'   => 'divisions.general',
            'update'  => 'divisions.general',
            'destroy' => 'divisions.general',
            'show'    => null,
        ];
    }

    /**
     * Test the index functionality without models.
     */
    public function testIndexWithoutModels()
    {
        $this->indexWithoutModels();
    }

    /**
     * Test the index functionality with models.
     */
    public function testIndexWithModels()
    {
        $this->indexWithModels(SimplifiedDivisionResource::class);
    }

    /**
     * Test the store functionality.
     */
    public function testStore()
    {
        $this->storeModel(DivisionResource::class, [$this, "prepareModel"], [$this, "prepareData"]);
    }

    /**
     * Test the update functionality.
     */
    public function testUpdate()
    {
        $this->updateModel(DivisionResource::class, [$this, "prepareModel"], [$this, "prepareData"]);
        $this->updateInvalidModel([$this, "prepareModel"], [$this, "prepareData"]);
    }

    /**
     * Test destroy functionality with a valid model.
     */
    public function testDestroyValid()
    {
        $this->destroyModel(DivisionResource::class);
    }

    /**
     * Test destroy functionality with an invalid model.
     */
    public function testDestroyInvalid()
    {
        $this->destroyInvalidModel();
    }

    /**
     * Test show functionality with a valid mode.
     */
    public function testShowValid()
    {
        $this->showModel(DivisionResource::class, [$this, "prepareModel"]);
    }

    /**
     * Test show functionality with an invalid model.
     */
    public function testShowInvalid()
    {
        $this->showInvalidModel();
    }

    /**
     * Prepare the data to be send to the store and update routes.
     *
     * @param \Illuminate\Database\Eloquent\Model
     * @return array
     */
    private function prepareData(Model $model): array
    {
        return [
            'name'       => $model->name,
            'short_name' => $model->short_name,
            'users'      => [$model->users()->firstOrFail()->id],
        ];
    }

    /**
     * Prepare the model for the store and update functionality.
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    private function prepareModel(): Model
    {
        $division = Division::factory()->create();
        $user = User::factory()->create();
        $user->division()->associate($division);
        $user->save();
        $division->save();
        return $division->fresh();
    }
}
