<?php

namespace Tests\Feature\Notifications;

use App\Models\Meeting;
use App\Models\User;
use App\Notifications\MeetingNotification;
use Tests\TestCase;

class MeetingNotificationTest extends TestCase
{
    /**
     * Test the to array function passing a single meeting.
     */
    public function testToArraySingleMeeting()
    {
        $meeting = Meeting::factory()->create();
        $user = User::factory()->create();

        $notification = new MeetingNotification($meeting);

        $array = $notification->toArray($user);
        $this->assertTrue(array_key_exists('meetings', $array));
        $this->assertEquals(1, sizeof($array['meetings']));
    }

    /**
     * Test the to array function passing a single meeting as collection.
     */
    public function testToArraySingleMeetingAsCollection()
    {
        $meeting = Meeting::factory()->create();
        $user = User::factory()->create();

        $notification = new MeetingNotification($meeting);

        $array = $notification->toArray($user);
        $this->assertTrue(array_key_exists('meetings', $array));
        $this->assertEquals(1, sizeof($array['meetings']));
    }

    /**
     * Test the to array function passing a multiple meetings as collection.
     */
    public function testToArrayMultipleMeetings()
    {
        $meeting = Meeting::factory()->count(5)->create();
        $user = User::factory()->create();

        $notification = new MeetingNotification($meeting);

        $array = $notification->toArray($user);
        $this->assertTrue(array_key_exists('meetings', $array));
        $this->assertEquals(5, sizeof($array['meetings']));
    }
}
