<?php

namespace Tests\Feature\Notifications;

use App\Models\Event;
use App\Models\Meeting;
use App\Models\User;
use App\Notifications\EventNotification;
use App\Notifications\MeetingNotification;
use App\Notifications\MeetingSummaryNotification;
use Tests\TestCase;

class NotificationPreferencesTest extends TestCase
{
    /**
     * Test that MeetingNotification::via() returns an array according to the NotificationPreference.
     */
    public function testMeetingNotifications()
    {
        $meeting = Meeting::factory()->create();
        $user = User::factory()->create();

        $notification = new MeetingNotification($meeting);

        /* Test for disabled notification. */

        $user->notificationPreference->meeting_notifications = false;
        $user->notificationPreference->task_notifications = true;
        $user->notificationPreference->event_notifications = true;

        $via = $notification->via($user);

        $this->assertTrue(count($via) == 0);

        /* Test for enabled notification. */

        $user->notificationPreference->meeting_notifications = true;
        $user->notificationPreference->task_notifications = false;
        $user->notificationPreference->event_notifications = false;

        $via = $notification->via($user);

        $this->assertTrue(count($via) > 0);
    }

    /**
     * Test that MeetingSummaryNotification::via() returns an array according to the NotificationPreference.
     */
    public function testMeetingSummaryNotifications()
    {
        $meeting = Meeting::factory()->create();
        $user = User::factory()->create();

        $notification = new MeetingSummaryNotification(collect($meeting));

        /* Test for disabled notification. */

        $user->notificationPreference->meeting_notifications = false;
        $user->notificationPreference->task_notifications = true;
        $user->notificationPreference->event_notifications = true;

        $via = $notification->via($user);

        $this->assertTrue(count($via) == 0);

        /* Test for enabled notification. */

        $user->notificationPreference->meeting_notifications = true;
        $user->notificationPreference->task_notifications = false;
        $user->notificationPreference->event_notifications = false;

        $via = $notification->via($user);

        $this->assertTrue(count($via) > 0);
    }

    /**
     * Test that EventNotification::via() returns an array according to the NotificationPreference.
     */
    public function testEventNotification()
    {
        $event = Event::factory()->create();
        $user = User::factory()->create();

        $notification = new EventNotification($event);

        /* Test for disabled notification. */

        $user->notificationPreference->meeting_notifications = true;
        $user->notificationPreference->task_notifications = true;
        $user->notificationPreference->event_notifications = false;

        $via = $notification->via($user);

        $this->assertTrue(count($via) == 0);

        /* Test for enabled notification. */

        $user->notificationPreference->meeting_notifications = false;
        $user->notificationPreference->task_notifications = false;
        $user->notificationPreference->event_notifications = true;

        $via = $notification->via($user);

        $this->assertTrue(count($via) > 0);
    }}
