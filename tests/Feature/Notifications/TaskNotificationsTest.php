<?php

namespace Tests\Feature\Notifications;

use App\Models\File;
use App\Models\Position;
use App\Models\Task;
use App\Models\TaskCategory;
use App\Models\User;
use App\Notifications\Tasks\AttributeChangedNotification;
use App\Notifications\Tasks\FilesAddedNotification;
use App\Notifications\Tasks\NewTaskNotification;
use App\Notifications\Tasks\SelfAddedNotification;
use App\Notifications\Tasks\UsersUpdatedNotification;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;
use Laravel\Passport\Passport;
use Tests\TestCase;
use Tests\Traits\DisablesAuth;

class TaskNotificationsTest extends TestCase
{
    use DisablesAuth;

    /**
     * Test if NewTaskNotification is sent on creation.
     */
    public function testNewTask()
    {
        Notification::fake();

        list($task, $responsible) = $this->createTaskWithResponsible();

        Notification::assertSentTo($responsible, NewTaskNotification::class);
    }

    /**
     * Test if NewTaskNotification is not sent if there is no responsible.
     */
    public function testNewTaskNoResponsible()
    {
        Notification::fake();

        $this->createTaskWithoutResponsible();

        Notification::assertNothingSent();
    }

    /**
     * Test if all participants and the responsible User is notified when the addition changes.
     */
    public function testAdditionChanged()
    {
        list($task, $responsible) = $this->createTaskWithResponsible();
        $users = $this->addUsers($task);

        Notification::fake();

        $task->addition = "hello world" . $task->addition;
        $task->save();

        Notification::assertSentTo($responsible, AttributeChangedNotification::class);
        foreach ($users as $user) {
            Notification::assertSentTo($user, AttributeChangedNotification::class);
        }
    }

    /**
     * Test if all participants and the responsible User is notified when the description changes.
     */
    public function testDescriptionChanged()
    {
        list($task, $responsible) = $this->createTaskWithResponsible();
        $users = $this->addUsers($task);

        Notification::fake();

        $task->description = "hello world" . $task->description;
        $task->save();

        Notification::assertSentTo($responsible, AttributeChangedNotification::class);
        foreach ($users as $user) {
            Notification::assertSentTo($user, AttributeChangedNotification::class);
        }
    }

    /**
     * Test if all participants and the responsible User is notified when the state changes.
     */
    public function testStateChanged()
    {
        list($task, $responsible) = $this->createTaskWithResponsible();
        $users = $this->addUsers($task);

        Notification::fake();

        $task->state = ++$task->state % 100;
        $task->save();

        Notification::assertSentTo($responsible, AttributeChangedNotification::class);
        foreach ($users as $user) {
            Notification::assertSentTo($user, AttributeChangedNotification::class);
        }
    }

    /**
     * Test if new Users and responsible User are notified about new Users.
     */
    public function testAddUsers()
    {
        $this->disableAuthentication();
        $this->disableAuthorisation();

        list($task, $responsible) = $this->createTaskWithResponsible();
        $users = User::factory()->count(2)->create();
        $taskCategory = TaskCategory::factory()->create();
        $data = [
            'title'         => $task->title,
            'description'   => $task->description,
            'addition'      => $task->addition,
            'duedate'       => $task->duedate,
            'priority'      => $task->priority,
            'state'         => $task->state,
            'responsible'   => $task->responsible->id,
            'task_category' => $taskCategory->id,
            'users'         => $users->map(function (User $user) {
                return $user->id;
            })->toArray(),
        ];

        Notification::fake();
        $response = $this->json('PUT', route('tasks.update', $task->id), $data);

        $response->assertStatus(200);
        Notification::assertSentTo($responsible, UsersUpdatedNotification::class);
        foreach ($users as $user) {
            Notification::assertSentTo($user, SelfAddedNotification::class);
        }
    }

    /**
     * Test if new Users and responsible User are notified about new Users.
     */
    public function testAddUsersLightUpdate()
    {
        $this->disableAuthorisation();
        $user = User::factory()->create();
        Passport::actingAs($user);

        list($task, $responsible) = $this->createTaskWithResponsible();
        $data = [
            'description'   => $task->description,
            'addition'      => $task->addition,
            'state'         => $task->state,
            'participating' => true,
        ];

        Notification::fake();
        $response = $this->json('PATCH', route('tasks.light.update', $task->id), $data);

        $response->assertStatus(200);
        Notification::assertSentTo($responsible, UsersUpdatedNotification::class);
        Notification::assertSentTo($user, SelfAddedNotification::class);
    }

    /**
     * Test if responsible is notified when Users are removed.
     */
    public function testRemoveUser()
    {
        $this->disableAuthentication();
        $this->disableAuthorisation();

        list($task, $responsible) = $this->createTaskWithResponsible();
        $this->addUsers($task);
        $taskCategory = TaskCategory::factory()->create();
        $data = [
            'title'         => $task->title,
            'description'   => $task->description,
            'addition'      => $task->addition,
            'duedate'       => $task->duedate,
            'priority'      => $task->priority,
            'state'         => $task->state,
            'responsible'   => $task->responsible->id,
            'task_category' => $taskCategory->id,
            'users'         => [],
        ];

        Notification::fake();
        $response = $this->json('PUT', route('tasks.update', $task->id), $data);

        $response->assertStatus(200);
        Notification::assertSentTo($responsible, UsersUpdatedNotification::class);
    }

    /**
     * Test if new Users and responsible User are removed about new Users.
     */
    public function testRemoveUsersLightUpdate()
    {
        $this->disableAuthorisation();
        list($task, $responsible) = $this->createTaskWithResponsible();
        $user = $this->addUsers($task, 1)->first();
        Passport::actingAs($user);

        $data = [
            'description'   => $task->description,
            'addition'      => $task->addition,
            'state'         => $task->state,
            'participating' => false,
        ];

        Notification::fake();
        $response = $this->json('PATCH', route('tasks.light.update', $task->id), $data);

        $response->assertStatus(200);
        Notification::assertSentTo($responsible, UsersUpdatedNotification::class);
    }

    /**
     * Test if Users are notified about new files.
     */
    public function testFilesAdded()
    {
        $this->disableAuthentication();
        $this->disableAuthorisation();

        list($task, $responsible) = $this->createTaskWithResponsible();
        $users = $this->addUsers($task);
        $taskCategory = TaskCategory::factory()->create();
        $file = $this->createFile();
        $data = [
            'title'         => $task->title,
            'description'   => $task->description,
            'addition'      => $task->addition,
            'duedate'       => $task->duedate,
            'priority'      => $task->priority,
            'state'         => $task->state,
            'responsible'   => $task->responsible->id,
            'task_category' => $taskCategory->id,
            'users'         => [],
            'files'         => [$file->id],
        ];

        Notification::fake();
        $response = $this->json('PUT', route('tasks.update', $task->id), $data);

        $response->assertStatus(200);
        Notification::assertSentTo($responsible, FilesAddedNotification::class);
        foreach ($users as $user) {
            Notification::assertSentTo($user, FilesAddedNotification::class);
        }
    }

    /**
     * Test if Users are notified when the responsible User changes.
     */
    public function testResponsibleChanged()
    {
        list($task, $oldResponsible) = $this->createTaskWithResponsible();
        $users = $this->addUsers($task);
        $responsible = User::factory()->create();
        $task->responsible()->associate($responsible);

        Notification::fake();
        $task->save();

        foreach ($users as $user) {
            Notification::assertSentTo($user, AttributeChangedNotification::class);
        }
        Notification::assertSentTo($responsible, AttributeChangedNotification::class);
        Notification::assertSentTo($oldResponsible, AttributeChangedNotification::class);
    }

    /**
     * Test if Users are notified when the responsible User changes
     * and no User has been responsible before.
     */
    public function testResponsibleAdded()
    {
        $task = $this->createTaskWithoutResponsible();
        $users = $this->addUsers($task);
        $responsible = User::factory()->create();
        $task->responsible()->associate($responsible);

        Notification::fake();
        $task->save();

        foreach ($users as $user) {
            Notification::assertSentTo($user, AttributeChangedNotification::class);
        }
    }

    /**
     * Test if executive users are notified about new Tasks
     * when the inform_executive attribute is set to true.
     */
    public function testInformExecutivesOnCreating()
    {
        $users = $this->createExecutiveUsers();

        Notification::fake();

        Task::factory()->create([
            'responsible_id'    => null,
            'inform_executives' => true,
        ]);

        foreach ($users as $user) {
            Notification::assertSentTo($user, NewTaskNotification::class);
        }
    }

    /**
     * Test if executive users are notified about updated Tasks
     * when the inform_executive attribute is set to true.
     */
    public function testInformExecutivesOnUpdating()
    {
        $users = $this->createExecutiveUsers();

        $task = Task::factory()->create([
            'responsible_id'    => null,
            'inform_executives' => true,
        ]);

        Notification::fake();

        $task->title = $task->title . "title";
        $task->save();

        foreach ($users as $user) {
            Notification::assertSentTo($user, AttributeChangedNotification::class);
        }
    }

    /**
     * Create executive positions and return a collection
     * with one user for each position.
     *
     * @return \Illuminate\Support\Collection
     */
    public function createExecutiveUsers(): Collection
    {
        $positions = Position::factory()->count(5)->create();
        $positions[0]->male = "Truppführer";
        $positions[0]->save();
        $positions[1]->male = "Gruppenführer";
        $positions[1]->save();
        $positions[2]->male = "Zugführer";
        $positions[2]->save();
        $positions[3]->male = "Ortsbeauftragter";
        $positions[3]->save();
        $positions[4]->male = "Stellv. OB";
        $positions[4]->save();

        $users = User::factory()->count(5)->create();
        $users[0]->position()->associate($positions[0]);
        $users[0]->save();
        $users[1]->position()->associate($positions[1]);
        $users[1]->save();
        $users[2]->position()->associate($positions[2]);
        $users[2]->save();
        $users[3]->position()->associate($positions[3]);
        $users[3]->save();
        $users[4]->position()->associate($positions[4]);
        $users[4]->save();

        return $users;
    }

    /**
     * Create a file on the disk and return a File object.
     *
     * @return \App\Models\File
     */
    private function createFile(): File
    {
        $fileModel = File::factory()->create();
        $file = UploadedFile::fake()->image($fileModel->name);
        $fileModel->path = Storage::put(File::$uploadFolder, $file);
        $fileModel->save();
        return $fileModel;
    }

    /**
     * Create a new task with a responsible User.
     *
     * @return array
     */
    private function createTaskWithResponsible(): array
    {
        $user = User::factory()->create();
        $task = Task::factory()->create(['responsible_id' => $user->id]);
        return [$task, $user];
    }

    /**
     * Create a new task without a responsible User.
     *
     * @return \App\Models\Task
     */
    private function createTaskWithoutResponsible(): Task
    {
        $task = Task::factory()->create();
        return $task;
    }

    /**
     * Add Users to the Task.
     *
     * @param  \App\Models\Task $task
     * @param  int $count
     * @return Illuminate\Support\Collection
     */
    private function addUsers(Task $task, int $count = 2): Collection
    {
        $users = User::factory()->count($count)->create();
        $task->users()->attach($users->map(function (User $user) {
            return $user->id;
        }));
        return $users;
    }
}
