<?php

namespace Tests\Feature;

use Illuminate\Routing\Route;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Tests\TestCase;

class PermissionsTest extends TestCase
{
    private $permissions = [
        'activate'                    => null,
        'change_password'             => null,
        'logged_in'                   => null,
        'login'                       => null,
        'logout'                      => null,
        'refresh'                     => null,
        'request_password_reset'      => null,
        'reset_password'              => null,

        'calendar'                    => null,

        'divisions.destroy'           => 'permission.api:divisions.general',
        'divisions.index'             => null,
        'divisions.show'              => null,
        'divisions.store'             => 'permission.api:divisions.general',
        'divisions.update'            => 'permission.api:divisions.general',

        'doodle.index_token'          => null,
        'doodle.index'                => null,
        'doodle.show'                 => null,
        'doodle.show_token'           => null,
        'doodle.update'               => null,
        'doodle.update_token'         => null,
        'doodle.archive'              => null,
        'doodle.archive_token'        => null,

        'fallback'                    => null,

        'files.destroy'               => null,
        'files.index'                 => null,
        'files.show'                  => null,
        'files.store'                 => null,
        'files.update'                => null,

        'meetings.archive'            => 'permission.api:meetings.general',
        'meetings.destroy'            => 'permission.api:meetings.general',
        'meetings.index'              => 'permission.api:meetings.general',
        'meetings.show'               => 'permission.api:meetings.general',
        'meetings.show_enhanced'      => 'permission.api.or:meetings.advanced,meetings.general',
        'meetings.store'              => 'permission.api:meetings.general',
        'meetings.update'             => 'permission.api:meetings.general',

        'positions.destroy'           => 'permission.api:positions.general',
        'positions.index'             => null,
        'positions.show'              => null,
        'positions.store'             => 'permission.api:positions.general',
        'positions.update'            => 'permission.api:positions.general',

        'roles.destroy'               => 'permission.api:roles.general',
        'roles.index'                 => 'permission.api:roles.general',
        'roles.show'                  => 'permission.api:roles.general',
        'roles.store'                 => 'permission.api:roles.general',
        'roles.update'                => 'permission.api:roles.general',

        'task_categories.destroy'     => 'permission.api:tasks.general',
        'task_categories.index'       => null,
        'task_categories.show'        => null,
        'task_categories.store'       => 'permission.api:tasks.general',
        'task_categories.update'      => 'permission.api:tasks.general',

        'tasks.destroy'               => 'permission.api:tasks.general',
        'tasks.index'                 => null,
        'tasks.show'                  => null,
        'tasks.store'                 => 'permission.api:tasks.general',
        'tasks.update'                => 'permission.api:tasks.general',
        'tasks.light.update'          => null,

        'users.destroy'               => 'permission.api:users.general',
        'users.index'                 => 'permission.api:users.general',
        'users.me'                    => null,
        'users.self_update'           => null,
        'users.show'                  => 'permission.api:users.general',
        'users.simplified_index'      => null,
        'users.store'                 => 'permission.api:users.general',
        'users.update'                => 'permission.api:users.general',

        'user_groups.destroy'         => 'permission.api:user_groups.general',
        'user_groups.index'           => null,
        'user_groups.show'            => 'permission.api:user_groups.general',
        'user_groups.store'           => 'permission.api:user_groups.general',
        'user_groups.update'          => 'permission.api:user_groups.general',

        'qualifications.scba'         => 'permission.api:qualifications.general',
        'qualifications.driver'       => 'permission.api:qualifications.general',

        'notifications.custom'        => 'permission.api:notifications.general',
        'notifications.meeting'       => 'permission.api:notifications.general',
        'notifications.activation'    => 'permission.api.or:notifications.administration,admin.general',
        'notifications.reminder'      => 'permission.api.or:notifications.administration,admin.general',
        'notifications.event'         => 'permission.api:events.general',

        'shops.destroy'               => 'permission.api:order_list.general',
        'shops.index'                 => 'permission.api:order_list.general',
        'shops.show'                  => 'permission.api:order_list.general',
        'shops.store'                 => 'permission.api:order_list.general',
        'shops.update'                => 'permission.api:order_list.general',

        'order_items.destroy'         => 'permission.api:order_list.general',
        'order_items.index'           => 'permission.api:order_list.general',
        'order_items.archive'         => 'permission.api:order_list.general',
        'order_items.show'            => 'permission.api:order_list.general',
        'order_items.store'           => 'permission.api:order_list.general',
        'order_items.update'          => 'permission.api:order_list.general',

        'presence.index'              => 'permission.api:users.presence',
        'presence.update'             => 'permission.api:users.presence',
        'presence.show'               => 'permission.api:users.presence',

        'failed_jobs.index'           => 'permission.api:admin.general',
        'failed_jobs.show'            => 'permission.api:admin.general',
        'failed_jobs.destroy'         => 'permission.api:admin.general',
        'failed_jobs.flush'           => 'permission.api:admin.general',

        'feedback_recipients.index'   => 'permission.api:admin.general',
        'feedback_recipients.show'    => 'permission.api:admin.general',
        'feedback_recipients.store'   => 'permission.api:admin.general',
        'feedback_recipients.update'  => 'permission.api:admin.general',
        'feedback_recipients.destroy' => 'permission.api:admin.general',

        'oauth_clients.index'         => 'permission.api:admin.general',
        'oauth_clients.show'          => 'permission.api:admin.general',
        'oauth_clients.store'         => 'permission.api:admin.general',
        'oauth_clients.update'        => 'permission.api:admin.general',
        'oauth_clients.destroy'       => 'permission.api:admin.general',

        'feedback_templates.index'    => null,
        'feedback_templates.show'     => null,
        'feedback_templates.store'    => 'permission.api:admin.general',
        'feedback_templates.update'   => 'permission.api:admin.general',
        'feedback_templates.destroy'  => 'permission.api:admin.general',

        'activations.index'           => 'permission.api:admin.general',
        'activations.show'            => 'permission.api:admin.general',

        'events.destroy'              => 'permission.api:events.general',
        'events.index'                => 'permission.api:events.general',
        'events.show'                 => 'permission.api:events.general',
        'events.show_enhanced'        => 'permission.api.or:events.advanced,events.general',
        'events.store'                => 'permission.api:events.general',
        'events.update'               => 'permission.api:events.general',

        'event_doodle.show'           => null,
        'event_doodle.update'         => null,
        'event_doodle.archive'        => null,
        'event_doodle.index'          => null,

        'event_doodle.show_token'     => null,
        'event_doodle.update_token'   => null,
        'event_doodle.archive_token'  => null,
        'event_doodle.index_token'    => null,
    ];

    /**
     * Test if all route names defined in $permissions exist.
     */
    public function testRouteNames()
    {
        $routeNames = $this->getRouteNames();
        foreach ($this->permissions as $key => $value) {
            $this->assertContains($key, $routeNames);
        }
    }

    /**
     * Test if the permissions are set correctly.
     */
    public function testPermissions()
    {
        $routes = $this->getAllRoutes();

        foreach ($routes as $route) {
            $this->checkRoute($route);
        }
    }

    /**
     * Return all routes.
     *
     * @return \Illuminate\Support\Collection Collection of Illuminate\Routing\Route
     */
    private function getAllRoutes(): Collection
    {
        $router = app()['router'];
        return collect($router->getRoutes());
    }

    /**
     * Return all route names.
     *
     * @return \Illuminate\Support\Collection
     */
    private function getRouteNames(): Collection
    {
        return $this->getAllRoutes()->map(function (Route $route) {
            return $route->getName();
        });
    }

    /**
     * Check the permissions for $route.
     *
     * @param \Illuminate\Routing\Route $route
     */
    private function checkRoute(Route $route): void
    {
        // Get route info
        $name = $route->getName();
        if ($name === null) {
            return;
        }
        $middlewares = $route->gatherMiddleware();

        // Get permission the route should have
        $permission = $this->getPermission($name);
        if ($permission === false) {
            return;
        }

        // Check the permission
        if ($this->isNoPermission($permission)) {
            $isSet = $this->checkNoPermissionMiddleware($middlewares);
        } else {
            $isSet = $this->checkMiddleware((string) $permission, $middlewares);
            $onePermissionMiddlware = $this->checkOnlyOnePermissionMiddleware($middlewares);
            $this->assertTrue($onePermissionMiddlware, "$name has more than one permissions middleware set.");
        }

        $this->assertTrue($isSet, "Permissions for $name are wrong. Should be: $permission.");
    }

    /**
     * Check if any middleware in $middlewares starts with 'permission'.
     *
     * @param  array $middlewares
     * @return bool
     */
    private function checkNoPermissionMiddleware(array $middlewares): bool
    {
        foreach ($middlewares as $middleware) {
            if (Str::startsWith($middleware, 'permission')) {
                return false;
            }
        }
        return true;
    }

    /**
     * Get the permission set in $this->permissions for $name.
     *
     * @param  string $name
     * @return mixed
     */
    private function getPermission(string $name)
    {
        return Arr::get($this->permissions, $name, false);
    }

    /**
     * Check if $middleware exists as value in $middlewares.
     *
     * @param  string $middleware
     * @param  string $middlewares
     * @return bool
     */
    private function checkMiddleware(string $middleware, array $middlewares): bool
    {
        foreach ($middlewares as $m) {
            if ($middleware == $m) {
                return true;
            }
        }
        return false;
    }

    /**
     * Return if the $value represents no permission.
     *
     * @param  mixed $value
     * @return bool
     */
    private function isNoPermission($value): bool
    {
        return $value === '' || $value === null;
    }

    /**
     * Check that only one permission middleware exists in $middlewares.
     *
     * @param  string $middlewares
     * @return bool
     */
    private function checkOnlyOnePermissionMiddleware(array $middlewares)
    {
        $i = 0;
        foreach ($middlewares as $middleware) {
            if (Str::startsWith($middleware, 'permission')) {
                $i++;
            }
        }
        return $i === 1;
    }
}
