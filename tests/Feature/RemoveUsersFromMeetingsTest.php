<?php

namespace Tests\Feature;

use App\Models\Meeting;
use App\Models\Position;
use App\Models\User;
use App\Models\UserGroup;
use Carbon\Carbon;
use Tests\TestCase;
use Tests\Traits\DisablesAuth;

/**
 * This class tests if User registrations are removed from a Meeting
 * when the User is no longer invited to the Meeting.
 */
class RemoveUsersFromMeetingsTest extends TestCase
{
    use DisablesAuth;

    /**
     * Disable auth.
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->disableAuthentication();
        $this->disableAuthorisation();
    }

    /**
     * Test that the doodle registration is removed when the User is removed
     * from the UserGroup in the UserGroupController.
     */
    public function testRemoveOneFromUserGroup()
    {
        [$userGroup, $users, $meeting] = $this->prepareUserGroupData(2);
        $data = $this->createUserGroupRequestData($userGroup);
        $usersArray = $data["users"];
        array_shift($usersArray);
        $data["users"] = $usersArray;

        $response = $this->json('PUT', route('user_groups.update', $userGroup->id), $data);

        $response->assertStatus(200);
        $this->assertEquals(1, $meeting->registeredUsers()->count());
        $this->assertEquals($users[1]->id, $meeting->registeredUsers()->first()->id);
    }

    /**
     * Test that the doodle registration is removed when all Users are removed
     * from the UserGroup in the UserGroupController.
     */
    public function testRemoveAllFromUserGroup()
    {
        [$userGroup, $users, $meeting] = $this->prepareUserGroupData(2);
        $data = $this->createUserGroupRequestData($userGroup);
        $data["users"] = [];

        $response = $this->json('PUT', route('user_groups.update', $userGroup->id), $data);

        $response->assertStatus(200);
        $this->assertTrue($meeting->registeredUsers->isEmpty());
    }

    /**
     * Test that the doodle registration is removed when the UserGroup is removed
     * from the User in the UserController.
     */
    public function testRemoveOneUserGroupFromUser()
    {
        [$userGroups, $user, $meeting] = $this->prepareUserData(2);
        $data = $this->createUserRequestData($user);
        $userGroupsArray = $data["user_groups"];
        $removed = array_shift($userGroupsArray);
        $data["user_groups"] = $userGroupsArray;
        $removedUserGroup = UserGroup::find($removed);
        $removedUserGroup->meetings()->attach($meeting);

        $this->assertFalse($meeting->registeredUsers->isEmpty());

        $response = $this->json('PUT', route('users.update', $user->id), $data);
        $meeting = $meeting->fresh();

        $response->assertStatus(200);
        $this->assertTrue($meeting->registeredUsers->isEmpty());
    }

    /**
     * Test that the doodle registration is removed when all UserGroups are removed
     * from the User in the UserController.
     */
    public function testRemoveAllUserGroupsFromUser()
    {
        [$userGroups, $user, $meeting] = $this->prepareUserData(2);
        $data = $this->createUserRequestData($user);
        $data["user_groups"] = [];
        $meeting->userGroups()->attach($userGroups);

        $this->assertFalse($meeting->registeredUsers->isEmpty());

        $response = $this->json('PUT', route('users.update', $user->id), $data);
        $meeting = $meeting->fresh();

        $response->assertStatus(200);
        $this->assertTrue($meeting->registeredUsers->isEmpty());
    }

    /**
     * Test that the doodle registration is removed when one UserGroup
     * is removed from the Meeting in the MeetingController.
     */
    public function testRemoveOneUserGroupFromMeeting()
    {
        [$userGroups, $user, $meeting] = $this->prepareUserData(2);
        $meeting->userGroups()->attach($userGroups);
        $data = $this->createMeetingRequestData($meeting);
        $userGroupsArray = $data["user_groups"];
        $removed = array_shift($userGroupsArray);
        $data["user_groups"] = $userGroupsArray;
        UserGroup::find($userGroupsArray[0])->users()->detach();

        $this->assertFalse($meeting->registeredUsers->isEmpty());

        $response = $this->json('PUT', route('meetings.update', $meeting->id), $data);
        $meeting = $meeting->fresh();

        $response->assertStatus(200);
        $this->assertTrue($meeting->registeredUsers->isEmpty());
    }

    /**
     * Test that the doodle registration is removed when all UserGroups
     * are removed from the User in the MeetingController.
     */
    public function testRemoveAllUserGroupsFromMeeting()
    {
        [$userGroups, $user, $meeting] = $this->prepareUserData(2);
        $data = $this->createMeetingRequestData($meeting);
        $data["user_groups"] = [];
        $meeting->userGroups()->attach($userGroups);

        $this->assertFalse($meeting->registeredUsers->isEmpty());

        $response = $this->json('PUT', route('meetings.update', $meeting->id), $data);
        $meeting = $meeting->fresh();

        $response->assertStatus(200);
        $this->assertTrue($meeting->registeredUsers->isEmpty());
    }

    /**
     * Test that the doodle registration is removed when the UserGroup is removed
     * from the Meeting in the UserController.
     */
    public function testRemoveOneUserFromMeeting()
    {
        $meeting = Meeting::factory()
            ->has(User::factory()->count(2))
            ->create();

        $data = $this->createMeetingRequestData($meeting);
        $usersArray = $data["users"];
        $removed = array_shift($usersArray);
        $data["users"] = $usersArray;
        $meeting->registeredUsers()->attach($removed, [
            'attendance' => 'maybe',
            'comment'    => '',
        ]);

        $this->assertFalse($meeting->registeredUsers->isEmpty());

        $response = $this->json('PUT', route('meetings.update', $meeting->id), $data);
        $meeting = $meeting->fresh();

        $response->assertStatus(200);
        $this->assertTrue($meeting->registeredUsers->isEmpty());
    }

    /**
     * Test that the doodle registration is removed when all UserGroups are removed
     * from the User in the UserController.
     */
    public function testRemoveAllUsersFromMeeting()
    {
        $meeting = Meeting::factory()->create();
        $users = User::factory()->count(2)->create();
        $meeting->users()->attach($users);
        $meeting->registeredUsers()->attach($users, [
            'attendance' => 'maybe',
            'comment'    => '',
        ]);
        $data = $this->createMeetingRequestData($meeting);
        $data["users"] = [];

        $this->assertFalse($meeting->registeredUsers->isEmpty());

        $response = $this->json('PUT', route('meetings.update', $meeting->id), $data);
        $meeting = $meeting->fresh();

        $response->assertStatus(200);
        $this->assertTrue($meeting->registeredUsers->isEmpty());
    }

    /**
     * Test switching UserGroups for a Meeting.
     */
    public function testMeetingSwitchUserGroup()
    {
        $meeting = Meeting::factory()->create();
        $user = User::factory()->create();
        $userGroups = UserGroup::factory()->count(2)->create();
        $user->userGroups()->attach($userGroups);
        $meeting->userGroups()->attach($userGroups[0]);
        $meeting->registeredUsers()->attach($user, [
            'attendance' => 'maybe',
            'comment'    => '',
        ]);
        $data = $this->createMeetingRequestData($meeting);
        $data['user_groups'] = [$userGroups[1]->id];

        $response = $this->json('PUT', route('meetings.update', $meeting->id), $data);
        $meeting = $meeting->fresh();

        $response->assertStatus(200);
        $this->assertFalse($meeting->registeredUsers->isEmpty());
    }

    /**
     * Create data for \App\Http\Request\UserGroupRequest
     *
     * @param  \App\Models\UserGroup $userGroup
     * @return array
     */
    private function createUserGroupRequestData(UserGroup $userGroup): array
    {
        return [
            'name'  => $userGroup->name,
            'users' => $userGroup->users->map(function (User $user) {
                return $user->id;
            })->toArray(),
        ];
    }

    /**
     * Create data for \App\Http\Request\UserRequest
     *
     * @param  \App\Models\User $user
     * @return array
     */
    private function createUserRequestData(User $user): array
    {
        return [
            'first_name'  => $user->first_name,
            'last_name'   => $user->last_name,
            'gender'      => $user->gender,
            'position'    => Position::factory()->create()->id,
            'user_groups' => $user->userGroups->map(function (UserGroup $userGroup) {
                return $userGroup->id;
            })->toArray(),
        ];
    }

    /**
     * Create data for \App\Http\Request\UserGroupRequest
     *
     * @param  \App\Models\Meeting $meeting
     * @return array
     */
    private function createMeetingRequestData(Meeting $meeting): array
    {
        return [
            'title'       => $meeting->title,
            'start'       => $meeting->start,
            'end'         => $meeting->end,
            'comment'     => $meeting->comment,
            'user_groups' => $meeting->userGroups->map(function (UserGroup $userGroup) {
                return $userGroup->id;
            })->toArray(),
            'users'       => $meeting->users->map(function (User $user) {
                return $user->id;
            })->toArray(),
        ];
    }

    /**
     * Prepare a UserGroup with Users registered in a Meeting
     *
     * @param int $userCount How many Users to create.
     */
    private function prepareUserGroupData(int $userCount)
    {
        $userGroup = UserGroup::factory()->create();
        $users = User::factory()->count($userCount)->create();
        $meeting = Meeting::factory()->create(['start' => Carbon::tomorrow()]);

        $meeting->userGroups()->attach($userGroup);
        $userGroup->users()->sync($users->map(function (User $user) {
            return $user->id;
        }));
        foreach ($users as $user) {
            $user->registeredMeetings()->attach($meeting, [
                'attendance' => 'yes',
                'comment'    => '',
            ]);
        }

        return [$userGroup, $users, $meeting];
    }

    /**
     * Prepare a User with UserGroups and register it to a Meeting.
     * Doesn't assign the UserGroups to the Meeting.
     *
     * @param int $userGroupCount How many Users to create.
     */
    private function prepareUserData(int $userGroupCount)
    {
        $userGroups = UserGroup::factory()->count($userGroupCount)->create();
        $user = User::factory()->create();
        $meeting = Meeting::factory()->create(['start' => Carbon::tomorrow()]);

        $user->userGroups()->sync($userGroups->map(function (UserGroup $userGroup) {
            return $userGroup->id;
        }));
        $user->registeredMeetings()->attach($meeting, [
            'attendance' => 'no',
            'comment'    => '',
        ]);

        return [$userGroups, $user, $meeting];
    }
}
