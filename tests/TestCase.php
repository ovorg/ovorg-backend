<?php

namespace Tests;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Tests\Traits\AddAssertions;
use Tests\Traits\CreatesApplication;

abstract class TestCase extends BaseTestCase
{
    use RefreshDatabase;
    use CreatesApplication;
    use AddAssertions;

    public function setUp(): void
    {
        parent::setUp();
        $this->addAssertions();
    }
}
