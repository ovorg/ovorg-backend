<?php

namespace Tests\Unit\Models\Event;

use App\Models\Event;
use App\Models\EventDateOption;
use App\Models\EventRegistration;
use App\Models\User;
use Tests\TestCase;

class UsersWithMissingRegistrationsTest extends TestCase {
    /**
     * Test that no Users are returned,
     * when several Users are registered to all EventDateOptions.
     */
    public function testAllRegistered() {
        $users = User::factory()->count(3)->create();
        $event = Event::factory()->create();
        $event->users()->attach($users);
        $dateOptions = EventDateOption::factory()->count(3)
            ->create(['event_id' => $event->id]);
        foreach ($users as $user) {
            foreach ($dateOptions as $dateOption) {
                EventRegistration::factory()->create([
                    'user_id'              => $user->id,
                    'event_date_option_id' => $dateOption->id,
                ]);
            }
        }

        $missingUsers = $event->usersWithMissingRegistration();

        $this->assertTrue($missingUsers->isEmpty());
    }

    /**
     * Test that the one not registerd User is returned,
     * but the other registered Users are not returned.
     */
    public function testOneNotRegistered() {
        $users = User::factory()->count(3)->create();
        $notRegisteredUser = User::factory()->create();
        $event = Event::factory()->create();
        $event->users()->attach($users);
        $event->users()->attach($notRegisteredUser);
        $dateOptions = EventDateOption::factory()->count(3)
            ->create(['event_id' => $event->id]);
        foreach ($users as $user) {
            foreach ($dateOptions as $dateOption) {
                EventRegistration::factory()->create([
                    'user_id'              => $user->id,
                    'event_date_option_id' => $dateOption->id,
                ]);
            }
        }

        $missingUsers = $event->usersWithMissingRegistration();

        $this->assertEquals(1, $missingUsers->count());
        $this->assertEquals($missingUsers->first()->id, $notRegisteredUser->id);
    }

    /**
     * Test that the one not registerd User,
     * that is registered to all dates except one,
     * is returned, but the other registered Users are not returned.
     */
    public function testOneNotRegisteredToOneDate() {
        $users = User::factory()->count(3)->create();
        $notRegisteredUser = User::factory()->create();
        $event = Event::factory()->create();
        $event->users()->attach($users);
        $event->users()->attach($notRegisteredUser);
        $dateOptions = EventDateOption::factory()->count(3)
            ->create(['event_id' => $event->id]);
        foreach ($dateOptions->slice(0, 2) as $dateOption) {
            EventRegistration::factory()->create([
                'user_id'              => $notRegisteredUser->id,
                'event_date_option_id' => $dateOption->id,
            ]);
        }
        foreach ($users as $user) {
            foreach ($dateOptions as $dateOption) {
                EventRegistration::factory()->create([
                    'user_id'              => $user->id,
                    'event_date_option_id' => $dateOption->id,
                ]);
            }
        }

        $missingUsers = $event->usersWithMissingRegistration();

        $this->assertEquals(1, $missingUsers->count());
        $this->assertEquals($missingUsers->first()->id, $notRegisteredUser->id);
    }

    /**
     * Test that the multiple not registerd User are returned,
     * but the other registered Users are not returned.
     */
    public function testMultipleNotRegistered() {
        $users = User::factory()->count(3)->create();
        $notRegisteredUsers = User::factory()->count(3)->create();
        $event = Event::factory()->create();
        $event->users()->attach($users);
        $event->users()->attach($notRegisteredUsers);
        $dateOptions = EventDateOption::factory()->count(3)
            ->create(['event_id' => $event->id]);
        foreach ($users as $user) {
            foreach ($dateOptions as $dateOption) {
                EventRegistration::factory()->create([
                    'user_id'              => $user->id,
                    'event_date_option_id' => $dateOption->id,
                ]);
            }
        }

        $missingUsers = $event->usersWithMissingRegistration();

        $this->assertEquals(3, $missingUsers->count());
        foreach ($notRegisteredUsers as $notRegisteredUser) {
            $this->assertTrue($missingUsers->contains('id', $notRegisteredUser->id));
        }
    }

    /**
     * Test that Users, that are not assigned to the Event
     * and are not registered, are not returned.
     */
    public function testNotAssignedUsers() {
        User::factory()->count(3)->create();
        $event = Event::factory()
            ->has(EventDateOption::factory()->count(3))
            ->create();

        $missingUsers = $event->usersWithMissingRegistration();

        $this->assertTrue($missingUsers->isEmpty());
    }
}
