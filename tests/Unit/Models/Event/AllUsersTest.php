<?php

namespace Tests\Unit\Models\Event;

use App\Models\Event;
use App\Models\User;
use App\Models\UserGroup;
use Illuminate\Support\Collection;
use Tests\TestCase;

class AllUsersTest extends TestCase {
    /**
     * Test Users directly assigned to the Event.
     */
    public function testUsers() {
        $event = Event::factory()->create();
        $users = User::factory()->count(3)->create();
        $event->users()->attach($users);

        $this->assertAllUsers($users, $event);
    }

    /**
     * Test Users assigned to the Event via an UserGroup.
     */
    public function testUsersFromGroups() {
        $event = Event::factory()->create();
        $users = User::factory()->count(3)->create();
        $userGroups = UserGroup::factory()->count(2)->create();
        $userGroups[0]->users()->attach($users->slice(0, 2));
        $userGroups[1]->users()->attach($users->slice(1, 2));
        $event->userGroups()->attach($userGroups);

        $this->assertAllUsers($users, $event);
    }

    /**
     * Public function test responsible User in Event::allUsers
     */
    public function testResponsibleUser() {
        $responsible = User::factory()->create();
        $event = Event::factory()->create([
            'responsible_id' => $responsible->id,
        ]);

        $this->assertAllUsers(collect()->push($responsible), $event);
    }

    /**
     * Test Event::allUsers with Users
     * directly assigned to the Event,
     * assigned via UserGroups
     * and the responsible User
     */
    public function testUsersFromAll() {
        $responsible = User::factory()->create();
        $event = Event::factory()->create([
            'responsible_id' => $responsible->id,
        ]);

        $usersFromGroups = User::factory()->count(2)->create();
        $userGroup = UserGroup::factory()->create();
        $userGroup->users()->attach($usersFromGroups);
        $event->userGroups()->attach($userGroup);

        $usersDirectly = User::factory()->count(2)->create();
        $event->users()->attach($usersDirectly);

        $users = collect();
        $users->push($responsible);
        $users = $users->concat($usersFromGroups);
        $users = $users->concat($usersDirectly);

        $this->assertAllUsers($users, $event);
    }

    /**
     * Test that Event::allUsers doesn't return duplicate Users
     * when assigning Users via multiple ways.
     */
    public function testDuplicateUsers() {
        $users = User::factory()->count(5)->create();
        $event = Event::factory()->create([
            'responsible_id' => $users->first()->id,
        ]);

        $userGroup = UserGroup::factory()->create();
        $userGroup->users()->attach($users);
        $event->userGroups()->attach($userGroup);

        $event->users()->attach($users);

        $this->assertAllUsers($users, $event);
    }

    /**
     * Assert that all Users in $users are returned from $event->assertAllUsers()
     * and that no other Users are returned.
     *
     * @param \Illuminate\Support\Collection $users
     * @param \App\Models\Event $event
     */
    private function assertAllUsers(Collection $users, Event $event) {
        $allUsers = $event->allUsers();

        $this->assertEquals($users->count(), $allUsers->count());
        foreach ($users as $user) {
            $this->assertTrue($allUsers->contains($user));
        }
    }
}
