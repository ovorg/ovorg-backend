<?php

namespace Tests\Unit\Models\Privacy;

use App\Models\Privacy;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Tests\Traits\CreatesModels;

class DefaultPrivacyTest extends TestCase {
    use CreatesModels;

    /**
     * Test the default values.
     */
    public function testDefaultValues() {
        $user = User::factory()->create();
        $privacy = $user->privacy;
        $this->assertFalse($privacy->email);
        $this->assertFalse($privacy->phone);
        $this->assertFalse($privacy->mobile);
    }

    /**
     * Test to override default values.
     */
    public function testOverrideDefaultValues() {
        $user = User::factory()->create();
        $privacy = $user->privacy;
        $privacy->email = true;
        $privacy->phone = true;
        $privacy->mobile = true;
        $privacy->save();

        $privacy = Privacy::find($user->id);
        $this->assertTrue($privacy->email);
        $this->assertTrue($privacy->phone);
        $this->assertTrue($privacy->mobile);
    }
}
