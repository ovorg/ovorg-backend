<?php

namespace Tests\Unit\Models\File;

use App\Models\File;
use Tests\TestCase;

class IsEnoughSpaceTest extends TestCase {
    /**
     * Test the File::isEnoughSpace functionality.
     */
    public function testIsEnoughSpace() {
        $maxSize = config('filesystems.max_storage_size') * 1000 * 1000;
        File::factory()->count(3)->create(['size' => $maxSize / 4]);
        $this->assertFalse(File::isEnoughSpace(($maxSize / 4) + 1));
        $this->assertTrue(File::isEnoughSpace($maxSize / 4));
    }
}
