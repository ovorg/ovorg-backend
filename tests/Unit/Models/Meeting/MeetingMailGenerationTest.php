<?php

namespace Tests\Unit\Models\Meeting;

use App\Models\Meeting;
use Carbon\Carbon;
use Tests\TestCase;

class MeetingMailGenerationTest extends TestCase {
    /**
     * Test if one MeetingMail is generated for a meeting for today.
     */
    public function testForToday() {
        $meeting = Meeting::factory()->create(['start' => Carbon::today()->endOfDay(), 'register_until' => null]);

        $this->assertEquals(1, $meeting->meetingMails->count());
        $this->assertTrue($meeting->meetingMails->first()->date->isToday());
    }

    /**
     * Test that no MeetingMail is generated for a meeting for yesterday.
     */
    public function testForPast() {
        $meeting = Meeting::factory()->create(['start' => Carbon::yesterday(), 'register_until' => null]);

        $this->assertEquals(0, $meeting->meetingMails->count());
    }

    /**
     * Test that no MeetingMail is generated for a meeting with register_until yesterday.
     */
    public function testForPastRegisterUntil() {
        $meeting = Meeting::factory()->create(['start' => Carbon::tomorrow(), 'register_until' => Carbon::yesterday()]);

        $this->assertEquals(0, $meeting->meetingMails->count());
    }

    /**
     * Test MeetingMail dates and count.
     */
    public function testDates() {
        $meeting = Meeting::factory()->create(['start' => Carbon::now()->addDays(30), 'register_until' => null]);

        $meetingMails = $meeting->meetingMails()->orderBy('date')->get();
        $this->assertTrue($meetingMails[0]->date->isSameDay($meeting->start->subDays(14)));
        $this->assertTrue($meetingMails[1]->date->isSameDay($meeting->start->subDays(7)));
        $this->assertTrue($meetingMails[2]->date->isSameDay($meeting->start->subDays(3)));
        $this->assertEquals(3, $meeting->meetingMails->count());
    }

    /**
     * Test MeetingMail dates and count with register_until.
     */
    public function testDatesRegisterUntil() {
        $meeting = Meeting::factory()->create(['start' => Carbon::now()->addDays(60), 'register_until' => Carbon::now()->addDays(30)]);

        $meetingMails = $meeting->meetingMails()->orderBy('date')->get();
        $this->assertTrue($meetingMails[0]->date->isSameDay($meeting->register_until->subDays(14)));
        $this->assertTrue($meetingMails[1]->date->isSameDay($meeting->register_until->subDays(7)));
        $this->assertTrue($meetingMails[2]->date->isSameDay($meeting->register_until->subDays(3)));
        $this->assertEquals(3, $meeting->meetingMails->count());
    }

    /**
     * Test MeetingMail dates and count when updating start.
     */
    public function testUpdateStart() {
        $oldDate = Carbon::now()->addDays(30);
        $meeting = Meeting::factory()->create(['start' => $oldDate, 'register_until' => null]);

        $meetingMail = $meeting->meetingMails()->orderBy('date')->first();
        $meetingMail->sent = true;
        $meetingMail->save();

        $meeting->start = Carbon::now()->addDays(45);
        $meeting->save();

        $meetingMails = $meeting->meetingMails()->orderBy('date')->get();
        $this->assertTrue($meetingMails[0]->date->isSameDay($oldDate->subDays(14)));
        $this->assertTrue($meetingMails[1]->date->isSameDay($meeting->start->subDays(7)));
        $this->assertTrue($meetingMails[2]->date->isSameDay($meeting->start->subDays(3)));
        $this->assertEquals(3, $meeting->meetingMails->count());
    }

    /**
     * Test MeetingMail dates and count when updating register_until.
     */
    public function testUpdateRegisterUntil() {
        $oldDate = Carbon::now()->addDays(30);
        $meeting = Meeting::factory()->create(['start' => Carbon::now()->addYears(1), 'register_until' => $oldDate]);

        $meetingMail = $meeting->meetingMails()->orderBy('date')->first();
        $meetingMail->sent = true;
        $meetingMail->save();

        $meeting->register_until = Carbon::now()->addDays(45);
        $meeting->save();

        $meetingMails = $meeting->meetingMails()->orderBy('date')->get();
        $this->assertTrue($meetingMails[0]->date->isSameDay($oldDate->subDays(14)));
        $this->assertTrue($meetingMails[1]->date->isSameDay($meeting->register_until->subDays(7)));
        $this->assertTrue($meetingMails[2]->date->isSameDay($meeting->register_until->subDays(3)));
        $this->assertEquals(3, $meeting->meetingMails->count());
    }

    /**
     * Test that no mail is created for today, when the meetings starts tomorrow
     * and a mail was already sent today.
     */
    public function testNoMailForToday() {
        $meeting = Meeting::factory()->create(['start' => Carbon::tomorrow()->addDays(1), 'register_until' => null])->first();
        $mail = $meeting->meetingMails()->first();
        $this->assertNotNull($mail);

        $mail->date = Carbon::today();
        $mail->sent = true;
        $mail->save();

        $meeting->start = Carbon::today();
        $meeting->save();

        $meetingMails = $meeting->meetingMails()->get();
        $this->assertEquals(1, $meetingMails->count());
        $this->assertTrue($meetingMails[0]->sent);
        $this->assertTrue($meetingMails[0]->date->isSameDay(Carbon::today()));
    }
}
