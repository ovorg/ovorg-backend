<?php

namespace Tests\Unit\Models\Meeting;

use App\Models\Meeting;
use App\Models\User;
use App\Models\UserGroup;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class NotRegisteredTest extends TestCase {
    public function testNotRegistered() {
        $meeting = Meeting::factory()->create();
        $users = User::factory()->count(3)->create();
        $meeting->users()->attach($users[0]);
        $userGroup = UserGroup::factory()->create();
        $users[1]->userGroups()->attach($userGroup);
        $meeting->userGroups()->attach($userGroup);
        $meeting->responsible()->associate($users[2]);
        $meeting->save();
        $notRegistered = $meeting->notRegistered();

        $this->assertTrue($notRegistered->count() == 3);

        $meeting->registeredUsers()->attach($users[0], ['attendance' => 'yes']);
        $meeting = $meeting->fresh();
        $notRegistered = $meeting->notRegistered();

        $this->assertTrue($notRegistered->count() == 2);

        $meeting->registeredUsers()->attach($users[1], ['attendance' => 'no']);
        $meeting = $meeting->fresh();
        $notRegistered = $meeting->notRegistered();

        $this->assertTrue($notRegistered->count() == 1);

        $meeting->registeredUsers()->attach($users[2], ['attendance' => 'maybe']);
        $meeting = $meeting->fresh();
        $notRegistered = $meeting->notRegistered();

        $this->assertTrue($notRegistered->count() == 0);
    }
}
