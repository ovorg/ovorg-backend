<?php

namespace Tests\Unit\Models\Meeting;

use App\Models\Meeting;
use App\Models\User;
use App\Models\UserGroup;
use Tests\TestCase;

class AllUsersTest extends TestCase {
    /**
     * Test Meeting::allUsers() without Users.
     */
    public function testWithoutUsers() {
        $meeting = Meeting::factory()->create();

        $users = $meeting->allUsers();
        $this->assertNotNull($users);
        $this->assertEquals(0, $users->count());
    }

    /**
     * Test Meeting::allUsers() with Users directly assigned to the Meeting.
     */
    public function testWithDirectUsers() {
        $meeting = Meeting::factory()->create();
        $users = User::factory()->count(3)->create();
        $meeting->users()->attach($users);

        $allUsers = $meeting->allUsers();

        $this->assertNotNull($allUsers);
        $this->assertEquals(3, $allUsers->count());
        foreach ($users as $user) {
            $this->assertTrue($allUsers->contains($user));
        }
    }

    /**
     * Test Meeting::allUsers() with a responsible User.
     */
    public function testResponsible() {
        $meeting = Meeting::factory()->create();
        $user = User::factory()->create();
        $meeting->responsible()->associate($user);

        $allUsers = $meeting->allUsers();

        $this->assertNotNull($allUsers);
        $this->assertEquals(1, $allUsers->count());
        $this->assertTrue($allUsers->contains($user));
    }

    /**
     * Test Meeting::allUsers() with Users through UserGroups.
     */
    public function testUserGroups() {
        $meeting = Meeting::factory()->create();
        $users = User::factory()->count(9)->create();
        $userGroups = UserGroup::factory()->count(3)->create();
        $userGroups[0]->users()->attach($users->slice(0, 3));
        $userGroups[1]->users()->attach($users->slice(3, 3));
        $userGroups[2]->users()->attach($users);
        $meeting->userGroups()->attach($userGroups);

        $allUsers = $meeting->allUsers();

        $this->assertNotNull($allUsers);
        $this->assertEquals(9, $allUsers->count());
        foreach ($users as $user) {
            $this->assertTrue($allUsers->contains($user));
        }
    }

    /**
     * Test Meeting::allUsers() with Users through UserGroups,
     * a responsible User and Users directly assigned to the Meeting.
     */
    public function testWithAll() {
        $meeting = Meeting::factory()->create();
        $users = User::factory()->count(13)->create();
        $userGroups = UserGroup::factory()->count(3)->create();
        $userGroups[0]->users()->attach($users->slice(0, 3));
        $userGroups[1]->users()->attach($users->slice(3, 3));
        $userGroups[2]->users()->attach($users->slice(0, 9));
        $meeting->userGroups()->attach($userGroups);
        $meeting->responsible()->associate($users[9]);
        $meeting->users()->attach($users->slice(10, 3));

        $allUsers = $meeting->allUsers();

        $this->assertNotNull($allUsers);
        $this->assertEquals(13, $allUsers->count());
        foreach ($users as $user) {
            $this->assertTrue($allUsers->contains($user));
        }
    }
}
