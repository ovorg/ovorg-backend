<?php

namespace Tests\Unit\Models\Meeting;

use App\Models\Meeting;
use App\Models\User;
use Tests\TestCase;

class IsUserRegisteredTest extends TestCase {
    /**
     * Test that isUserRegistered returns false it the User is not registered.
     */
    public function testNotRegistered() {
        $meeting = Meeting::factory()->create();
        $user = User::factory()->create();

        $this->assertFalse($meeting->isUserRegistered($user));
    }

    /**
     * Test that isUserRegistered returns true it the User is registered.
     */
    public function testRegistered() {
        $meeting = Meeting::factory()->create();
        $user = User::factory()->create();

        $meeting->registeredUsers()->attach($user, [
            'attendance' => 'yes',
        ]);

        $this->assertTrue($meeting->isUserRegistered($user));
    }
}
