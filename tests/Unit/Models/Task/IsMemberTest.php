<?php

namespace Tests\Unit\Models\Task;

use App\Models\Task;
use App\Models\User;
use Tests\TestCase;

class IsMemberTest extends TestCase {
    public function testNoMember() {
        $task = Task::factory()->create();
        $user = User::factory()->create();

        $this->assertFalse($task->isMember($user));
    }

    public function testMember() {
        $task = Task::factory()->create();
        $user = User::factory()->create();
        $task->users()->attach($user);

        $this->assertTrue($task->isMember($user));
    }
}
