<?php

namespace Tests\Unit\Models\Task;

use App\Models\Task;
use App\Models\User;
use Tests\TestCase;

class GetParticipationTypeTest extends TestCase {
    /**
     * Test getParticipationType for no participation.
     */
    public function testNo() {
        $task = Task::factory()->create();
        $user = User::factory()->create();

        $this->assertEquals(Task::PARTICIPATION_NO, $task->getParticipationType($user));
    }

    /**
     * Test getParticipationType for members.
     */
    public function testMember() {
        $task = Task::factory()->create();
        $user = User::factory()->create();
        $task->users()->attach($user);

        $this->assertEquals(Task::PARTICIPATION_MEMBER, $task->getParticipationType($user));
    }

    /**
     * Test getParticipationType for responsible.
     */
    public function testResponsible() {
        $task = Task::factory()->create();
        $user = User::factory()->create();
        $task->responsible()->associate($user);
        $task->save();

        $this->assertEquals(Task::PARTICIPATION_RESPONSIBLE, $task->getParticipationType($user));
    }

    /**
     * Test getParticipationType for responsible and member.
     */
    public function testResponsibleMember() {
        $task = Task::factory()->create();
        $user = User::factory()->create();
        $task->users()->attach($user);
        $task->responsible()->associate($user);
        $task->save();

        $this->assertEquals(Task::PARTICIPATION_RESPONSIBLE_MEMBER, $task->getParticipationType($user));
    }
}
