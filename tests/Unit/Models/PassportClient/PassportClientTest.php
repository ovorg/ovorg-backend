<?php

namespace Tests\Unit\Models\PassportClient;

use App\Models\PassportClient;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PassportClientTest extends TestCase {
    /**
     * Test the passwordGrantClient function.
     */
    public function testPasswordGrantClient(): void {
        $this->expectException(ModelNotFoundException::class);
        $client = PassportClient::passwordGrantClient();

        Artisan::call('passport:init');

        $client = PassportClient::passwordGrantClient();
        $this->assertNotNull($client);
    }

}
