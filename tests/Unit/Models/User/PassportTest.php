<?php

namespace Tests\Unit\Models\User;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Tests\Traits\CreatesUsers;

class PassportTest extends TestCase {
    use CreatesUsers;

    /**
     * Test the function User::findForPassport.
     *
     * @return void
     */
    public function testFindForPassport() {
        $emptyUser = new User();
        $user1 = self::createUser();
        $user2 = self::createUser();
        $user3 = self::createUser();
        $user3->username = $user2->email;
        $user3->save();

        $this->assertNotNull($emptyUser->findForPassport($user1->username));
        $this->assertNull($emptyUser->findForPassport(''));
        $this->assertNull($emptyUser->findForPassport(null));
        $this->assertNotEquals($emptyUser->findForPassport($user1->username), $emptyUser->findForPassport($user2->username));
        $this->assertNotNull($emptyUser->findForPassport($user1->email));

        // Test that the username cannot override someones email for login.
        $this->assertEquals($emptyUser->findForPassport($user3->username)->id, $user2->id);
    }

    /**
     * Test the function User::validateForPassportPasswordGrant.
     *
     * @return void
     */
    public function testValidateForPassportPasswordGrant() {
        $user = self::createUser();
        $activatedUser = self::activateUser(self::createUser());

        // User is activated
        $this->assertTrue($activatedUser->validateForPassportPasswordGrant(self::$password));
        $this->assertFalse($activatedUser->validateForPassportPasswordGrant(self::$password.'1'));

        // User is not activated
        $this->assertFalse($user->validateForPassportPasswordGrant(self::$password));
        $this->assertFalse($user->validateForPassportPasswordGrant(self::$password.'1'));
    }
}
