<?php

namespace Tests\Unit\Models\User;

use App\Models\Meeting;
use App\Models\User;
use App\Models\UserGroup;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Tests\Traits\CreatesModels;

class AllMeetingsTest extends TestCase {
    use CreatesModels;

    private $modelCount = 5;

    /**
     * Test getAllMeetings without Meetings.
     */
    public function testWithoutModels() {
        $user = User::factory()->create();
        $this->assertTrue($user->getAllMeetings()->count() == 0);
    }

    /**
     * Test getAllMeetings with Meetings assigned directly to the User.
     */
    public function testWithUserMeetings() {
        $user = User::factory()->create();
        $this->assignMeetingsDirectly($user);
        $this->assertTrue($user->getAllMeetings()->count() == $this->modelCount);
    }

    /**
     * Test getAllMeetings with Meetings assigned to the User via UserGroups.
     */
    public function testWithUserGroupMeetings() {
        $user = User::factory()->create();
        $this->assignMeetingsUserGroups($user);
        $this->assertTrue($user->getAllMeetings()->count() == $this->modelCount);
    }

    /**
     * Test getAllMeetings with Meetings assigned to the User as responsible.
     */
    public function testWithResponsibleMeetings() {
        $user = User::factory()->create();
        $this->assignMeetingsResponsible($user);
        $this->assertTrue($user->getAllMeetings()->count() == $this->modelCount);
    }

    /**
     * Test getAllMeetings with Meetings assigned to the User through all 3 ways
     */
    public function testWithAll() {
        $user = User::factory()->create();
        $this->assignMeetingsDirectly($user);
        $this->assignMeetingsUserGroups($user);
        $this->assignMeetingsResponsible($user);
        $this->assertTrue($user->getAllMeetings()->count() == $this->modelCount * 3);
    }

    /**
     * Assign $this->modelCount Meetings directly to the User.
     *
     * @param \App\Models\User $user
     */
    private function assignMeetingsDirectly(User $user) {
        $meetings = self::createModels(Meeting::class, $this->modelCount);
        foreach ($meetings as $meeting) {
            $user->meetings()->attach($meeting);
        }
    }

    /**
     * Assign $this->modelCount Meetings through UserGroups.
     *
     * @param \App\Models\User $user
     */
    private function assignMeetingsUserGroups(User $user) {
        $meetings = self::createModels(Meeting::class, $this->modelCount);
        $userGroups = self::createModels(UserGroup::class, $this->modelCount);
        for ($i = 0; $i < $this->modelCount; $i++) {
            $userGroups[$i]->meetings()->attach($meetings[$i]);
            $user->userGroups()->attach($userGroups[$i]);
        }
    }

    /**
     * Assign $this->modelCount Meetings as responsible
     *
     * @param \App\Models\User $user
     */
    private function assignMeetingsResponsible(User $user) {
        $meetings = self::createModels(Meeting::class, $this->modelCount);
        foreach ($meetings as $meeting) {
            $meeting->responsible()->associate($user);
            $meeting->save();
        }
    }
}
