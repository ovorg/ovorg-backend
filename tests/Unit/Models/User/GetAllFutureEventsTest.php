<?php

namespace Tests\Unit\Models\User;

use App\Models\Event;
use App\Models\EventDateOption;
use App\Models\User;
use Carbon\Carbon;
use Tests\TestCase;

class GetAllFutureEventsTest extends TestCase {
    public function testGetAllFutureEvents() {
        $events = Event::factory()->count(4)->create();
        EventDateOption::factory()->create([
            'event_id' => $events[1]->id,
            'date'     => Carbon::today(),
        ]);
        EventDateOption::factory()->create([
            'event_id' => $events[1]->id,
            'date'     => Carbon::yesterday(),
        ]);

        EventDateOption::factory()->create([
            'event_id' => $events[2]->id,
            'date'     => Carbon::yesterday(),
        ]);
        EventDateOption::factory()->create([
            'event_id' => $events[2]->id,
            'date'     => Carbon::tomorrow(),
        ]);

        EventDateOption::factory()->create([
            'event_id' => $events[3]->id,
            'date'     => Carbon::tomorrow()->addDay(),
        ]);

        $user = User::factory()->create();
        $user->events()->attach($events);

        $futureEvents = $user->getAllFutureEvents();

        $this->assertEquals(2, $futureEvents->count());
        $this->assertTrue($futureEvents->contains('id', $events[2]->id));
        $this->assertTrue($futureEvents->contains('id', $events[3]->id));
    }
}
