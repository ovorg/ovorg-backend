<?php

namespace Tests\Unit\Models\User;

use App\Models\Event;
use App\Models\User;
use App\Models\UserGroup;
use Illuminate\Support\Collection;
use Tests\TestCase;

class AllEventsTest extends TestCase {
    /**
     * Test Events direclty assigned to the User.
     */
    public function testEvents() {
        $user = User::factory()->create();
        $events = Event::factory()->count(5)->create();
        $user->events()->attach($events);

        $this->assertAllEvents($events, $user);
    }

    /**
     * Test Events assigned to the User as reponsible.
     */
    public function testResponsibleEvents() {
        $user = User::factory()->create();
        $events = Event::factory()->count(2)->create([
            'responsible_id' => $user->id,
        ]);

        $this->assertAllEvents($events, $user);
    }

    /**
     * Test Events assigned to the User via UserGroups.
     */
    public function testUserGroupEvents() {
        $user = User::factory()->create();
        $userGroups = UserGroup::factory()->count(2)->create();
        $user->userGroups()->attach($userGroups);
        $events = Event::factory()->count(3)->create();
        $userGroups[0]->events()->attach($events->slice(0, 2));
        $userGroups[1]->events()->attach($events->slice(1, 2));

        $this->assertAllEvents($events, $user);
    }

    /**
     * Test User::getAllEvents with Events
     * directly assigned to the User,
     * assigned via UserGroups
     * and the responsible Events.
     */
    public function testEventsFromAll() {
        $user = User::factory()->create();

        $directEvents = Event::factory()->count(5)->create();
        $user->events()->attach($directEvents);

        $responsibleEvents = Event::factory()->count(2)->create([
            'responsible_id' => $user->id,
        ]);

        $userGroups = UserGroup::factory()->count(2)->create();
        $user->userGroups()->attach($userGroups);
        $userGroupEvents = Event::factory()->count(3)->create();
        $userGroups[0]->events()->attach($userGroupEvents->slice(0, 2));
        $userGroups[1]->events()->attach($userGroupEvents->slice(1, 2));

        $events = collect();
        $events = $events->concat($directEvents);
        $events = $events->concat($responsibleEvents);
        $events = $events->concat($userGroupEvents);

        $this->assertAllEvents($events, $user);
    }

    /**
     * Test that User::getAllEvents doens't contain duplicate Events
     * when assigning Events via multiple ways.
     */
    public function testDuplicateEvents() {
        $user = User::factory()->create();

        $events = Event::factory()->count(5)->create([
            'responsible_id' => $user->id,
        ]);
        $user->events()->attach($events);

        $userGroup = UserGroup::factory()->create();
        $user->userGroups()->attach($userGroup);
        $userGroup->events()->attach($events);

        $this->assertAllEvents($events, $user);
    }

    /**
     * Assert that all Events in $events are returned from $user->getAllEvents()
     * and that no other Events are returned.
     *
     * @param \Illuminate\Support\Collection $events
     * @param \App\Models\User $user
     */
    private function assertAllEvents(Collection $events, User $user) {
        $allEvents = $user->getAllEvents();

        $this->assertEquals($events->count(), $allEvents->count());
        foreach ($events as $event) {
            $this->assertTrue($allEvents->contains($event));
        }
    }
}
