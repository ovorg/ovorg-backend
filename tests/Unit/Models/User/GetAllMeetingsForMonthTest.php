<?php

namespace Tests\Unit\Models\User;

use App\Models\Meeting;
use App\Models\User;
use App\Models\UserGroup;
use Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class GetAllMeetingsForMonthTest extends TestCase {

    /**
     * Test User::getMeetingsForMonth functionality.
     */
    public function testGetMeetingsForMonth() {
        list($user, $meetings) = $this->prepareModels();
        $now = Carbon::now();

        $this->assertTrue($user->getMeetingsForMonth($now)->isEmpty());
        $user->meetings()->attach($meetings);

        $monthMeetings = $user->getMeetingsForMonth($now);
        $this->assertEquals(2, $monthMeetings->count());
        $this->assertTrue($monthMeetings->contains($meetings[0]));
        $this->assertTrue($monthMeetings->contains($meetings[1]));
    }

    /**
     * Test User::getMeetingsFromGroupsForMonth functionality.
     */
    public function testGetMeetingsFromGroupsForMonth() {
        list($user, $meetings) = $this->prepareModels();
        $userGroup = UserGroup::factory()->create();
        $userGroup->users()->attach($user);
        $now = Carbon::now();

        $this->assertTrue($user->getMeetingsFromGroupsForMonth($now)->isEmpty());
        $userGroup->meetings()->attach($meetings);

        $monthMeetings = $user->getMeetingsFromGroupsForMonth($now);
        $this->assertEquals(2, $monthMeetings->count());
        $this->assertTrue($monthMeetings->contains($meetings[0]));
        $this->assertTrue($monthMeetings->contains($meetings[1]));
    }

    /**
     * Test User::getResponsibleMeetingsForMonth functionality.
     */
    public function testGetResponsibleMeetingsForMonth() {
        list($user, $meetings) = $this->prepareModels();
        $now = Carbon::now();

        $this->assertTrue($user->getResponsibleMeetingsForMonth($now)->isEmpty());
        $meetings[0]->responsible()->associate($user);
        $meetings[0]->save();
        $meetings[1]->responsible()->associate($user);
        $meetings[1]->save();

        $monthMeetings = $user->getResponsibleMeetingsForMonth($now);
        $this->assertEquals(2, $monthMeetings->count());
        $this->assertTrue($monthMeetings->contains($meetings[0]));
        $this->assertTrue($monthMeetings->contains($meetings[1]));
    }

    /**
     * Test User::getAllMeetingsForMonth functionality.
     */
    public function testGetAllMeetingsForMonth() {
        $user = User::factory()->create();
        $userGroup = UserGroup::factory()->create();
        $userGroup->users()->attach($user);
        $meetings = Meeting::factory()->count(5)->create();
        $meetings[0]->start = new Carbon('first day of this month');
        $meetings[0]->save();
        $meetings[1]->start = new Carbon('last day of this month');
        $meetings[1]->save();
        $meetings[2]->start = (new Carbon('first day of this month'))->addDays(15);
        $meetings[2]->save();
        $meetings[3]->start = new Carbon('last day of last month');
        $meetings[3]->save();
        $meetings[4]->start = new Carbon('first day of next month');
        $meetings[4]->save();
        $now = Carbon::now();

        $this->assertTrue($user->getAllMeetingsForMonth($now)->isEmpty());

        $meetings[0]->users()->attach($user);
        $meetings[1]->userGroups()->attach($userGroup);
        $meetings[2]->responsible()->associate($user);
        $meetings[2]->save();

        $monthMeetings = $user->getAllMeetingsForMonth($now);
        $this->assertEquals(3, $monthMeetings->count());
        $this->assertTrue($monthMeetings->contains($meetings[0]));
        $this->assertTrue($monthMeetings->contains($meetings[1]));
        $this->assertTrue($monthMeetings->contains($meetings[2]));
    }

    /**
     * Test User::getAllMeetingsForMonth functionality with meetings from last year.
     */
    public function testGetAllMeetingsForMonthLastYear() {
        $user = User::factory()->create();
        $userGroup = UserGroup::factory()->create();
        $userGroup->users()->attach($user);
        $meetings = Meeting::factory()->count(5)->create();
        $meetings[0]->start = (new Carbon('first day of this month'))->subYears(1);
        $meetings[0]->save();
        $meetings[1]->start = (new Carbon('last day of this month'))->subYears(1);
        $meetings[1]->save();
        $meetings[2]->start = (new Carbon('first day of this month'))->addDays(15)->subYears(1);
        $meetings[2]->save();
        $meetings[3]->start = (new Carbon('last day of last month'))->subYears(1);
        $meetings[3]->save();
        $meetings[4]->start = (new Carbon('first day of next month'))->subYears(1);
        $meetings[4]->save();
        $now = Carbon::now();

        $this->assertTrue($user->getAllMeetingsForMonth($now)->isEmpty());

        $meetings[0]->users()->attach($user);
        $meetings[1]->userGroups()->attach($userGroup);
        $meetings[2]->responsible()->associate($user);
        $meetings[2]->save();

        $monthMeetings = $user->getAllMeetingsForMonth($now);
        $this->assertTrue($monthMeetings->isEmpty());
    }

    /**
     * Prepare \App\Models\User and \App\Model\Meetings for the tests.
     *
     * @return array
     */
    private function prepareModels(): array{
        $user = User::factory()->create();
        $meetings = Meeting::factory()->count(4)->create();
        $meetings[0]->start = new Carbon('first day of this month');
        $meetings[0]->save();
        $meetings[1]->start = new Carbon('last day of this month');
        $meetings[1]->save();
        $meetings[2]->start = new Carbon('last day of last month');
        $meetings[2]->save();
        $meetings[3]->start = new Carbon('first day of next month');
        $meetings[3]->save();

        return [$user, $meetings];
    }
}
