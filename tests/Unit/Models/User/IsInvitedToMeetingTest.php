<?php

namespace Tests\Unit\Models\User;

use App\Models\Meeting;
use App\Models\User;
use App\Models\UserGroup;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class IsInvitedToMeetingTest extends TestCase {
    /**
     * Test that a User is not invited to a Meeting.
     */
    public function testNotInvited() {
        $meeting = Meeting::factory()->create();
        $user = User::factory()->create();

        $this->assertFalse($user->isInvitedToMeeting($meeting));
    }

    /**
     * Test that a User is invited to a Meeting when invited directly to the Meeting.
     */
    public function testInvitedDirectly() {
        $meeting = Meeting::factory()->create();
        $user = User::factory()->create();
        $user->meetings()->attach($meeting);

        $this->assertTrue($user->isInvitedToMeeting($meeting));
    }

    /**
     * Test that a User is invited to a Meeting when invited via a UserGroup.
     */
    public function testInvitedUserGroup() {
        $meeting = Meeting::factory()->create();
        $user = User::factory()->create();
        $userGroup = UserGroup::factory()->create();
        $userGroup->users()->attach($user);
        $userGroup->meetings()->attach($meeting);

        $this->assertTrue($user->isInvitedToMeeting($meeting));
    }

    /**
     * Test that a User is invited to a Meeting when he is responsible.
     */
    public function testInvitedResponsible() {
        $meeting = Meeting::factory()->create();
        $user = User::factory()->create();
        $meeting->responsible()->associate($user);
        $meeting->save();

        $this->assertTrue($user->isInvitedToMeeting($meeting));
    }

    /**
     * Test that isInvitedToMeeting returns true when using a Meeting
     * from the registeredMeetings relation.
     */
    public function testInvitedToRegisteredMeeting() {
        $meeting = Meeting::factory()->create();
        $user = User::factory()->create();
        $meeting->users()->attach($user);
        $meeting->registeredUsers()->attach($user, [
            'attendance' => 'yes',
            'comment'    => '',
        ]);
        $meeting = $user->registeredMeetings()->first();

        $this->assertTrue($user->isInvitedToMeeting($meeting));
    }

}
