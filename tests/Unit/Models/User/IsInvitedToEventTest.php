<?php

namespace Tests\Unit\Models\User;

use App\Models\Event;
use App\Models\User;
use App\Models\UserGroup;
use Tests\TestCase;

class IsInvitedToEventTest extends TestCase {
    /**
     * Test with the User directly added to the Event.
     */
    public function testDirectlyAddedUser() {
        $event = Event::factory()->create();
        $user = User::factory()->create();
        $event->users()->attach($user);

        $this->assertTrue($user->isInvitedToEvent($event));
    }

    /**
     * Test with the User added to the Event via a UserGroup.
     */
    public function testAddedUserViaUserGroup() {
        $event = Event::factory()->create();
        $user = User::factory()->create();
        $userGroup = UserGroup::factory()->create();
        $userGroup->users()->attach($user);
        $event->userGroups()->attach($userGroup);

        $this->assertTrue($user->isInvitedToEvent($event));
    }

    /**
     * Test with the User added to the Event via a UserGroup.
     */
    public function testAddedUserAsResponsible() {
        $user = User::factory()->create();
        $event = Event::factory()->create([
            'responsible_id' => $user->id,
        ]);

        $this->assertTrue($user->isInvitedToEvent($event));
    }

    /**
     * Test with the User not added to the Event.
     */
    public function testNotInvited() {
        $event = Event::factory()->create();
        $user = User::factory()->create();

        $this->assertFalse($user->isInvitedToEvent($event));
    }
}
