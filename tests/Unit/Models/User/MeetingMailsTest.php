<?php

namespace Tests\Unit\Models\User;

use App\Models\Meeting;
use App\Models\MeetingMail;
use App\Models\User;
use App\Models\UserGroup;
use Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class MeetingMailsTest extends TestCase {
    use WithFaker;

    /**
     * Test if getAllMeetingMails returns all MeetingMails assigned to an User.
     */
    public function testGetAllMeetingMails() {
        list($user, $meetings) = $this->createTestData();

        foreach ($meetings as $meeting) {
            $meeting->meetingMails()->save(new MeetingMail([
                'date'           => $this->faker->date,
                'include_maybes' => $this->faker->boolean,
                'sent'           => $this->faker->boolean,
            ]));
        }
        $this->assertEquals(3, $user->getAllMeetingMails()->count());

        foreach ($meetings as $meeting) {
            $meeting->meetingMails()->save(new MeetingMail([
                'date'           => $this->faker->date,
                'include_maybes' => $this->faker->boolean,
                'sent'           => $this->faker->boolean,
            ]));
        }
        $this->assertEquals(6, $user->getAllMeetingMails()->count());
    }

    /**
     * Test getAllMeetingMailsForToday.
     */
    public function testGetAllMeetingMailsForToday() {
        list($user, $meetings) = $this->createTestData();

        foreach ($meetings as $meeting) {
            $meeting->meetingMails()->save(new MeetingMail([
                'date'           => Carbon::tomorrow(),
                'include_maybes' => $this->faker->boolean,
                'sent'           => $this->faker->boolean,
            ]));
        }
        $this->assertEquals(0, $user->getAllMeetingMailsForToday()->count());

        foreach ($meetings as $meeting) {
            $meeting->meetingMails()->save(new MeetingMail([
                'date'           => Carbon::today(),
                'include_maybes' => $this->faker->boolean,
                'sent'           => $this->faker->boolean,
            ]));
        }
        $this->assertEquals(3, $user->getAllMeetingMailsForToday()->count());
    }

    /**
     * Test getAllUnsentMeetingMailsForToday.
     */
    public function testGetAllUnsentMeetingMailsForToday() {
        list($user, $meetings) = $this->createTestData();

        foreach ($meetings as $meeting) {
            $meeting->meetingMails()->save(new MeetingMail([
                'date'           => Carbon::tomorrow(),
                'include_maybes' => $this->faker->boolean,
                'sent'           => $this->faker->boolean,
            ]));
        }
        $this->assertEquals(0, $user->getAllUnsentMeetingMailsForToday()->count());

        foreach ($meetings as $meeting) {
            $meeting->meetingMails()->save(new MeetingMail([
                'date'           => Carbon::today(),
                'include_maybes' => $this->faker->boolean,
                'sent'           => true,
            ]));
        }
        $this->assertEquals(0, $user->getAllUnsentMeetingMailsForToday()->count());

        foreach ($meetings as $meeting) {
            $meeting->meetingMails()->save(new MeetingMail([
                'date'           => Carbon::today(),
                'include_maybes' => $this->faker->boolean,
                'sent'           => false,
            ]));
        }
        $this->assertEquals(3, $user->getAllUnsentMeetingMailsForToday()->count());
    }

    /**
     * Create User, UserGroup and Meetings for tests.
     *
     * @return array
     */
    public function createTestData(): array{
        $user = User::factory()->create();
        $userGroup = UserGroup::factory()->create();
        $user->userGroups()->attach($userGroup);
        $meetings = Meeting::factory()->count(3)->create();
        $meetings[0]->responsible()->associate($user);
        $meetings[0]->save();
        $meetings[1]->userGroups()->attach($userGroup);
        $meetings[2]->users()->attach($user);

        // Remove automatically generated MeetingMails.
        foreach (MeetingMail::get() as $meetingMail) {
            $meetingMail->delete();
        }

        return [$user, $meetings];
    }
}
