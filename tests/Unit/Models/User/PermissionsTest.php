<?php

namespace Tests\Unit\Models\User;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Tests\Traits\CreatesModels;

class PermissionsTest extends TestCase {
    use CreatesModels;

    /**
     * Test getAllPermissions.
     * Also automatically tests getRolePermissions.
     *
     * @return void
     */
    public function testGetAllPermissions() {
        $user = User::factory()->create();
        $roles = Role::factory()->count(2)->create();
        $user->roles()->sync($roles);

        $user->permissions = [
            'test0' => true,
            'test1' => true,
        ];
        $user->save();
        $roles[0]->permissions = [
            'test2' => true,
            'test3' => true,
        ];
        $roles[0]->save();
        $roles[1]->permissions = [
            'test4' => true,
            'test5' => true,
        ];
        $roles[1]->save();

        $permissions = $user->getAllPermissions();
        $this->assertArrayHasKey('test0', $permissions);
        $this->assertTrue($permissions['test0']);
        $this->assertArrayHasKey('test1', $permissions);
        $this->assertTrue($permissions['test1']);
        $this->assertArrayHasKey('test2', $permissions);
        $this->assertTrue($permissions['test2']);
        $this->assertArrayHasKey('test3', $permissions);
        $this->assertTrue($permissions['test3']);
        $this->assertArrayHasKey('test4', $permissions);
        $this->assertTrue($permissions['test4']);
        $this->assertArrayHasKey('test5', $permissions);
        $this->assertTrue($permissions['test5']);
    }

    public function testGetAllPermissionsWithoutPermissions() {
        $user = User::factory()->create();
        $roles = Role::factory()->count(2)->create();
        $user->roles()->sync($roles);

        $permissions = $user->getAllPermissions();
        $this->assertEquals($permissions, []);
    }

    public function testGetAllPermissionsWithUserPermissions() {
        $user = User::factory()->create();
        $roles = Role::factory()->count(2)->create();
        $user->roles()->sync($roles);

        $user->permissions = [
            'test0' => true,
            'test1' => true,
        ];
        $user->save();

        $permissions = $user->getAllPermissions();
        $this->assertArrayHasKey('test0', $permissions);
        $this->assertTrue($permissions['test0']);
        $this->assertArrayHasKey('test1', $permissions);
        $this->assertTrue($permissions['test1']);
    }

    public function testGetAllPermissionsWithRolePermissions() {
        $user = User::factory()->create();
        $roles = Role::factory()->count(2)->create();
        $user->roles()->sync($roles);

        $roles[0]->permissions = [
            'test2' => true,
            'test3' => true,
        ];
        $roles[0]->save();
        $roles[1]->permissions = [
            'test4' => true,
            'test5' => true,
        ];
        $roles[1]->save();

        $permissions = $user->getAllPermissions();
        $this->assertArrayHasKey('test2', $permissions);
        $this->assertTrue($permissions['test2']);
        $this->assertArrayHasKey('test3', $permissions);
        $this->assertTrue($permissions['test3']);
        $this->assertArrayHasKey('test4', $permissions);
        $this->assertTrue($permissions['test4']);
        $this->assertArrayHasKey('test5', $permissions);
        $this->assertTrue($permissions['test5']);
    }
}
