<?php

namespace Tests\Unit\Models\User;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Tests\Traits\CreatesModels;

class DoodleTokenTest extends TestCase {
    use CreatesModels;

    public function testDoodleToken() {
        $user = User::factory()->create();
        $this->assertNotNull($user->doodle_token);
    }
}
