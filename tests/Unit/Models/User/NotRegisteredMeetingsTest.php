<?php

namespace Tests\Unit\Models\User;

use App\Models\Meeting;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class NotRegisteredMeetingsTest extends TestCase {
    /**
     * Test for a Meeting where the User is not registered.
     *
     * @return void
     */
    public function testNotRegistered() {
        $meeting = Meeting::factory()->create();
        $user = User::factory()->create();
        $user->meetings()->attach($meeting);

        $notRegistered = $user->notRegisteredMeetings();

        $this->assertTrue($notRegistered->count() == 1);
        $this->assertTrue($notRegistered->contains($meeting));
    }

    /**
     * Test for a Meeting where the User is registered with yes.
     */
    public function testRegisteredYes() {
        $meeting = Meeting::factory()->create();
        $user = User::factory()->create();
        $user->meetings()->attach($meeting);
        $meeting->registeredUsers()->attach($user, [
            'attendance' => 'yes',
            'comment'    => '',
        ]);

        $notRegistered = $user->notRegisteredMeetings();

        $this->assertTrue($notRegistered->count() == 0);
        $this->assertFalse($notRegistered->contains($meeting));
    }

    /**
     * Test for a Meeting where the User is registered with no.
     */
    public function testRegisteredNo() {
        $meeting = Meeting::factory()->create();
        $user = User::factory()->create();
        $user->meetings()->attach($meeting);
        $meeting->registeredUsers()->attach($user, [
            'attendance' => 'no',
            'comment'    => '',
        ]);

        $notRegistered = $user->notRegisteredMeetings();

        $this->assertTrue($notRegistered->count() == 0);
        $this->assertFalse($notRegistered->contains($meeting));
    }

    /**
     * Test for a Meeting where the User is registered with maybe.
     */
    public function testRegisteredMaybe() {
        $meeting = Meeting::factory()->create();
        $user = User::factory()->create();
        $user->meetings()->attach($meeting);
        $meeting->registeredUsers()->attach($user, [
            'attendance' => 'maybe',
            'comment'    => '',
        ]);

        $notRegistered = $user->notRegisteredMeetings();

        $this->assertTrue($notRegistered->count() == 0);
        $this->assertFalse($notRegistered->contains($meeting));
    }
}
