<?php

namespace Tests\Unit\Jobs;

use App\Jobs\Auth\CreateReminder;
use App\Notifications\Auth\ReminderNotification;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;
use Tests\Traits\CreatesUsers;

class CreateReminderTest extends TestCase
{
    use CreatesUsers;

    /**
     * Test the CreateReminder Job.
     */
    public function testCreateReminder()
    {
        $user = self::createUser();
        $userWithReminder = self::createUser();
        $reminder = self::createReminder($userWithReminder);

        Notification::fake();

        // User without an existing reminder
        CreateReminder::dispatch($user);
        Notification::assertSentTo($user, ReminderNotification::class, function ($notification, $channels) use ($user) {
            return $notification->reminder->user_id === $user->id;
        });

        // User with an existing reminder
        CreateReminder::dispatch($userWithReminder);
        Notification::assertSentTo($userWithReminder, ReminderNotification::class, function ($notification, $channels) use ($userWithReminder, $reminder) {
            return $notification->reminder->user_id === $userWithReminder->id && $notification->reminder->id === $reminder->id;
        });
    }
}
