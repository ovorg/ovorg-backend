<?php

namespace Tests\Unit\Jobs;

use Activation;
use App\Jobs\Auth\CreateActivation;
use App\Models\User;
use App\Notifications\Auth\ActivationNotification;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;
use Tests\Traits\CreatesUsers;

class CreateActivationTest extends TestCase
{
    use CreatesUsers;

    /**
     * Test the CreateActivation Job.
     */
    public function testCreateActivation()
    {
        $user = self::createUser();

        Notification::fake();

        // User without an existing activation
        $user->activations()->delete();
        CreateActivation::dispatch($user);
        Notification::assertSentTo($user, ActivationNotification::class, function ($notification, $channels) use ($user) {
            return $notification->activation->user_id === $user->id;
        });
        $this->assertTrue(Activation::exists($user));
    }

    /**
     * Test the CreateActivation Job for an User with an existing Activation.
     * This test asserts that the UserObserver creates an Activation when an User is created.
     */
    public function testCreateActivationWithExistingActvation()
    {
        $user = User::factory()->create();

        // Activation should be created by event UserObserver.
        $this->assertTrue(Activation::exists($user));
        $activation = $user->activations()->first();

        Notification::fake();
        CreateActivation::dispatch($user);

        Notification::assertSentTo($user, ActivationNotification::class, function ($notification, $channels) use ($user, $activation) {
            return $notification->activation->user_id === $user->id && $notification->activation->id === $activation->id;
        });
    }
}
