<?php

namespace Tests\Unit\Jobs;

use App\Jobs\SendEventNotification;
use App\Models\Event;
use App\Models\User;
use App\Notifications\EventNotification;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

class SendEventNotificationTest extends TestCase
{
    /**
     * Test that the SendEventNotification Job sends a
     * EventNotification to all Users assigned to the Event.
     */
    public function testSendEventNotification()
    {
        $event = Event::factory()->create();
        $users = User::factory()->count(3)->create();
        $event->users()->attach($users);
        Notification::fake();

        SendEventNotification::dispatch($event);

        foreach ($users as $user) {
            Notification::assertSentTo($user, EventNotification::class);
        }
    }

    /**
     * Test that the SendEventNotification Job is dispatched
     * when creating an Event.
     */
    public function testSendEventNotificationDispatched()
    {
        Bus::fake();

        Event::factory()->create();

        Bus::assertDispatched(SendEventNotification::class);
    }
}
