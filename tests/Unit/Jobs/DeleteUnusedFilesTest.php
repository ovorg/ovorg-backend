<?php

namespace Tests\Unit\Jobs;

use App\Jobs\DeleteUnusedFiles;
use App\Models\File;
use App\Models\Task;
use Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class DeleteUnusedFilesTest extends TestCase
{

    /**
     * Test that a keep_unlinked config +1 hours old file is deleted.
     */
    public function testDelete()
    {
        $file = File::factory()->create();

        $file->created_at = Carbon::now()->subHours(config('filesystems.keep_unlinked') + 1);
        $file->save();

        $this->assertNotNull(File::find($file->id));

        DeleteUnusedFiles::dispatchNow();

        $this->assertNull(File::find($file->id));
    }

    /**
     * Test that a  keep_unlinked config -1 hours old file is not deleted.
     */
    public function testNotDelete()
    {
        $file = File::factory()->create();

        $file->created_at = Carbon::now()->subHours(config('filesystems.keep_unlinked') - 1);
        $file->save();

        $this->assertNotNull(File::find($file->id));

        DeleteUnusedFiles::dispatchNow();

        $this->assertNotNull(File::find($file->id));
    }

    /**
     * Test that a  keep_unlinked config +1 hours old file assigned to a task is not deleted.
     */
    public function testNotDeleteTaskFile()
    {
        $file = File::factory()
            ->for(Task::factory())
            ->create();
        $file->created_at = Carbon::now()->subHours(config('filesystems.keep_unlinked') + 1);
        $file->save();

        $this->assertNotNull(File::find($file->id));

        DeleteUnusedFiles::dispatchNow();

        $this->assertNotNull(File::find($file->id));
    }
}
