<?php

namespace Tests\Unit\Jobs;

use App\Jobs\UpdateUserPresent;
use App\Models\User;
use Carbon\Carbon;
use Tests\TestCase;

class UpdateUserPresentTest extends TestCase
{
    /**
     * Test that users where preset is true and scan_timeout is now minus one day
     * are set to present = false.
     */
    public function testUpdatePresentUsers()
    {
        $users = User::factory()->count(2)->create([
            'present'      => true,
            'scan_timeout' => Carbon::now()->subDays(1),
        ]);

        UpdateUserPresent::dispatchNow();

        foreach ($users as $user) {
            $user = $user->fresh();
            $this->assertFalse($user->present);
        }
    }

    /**
     * Test that users where preset is true and scan_timeout is greater now minus one day
     * are set to present = false.
     */
    public function testDontUpdatePresentUsers()
    {
        $users = User::factory()->count(2)->create([
            'present'      => true,
            'scan_timeout' => Carbon::now()->subDays(1)->addMinutes(1),
        ]);

        UpdateUserPresent::dispatchNow();

        foreach ($users as $user) {
            $user = $user->fresh();
            $this->assertTrue($user->present);
        }
    }

    /**
     * Test that users where preset is false and scan_timeout is now minus one day
     * are set to present = false.
     */
    public function testDontUpdateNotPresentUsersInPast()
    {
        $users = User::factory()->count(2)->create([
            'present'      => false,
            'scan_timeout' => Carbon::now()->subDays(1),
        ]);

        UpdateUserPresent::dispatchNow();

        foreach ($users as $user) {
            $user = $user->fresh();
            $this->assertFalse($user->present);
        }
    }

    /**
     * Test that users where preset is false and scan_timeout is greater now minus one day
     * are set to present = false.
     */
    public function testDontUpdateNotPresentUsers()
    {
        $users = User::factory()->count(2)->create([
            'present'      => false,
            'scan_timeout' => Carbon::now()->subDays(1)->addMinutes(1),
        ]);

        UpdateUserPresent::dispatchNow();

        foreach ($users as $user) {
            $user = $user->fresh();
            $this->assertFalse($user->present);
        }
    }
}
