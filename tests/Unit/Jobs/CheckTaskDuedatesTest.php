<?php

namespace Tests\Unit\Jobs;

use App\Jobs\CheckTaskDuedates;
use App\Models\Task;
use App\Models\User;
use App\Notifications\Tasks\DuedateExpiredNotification;
use Carbon\Carbon;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

class CheckTaskDuedatesTest extends TestCase
{
    /**
     * Test the CheckTaskDuedatesTest Job with duedate today.
     */
    public function testDuedateToday()
    {
        $task = Task::factory()->create(['duedate' => Carbon::today(), 'state' => 99]);
        $users = User::factory()->count(2)->create();
        $responsible = User::factory()->create();
        $task->users()->attach($users);
        $task->responsible()->associate($responsible);
        $task->save();

        Notification::fake();

        CheckTaskDuedates::dispatch();
        foreach ($users as $user) {
            Notification::assertSentTo($user, DuedateExpiredNotification::class);
        }
        Notification::assertSentTo($responsible, DuedateExpiredNotification::class);
    }

    /**
     * Test the CheckTaskDuedatesTest Job with duedate tomorrow.
     */
    public function testDuedateTomorrow()
    {
        $this->nothingSent(Carbon::tomorrow());
    }

    /**
     * Test the CheckTaskDuedatesTest Job with duedate yesterday.
     */
    public function testDuedateYesterday()
    {
        $this->nothingSent(Carbon::yesterday());
    }

    /**
     * Assert that nothing is sent for a task with $duedate.
     */
    private function nothingSent($duedate)
    {
        $task = Task::factory()->create(['duedate' => $duedate, 'state' => 99]);
        $users = User::factory()->count(2)->create();
        $responsible = User::factory()->create();
        $task->users()->attach($users);
        $task->responsible()->associate($responsible);
        $task->save();

        Notification::fake();

        CheckTaskDuedates::dispatch();
        Notification::assertNothingSent();
    }

    /**
     * Test that no notification is sent for finished tasks.
     */
    public function testFinishedTask()
    {
        $task = Task::factory()->create(['duedate' => Carbon::today(), 'state' => 100]);
        $user = User::factory()->create();
        $task->users()->attach($user);

        Notification::fake();
        CheckTaskDuedates::dispatch();

        Notification::assertNothingSent();
    }
}
