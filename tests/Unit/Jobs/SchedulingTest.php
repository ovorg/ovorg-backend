<?php

namespace Tests\Unit\Jobs;

use App\Jobs\SendMeetingSummary;
use Carbon\Carbon;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Bus;
use Tests\TestCase;

class SchedulingTest extends TestCase
{
    /**
     * Test that the MeetingSummary is sent at the second saturday.
     */
    public function testSendMeetingSummary()
    {
        Carbon::setTestNow((new Carbon('second saturday of this month'))->hour('10'));
        Bus::fake();

        Artisan::call('schedule:run');

        Bus::assertDispatched(SendMeetingSummary::class);
    }

    /**
     * Test that the MeetingSummary is not sent at the first saturday.
     */
    public function testDontSendMeetingSummaryFirst()
    {
        Carbon::setTestNow((new Carbon('first saturday of this month'))->hour('10'));
        Bus::fake();

        Artisan::call('schedule:run');

        Bus::assertNotDispatched(SendMeetingSummary::class);
    }

    /**
     * Test that the MeetingSummary is not sent at the third saturday.
     */
    public function testDontSendMeetingSummaryThird()
    {
        Carbon::setTestNow((new Carbon('third saturday of this month'))->hour('10'));
        Bus::fake();

        Artisan::call('schedule:run');

        Bus::assertNotDispatched(SendMeetingSummary::class);
    }
}
