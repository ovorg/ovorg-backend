<?php

namespace Tests\Unit\Jobs;

use App\Jobs\SendMeetingSummary;
use App\Models\Meeting;
use App\Models\User;
use App\Notifications\MeetingSummaryNotification;
use Carbon;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

class SendMeetingSummaryTest extends TestCase
{
    /**
     * Test SendMeetingSummary when passing a date.
     *
     * @return void
     */
    public function testWithDate()
    {
        $users = User::factory()->count(2)->create();
        $meeting = Meeting::factory()->create();
        $meeting->start = new Carbon('last day of this month');
        $meeting->save();

        $users[0]->meetings()->attach($meeting);

        Notification::fake();

        SendMeetingSummary::dispatchNow(Carbon::now());

        Notification::assertSentTo($users[0], MeetingSummaryNotification::class);
        Notification::assertNotSentTo($users[1], MeetingSummaryNotification::class);
    }

    /**
     * Test SendMeetingSummary when passing no date.
     *
     * @return void
     */
    public function testWithoutDate()
    {
        $users = User::factory()->count(2)->create();
        $meeting = Meeting::factory()->create();
        $meeting->start = new Carbon('first day of next month');
        $meeting->save();

        $users[0]->meetings()->attach($meeting);

        Notification::fake();

        SendMeetingSummary::dispatchNow();

        Notification::assertSentTo($users[0], MeetingSummaryNotification::class);
        Notification::assertNotSentTo($users[1], MeetingSummaryNotification::class);
    }

    /**
     * Test that meetings from the last year in the same month are not send in meeting summary.
     */
    public function testDontSendLastYear()
    {
        $user = User::factory()->create();
        $meeting = Meeting::factory()->create();
        $meeting->start = (new Carbon('first day of next month'))->subYear(1);
        $meeting->save();

        $user->meetings()->attach($meeting);

        Notification::fake();

        SendMeetingSummary::dispatchNow();

        Notification::assertNotSentTo($user, MeetingSummaryNotification::class);
    }
}
