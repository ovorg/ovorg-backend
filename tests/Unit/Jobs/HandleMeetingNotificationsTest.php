<?php

namespace Tests\Unit\Jobs;

use App\Jobs\HandleMeetingNotifications;
use App\Models\Meeting;
use App\Models\MeetingMail;
use App\Models\User;
use App\Notifications\MeetingNotification;
use Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

class HandleMeetingNotificationsTest extends TestCase
{

    /**
     * Test collective notification.
     */
    public function testCollectiveNotification()
    {
        $user = User::factory()->create();
        $meetings = Meeting::factory()->count(5)->create(['start' => Carbon::tomorrow(), 'register_until' => null]);
        $user->meetings()->attach($meetings);

        foreach (MeetingMail::get() as $meetingMail) {
            $meetingMail->sent = false;
            $meetingMail->save();
        }

        Notification::fake();

        HandleMeetingNotifications::dispatchNow();
        Notification::assertSentTo($user, MeetingNotification::class, function ($notification, $channels) {
            return $notification->meetings->count() === 5;
        });
        Notification::assertTimesSent(1, MeetingNotification::class);
        foreach (MeetingMail::get() as $meetingMail) {
            $this->assertTrue($meetingMail->fresh()->sent);
        }
    }

    /**
     * Test a MeetingNotification is sent to maybe if it's included.
     */
    public function testMaybeIncluded()
    {
        $user = User::factory()->create();
        $meeting = Meeting::factory()->create(['start' => Carbon::tomorrow(), 'register_until' => null]);
        $user->meetings()->attach($meeting);
        $user->registeredMeetings()->attach($meeting, ['attendance' => 'maybe']);

        $meetingMail = MeetingMail::first();
        $this->assertNotNull($meetingMail);
        $meetingMail->sent = false;
        $meetingMail->include_maybes = true;
        $meetingMail->save();

        Notification::fake();

        HandleMeetingNotifications::dispatchNow();
        Notification::assertSentTo($user, MeetingNotification::class);
        $this->assertTrue($meetingMail->fresh()->sent);
    }

    /**
     * Test a MeetingNotification is not sent to maybe if it's not included.
     */
    public function testMaybeNotIncluded()
    {
        list($user, $meeting) = $this->createModels('maybe');

        $meetingMail = $meeting->meetingMails()->first();
        $meetingMail->include_maybes = false;
        $meetingMail->save();

        Notification::fake();

        HandleMeetingNotifications::dispatchNow();
        Notification::assertNotSentTo($user, MeetingNotification::class);
        $this->assertTrue($meetingMail->fresh()->sent);
    }

    /**
     * Test a MeetingNotification is not sent to yes.
     */
    public function testMaybYes()
    {
        $this->checkAttendanceNotSent('yes');
    }

    /**
     * Test a MeetingNotification is not sent to no.
     */
    public function testMaybNo()
    {
        $this->checkAttendanceNotSent('no');
    }

    public function testForSeveralUsers()
    {
        $users = User::factory()->count(5)->create();
        $meeting = Meeting::factory()->create(['start' => Carbon::tomorrow(), 'register_until' => null]);
        $meeting->users()->attach($users);

        foreach (MeetingMail::get() as $meetingMail) {
            $meetingMail->sent = false;
            $meetingMail->save();
        }

        Notification::fake();

        HandleMeetingNotifications::dispatchNow();

        foreach ($users as $user) {
            Notification::assertSentTo($user, MeetingNotification::class);
        }
        $this->assertTrue($meetingMail->fresh()->sent);
    }

    /**
     * Check that a mail is not sent when user is registered with $attedance.
     *
     * @param string $attendance
     */
    private function checkAttendanceNotSent(string $attendance)
    {
        list($user, $meeting) = $this->createModels($attendance);
        Notification::fake();

        HandleMeetingNotifications::dispatchNow();
        Notification::assertNotSentTo($user, MeetingNotification::class);
    }

    /**
     * Create User and Meeting registering User to Meeting with $attendance
     *
     * @param string $attedance
     * @return array [User, Meeting]
     */
    private function createModels(string $attendance): array
    {
        $user = User::factory()->create();
        $meeting = Meeting::factory()->create(['start' => Carbon::tomorrow(), 'register_until' => null]);
        $user->meetings()->attach($meeting);
        $user->registeredMeetings()->attach($meeting, ['attendance' => $attendance]);

        $meetingMail = $meeting->meetingMails()->first();
        $meetingMail->sent = false;
        $meetingMail->save();
        return [$user, $meeting];
    }
}
