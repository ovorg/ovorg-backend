<?php

namespace Tests\Unit\Mails;

use App\Mail\MeetingMail;
use App\Models\Meeting;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class MeetingMailTest extends TestCase
{
    /**
     * Test for a single meeting.
     */
    public function testSingleMeeting()
    {
        $user = User::factory()->create();
        $meeting = Meeting::factory()->create();

        Mail::fake();

        Mail::to($user->email)->send(new MeetingMail($meeting, $user));

        Mail::assertSent(MeetingMail::class);
    }

    /**
     * Test for multiple meetings.
     */
    public function testMulitpleMeetings()
    {
        $user = User::factory()->create();
        $meetings = Meeting::factory()->count(5)->create();

        Mail::fake();

        Mail::to($user->email)->send(new MeetingMail($meetings, $user));

        Mail::assertSent(MeetingMail::class);
    }
}
