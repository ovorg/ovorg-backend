<?php

namespace Tests\Unit\Resources;

use App\Http\Resources\Meeting\DoodleMeetingResource;
use App\Http\Resources\Meeting\FullMeetingResource;
use App\Http\Resources\Meeting\MeetingResource;
use App\Http\Resources\OrderItem\OrderItemResource;
use App\Http\Resources\OrderItem\SimplifiedOrderItemResource;
use App\Http\Resources\Task\SimplifiedTaskResource;
use App\Http\Resources\Task\TaskResource;
use App\Models\Meeting;
use App\Models\OrderItem;
use App\Models\Task;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use Tests\TestCase;

class PurifierTest extends TestCase
{
    private $xss = "<script>alert('xss');</script>";

    /**
     * Test that the Task description and addition are purified in Task Resources
     */
    public function testTaskPurified()
    {
        $this->checkAttributePurified(Task::factory(), 'description', TaskResource::class);
        $this->checkAttributePurified(Task::factory(), 'addition', TaskResource::class);
    }

    /**
     * Test that the Meeting description is purified in Meeting Resources.
     */
    public function testMeetingPurified()
    {
        $this->checkAttributePurified(Meeting::factory(), 'description', MeetingResource::class);
        $this->checkAttributePurified(Meeting::factory(), 'description', FullMeetingResource::class);
        $this->checkAttributePurified(Meeting::factory(), 'description', DoodleMeetingResource::class);
    }

    /**
     * Test that the OrderItem description is purified in OrderItem Resources.
     */
    public function testOrderItemPurified()
    {
        $this->checkAttributePurified(OrderItem::factory(), 'description', OrderItemResource::class);
        $this->checkAttributePurified(OrderItem::factory(), 'description', SimplifiedOrderItemResource::class);
    }

    /**
     * Test if $attribute from $model is purified in $resource.
     * Creates a new instance of $model using the factory.
     * Sets the $attribute of the model to $this->xss.
     * Assert that $attribute is cleaned in $resource.
     *
     * @param  string $model
     * @param  string $attribute
     * @param  string $resource
     */
    public function checkAttributePurified(Factory $factory, string $attribute, string $resource)
    {
        $model = $factory->create([
            $attribute => $this->xss,
        ]);

        $resource = new $resource($model);
        $array = $resource->toArray(null);

        $purifiedAttribute = $array[$attribute];

        $this->assertFalse(Str::contains($purifiedAttribute, $this->xss));
    }
}
