<?php

namespace Tests\Unit;

use Tests\TestCase;

class EnvTest extends TestCase
{
    /**
     * Test that the correct env is loaded.
     */
    public function testEnv()
    {
        $this->assertTrue(app()->environment() == "testing");
    }
}
