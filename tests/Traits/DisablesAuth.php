<?php

namespace Tests\Traits;

use App\Http\Middleware\ApiPermission;
use App\Http\Middleware\ApiPermissionAnd;
use App\Http\Middleware\ApiPermissionOr;
use App\Http\Middleware\Authenticate;
use App\Http\Middleware\RedirectIfAuthenticated;
use Laravel\Passport\Http\Middleware\CheckClientCredentials;
use Laravel\Passport\Http\Middleware\CheckClientCredentialsForAnyScope;
use Laravel\Passport\Http\Middleware\CheckForAnyScope;
use Laravel\Passport\Http\Middleware\CheckScopes;

trait DisablesAuth
{
    /**
     * Disable the authorisation middlewares.
     */
    public function disableAuthorisation()
    {
        $this->withoutMiddleware([
            ApiPermission::class,
            ApiPermissionAnd::class,
            ApiPermissionOr::class,
            CheckScopes::class,
            CheckForAnyScope::class,
        ]);
    }

    /**
     * Disable the authentication middlewares.
     */
    public function disableAuthentication()
    {
        $this->withoutMiddleware([
            Authenticate::class,
            CheckClientCredentials::class,
            CheckClientCredentialsForAnyScope::class,
            RedirectIfAuthenticated::class,
        ]);
    }
}
