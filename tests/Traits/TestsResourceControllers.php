<?php

namespace Tests\Traits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Testing\TestResponse;
use Sentinel;

trait TestsResourceControllers
{
    use ActingAs;
    use CreatesModels;

    private $modelType;
    private $modelCount = 5;
    private $routeNamePrefix;
    private $permissions;

    private static $METHODS = [
        'index'   => 'GET',
        'store'   => 'POST',
        'update'  => 'PUT',
        'destroy' => 'DELETE',
        'show'    => 'GET',
    ];

    /**
     * Test the index permissions.
     */
    public function testIndexPermissions()
    {
        $this->checkPermission('index');
    }

    /**
     * Test the store permissions.
     */
    public function testStorePermissions()
    {
        $this->checkPermission('store');
    }

    /**
     * Test the update permissions.
     */
    public function testUpdatePermissions()
    {
        $model = self::createModels($this->modelType);
        $this->checkPermission('update', $model->id);
    }

    /**
     * Test the destroy permissions.
     */
    public function testDestroyPermissions()
    {
        $model = self::createModels($this->modelType);
        $this->checkPermission('destroy', $model->id);
    }

    /**
     * Test the show permissions.
     */
    public function testShowPermissions()
    {
        $model = self::createModels($this->modelType);
        $this->checkPermission('show', $model->id);
    }

    /**
     * Test the permissions for the route.
     *
     * @param string $route
     */
    private function checkPermission(string $routeNameSuffix, ?int $id = null): void
    {
        // When the route has no permissions, no tests need to be executed.
        if (!$this->permissions[$routeNameSuffix]) {
            $this->assertTrue(true); // To avoid phpunit warning.
            return;
        }

        // No permissions assigned.
        $this::setActingAs();
        $response = $this->makeRequestToRoute($routeNameSuffix, $id);
        $response->assertForbidden();

        // Correct permissions assigned.
        $this::setActingAs($this->permissions[$routeNameSuffix]);
        $response = $this->makeRequestToRoute($routeNameSuffix, $id);
        $response->assertNotForbidden();
    }

    /**
     * Test the index route without models.
     *
     * @return \Illuminate\Testing\TestResponse;
     */
    public function indexWithoutModels(): TestResponse
    {
        // Authentication
        $this::setActingAs([$this->permissions['index']]);

        // Execution
        $response = $this->makeRequestToRoute('index');

        // Assertions
        $response->assertStatus(200);
        $response->assertExactJson(["data" => []]);
        return $response;
    }

    /**
     * Test the index route with $this->modelCount models.
     *
     * @param string $resource The class of a \Illuminate\Http\Resources\Json\JsonResource
     * @param callable $prepareModels A function returning a \Illuminate\Database\Eloquent\Model
     * @return \Illuminate\Testing\TestResponse;
     */
    public function indexWithModels(string $resource, callable $prepareModels = null): TestResponse
    {
        // Authentication
        $user = $this::setActingAs([$this->permissions['index']]);

        // Preparation
        $models = $prepareModels == null ? self::createModels($this->modelType, $this->modelCount) : $prepareModels($this->modelCount, $user);

        // Execution
        $response = $this->makeRequestToRoute('index');

        // Assertions
        $response->assertStatus(200);
        $response->assertResource($resource::collection($models));

        return $response;
    }

    /**
     * Test store with valid data.
     *
     * @param string $resource The class of a \Illuminate\Http\Resources\Json\JsonResource
     * @param callable $prepareModel A function returning a \Illuminate\Database\Eloquent\Model
     * @param callable $prepareData A function taking a \Illuminate\Database\Eloquent\Model and returning an array
     * @return \Illuminate\Testing\TestResponse;
     */
    public function storeModel(string $resource, callable $prepareModel, callable $prepareData): TestResponse
    {
        // Authentication
        $user = $this::setActingAs([$this->permissions['store']]);

        // Preparation
        $model = $prepareModel($user);
        $data = $prepareData($model);
        $model->delete(); // Delete this model so that no validation errors can be caused by the old model.
        $model->id++;     // The newly created model will have the next id.

        // Execution
        $response = $this->makeRequestToRoute('store', null, $data);

        // Assertions
        $response->assertStatus(201);
        $response->assertResource(new $resource($model));
        $this->assertNotNull($this->modelType::find($model->id));

        return $response;
    }

    /**
     * Test update with valid data.
     *
     * @param string $resource The class of a \Illuminate\Http\Resources\Json\JsonResource
     * @param callable $prepareModel A function returning a \Illuminate\Database\Eloquent\Model
     * @param callable $prepareData A function taking a \Illuminate\Database\Eloquent\Model and returning an array
     * @return \Illuminate\Testing\TestResponse;
     */
    public function updateModel(string $resource, callable $prepareModel, callable $prepareData): TestResponse
    {
        // Authentication
        $user = $this::setActingAs([$this->permissions['update']]);

        // Preparation
        $model = $prepareModel($user);
        $data = $prepareData($model);

        // Execution
        $response = $this->makeRequestToRoute('update', $model->id, $data);

        // Assertions
        $response->assertStatus(200);
        $response->assertResource(new $resource($model));

        return $response;
    }

    /**
     * Test update for not existing model id.
     * This test asserts that there is never a model where id == 0.
     *
     * @param string $resource The class of a \Illuminate\Http\Resources\Json\JsonResource
     * @param callable $prepareModel A function returning a \Illuminate\Database\Eloquent\Model
     * @param callable $prepareData A function taking a \Illuminate\Database\Eloquent\Model and returning an array
     * @return \Illuminate\Testing\TestResponse;
     */
    public function updateInvalidModel(callable $prepareModel, callable $prepareData): TestResponse
    {
        // Authentication
        $this::setActingAs([$this->permissions['update']]);

        // Preparation
        $data = $prepareData($prepareModel());

        //Execution
        $response = $this->makeRequestToRoute('update', 0, $data);

        // Assertions
        $response->assertClientError();

        return $response;
    }

    /**
     * Test destroy with valid data.
     *
     * @param string $resource The class of a \Illuminate\Http\Resources\Json\JsonResource
     * @param callable $prepareModel A function returning a \Illuminate\Database\Eloquent\Model
     * @return \Illuminate\Testing\TestResponse;
     */
    public function destroyModel(string $resource, callable $prepareModel = null): TestResponse
    {
        // Authentication
        $this::setActingAs([$this->permissions['destroy']]);

        // Preparation
        $model = ($prepareModel == null) ? self::createModels($this->modelType) : $prepareModel();

        // Execution
        $response = $this->makeRequestToRoute('destroy', $model->id);

        // Assertions
        $response->assertStatus(200);
        $response->assertResource(new $resource($model));
        $this->assertNull($this->modelType::find($model->id));

        return $response;
    }

    /**
     * Test destroy for not existing model id.
     *
     * @return \Illuminate\Testing\TestResponse;
     */
    public function destroyInvalidModel(): TestResponse
    {
        // Authentication
        $this::setActingAs([$this->permissions['destroy']]);

        // Execution
        $response = $this->makeRequestToRoute('destroy', 0);

        // Assertions
        $response->assertClientError();

        return $response;
    }

    /**
     * Test show with valid data.
     *
     * @param string $resource The class of a \Illuminate\Http\Resources\Json\JsonResource
     * @param callable $prepareModel A function returning a \Illuminate\Database\Eloquent\Model
     * @return \Illuminate\Testing\TestResponse;
     */
    public function showModel(string $resource, callable $prepareModel): TestResponse
    {
        // Authentication
        $user = $this::setActingAs([$this->permissions['show']]);

        // Preparation
        $model = $prepareModel($user);

        // Execution
        $response = $this->makeRequestToRoute('show', $model->id);

        // Assertions
        $response->assertStatus(200);
        $response->assertResource(new $resource($model));

        return $response;
    }

    /**
     * Test show for not existing model id.
     *
     * @return \Illuminate\Testing\TestResponse;
     */
    public function showInvalidModel(): TestResponse
    {
        // Authentication
        $this::setActingAs([$this->permissions['show']]);

        // Execution
        $response = $this->makeRequestToRoute('show', 0);

        // Assertions
        $response->assertClientError();

        return $response;
    }

    /**
     * Make a request to the url generated with $this->generateUrl()
     * with the method from self::$METHODS with no data.
     *
     * @param string $routeNameSuffix
     * @param int|null $id ID to use to generate the url
     * @param array|null $data
     * @return \Illuminate\Testing\TestResponse
     */
    private function makeRequestToRoute(string $routeNameSuffix, ?int $id = null, array $data = []): TestResponse
    {
        $url = $this->generateUrl($routeNameSuffix, $id);
        $method = self::$METHODS[$routeNameSuffix];
        return $this->json($method, $url, $data);
    }

    /**
     * Generate the url from $this->routeNamePrefix, $routeNameSuffix
     * and self::$PARAMS[$routeNameSuffix].
     *
     * @param string $routeNameSuffix
     * @param int|null $id ID to use to generate the url
     * @return string
     */
    private function generateUrl(string $routeNameSuffix, ?int $id = null): string
    {
        $routeName = "$this->routeNamePrefix.$routeNameSuffix";
        return route($routeName, $id);
    }
}
