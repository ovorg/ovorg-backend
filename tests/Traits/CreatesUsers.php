<?php

namespace Tests\Traits;

use Activation;
use App\Models\User;
use Cartalyst\Sentinel\Activations\EloquentActivation;
use Cartalyst\Sentinel\Reminders\EloquentReminder;
use Illuminate\Support\Facades\Hash;
use Reminder;

trait CreatesUsers
{
    private static $password = '0123456789';

    /**
     * Creates a new user with username = $username and password = self::$password.
     *
     * @return App\Models\User
     */
    public static function createUser(): User
    {
        return User::factory()->create([
            'password' => Hash::make(self::$password),
        ]);
    }

    /**
     * Create a new user and give it the $permissions.
     *
     * @param array $permissions ['permission1' => true, 'permission2' => true, ...]
     * @return App\Models\User
     */
    public static function createUserWithPermissions($permissions): User
    {
        $user = self::createUser();
        $user->permissions = $permissions;
        $user->save();
        return $user;
    }

    /**
     * Create a new user and activate it.
     *
     * @return App\Models\User
     */
    public static function createActivatedUser(): User
    {
        $user = self::createUser();
        self::activateUser($user);
        return $user;
    }

    /**
     * Creates a sentinal activation for $user and completes it.
     *
     * @param App\Models\User $user
     * @return App\Models\User
     */
    public static function activateUser(User $user): User
    {
        $activation = self::createActivation($user);
        self::completeActivation($user, $activation);
        return $user;
    }

    /**
     * Create a sentinel activation for $user.
     *
     * @param App\Models\User $user
     * @return Cartalyst\Sentinel\Activations\EloquentActivation
     */
    public static function createActivation(User $user): EloquentActivation
    {
        return Activation::create($user);
    }

    /**
     * Complete a sentinel activation for $user.
     *
     * @param App\Models\User $user
     * @param Cartalyst\Sentinel\Activations\EloquentActivation $activation
     * @return User
     */
    public static function completeActivation(User $user, EloquentActivation $activation): User
    {
        Activation::complete($user, $activation->code);
        return $user;
    }

    /**
     * Create a sentinel reminder for $user.
     *
     * @param App\Models\User $user
     * @return Cartalyst\Sentinel\Reminders\EloquentReminder $activation
     */
    public static function createReminder(User $user): EloquentReminder
    {
        return Reminder::create($user);
    }
}
