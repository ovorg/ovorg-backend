<?php

namespace Tests\Traits;

trait CreatesModels
{
    /**
     * Create $count instances of $modelType and return it.
     *
     * @param type $modelType The Model type to create
     * @param int $count How many Models to create
     * @return \Illuminate\Support\Collection|\Illuminate\Database\Eloquent\Model
     */
    public static function createModels(string $modelType, int $count = 1)
    {
        $models = $modelType::factory()->count($count)->create();

        if ($count == 1) {
            $models = $models->first();
        }
        return $models;
    }

    /**
     * Create $count instances of $modelType and return it.
     *
     * @param type $modelType The Model type to create
     * @param string $key The foreign key
     * @param int $id The user the models will be assosiated with
     * @param int $count How many Models to create
     * @return \Illuminate\Support\Collection|\Illuminate\Database\Eloquent\Model
     */
    public static function createModelFor(string $modelType, string $key, int $id, int $count = 1)
    {
        $models = $modelType::factory()->count($count)->create([$key => $id]);

        if ($count == 1) {
            $models = $models->first();
        }
        return $models;
    }
}
