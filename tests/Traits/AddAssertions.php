<?php

namespace Tests\Traits;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Testing\Assert as PHPUnit;
use Illuminate\Testing\TestResponse;

trait AddAssertions
{
    protected function addAssertions()
    {
        // Assert response status 4xx.
        TestResponse::macro('assertClientError', function () {
            PHPUnit::assertTrue($this->status() >= 400 && $this->status() < 500);
            return $this;
        });

        // Assert response status != 403 and < 500.
        TestResponse::macro('assertNotForbidden', function () {
            PHPUnit::assertTrue($this->status() != 403 && $this->status() < 500, 'Failed asserting that request is not forbidden. Status is: ' . $this->status());
            return $this;
        });

        // Assert that the response data is a specific JsonResponse.
        TestResponse::macro('assertResource', function (JsonResource $resource) {
            $this->assertSimilarJson($resource->response()->getData(true));
            return $this;
        });
    }
}
