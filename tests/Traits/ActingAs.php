<?php

namespace Tests\Traits;

use App\Models\User;
use Laravel\Passport\Passport;
use TypeError;

trait ActingAs
{
    use CreatesUsers;

    /**
     * Create a new user with $permissions and set it as passport actingAs
     *
     * @param array $permissions
     *
     * @return \App\Models\User
     */
    public static function setActingAs($permissions = null): User
    {
        $user = self::createPermissionsUser($permissions);
        Passport::actingAs($user, ['app']);
        return $user;
    }

    /**
     * Create a user with $permissions.
     *
     * @param null|array|string $permissions
     * @return \App\Models\User
     */
    private static function createPermissionsUser($permissions): User
    {
        if (!$permissions) {
            return self::createUser();
        }
        $permissions = self::convertPermissions($permissions);
        return self::createUserWithPermissions($permissions);
    }

    /**
     * Convert $permissions into an array understood by Sentinel.
     *
     * @param array|string $permissions
     * @return array
     */
    private static function convertPermissions($permissions): array
    {
        if (is_array($permissions)) {
            return self::convertPermissionsArray($permissions);
        }
        if (is_string($permissions)) {
            return self::convertPermissionsString($permissions);
        }
        throw new TypeError('$permissions must be of type array or string.');
    }

    /**
     * Convert a array to a permissions array understood by Sentinel.
     *
     * @param array $permissions
     * @return array
     */
    private static function convertPermissionsArray(array $permissions): array
    {
        $converted = [];
        foreach ($permissions as $permission) {
            $converted[$permission] = true;
        }
        return $converted;
    }

    /**
     * Convert a string to a permission array understood by Sentinel.
     *
     * @param string $permission
     * @return array
     */
    private static function convertPermissionsString(string $permission): array
    {
        return [$permission => true];
    }
}
